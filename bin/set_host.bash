#!/bin/bash

# Updates /etc/hosts to add 'host_parent' to point to the local machine
# This allows access to MySQL/MariaDB and other services that are running on the bare metal machine.

# This is intended for use with dev containers, and shouldn't be used in production.

### Based on StackOverflow - CC BY-SA 3.0
### https://stackoverflow.com/a/37824076/666727

TMP_HOSTS=/tmp/hosts

hostip=$(ip route show | awk '/default/ {print $3}')
hostname="host_parent"

host_entry="${hostip} ${hostname}"

matches_in_hosts="$(grep -n $hostname /etc/hosts | cut -f1 -d:)"

if [ ! -z "$matches_in_hosts" ]
then
  echo "Host $hostname already exists in /etc/hosts, updating all matches."
  # use /tmp/hosts because docker keeps /etc/hosts open
  cp -f /etc/hosts $TMP_HOSTS
  while read -r line_number; do
    sed -i "${line_number}s/.*/${host_entry} /" $TMP_HOSTS
  done <<< "$matches_in_hosts"
  cp -f $TMP_HOSTS /etc/hosts
else
  echo "Host $hostname does not exist in /etc/hosts, adding now."
  echo "$host_entry" | sudo tee -a /etc/hosts > /dev/null
fi
