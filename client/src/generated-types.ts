import gql from 'graphql-tag';
import * as Urql from 'urql';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type AddLinkedPageInput = {
  /** The ID of the parent page that should list a child-page. */
  pageId: Scalars['ID'];
  /** The ID of the sub-page that should be listed. */
  linkedPageId: Scalars['ID'];
};

export type AddSectionInput = {
  /** The ID of the page to update. */
  pageId: Scalars['ID'];
  /** The title of the section. */
  title: Scalars['String'];
  /** Any stock lists this section should be inspired by. */
  inspiration?: Maybe<Array<StockInspiration>>;
};

export enum CommonName {
  ENVIRONMENT = 'ENVIRONMENT',
  WORK = 'WORK',
  SCHOOL = 'SCHOOL',
  FRIENDS = 'FRIENDS',
  FAMILY = 'FAMILY',
  PROJECT = 'PROJECT',
  ONLINE = 'ONLINE',
  CODE = 'CODE',
  GIT = 'GIT',
  SOCIAL = 'SOCIAL',
  TWITTER = 'TWITTER',
  DISCORD = 'DISCORD',
  SLACK = 'SLACK',
  INSTAGRAM = 'INSTAGRAM',
  GITHUB = 'GITHUB',
  GITLAB = 'GITLAB',
  LANGUAGE = 'LANGUAGE',
  EN = 'EN',
  ES = 'ES'
}

export type CreateCategoryInput = {
  /** The ID of the nickname this page is stored under. */
  nicknameId: Scalars['ID'];
  presetName?: Maybe<CommonName>;
  customName?: Maybe<Scalars['String']>;
  /** If 'true', the URL to this category will default to the 'url' pattern.  Ignored if 'customName' is set. */
  defaultSubdomain: Scalars['Boolean'];
  /** If set, defines a word that will be inserted before this category in the URL.  Ignored if 'defaultSubdomain'. */
  defaultJoiningWord?: Maybe<JoiningWord>;
  /** If 'true', category will be listed in the top-level nickname page. */
  listed: Scalars['Boolean'];
};

export type CreateCategoryPageInput = {
  /** The ID of the nickname this page is stored under. */
  nicknameId: Scalars['ID'];
  /** The ID of the category this page is stored under. */
  categoryId: Scalars['ID'];
  name: Scalars['String'];
  /** If set, defines a word that will be inserted before this page in the URL. */
  defaultJoiningWord?: Maybe<JoiningWord>;
  /** If 'true', category will be listed in the category page. */
  listed: Scalars['Boolean'];
};

export type CreateNicknameInput = {
  nickname: Scalars['String'];
};

export type CreateNicknamePageInput = {
  /** The ID of the nickname this page is stored under. */
  nicknameId: Scalars['ID'];
};

export type CreateSectionValueInput = {
  sectionId: Scalars['ID'];
  text: Scalars['String'];
  previous?: Maybe<Scalars['ID']>;
  next?: Maybe<Scalars['ID']>;
};

export type EditSectionTextFieldInput = {
  sectionId: Scalars['ID'];
  field: SectionTextField;
  value: Scalars['String'];
};

export type EditSectionValueTextInput = {
  sectionValueId: Scalars['ID'];
  text: Scalars['String'];
};

export type EmailTokenInput = {
  email: Scalars['String'];
  captchaToken: Scalars['String'];
};

export type EmailedTokenInput = {
  email: Scalars['String'];
  token: Scalars['String'];
};

export enum JoiningWord {
  A = 'A',
  AN = 'AN',
  AT = 'AT',
  FOR = 'FOR',
  IN = 'IN',
  AND = 'AND',
  WITH = 'WITH',
  ON = 'ON'
}

/** A sub-page listed on a parent page. */
export type ListedSubPage = {
  __typename?: 'ListedSubPage';
  id: Scalars['ID'];
  /** The page being linked. */
  linked: Page;
  /** The name to display.  If null, fallback to the page's pageBioTitle/displayName/name. */
  customName?: Maybe<Scalars['String']>;
  /** If true, shows either a custom or inherited subtitle.  Otherwise leaves blank. */
  showSubtitle: Scalars['Boolean'];
  /** A subtitle to list.  If null, fallback to the page's pageBioSubtitle */
  customSubtitle?: Maybe<Scalars['String']>;
  /** A (optionally multi-line) caption to display under the title/subtitle */
  caption?: Maybe<Scalars['String']>;
  /** If the page password should be embedded.  Otherwise will prompt on the published site. */
  includePassword: Scalars['Boolean'];
};

export type MoveSectionValueInput = {
  sectionValueId: Scalars['ID'];
  newPrevious?: Maybe<Scalars['ID']>;
  newNext?: Maybe<Scalars['ID']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  emailToken: Scalars['String'];
  submitEmailedToken: Scalars['String'];
  submitDiscordToken: Scalars['String'];
  runMigrateTask: TaskShellResponse;
  createNickname: NicknameCreationOutput;
  editSectionTextField: Section;
  updateSectionValueOrder: Section;
  deleteSection: Page;
  createSectionValue: SectionValue;
  deleteSectionValue: Section;
  editSectionValueText: SectionValue;
  editSectionValueStrength: SectionValue;
  moveSectionValue: SectionValue;
  createNicknamePage: Page;
  createCategory: Page;
  createCategoryPage: Page;
  updatePageBio: Page;
  addNameSection: Section;
  addPronounSection: Section;
  addSection: Section;
  updatePageOrder: Page;
  addLinkedPage: ListedSubPage;
  updateListedSubPageOrder: Page;
  updateLinkedPage: ListedSubPage;
  deleteListedSubPage: Page;
  updatePagePassword: Page;
  updatePageShowBadges: Page;
  publish: PublishedPage;
};


export type MutationEmailTokenArgs = {
  input: EmailTokenInput;
};


export type MutationSubmitEmailedTokenArgs = {
  input: EmailedTokenInput;
};


export type MutationSubmitDiscordTokenArgs = {
  discordToken: Scalars['String'];
};


export type MutationRunMigrateTaskArgs = {
  password: Scalars['String'];
};


export type MutationCreateNicknameArgs = {
  input: CreateNicknameInput;
};


export type MutationEditSectionTextFieldArgs = {
  input: EditSectionTextFieldInput;
};


export type MutationUpdateSectionValueOrderArgs = {
  input: UpdateSectionValueOrderInput;
};


export type MutationDeleteSectionArgs = {
  id: Scalars['ID'];
};


export type MutationCreateSectionValueArgs = {
  input: CreateSectionValueInput;
};


export type MutationDeleteSectionValueArgs = {
  id: Scalars['ID'];
};


export type MutationEditSectionValueTextArgs = {
  input: EditSectionValueTextInput;
};


export type MutationEditSectionValueStrengthArgs = {
  input: UpdateStrengthInput;
};


export type MutationMoveSectionValueArgs = {
  input: MoveSectionValueInput;
};


export type MutationCreateNicknamePageArgs = {
  input: CreateNicknamePageInput;
};


export type MutationCreateCategoryArgs = {
  input: CreateCategoryInput;
};


export type MutationCreateCategoryPageArgs = {
  input: CreateCategoryPageInput;
};


export type MutationUpdatePageBioArgs = {
  input: UpdatePageBioInput;
};


export type MutationAddNameSectionArgs = {
  input: AddSectionInput;
};


export type MutationAddPronounSectionArgs = {
  input: AddSectionInput;
};


export type MutationAddSectionArgs = {
  input: AddSectionInput;
};


export type MutationUpdatePageOrderArgs = {
  input: UpdatePageOrderInput;
};


export type MutationAddLinkedPageArgs = {
  input: AddLinkedPageInput;
};


export type MutationUpdateListedSubPageOrderArgs = {
  input: UpdatePageOrderInput;
};


export type MutationUpdateLinkedPageArgs = {
  input: UpdateLinkedPageInput;
};


export type MutationDeleteListedSubPageArgs = {
  id: Scalars['ID'];
};


export type MutationUpdatePagePasswordArgs = {
  input: UpdatePagePasswordInput;
};


export type MutationUpdatePageShowBadgesArgs = {
  input: UpdatePageShowBadgesInput;
};


export type MutationPublishArgs = {
  id: Scalars['ID'];
};

/** A namespace to publish users */
export type Nickname = {
  __typename?: 'Nickname';
  id: Scalars['ID'];
  /** A human-readable display name. */
  displayName?: Maybe<Scalars['String']>;
  /** The applicable labels of staff for this user. */
  staffType: Array<StaffType>;
  /** The level of financial support. */
  supporterRole?: Maybe<SupporterRole>;
  /** If staff/supporter badges should be shown by default for sub-pages. */
  showBadgesByDefault?: Maybe<Scalars['Boolean']>;
  /** Get all pages in this nickname. */
  pages: Array<Page>;
  /** Get the primary page for this nickname */
  nicknamePage?: Maybe<Page>;
};

/** The returned output from creating a new nickname (see `createNickname`) */
export type NicknameCreationOutput = {
  __typename?: 'NicknameCreationOutput';
  nickname: Nickname;
  /** The updated JWT. */
  jwt: Scalars['String'];
};

/** A publishable page - can be a namespace page, category page, or sub-page. */
export type Page = {
  __typename?: 'Page';
  id: Scalars['ID'];
  /** The name of this page/category.  'null' if this is the page for the nickname. */
  name?: Maybe<Scalars['String']>;
  /** A non-URL-safe name for this page, to be shown in the header, etc.  Default to 'name'. */
  displayName?: Maybe<Scalars['String']>;
  pageBioTitle?: Maybe<Scalars['String']>;
  pageBioSubtitle?: Maybe<Scalars['String']>;
  /** If this is a category, if the default URL should be a category subdomain. */
  defaultSubdomain?: Maybe<Scalars['Boolean']>;
  /** The default word to insert before this page.  Ignore if this page is a category and 'defaultSubdomain' is set, or if this is a nickname page. */
  defaultJoiningWord?: Maybe<JoiningWord>;
  /** A password to protect this page from casual scraping.  Null if no password is required. */
  password?: Maybe<Scalars['String']>;
  /** The default format for inserting a password into the URL. */
  defaultPasswordFormat?: Maybe<PasswordFormat>;
  /** If staff and/or supporter badges should be shown on this page.  If 'null', uses the setting from the associated nickname. */
  showBadges?: Maybe<Scalars['Boolean']>;
  /** If this page has been published */
  isPublished: Scalars['Boolean'];
  /** The nickname this page is under. */
  nickname: Nickname;
  /** The special section for name options. */
  nameSection?: Maybe<Section>;
  /** The special section for pronouns. */
  pronounSection?: Maybe<Section>;
  /** A list of sections - including the special sections. */
  sections: Array<Section>;
  /** A list of the section IDs in order.  Long-term we'll use next/previous on the sections. */
  sectionOrder: Array<Scalars['String']>;
  /** A list of sub-pages to include on the page. */
  listedSubPages: Array<ListedSubPage>;
  /** A list of the sub-page listing IDs in order.  Long-term we'll use next/previous. */
  listedSubPageOrder: Array<Scalars['String']>;
  /** The pages that can be linked as sub-pages. */
  subPageOptions: Array<Page>;
  /** The parent page for this page - null for nickname pages and category pages. */
  parent?: Maybe<Page>;
};

/** Sets the default format for URLs when linking to this entity. */
export enum PasswordFormat {
  /**
   * For pages that have the category or page as the final path segment,
   * add the password into the category parameter as the default URL.
   *
   * e.g. for category `friends` and password `a4`,
   * https://alexs.pnouns.fyi/with/friends+a4/
   */
  IN_PAGE = 'IN_PAGE',
  /** Default to providing URLs with a `?password=a4` parameter. */
  QUERY_PASSWORD = 'QUERY_PASSWORD',
  /** Default to providing URLs with a `?=a4` parameter. */
  QUERY_P = 'QUERY_P'
}

/** A link to a sub-page in the same namespace. */
export type PublishListedSubPage = {
  __typename?: 'PublishListedSubPage';
  id: Scalars['ID'];
  title: Scalars['String'];
  subtitle?: Maybe<Scalars['String']>;
  caption?: Maybe<Scalars['String']>;
  url: Scalars['String'];
  needsPassword: Scalars['Boolean'];
};

export type PublishedNamespace = {
  __typename?: 'PublishedNamespace';
  /** A human-readable ID for the namespace. */
  id: Scalars['ID'];
  /** A summary of the public pages in this namespace. */
  publicPages: Array<PublishedNamespacePage>;
};

/** A summary of a public page linked from a namespace. */
export type PublishedNamespacePage = {
  __typename?: 'PublishedNamespacePage';
  /** The backend ID of the page. */
  id: Scalars['ID'];
  /** [COMPUTE] Get the contents of the page.  Requires more fetching than other fields. */
  page: PublishedPage;
};

export type PublishedPage = {
  __typename?: 'PublishedPage';
  id: Scalars['ID'];
  /** The nickname/name for this account. */
  nicknamePath: Scalars['ID'];
  /** A human-readable version of the nickname. */
  nicknameDisplay?: Maybe<Scalars['String']>;
  /** The name of the category under the nickname.  Null if this is a top-level page. */
  categoryPath?: Maybe<Scalars['String']>;
  /** A more human-readable version of the category name/path.  Default to 'categoryPath'. */
  categoryDisplay?: Maybe<Scalars['String']>;
  /** A title in the bio section. */
  pageBioTitle?: Maybe<Scalars['String']>;
  /** A subtitle in the bio section. */
  pageBioSubtitle?: Maybe<Scalars['String']>;
  /** The name of the page under a category.  Null if this is a top-level page or category page. */
  pagePath?: Maybe<Scalars['String']>;
  nameSection?: Maybe<PublishedSection>;
  pronounsSection?: Maybe<PublishedSection>;
  /** The sections of different language that should be used. */
  languageSections: Array<PublishedSection>;
  /** The list of section IDs (excluding name/pronoun) in the sorted order. */
  sectionOrder: Array<Scalars['String']>;
  /** A list of pages to link to. */
  listedSubPages?: Maybe<Array<PublishListedSubPage>>;
  /** The list of sub-page IDs in the sorted order. */
  listedSubPageOrder?: Maybe<Array<Scalars['String']>>;
  /** If badges should be shown for this page. */
  showBadges?: Maybe<Scalars['Boolean']>;
  /** [COMPUTE] Get the parent namespace.  Requires more fetching than other fields. */
  namespace: PublishedNamespace;
  /** [COMPUTE] Get the parent page.  Requires more fetching than other fields. */
  parentCategory?: Maybe<PublishedPage>;
  /** [COMPUTE] Get the staff badges to show.  Empty if no staff rules or if hidden. */
  staffBadges: Array<StaffType>;
  /** [COMPUTE] Get a supporter role to show.  Null if no supporter role or if hidden. */
  supporterRoleBadge?: Maybe<SupporterRole>;
};

export type PublishedPageResolverArgs = {
  namespace: Scalars['String'];
  category?: Maybe<Scalars['String']>;
  page?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
};

/** The contents of a published section */
export type PublishedSection = {
  __typename?: 'PublishedSection';
  id: Scalars['ID'];
  name: Scalars['String'];
  subtitle?: Maybe<Scalars['String']>;
  lead?: Maybe<Scalars['String']>;
  /** The items to list for this section. */
  values: Array<PublishedValue>;
  /** The list of value IDs in the sorted order. */
  valueOrder: Array<Scalars['String']>;
};

/** An option to include as part of a section */
export type PublishedValue = {
  __typename?: 'PublishedValue';
  id: Scalars['ID'];
  /** The name of the item. */
  text: Scalars['String'];
  /** How the author feels about this value. */
  strength: Strength;
  /** If the user uses a custom strength, the text to display. */
  customStrengthText?: Maybe<Scalars['String']>;
  /** If the user uses a custom strength, the icon to display. */
  customStrengthIcon?: Maybe<Scalars['String']>;
  /** A short (1-line) description to attach to the value. */
  shortDescription?: Maybe<Scalars['String']>;
  /** A long description for the item. */
  info?: Maybe<Scalars['String']>;
};

export type Query = {
  __typename?: 'Query';
  getPublishedPage: PublishedPage;
  /** Get one of your own nicknames. */
  nickname: Nickname;
  nicknameTaken: Scalars['Boolean'];
  section: Section;
  myPages: Array<Page>;
  getPage: Page;
};


export type QueryGetPublishedPageArgs = {
  input: PublishedPageResolverArgs;
};


export type QueryNicknameArgs = {
  nickname: Scalars['ID'];
};


export type QueryNicknameTakenArgs = {
  nickname: Scalars['String'];
};


export type QuerySectionArgs = {
  id: Scalars['ID'];
};


export type QueryGetPageArgs = {
  id: Scalars['ID'];
};

/** A list of options on a page. */
export type Section = {
  __typename?: 'Section';
  id: Scalars['ID'];
  type?: Maybe<SectionType>;
  title: Scalars['String'];
  subtitle?: Maybe<Scalars['String']>;
  lead?: Maybe<Scalars['String']>;
  /** A list of the value IDs in order.  Long-term we'll use next/previous on the values. */
  valueOrder: Array<Scalars['String']>;
  values: Array<SectionValue>;
};

/** One of the regular text fields in a section. */
export enum SectionTextField {
  TITLE = 'TITLE',
  SUBTITLE = 'SUBTITLE',
  LEAD = 'LEAD'
}

/** Marks special types of sections. */
export enum SectionType {
  NAME = 'NAME',
  PRONOUNS = 'PRONOUNS'
}

/** A single value in a section */
export type SectionValue = {
  __typename?: 'SectionValue';
  id: Scalars['ID'];
  next?: Maybe<SectionValue>;
  previous?: Maybe<SectionValue>;
  text: Scalars['String'];
  strength: Strength;
  customStrengthText?: Maybe<Scalars['String']>;
};

export enum StaffType {
  FOUNDER = 'FOUNDER',
  LEAD_DEV = 'LEAD_DEV'
}

export enum StockInspiration {
  PRONOUNS = 'PRONOUNS',
  PREFIX_MX_MR_MS = 'PREFIX_MX_MR_MS',
  HONORIFIC_SIR_MAAM = 'HONORIFIC_SIR_MAAM',
  REFERENCES_PERSON = 'REFERENCES_PERSON',
  REFERENCES_FRIEND_SLANG = 'REFERENCES_FRIEND_SLANG',
  COMPLIMENTS = 'COMPLIMENTS',
  RELATIONSHIP_PARENT = 'RELATIONSHIP_PARENT',
  RELATIONSHIP_CHILD = 'RELATIONSHIP_CHILD',
  RELATIONSHIP_PARTNER = 'RELATIONSHIP_PARTNER'
}

/** How the user feels about a specific option. */
export enum Strength {
  BEST = 'BEST',
  TESTING = 'TESTING',
  GOOD = 'GOOD',
  OK = 'OK',
  JOKINGLY = 'JOKINGLY',
  CASUALLY_TESTING = 'CASUALLY_TESTING',
  ITS_COMPLICATED = 'ITS_COMPLICATED',
  UNSURE = 'UNSURE',
  AVOID = 'AVOID',
  CONDITIONAL = 'CONDITIONAL',
  NEVER = 'NEVER',
  CUSTOM = 'CUSTOM'
}

export enum SupporterRole {
  GOLD = 'GOLD',
  PLATINUM = 'PLATINUM',
  DIAMOND = 'DIAMOND'
}

/** The output of a shell command run as a task. */
export type TaskShellResponse = {
  __typename?: 'TaskShellResponse';
  stderr: Scalars['String'];
  stdout: Scalars['String'];
};

export type UpdateLinkedPageInput = {
  /** The ID of the page listing. */
  linkedSubPageId: Scalars['ID'];
  /** Control if the field uses a custom name.  Leave 'null' to keep current value. */
  hasCustomName?: Maybe<Scalars['Boolean']>;
  /** Set the current field.  Leave 'null' to keep current value (use hasCustomName=false to clear) */
  customName?: Maybe<Scalars['String']>;
  /** Set if the subtitle is visible.  Leave 'null' to keep current value. */
  showSubtitle?: Maybe<Scalars['Boolean']>;
  /** Control if the field uses a custom subtitle.  Leave 'null' to keep current value. */
  hasCustomSubtitle?: Maybe<Scalars['Boolean']>;
  /** Set the subtitle.  Leave 'null' to keep current value (use hasCustomSubtitle=false to clear) */
  customSubtitle?: Maybe<Scalars['String']>;
  /** Control if a caption is displayed.  Leave 'null' to keep current value. */
  hasCaption?: Maybe<Scalars['Boolean']>;
  /** Set the caption.  Leave 'null' to keep current value (use hasCaption=false to clear) */
  caption?: Maybe<Scalars['String']>;
  /** Control if the linked page's password is included.  Leave 'null' to keep current value. */
  includePassword?: Maybe<Scalars['Boolean']>;
};

export type UpdatePageBioInput = {
  pageId: Scalars['ID'];
  title?: Maybe<Scalars['String']>;
  subtitle?: Maybe<Scalars['String']>;
};

export type UpdatePageOrderInput = {
  pageId: Scalars['ID'];
  /** A list of section IDs that should be ordered. */
  order: Array<Scalars['String']>;
};

export type UpdatePagePasswordInput = {
  pageId: Scalars['ID'];
  /** Set to 'null' to not require a password. */
  password?: Maybe<Scalars['String']>;
  /** The default format for inserting a password into the URL. */
  defaultPasswordFormat?: Maybe<PasswordFormat>;
};

export type UpdatePageShowBadgesInput = {
  pageId: Scalars['ID'];
  /** 'true' if the page should inherit this from the category. */
  inherit: Scalars['Boolean'];
  /** if the badges should be shown.  Leave undefined if inheriting. */
  showBadges?: Maybe<Scalars['Boolean']>;
};

export type UpdateSectionValueOrderInput = {
  sectionId: Scalars['ID'];
  /** A list of section value IDs that should be ordered. */
  order: Array<Scalars['String']>;
};

export type UpdateStrengthInput = {
  sectionValueId: Scalars['ID'];
  strength: Strength;
};

export type EmailTokenMutationVariables = Exact<{
  input: EmailTokenInput;
}>;


export type EmailTokenMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'emailToken'>
);

export type SubmitEmailedTokenMutationVariables = Exact<{
  input: EmailedTokenInput;
}>;


export type SubmitEmailedTokenMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'submitEmailedToken'>
);

export type SubmitDiscordTokenMutationVariables = Exact<{
  discordToken: Scalars['String'];
}>;


export type SubmitDiscordTokenMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'submitDiscordToken'>
);

export type CreateNicknameMutationVariables = Exact<{
  input: CreateNicknameInput;
}>;


export type CreateNicknameMutation = (
  { __typename?: 'Mutation' }
  & { createNickname: (
    { __typename?: 'NicknameCreationOutput' }
    & Pick<NicknameCreationOutput, 'jwt'>
    & { nickname: (
      { __typename?: 'Nickname' }
      & Pick<Nickname, 'id'>
    ) }
  ) }
);

export type CreateNicknamePageMutationVariables = Exact<{
  input: CreateNicknamePageInput;
}>;


export type CreateNicknamePageMutation = (
  { __typename?: 'Mutation' }
  & { createNicknamePage: (
    { __typename?: 'Page' }
    & Pick<Page, 'id'>
  ) }
);

export type CreateCategoryMutationVariables = Exact<{
  input: CreateCategoryInput;
}>;


export type CreateCategoryMutation = (
  { __typename?: 'Mutation' }
  & { createCategory: (
    { __typename?: 'Page' }
    & Pick<Page, 'id'>
  ) }
);

export type CreateCategoryPageMutationVariables = Exact<{
  input: CreateCategoryPageInput;
}>;


export type CreateCategoryPageMutation = (
  { __typename?: 'Mutation' }
  & { createCategoryPage: (
    { __typename?: 'Page' }
    & Pick<Page, 'id'>
  ) }
);

export type NicknameWizardQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type NicknameWizardQuery = (
  { __typename?: 'Query' }
  & { nickname: (
    { __typename?: 'Nickname' }
    & Pick<Nickname, 'displayName'>
    & { nicknamePage?: Maybe<(
      { __typename?: 'Page' }
      & Pick<Page, 'id' | 'name' | 'displayName'>
    )>, pages: Array<(
      { __typename?: 'Page' }
      & Pick<Page, 'id' | 'name' | 'displayName' | 'defaultSubdomain' | 'defaultJoiningWord' | 'isPublished'>
      & { nickname: (
        { __typename?: 'Nickname' }
        & Pick<Nickname, 'id'>
      ), parent?: Maybe<(
        { __typename?: 'Page' }
        & Pick<Page, 'id'>
      )> }
    )> }
  ) }
);

export type CategoryWizardQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type CategoryWizardQuery = (
  { __typename?: 'Query' }
  & { getPage: (
    { __typename?: 'Page' }
    & Pick<Page, 'id' | 'name' | 'displayName' | 'defaultSubdomain' | 'defaultJoiningWord' | 'isPublished'>
    & { nickname: (
      { __typename?: 'Nickname' }
      & Pick<Nickname, 'id'>
    ), parent?: Maybe<(
      { __typename?: 'Page' }
      & Pick<Page, 'id'>
    )>, subPageOptions: Array<(
      { __typename?: 'Page' }
      & Pick<Page, 'id' | 'name' | 'displayName' | 'defaultSubdomain' | 'defaultJoiningWord' | 'isPublished'>
      & { nickname: (
        { __typename?: 'Nickname' }
        & Pick<Nickname, 'id'>
      ), parent?: Maybe<(
        { __typename?: 'Page' }
        & Pick<Page, 'id'>
      )> }
    )> }
  ) }
);

export type PageEditor_CreateNameSectionMutationVariables = Exact<{
  input: AddSectionInput;
}>;


export type PageEditor_CreateNameSectionMutation = (
  { __typename?: 'Mutation' }
  & { addNameSection: (
    { __typename?: 'Section' }
    & Pick<Section, 'id'>
  ) }
);

export type PageEditor_CreatePronounSectionMutationVariables = Exact<{
  input: AddSectionInput;
}>;


export type PageEditor_CreatePronounSectionMutation = (
  { __typename?: 'Mutation' }
  & { addPronounSection: (
    { __typename?: 'Section' }
    & Pick<Section, 'id'>
  ) }
);

export type PageEditor_CreateSectionMutationVariables = Exact<{
  input: AddSectionInput;
}>;


export type PageEditor_CreateSectionMutation = (
  { __typename?: 'Mutation' }
  & { addSection: (
    { __typename?: 'Section' }
    & Pick<Section, 'id'>
  ) }
);

export type PageEditor_EditSectionTextFieldMutationVariables = Exact<{
  input: EditSectionTextFieldInput;
}>;


export type PageEditor_EditSectionTextFieldMutation = (
  { __typename?: 'Mutation' }
  & { editSectionTextField: (
    { __typename?: 'Section' }
    & Pick<Section, 'id'>
  ) }
);

export type PageEditor_CreateSectionValueMutationVariables = Exact<{
  input: CreateSectionValueInput;
}>;


export type PageEditor_CreateSectionValueMutation = (
  { __typename?: 'Mutation' }
  & { createSectionValue: (
    { __typename?: 'SectionValue' }
    & Pick<SectionValue, 'id'>
  ) }
);

export type PageEditor_PublishMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type PageEditor_PublishMutation = (
  { __typename?: 'Mutation' }
  & { publish: (
    { __typename?: 'PublishedPage' }
    & Pick<PublishedPage, 'id'>
  ) }
);

export type PageEditor_UpdateStrengthMutationVariables = Exact<{
  input: UpdateStrengthInput;
}>;


export type PageEditor_UpdateStrengthMutation = (
  { __typename?: 'Mutation' }
  & { editSectionValueStrength: (
    { __typename?: 'SectionValue' }
    & Pick<SectionValue, 'id'>
  ) }
);

export type PageEditor_MoveSectionValueMutationVariables = Exact<{
  input: MoveSectionValueInput;
}>;


export type PageEditor_MoveSectionValueMutation = (
  { __typename?: 'Mutation' }
  & { moveSectionValue: (
    { __typename?: 'SectionValue' }
    & Pick<SectionValue, 'id'>
  ) }
);

export type PageEditor_UpdateSectionValueOrderMutationVariables = Exact<{
  input: UpdateSectionValueOrderInput;
}>;


export type PageEditor_UpdateSectionValueOrderMutation = (
  { __typename?: 'Mutation' }
  & { updateSectionValueOrder: (
    { __typename?: 'Section' }
    & Pick<Section, 'id'>
  ) }
);

export type PageEditor_UpdateSectionOrderMutationVariables = Exact<{
  input: UpdatePageOrderInput;
}>;


export type PageEditor_UpdateSectionOrderMutation = (
  { __typename?: 'Mutation' }
  & { updatePageOrder: (
    { __typename?: 'Page' }
    & Pick<Page, 'id'>
  ) }
);

export type PageEditor_DeleteSectionMutationVariables = Exact<{
  input: Scalars['ID'];
}>;


export type PageEditor_DeleteSectionMutation = (
  { __typename?: 'Mutation' }
  & { deleteSection: (
    { __typename?: 'Page' }
    & Pick<Page, 'id'>
  ) }
);

export type PageEditor_EditSectionValueTextMutationVariables = Exact<{
  input: EditSectionValueTextInput;
}>;


export type PageEditor_EditSectionValueTextMutation = (
  { __typename?: 'Mutation' }
  & { editSectionValueText: (
    { __typename?: 'SectionValue' }
    & Pick<SectionValue, 'id'>
  ) }
);

export type PageEditor_UpdatePageBioMutationVariables = Exact<{
  input: UpdatePageBioInput;
}>;


export type PageEditor_UpdatePageBioMutation = (
  { __typename?: 'Mutation' }
  & { updatePageBio: (
    { __typename?: 'Page' }
    & Pick<Page, 'id'>
  ) }
);

export type PageEditor_DeleteSectionValueMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type PageEditor_DeleteSectionValueMutation = (
  { __typename?: 'Mutation' }
  & { deleteSectionValue: (
    { __typename?: 'Section' }
    & Pick<Section, 'id'>
  ) }
);

export type PageEditor_UpdatePagePasswordMutationVariables = Exact<{
  input: UpdatePagePasswordInput;
}>;


export type PageEditor_UpdatePagePasswordMutation = (
  { __typename?: 'Mutation' }
  & { updatePagePassword: (
    { __typename?: 'Page' }
    & Pick<Page, 'id'>
  ) }
);

export type PageEditor_AddLinkedPageMutationVariables = Exact<{
  input: AddLinkedPageInput;
}>;


export type PageEditor_AddLinkedPageMutation = (
  { __typename?: 'Mutation' }
  & { addLinkedPage: (
    { __typename?: 'ListedSubPage' }
    & Pick<ListedSubPage, 'id'>
  ) }
);

export type PageEditor_UpdateLinkedPageMutationVariables = Exact<{
  input: UpdateLinkedPageInput;
}>;


export type PageEditor_UpdateLinkedPageMutation = (
  { __typename?: 'Mutation' }
  & { updateLinkedPage: (
    { __typename?: 'ListedSubPage' }
    & Pick<ListedSubPage, 'id'>
  ) }
);

export type PageEditor_UpdateLinkedPageOrderMutationVariables = Exact<{
  input: UpdatePageOrderInput;
}>;


export type PageEditor_UpdateLinkedPageOrderMutation = (
  { __typename?: 'Mutation' }
  & { updateListedSubPageOrder: (
    { __typename?: 'Page' }
    & Pick<Page, 'id'>
  ) }
);

export type PageEditor_DeleteLinkedPageMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type PageEditor_DeleteLinkedPageMutation = (
  { __typename?: 'Mutation' }
  & { deleteListedSubPage: (
    { __typename?: 'Page' }
    & Pick<Page, 'id'>
  ) }
);

export type PageEditor_UpdatePageShowBadgesMutationVariables = Exact<{
  input: UpdatePageShowBadgesInput;
}>;


export type PageEditor_UpdatePageShowBadgesMutation = (
  { __typename?: 'Mutation' }
  & { updatePageShowBadges: (
    { __typename?: 'Page' }
    & Pick<Page, 'id'>
  ) }
);

export type PageEditor_PageQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type PageEditor_PageQuery = (
  { __typename?: 'Query' }
  & { getPage: (
    { __typename?: 'Page' }
    & Pick<Page, 'id' | 'name' | 'displayName' | 'defaultSubdomain' | 'defaultJoiningWord' | 'pageBioTitle' | 'pageBioSubtitle' | 'password' | 'defaultPasswordFormat' | 'showBadges' | 'sectionOrder' | 'listedSubPageOrder'>
    & { nickname: (
      { __typename?: 'Nickname' }
      & Pick<Nickname, 'id' | 'displayName' | 'showBadgesByDefault' | 'staffType' | 'supporterRole'>
    ), nameSection?: Maybe<(
      { __typename?: 'Section' }
      & Pick<Section, 'id'>
    )>, pronounSection?: Maybe<(
      { __typename?: 'Section' }
      & Pick<Section, 'id'>
    )>, sections: Array<(
      { __typename?: 'Section' }
      & Pick<Section, 'id' | 'title' | 'type'>
    )>, listedSubPages: Array<(
      { __typename?: 'ListedSubPage' }
      & Pick<ListedSubPage, 'id' | 'customName' | 'showSubtitle' | 'customSubtitle' | 'caption' | 'includePassword'>
      & { linked: (
        { __typename?: 'Page' }
        & Pick<Page, 'id' | 'name' | 'displayName' | 'pageBioTitle' | 'pageBioSubtitle' | 'password' | 'defaultPasswordFormat'>
      ) }
    )>, subPageOptions: Array<(
      { __typename?: 'Page' }
      & Pick<Page, 'id' | 'name' | 'displayName' | 'pageBioTitle' | 'pageBioSubtitle' | 'defaultSubdomain' | 'defaultJoiningWord' | 'password' | 'defaultPasswordFormat'>
    )>, parent?: Maybe<(
      { __typename?: 'Page' }
      & Pick<Page, 'id' | 'name' | 'displayName' | 'defaultSubdomain' | 'defaultJoiningWord'>
    )> }
  ) }
);

export type PageEditor_GetSectionQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type PageEditor_GetSectionQuery = (
  { __typename?: 'Query' }
  & { section: (
    { __typename?: 'Section' }
    & Pick<Section, 'id' | 'type' | 'title' | 'subtitle' | 'lead' | 'valueOrder'>
    & { values: Array<(
      { __typename?: 'SectionValue' }
      & Pick<SectionValue, 'id' | 'text' | 'strength' | 'customStrengthText'>
    )> }
  ) }
);

export type PageList_MyPagesQueryVariables = Exact<{ [key: string]: never; }>;


export type PageList_MyPagesQuery = (
  { __typename?: 'Query' }
  & { myPages: Array<(
    { __typename?: 'Page' }
    & Pick<Page, 'id' | 'name' | 'displayName'>
    & { nickname: (
      { __typename?: 'Nickname' }
      & Pick<Nickname, 'id' | 'displayName'>
    ) }
  )> }
);

export type PublishedPageQueryVariables = Exact<{
  input: PublishedPageResolverArgs;
}>;


export type PublishedPageQuery = (
  { __typename?: 'Query' }
  & { getPublishedPage: (
    { __typename?: 'PublishedPage' }
    & Pick<PublishedPage, 'id' | 'nicknamePath' | 'nicknameDisplay' | 'categoryPath' | 'categoryDisplay' | 'pagePath' | 'pageBioTitle' | 'pageBioSubtitle' | 'sectionOrder' | 'listedSubPageOrder' | 'showBadges' | 'staffBadges' | 'supporterRoleBadge'>
    & { nameSection?: Maybe<(
      { __typename?: 'PublishedSection' }
      & Pick<PublishedSection, 'id' | 'name' | 'subtitle' | 'lead' | 'valueOrder'>
      & { values: Array<(
        { __typename?: 'PublishedValue' }
        & Pick<PublishedValue, 'id' | 'text' | 'strength' | 'customStrengthText' | 'customStrengthIcon' | 'shortDescription' | 'info'>
      )> }
    )>, pronounsSection?: Maybe<(
      { __typename?: 'PublishedSection' }
      & Pick<PublishedSection, 'id' | 'name' | 'subtitle' | 'lead' | 'valueOrder'>
      & { values: Array<(
        { __typename?: 'PublishedValue' }
        & Pick<PublishedValue, 'id' | 'text' | 'strength' | 'customStrengthText' | 'customStrengthIcon' | 'shortDescription' | 'info'>
      )> }
    )>, languageSections: Array<(
      { __typename?: 'PublishedSection' }
      & Pick<PublishedSection, 'id' | 'name' | 'subtitle' | 'lead' | 'valueOrder'>
      & { values: Array<(
        { __typename?: 'PublishedValue' }
        & Pick<PublishedValue, 'id' | 'text' | 'strength' | 'customStrengthText' | 'customStrengthIcon' | 'shortDescription' | 'info'>
      )> }
    )>, listedSubPages?: Maybe<Array<(
      { __typename?: 'PublishListedSubPage' }
      & Pick<PublishListedSubPage, 'id' | 'title' | 'subtitle' | 'caption' | 'url' | 'needsPassword'>
    )>> }
  ) }
);


export const EmailTokenDocument = gql`
    mutation EmailToken($input: EmailTokenInput!) {
  emailToken(input: $input)
}
    `;

export function useEmailTokenMutation() {
  return Urql.useMutation<EmailTokenMutation, EmailTokenMutationVariables>(EmailTokenDocument);
};
export const SubmitEmailedTokenDocument = gql`
    mutation SubmitEmailedToken($input: EmailedTokenInput!) {
  submitEmailedToken(input: $input)
}
    `;

export function useSubmitEmailedTokenMutation() {
  return Urql.useMutation<SubmitEmailedTokenMutation, SubmitEmailedTokenMutationVariables>(SubmitEmailedTokenDocument);
};
export const SubmitDiscordTokenDocument = gql`
    mutation SubmitDiscordToken($discordToken: String!) {
  submitDiscordToken(discordToken: $discordToken)
}
    `;

export function useSubmitDiscordTokenMutation() {
  return Urql.useMutation<SubmitDiscordTokenMutation, SubmitDiscordTokenMutationVariables>(SubmitDiscordTokenDocument);
};
export const CreateNicknameDocument = gql`
    mutation CreateNickname($input: CreateNicknameInput!) {
  createNickname(input: $input) {
    nickname {
      id
    }
    jwt
  }
}
    `;

export function useCreateNicknameMutation() {
  return Urql.useMutation<CreateNicknameMutation, CreateNicknameMutationVariables>(CreateNicknameDocument);
};
export const CreateNicknamePageDocument = gql`
    mutation CreateNicknamePage($input: CreateNicknamePageInput!) {
  createNicknamePage(input: $input) {
    id
  }
}
    `;

export function useCreateNicknamePageMutation() {
  return Urql.useMutation<CreateNicknamePageMutation, CreateNicknamePageMutationVariables>(CreateNicknamePageDocument);
};
export const CreateCategoryDocument = gql`
    mutation CreateCategory($input: CreateCategoryInput!) {
  createCategory(input: $input) {
    id
  }
}
    `;

export function useCreateCategoryMutation() {
  return Urql.useMutation<CreateCategoryMutation, CreateCategoryMutationVariables>(CreateCategoryDocument);
};
export const CreateCategoryPageDocument = gql`
    mutation CreateCategoryPage($input: CreateCategoryPageInput!) {
  createCategoryPage(input: $input) {
    id
  }
}
    `;

export function useCreateCategoryPageMutation() {
  return Urql.useMutation<CreateCategoryPageMutation, CreateCategoryPageMutationVariables>(CreateCategoryPageDocument);
};
export const NicknameWizardDocument = gql`
    query NicknameWizard($id: ID!) {
  nickname(nickname: $id) {
    displayName
    nicknamePage {
      id
      name
      displayName
    }
    pages {
      id
      name
      displayName
      defaultSubdomain
      defaultJoiningWord
      isPublished
      nickname {
        id
      }
      parent {
        id
      }
    }
  }
}
    `;

export function useNicknameWizardQuery(options: Omit<Urql.UseQueryArgs<NicknameWizardQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<NicknameWizardQuery>({ query: NicknameWizardDocument, ...options });
};
export const CategoryWizardDocument = gql`
    query CategoryWizard($id: ID!) {
  getPage(id: $id) {
    id
    name
    displayName
    defaultSubdomain
    defaultJoiningWord
    isPublished
    nickname {
      id
    }
    parent {
      id
    }
    subPageOptions {
      id
      name
      displayName
      defaultSubdomain
      defaultJoiningWord
      isPublished
      nickname {
        id
      }
      parent {
        id
      }
    }
  }
}
    `;

export function useCategoryWizardQuery(options: Omit<Urql.UseQueryArgs<CategoryWizardQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<CategoryWizardQuery>({ query: CategoryWizardDocument, ...options });
};
export const PageEditor_CreateNameSectionDocument = gql`
    mutation PageEditor_CreateNameSection($input: AddSectionInput!) {
  addNameSection(input: $input) {
    id
  }
}
    `;

export function usePageEditor_CreateNameSectionMutation() {
  return Urql.useMutation<PageEditor_CreateNameSectionMutation, PageEditor_CreateNameSectionMutationVariables>(PageEditor_CreateNameSectionDocument);
};
export const PageEditor_CreatePronounSectionDocument = gql`
    mutation PageEditor_CreatePronounSection($input: AddSectionInput!) {
  addPronounSection(input: $input) {
    id
  }
}
    `;

export function usePageEditor_CreatePronounSectionMutation() {
  return Urql.useMutation<PageEditor_CreatePronounSectionMutation, PageEditor_CreatePronounSectionMutationVariables>(PageEditor_CreatePronounSectionDocument);
};
export const PageEditor_CreateSectionDocument = gql`
    mutation PageEditor_CreateSection($input: AddSectionInput!) {
  addSection(input: $input) {
    id
  }
}
    `;

export function usePageEditor_CreateSectionMutation() {
  return Urql.useMutation<PageEditor_CreateSectionMutation, PageEditor_CreateSectionMutationVariables>(PageEditor_CreateSectionDocument);
};
export const PageEditor_EditSectionTextFieldDocument = gql`
    mutation PageEditor_EditSectionTextField($input: EditSectionTextFieldInput!) {
  editSectionTextField(input: $input) {
    id
  }
}
    `;

export function usePageEditor_EditSectionTextFieldMutation() {
  return Urql.useMutation<PageEditor_EditSectionTextFieldMutation, PageEditor_EditSectionTextFieldMutationVariables>(PageEditor_EditSectionTextFieldDocument);
};
export const PageEditor_CreateSectionValueDocument = gql`
    mutation PageEditor_CreateSectionValue($input: CreateSectionValueInput!) {
  createSectionValue(input: $input) {
    id
  }
}
    `;

export function usePageEditor_CreateSectionValueMutation() {
  return Urql.useMutation<PageEditor_CreateSectionValueMutation, PageEditor_CreateSectionValueMutationVariables>(PageEditor_CreateSectionValueDocument);
};
export const PageEditor_PublishDocument = gql`
    mutation PageEditor_Publish($id: ID!) {
  publish(id: $id) {
    id
  }
}
    `;

export function usePageEditor_PublishMutation() {
  return Urql.useMutation<PageEditor_PublishMutation, PageEditor_PublishMutationVariables>(PageEditor_PublishDocument);
};
export const PageEditor_UpdateStrengthDocument = gql`
    mutation PageEditor_UpdateStrength($input: UpdateStrengthInput!) {
  editSectionValueStrength(input: $input) {
    id
  }
}
    `;

export function usePageEditor_UpdateStrengthMutation() {
  return Urql.useMutation<PageEditor_UpdateStrengthMutation, PageEditor_UpdateStrengthMutationVariables>(PageEditor_UpdateStrengthDocument);
};
export const PageEditor_MoveSectionValueDocument = gql`
    mutation PageEditor_MoveSectionValue($input: MoveSectionValueInput!) {
  moveSectionValue(input: $input) {
    id
  }
}
    `;

export function usePageEditor_MoveSectionValueMutation() {
  return Urql.useMutation<PageEditor_MoveSectionValueMutation, PageEditor_MoveSectionValueMutationVariables>(PageEditor_MoveSectionValueDocument);
};
export const PageEditor_UpdateSectionValueOrderDocument = gql`
    mutation PageEditor_UpdateSectionValueOrder($input: UpdateSectionValueOrderInput!) {
  updateSectionValueOrder(input: $input) {
    id
  }
}
    `;

export function usePageEditor_UpdateSectionValueOrderMutation() {
  return Urql.useMutation<PageEditor_UpdateSectionValueOrderMutation, PageEditor_UpdateSectionValueOrderMutationVariables>(PageEditor_UpdateSectionValueOrderDocument);
};
export const PageEditor_UpdateSectionOrderDocument = gql`
    mutation PageEditor_UpdateSectionOrder($input: UpdatePageOrderInput!) {
  updatePageOrder(input: $input) {
    id
  }
}
    `;

export function usePageEditor_UpdateSectionOrderMutation() {
  return Urql.useMutation<PageEditor_UpdateSectionOrderMutation, PageEditor_UpdateSectionOrderMutationVariables>(PageEditor_UpdateSectionOrderDocument);
};
export const PageEditor_DeleteSectionDocument = gql`
    mutation PageEditor_DeleteSection($input: ID!) {
  deleteSection(id: $input) {
    id
  }
}
    `;

export function usePageEditor_DeleteSectionMutation() {
  return Urql.useMutation<PageEditor_DeleteSectionMutation, PageEditor_DeleteSectionMutationVariables>(PageEditor_DeleteSectionDocument);
};
export const PageEditor_EditSectionValueTextDocument = gql`
    mutation PageEditor_EditSectionValueText($input: EditSectionValueTextInput!) {
  editSectionValueText(input: $input) {
    id
  }
}
    `;

export function usePageEditor_EditSectionValueTextMutation() {
  return Urql.useMutation<PageEditor_EditSectionValueTextMutation, PageEditor_EditSectionValueTextMutationVariables>(PageEditor_EditSectionValueTextDocument);
};
export const PageEditor_UpdatePageBioDocument = gql`
    mutation PageEditor_UpdatePageBio($input: UpdatePageBioInput!) {
  updatePageBio(input: $input) {
    id
  }
}
    `;

export function usePageEditor_UpdatePageBioMutation() {
  return Urql.useMutation<PageEditor_UpdatePageBioMutation, PageEditor_UpdatePageBioMutationVariables>(PageEditor_UpdatePageBioDocument);
};
export const PageEditor_DeleteSectionValueDocument = gql`
    mutation PageEditor_DeleteSectionValue($id: ID!) {
  deleteSectionValue(id: $id) {
    id
  }
}
    `;

export function usePageEditor_DeleteSectionValueMutation() {
  return Urql.useMutation<PageEditor_DeleteSectionValueMutation, PageEditor_DeleteSectionValueMutationVariables>(PageEditor_DeleteSectionValueDocument);
};
export const PageEditor_UpdatePagePasswordDocument = gql`
    mutation PageEditor_UpdatePagePassword($input: UpdatePagePasswordInput!) {
  updatePagePassword(input: $input) {
    id
  }
}
    `;

export function usePageEditor_UpdatePagePasswordMutation() {
  return Urql.useMutation<PageEditor_UpdatePagePasswordMutation, PageEditor_UpdatePagePasswordMutationVariables>(PageEditor_UpdatePagePasswordDocument);
};
export const PageEditor_AddLinkedPageDocument = gql`
    mutation PageEditor_AddLinkedPage($input: AddLinkedPageInput!) {
  addLinkedPage(input: $input) {
    id
  }
}
    `;

export function usePageEditor_AddLinkedPageMutation() {
  return Urql.useMutation<PageEditor_AddLinkedPageMutation, PageEditor_AddLinkedPageMutationVariables>(PageEditor_AddLinkedPageDocument);
};
export const PageEditor_UpdateLinkedPageDocument = gql`
    mutation PageEditor_UpdateLinkedPage($input: UpdateLinkedPageInput!) {
  updateLinkedPage(input: $input) {
    id
  }
}
    `;

export function usePageEditor_UpdateLinkedPageMutation() {
  return Urql.useMutation<PageEditor_UpdateLinkedPageMutation, PageEditor_UpdateLinkedPageMutationVariables>(PageEditor_UpdateLinkedPageDocument);
};
export const PageEditor_UpdateLinkedPageOrderDocument = gql`
    mutation PageEditor_UpdateLinkedPageOrder($input: UpdatePageOrderInput!) {
  updateListedSubPageOrder(input: $input) {
    id
  }
}
    `;

export function usePageEditor_UpdateLinkedPageOrderMutation() {
  return Urql.useMutation<PageEditor_UpdateLinkedPageOrderMutation, PageEditor_UpdateLinkedPageOrderMutationVariables>(PageEditor_UpdateLinkedPageOrderDocument);
};
export const PageEditor_DeleteLinkedPageDocument = gql`
    mutation PageEditor_DeleteLinkedPage($id: ID!) {
  deleteListedSubPage(id: $id) {
    id
  }
}
    `;

export function usePageEditor_DeleteLinkedPageMutation() {
  return Urql.useMutation<PageEditor_DeleteLinkedPageMutation, PageEditor_DeleteLinkedPageMutationVariables>(PageEditor_DeleteLinkedPageDocument);
};
export const PageEditor_UpdatePageShowBadgesDocument = gql`
    mutation PageEditor_UpdatePageShowBadges($input: UpdatePageShowBadgesInput!) {
  updatePageShowBadges(input: $input) {
    id
  }
}
    `;

export function usePageEditor_UpdatePageShowBadgesMutation() {
  return Urql.useMutation<PageEditor_UpdatePageShowBadgesMutation, PageEditor_UpdatePageShowBadgesMutationVariables>(PageEditor_UpdatePageShowBadgesDocument);
};
export const PageEditor_PageDocument = gql`
    query PageEditor_Page($id: ID!) {
  getPage(id: $id) {
    id
    name
    displayName
    defaultSubdomain
    defaultJoiningWord
    pageBioTitle
    pageBioSubtitle
    password
    defaultPasswordFormat
    showBadges
    nickname {
      id
      displayName
      showBadgesByDefault
      staffType
      supporterRole
    }
    nameSection {
      id
    }
    pronounSection {
      id
    }
    sections {
      id
      title
      type
    }
    sectionOrder
    listedSubPages {
      id
      linked {
        id
        name
        displayName
        pageBioTitle
        pageBioSubtitle
        password
        defaultPasswordFormat
      }
      customName
      showSubtitle
      customSubtitle
      caption
      includePassword
    }
    listedSubPageOrder
    subPageOptions {
      id
      name
      displayName
      pageBioTitle
      pageBioSubtitle
      defaultSubdomain
      defaultJoiningWord
      password
      defaultPasswordFormat
    }
    parent {
      id
      name
      displayName
      defaultSubdomain
      defaultJoiningWord
    }
  }
}
    `;

export function usePageEditor_PageQuery(options: Omit<Urql.UseQueryArgs<PageEditor_PageQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<PageEditor_PageQuery>({ query: PageEditor_PageDocument, ...options });
};
export const PageEditor_GetSectionDocument = gql`
    query PageEditor_GetSection($id: ID!) {
  section(id: $id) {
    id
    type
    title
    subtitle
    lead
    valueOrder
    values {
      id
      text
      strength
      customStrengthText
    }
  }
}
    `;

export function usePageEditor_GetSectionQuery(options: Omit<Urql.UseQueryArgs<PageEditor_GetSectionQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<PageEditor_GetSectionQuery>({ query: PageEditor_GetSectionDocument, ...options });
};
export const PageList_MyPagesDocument = gql`
    query PageList_MyPages {
  myPages {
    id
    name
    displayName
    nickname {
      id
      displayName
    }
  }
}
    `;

export function usePageList_MyPagesQuery(options: Omit<Urql.UseQueryArgs<PageList_MyPagesQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<PageList_MyPagesQuery>({ query: PageList_MyPagesDocument, ...options });
};
export const PublishedPageDocument = gql`
    query PublishedPage($input: PublishedPageResolverArgs!) {
  getPublishedPage(input: $input) {
    id
    nicknamePath
    nicknameDisplay
    categoryPath
    categoryDisplay
    pagePath
    pageBioTitle
    pageBioSubtitle
    nameSection {
      id
      name
      subtitle
      lead
      values {
        id
        text
        strength
        customStrengthText
        customStrengthIcon
        shortDescription
        info
      }
      valueOrder
    }
    pronounsSection {
      id
      name
      subtitle
      lead
      values {
        id
        text
        strength
        customStrengthText
        customStrengthIcon
        shortDescription
        info
      }
      valueOrder
    }
    languageSections {
      id
      name
      subtitle
      lead
      values {
        id
        text
        strength
        customStrengthText
        customStrengthIcon
        shortDescription
        info
      }
      valueOrder
    }
    sectionOrder
    listedSubPages {
      id
      title
      subtitle
      caption
      url
      needsPassword
    }
    listedSubPageOrder
    showBadges
    staffBadges
    supporterRoleBadge
  }
}
    `;

export function usePublishedPageQuery(options: Omit<Urql.UseQueryArgs<PublishedPageQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<PublishedPageQuery>({ query: PublishedPageDocument, ...options });
};