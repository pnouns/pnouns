export type PublishedPageMatch = {
  namespace: string;
  category?: string;
  page?: string;
  password?: string;
};

export const JOINING_WORDS = [
  "a",
  "an",
  "at",
  "for",
  "in",
  "and",
  "with",
  "on",
];

export function matchUrl(pattern: string, url: string): undefined | Record<string, string> {
  const enhancedPattern = pattern
    .replace(/\./, "\\.")
    .replace(/\+/, "\\+")
    .replace(/<namespace>/g, "(?<namespace>[a-zA-Z][a-zA-Z0-9_-]{1,29}[a-zA-Z0-9])")
    .replace(/<category>/g, "(?<category>[a-zA-Z]+)")
    .replace(/<page>/g, "(?<page>[a-zA-Z][a-zA-Z0-9_-]{0,29}[a-zA-Z0-9])")
    .replace(/<password>/g, "(?<password>[a-zA-Z0-9]{2,6})")
    .replace(/<join>/g, `(?:${JOINING_WORDS.join("|")})`)
  const completePattern = `^${enhancedPattern}/?$`;
  const matches = url.match(new RegExp(completePattern));
  if(matches) {
    return (matches as unknown as any).groups as Record<string, string>;
  }
  return;
}

export function matchPublishedPage(url: string): undefined | PublishedPageMatch {
  const routes = [
    // flyyns.work.pnouns.fyi/acme-corp/
    "<namespace>.<category>.pnouns.fyi/<page>",

    // flyyns.work.pnouns.fyi/acme-corp+PW/
    "<namespace>.<category>.pnouns.fyi/<page>+<password>",

    // flyyns.work.pnouns.fyi/at/acme-corp/
    "<namespace>.<category>.pnouns.fyi/<join>/<page>",

    // flyyns.work.pnouns.fyi/at/acme-corp+PW/
    "<namespace>.<category>.pnouns.fyi/<join>/<page>+<password>",

    // flyyns.work.pnouns.fyi/
    "<namespace>.<category>.pnouns.fyi",

    // flyyns.pnouns.fyi/for/work/at/acme-corp/
    "<namespace>.pnouns.fyi/<join>/<category>/<join>/<page>",

    // flyyns.pnouns.fyi/for/work/at/acme-corp+PW/
    "<namespace>.pnouns.fyi/<join>/<category>/<join>/<page>+<password>",

    // flyyns.pnouns.fyi/for/jones/family/
    "<namespace>.pnouns.fyi/<join>/<category>/<page>",

    // flyyns.pnouns.fyi/for/jones/family+PW/
    "<namespace>.pnouns.fyi/<join>/<category>/<page>+<password>",

    // flyyns.pnouns.fyi/work/at/acme-corp/
    "<namespace>.pnouns.fyi/<category>/<join>/<page>",

    // flyyns.pnouns.fyi/work/at/acme-corp+PW/
    "<namespace>.pnouns.fyi/<category>/<join>/<page>+<password>",

    // flyyns.pnouns.fyi/at/school/
    // flyyns.pnouns.fyi/with/family/
    "<namespace>.pnouns.fyi/<join>/<category>",

    // flyyns.pnouns.fyi/at/school+PW/
    "<namespace>.pnouns.fyi/<join>/<category>+<password>",

    // flyyns.pnouns.fyi/work/acme-corp/
    "<namespace>.pnouns.fyi/<category>/<page>",

    // flyyns.pnouns.fyi/work/acme-corp+PW/
    "<namespace>.pnouns.fyi/<category>/<page>+<password>",


    // flyyns.pnouns.fyi/family/
    "<namespace>.pnouns.fyi/<category>",

    // flyyns.pnouns.fyi/family+PW/
    "<namespace>.pnouns.fyi/<category>+<password>",

    // flyyns.pnouns.fyi/
    "<namespace>.pnouns.fyi",
  ];
  for (const route of routes) {
    const found = matchUrl(route, url);
    if(found && typeof found === "object" && typeof found.namespace === "string") {
      return found as unknown as PublishedPageMatch;
    }
  }
  return undefined;
}
