import { StaffType, SupporterRole } from "../generated-types";

export function staffTypeText(type: StaffType): string {
  if(type === StaffType.FOUNDER) {
    return "Founder";
  } else if(type === StaffType.LEAD_DEV) {
    return "Lead Dev";
  } else {
    return "";
  }
}
