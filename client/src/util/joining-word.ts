import { JoiningWord } from "../generated-types";

/**
 * Sync with server/src/util/joining-word.ts
 */
export function joiningWordLabel(word: JoiningWord): string {
  switch(word) {
    case JoiningWord.A:
      return "a";
    case JoiningWord.AN:
      return "an";
    case JoiningWord.AND:
      return "and";
    case JoiningWord.AT:
      return "at";
    case JoiningWord.FOR:
      return "for";
    case JoiningWord.IN:
      return "in";
    case JoiningWord.WITH:
      return "with";
    case JoiningWord.ON:
      return "on";
  }
}
