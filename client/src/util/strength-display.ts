import { Maybe, Strength } from "../generated-types";
import { IconType } from "react-icons/lib";
import {
  MdGrade,
  MdWarning,
} from "react-icons/md";
import {
  BiCheck,
  BiConfused,
  BiCustomize,
  BiTestTube,
  BiWinkSmile,
} from "react-icons/bi";
import {
  FaBalanceScaleLeft,
  FaCheck,
} from "react-icons/fa";
import {
  WiGaleWarning,
} from "react-icons/wi";
import {
  ImCross,
} from "react-icons/im";

export interface CalculatedStrengthDisplay {

  /**
   * The icon to display.
   */
  i: IconType,

  /**
   * The text to display.
   */
  t: string,

  /**
   * The name of a text style to use.
   */
  s: string,

  /**
   * The color scheme.
   */
  c: string;

  /**
   * The variant.
   */
  v: string;
}

export function strengthDisplay(
  strength: Strength,
  customText?: Maybe<string>,
  customIcon?: Maybe<string>,
): CalculatedStrengthDisplay {
  switch (strength) {
    case Strength.BEST:
      return { i: MdGrade, t: "Best", s: "strength-bold-green", c: "green", v: "solid" };
    case Strength.TESTING:
      return { i: BiTestTube, t: "Testing", s: "strength-bold-green", c: "green", v: "outline" };
    case Strength.GOOD:
      return { i: FaCheck, t: "Good", s: "strength-semi-bold-green", c: "green", v: "outline" };
    case Strength.OK:
      return { i: BiCheck, t: "OK", s: "strength-green", c: "green", v: "outline" };
    case Strength.JOKINGLY:
      return { i: BiWinkSmile, t: "Jokingly", s: "strength-warning", c: "orange", v: "solid" };
    case Strength.CASUALLY_TESTING:
      return { i: BiTestTube, t: "Casually Testing", s: "strength-regular", c: "orange", v: "solid" };
    case Strength.ITS_COMPLICATED:
      return { i: WiGaleWarning, t: "It's Complicated", s: "strength-warning", c: "orange", v: "solid" };
    case Strength.UNSURE:
      return { i: BiConfused, t: "Unsure", s: "strength-warning", c: "orange", v: "solid" };
    case Strength.AVOID:
      return { i: MdWarning, t: "Avoid", s: "strength-critical", c: "red", v: "solid" };
    case Strength.CONDITIONAL:
      return { i: FaBalanceScaleLeft, t: "Conditional", s: "strength-warning", c: "red", v: "outline" };
    case Strength.NEVER:
      return { i: ImCross, t: "Never", s: "strength-bold-critical", c: "red", v: "solid" };
    case Strength.CUSTOM:
      return { i: BiCustomize, t: customText || "", s: "strength-custom", c: "teal", v: "solid" };
  }
}
