import React, {
  createContext,
  useContext,
  useState,
} from "react";
import { ReportFunction } from "../components/error-modal/util";

export interface EditorContext {
  pageId: string;
  displayName: string;
  categoryDisplayName: string;
  saving: string[];
  isSaving: boolean;
  startSaving: (saveId: string) => () => void;
  report: ReportFunction;
};

const editorContext = createContext<EditorContext>({
  pageId: "",
  displayName: "",
  categoryDisplayName: "",
  saving: [],
  isSaving: false,
  startSaving: () => {
    const cancel = () => { return; };
    return cancel;
  },
  report: () => {},
});

export const EditorProvider: React.FC<{
  pageId: string,
  displayName: string,
  categoryDisplayName: string,
  report: ReportFunction,
}> = ({
  pageId,
  displayName,
  categoryDisplayName,
  children,
  report,
}) => {
  const [saves, setSaves] = useState<string[]>([]);

  const isSaving = saves.length !== 0;

  function startSaving(saveId: string) {
    if(saves.includes(saveId)) {
      throw new Error(`Already saving ${saveId}`);
    }
    setSaves([ ...saves, saveId ]);
    return () => {
      setSaves(saves.filter(save => save !== saveId));
    };
  }


  const val: EditorContext = {
    pageId,
    displayName,
    categoryDisplayName,
    saving: saves,
    isSaving,
    startSaving,
    report,
  };

  return (
    <editorContext.Provider value={val}>
      { children }
    </editorContext.Provider>
  );
};

export function useEditorContext(): EditorContext {
  return useContext(editorContext);
}
