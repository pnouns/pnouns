import React, {
  createContext,
  useContext,
  useState,
} from "react";
import jwt_decode from "jwt-decode";
import posthog from "posthog-js";

export const JWT_SESSION_STORAGE = "pronouns_jwt";

export function loadJwt(): string | undefined {
  const searchParams = new URLSearchParams(window.location.search);
  const queryParam = searchParams.get("jwt");
  if(queryParam && queryParam.length > 0) {
    localStorage.setItem(JWT_SESSION_STORAGE, queryParam);
    window.history.pushState({}, document.title, window.location.href.split("?")[0]);
    return queryParam;
  }
  const fromStorage = localStorage.getItem(JWT_SESSION_STORAGE);
  if(fromStorage) {
    return fromStorage;
  }
}

interface JwtPayload {
  exp: number;
  sub: string;
  namespaces: string[];
}

interface CommonUserContext {
  loggedIn: boolean;
  rawJwt?: string;
  userId?: string;
  namespaces?: string[];
  updateJwt: (newJwt: string) => void;
}

interface AuthenticatedUserContext extends CommonUserContext {
  loggedIn: true;
  rawJwt: string;
  userId: string;
  namespaces: string[];
}

interface UnauthenticatedUserContext extends CommonUserContext {
  loggedIn: false;
  rawJwt: undefined;
  userId: undefined;
  namespaces: undefined;
}

export type UserContext = AuthenticatedUserContext | UnauthenticatedUserContext;

const userContext = createContext<UserContext>({
  loggedIn: false,
  rawJwt: undefined,
  userId: undefined,
  namespaces: undefined,
  updateJwt: () => { return; },
});

export const UserProvider: React.FC<{}> = ({ children }) => {
  const [rawJwt, setRawJwt] = useState<string | undefined>(loadJwt() ?? undefined);

  const decoded = rawJwt && jwt_decode<JwtPayload>(rawJwt);

  const updateJwt = (jwt: string) => {
    if(jwt.length < 1) {
      posthog.reset();
    }
    window.localStorage.setItem("pronouns_jwt", jwt);
    setRawJwt(jwt);
  };
  if(rawJwt && decoded && Date.now() >= decoded.exp * 1000) {
    // JWT has expired
    updateJwt("");
  }

  const val: UserContext = rawJwt && decoded ?
    {
      loggedIn: true,
      rawJwt,
      userId: decoded.sub,
      namespaces: decoded.namespaces,
      updateJwt,
    } : {
      loggedIn: false,
      rawJwt: undefined,
      userId: undefined,
      namespaces: undefined,
      updateJwt,
    };

  if(val.loggedIn) {
    posthog.identify(val.userId);
  }

  return (
    <userContext.Provider value={val}>
      { children }
    </userContext.Provider>
  );
};

export function useUserContext(): UserContext {
  return useContext(userContext);
}
