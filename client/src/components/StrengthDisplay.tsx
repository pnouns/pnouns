import { Box, Button, Icon } from "@chakra-ui/react";
import React, { ReactElement } from "react";
import { Maybe, Strength } from "../generated-types";
import { strengthDisplay } from "../util/strength-display";

export const StrengthDisplay: React.FC<{
  strength: Strength,
  customStrengthText?: Maybe<string>,
  customStrengthIcon?: Maybe<string>,
  rightIcon?: ReactElement,
  renderCustomText?: (customText: string) => ReactElement<any>,
  onClick?: () => void,
  flex?: string,
}> = ({
  strength,
  customStrengthText,
  customStrengthIcon,
  rightIcon,
  renderCustomText,
  onClick,
  flex,
}) => {
  const info = strengthDisplay(
    strength,
    customStrengthText,
    customStrengthIcon,
  );
  const clickDependentFields = typeof onClick === "function"
    ? {}
    : {
      _hover: {},
    };
  return (
    <Box flex={flex}>
      <Button
        leftIcon={<Icon as={info.i} />}
        rightIcon={rightIcon}
        onClick={onClick}
        textStyle={info.s}
        colorScheme={info.c}
        variant={info.v}
        {...clickDependentFields}
        margin="1"
      >
        {info.t}
      </Button>
    </Box>
  );
};
