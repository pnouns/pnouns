import React, { useRef, useState } from "react";
import { Button, ButtonGroup, IconButton } from "@chakra-ui/button";
import {
  Editable,
  EditableInput,
  EditablePreview,
  useEditableControls,
} from "@chakra-ui/editable";
import { Flex } from "@chakra-ui/layout";
import { CheckIcon, CloseIcon, EditIcon } from "@chakra-ui/icons";

export interface EditableTextSourcedFromProps {

  /**
   * If 'true', uses 'upstreamValue' over 'value'.
   */
  inheritValue: boolean;

  /**
   * The value from the upstream source.
   */
  upstreamValue?: string;

  /**
   * Called if the user requests using the upstream value
   * instead of the locally set value.
   */
  onUseUpstream?: () => Promise<void>;

  /**
   * Called if the user requests using a local value
   * over the upstream value.
   */
  onOverride?: () => Promise<void>;
}

export const EditableText: React.FC<{
  value?: string;
  placeholder?: string;
  disabled?: boolean;
  /**
   * If set, this editable text is overriding an upstream value,
   * and the user will be able to choose if they want to override or use the upstream value.
   *
   * Setting this value will adjust the controls shown.
   */
  inheritedValue?: EditableTextSourcedFromProps;
  /**
   * Called when the field should be saved.
   * Return a promise that resolves once the save is complete.
   */
  onSave?: (newValue: string) => Promise<void>;
}> = ({
  value,
  placeholder,
  disabled,
  inheritedValue,
  onSave,
}) => {
  const revertToInheritedValue = useRef(false);
  const [hasUpdated, setHasUpdated] = useState(false);
  const [isSaving, setIsSaving] = useState(false);
  const [newValue, setNewValue] = useState("");

  const handleSave = async (value: string) => {
    if(isSaving) {
      return;
    }
    setNewValue(value);
    setIsSaving(true);
    if(typeof onSave === "function") {
      await onSave(value);
    }
    setIsSaving(false);
    setNewValue("");
  }

  function EditableControls({ flex }: { flex: string }) {
    const {
      isEditing,
      getSubmitButtonProps,
      getCancelButtonProps,
      getEditButtonProps,
    } = useEditableControls();

    const cancel = getCancelButtonProps().onClick;

    const inheritedControls = inheritedValue && (
      <>
        {
          inheritedValue.inheritValue
            ? <Button
                disabled={!inheritedValue.onOverride}
                onClick={() => inheritedValue.onOverride && inheritedValue.onOverride()}
              >
                Override Value
              </Button>
            : <Button
                disabled={!inheritedValue.onUseUpstream}
                {...getCancelButtonProps({
                  onMouseDown: () => {revertToInheritedValue.current = true},
                })}
              >
                Use source ({inheritedValue.upstreamValue})
              </Button>
          }
      </>
    );

    const saveButtons = (!inheritedValue || !inheritedValue.inheritValue) && (
      <>
        <IconButton aria-label="Save" icon={<CheckIcon />} {...getSubmitButtonProps()} />
      </>
    );

    return isEditing
      ? (
        <ButtonGroup flex={flex} justifyContent="center" size="sm">
          { inheritedControls }
          { saveButtons }
          <IconButton aria-label="Cancel edits" icon={<CloseIcon />} {...getCancelButtonProps()} />
        </ButtonGroup>
      ) : (
        <Flex flex={flex} justifyContent="center">
          <IconButton
            aria-label="Edit"
            size="sm"
            icon={<EditIcon />}
            isLoading={isSaving}
            {...getEditButtonProps()}
          />
        </Flex>
      );
  }


  return (
    <Editable
      value={
        (inheritedValue && inheritedValue.inheritValue)
          ? inheritedValue.upstreamValue
          : hasUpdated
            ? newValue
            : value
      }
      onChange={(v) => { setHasUpdated(true); setNewValue(v); }}
      placeholder={placeholder}
      isDisabled={(inheritedValue && inheritedValue.inheritValue) || disabled || isSaving}
      onSubmit={(val) => {
        if(revertToInheritedValue.current) {
          inheritedValue && inheritedValue.onUseUpstream && inheritedValue.onUseUpstream();
          revertToInheritedValue.current = false;
          setHasUpdated(false);
          return;
        }
        handleSave(val);
        setHasUpdated(false);
      }}
    >
      <Flex>
        <EditablePreview flex="10" />
        <EditableInput flex="10" />
        <EditableControls flex="1" />
      </Flex>
    </Editable>
  )
};
