import React from "react";
import { Box, Center, Flex, Text } from "@chakra-ui/layout";
import { Button } from "@chakra-ui/button";
import { StockInspiration } from "../generated-types";

export const StockInspirationListItem: React.FC<{
  name: string;
  example?: string;
  item: StockInspiration;
  onSelect: (name: string, inspiration: StockInspiration) => void;
}> = ({
  name,
  example,
  item,
  onSelect,
}) => {
  return (
    <Flex>
      <Center flex="10">
        <Text fontSize="md">{ name }</Text>
        { example && <>&nbsp;<Text fontSize="sm">{ example }</Text></>}
      </Center>
      <Button flex="3" onClick={() => onSelect(name, item)}>
        Create
      </Button>
    </Flex>
  );
};

export const StockInspirationList: React.FC<{}> = ({ children }) => {
  return (
    <Box>
      { children }
    </Box>
  );
};
