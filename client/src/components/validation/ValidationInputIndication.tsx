import React from "react";
import { ValidationResult } from "./useValidations";
import { Spinner } from "@chakra-ui/react";
import { CheckIcon, CloseIcon } from "@chakra-ui/icons";

export const ValidationInputIndication: React.FC<{
  validations: ValidationResult<any>,
}> = ({ validations }) => {
  if(validations.validatedShortCircuit === false || (!validations.loading && !validations.valid)) {
    return <CloseIcon color="red.500" />;
  } else if(validations.loading) {
    return <Spinner color="blue.500" />;
  } else {
    return <CheckIcon color="green.500" />;
  }
};
