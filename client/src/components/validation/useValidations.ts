import { useState } from "react";
import mapValues from "lodash.mapvalues";
import { useDebouncedCallback } from "use-debounce";

export enum ValidationResultStatus {
  FAILING,
  PENDING,
  PASSING,
}

export interface ValidationStatus {
  result: ValidationResultStatus;
  load: (...args: any) => void;
}

interface ValidationInfo {
  loading: boolean;
  passing: boolean;
  load: (...args: any) => void;
}

export interface InstantValidationDefinitionObject {
  test: boolean;
}

export interface DebouncedValidationDefinition {
  debounce: number;
  test: (...args: any) => boolean;
  maxWait?: number;
}

export type ValidationDefinition = InstantValidationDefinitionObject
  | boolean
  | DebouncedValidationDefinition;

export function isInstantValidationObject(validation: ValidationDefinition): validation is InstantValidationDefinitionObject {
  return typeof validation === "object" && typeof validation.test === "boolean";
}

export interface ValidationResult<T extends string> {
  /**
   * If any of the validations are loading.
   */
  loading: boolean;

  /**
   * Returns 'false' when any item fails.  Ignores pending items.
   * Allows displaying a failure message sooner even if longer checks are pending.
   */
  validatedShortCircuit: boolean;

  /**
   * If all validations are loaded and passed.
   */
  valid: boolean;

  passing: Record<T, ValidationStatus>;
}

export function useValidations<T extends string>(
  validations: Record<T, ValidationDefinition>,
): ValidationResult<T> {
  const status = mapValues(validations, (validation: ValidationDefinition): ValidationInfo => {
    if(typeof validation === "boolean") {
      return {
        loading: false,
        passing: validation,
        load: () => {},
      };
    }
    if(isInstantValidationObject(validation)) {
      return {
        loading: false,
        passing: validation.test,
        load: () => {},
      };
    }
    const [pending, setPending] = useState(false);
    const [result, setResult] = useState(false);
    const debounced = useDebouncedCallback(
      (...args) => {
        setResult(validation.test(...args));
        setPending(false);
      },
      validation.debounce,
      {
        maxWait: validation.maxWait,
      },
    );
    return {
      loading: pending,
      passing: result,
      load: (...args) => {
        setPending(true);
        debounced(...args);
      },
    };
  });
  const statuses = Object.keys(status).map<ValidationInfo>(s => status[s]);
  return {
    loading: statuses.find(status => status.loading) !== undefined,
    validatedShortCircuit: statuses.every(status => status.loading || status.passing),
    valid: statuses.every(status => !status.loading && status.passing),
    passing: mapValues(status, (s): ValidationStatus => {
      return {
        result: s.loading
          ? ValidationResultStatus.PENDING
          : s.passing
            ? ValidationResultStatus.PASSING
            : ValidationResultStatus.FAILING,
        load: s.load,
      };
    }),
  };
}

/**
(validatedShortCircuit === FAILED || !loading && !validated)
  ? fail
  : loading
    ? loading
    : success
 */
