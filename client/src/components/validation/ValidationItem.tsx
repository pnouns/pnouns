import { SpinnerIcon } from "@chakra-ui/icons";
import { ListIcon, ListItem, Spinner } from "@chakra-ui/react";
import React from "react";
import { MdCancel, MdCheckCircle } from "react-icons/md";
import { ValidationResultStatus, ValidationStatus } from "./useValidations";

export const ValidationItem: React.FC<{
  validation: ValidationStatus,
}> = ({ validation, children }) => {
  return (
    <ListItem>
      <ListIcon
        as={
          validation.result === ValidationResultStatus.PENDING
            ? SpinnerIcon
            : validation.result === ValidationResultStatus.PASSING
              ? MdCheckCircle
              : MdCancel
        }
        color={
          validation.result === ValidationResultStatus.PENDING
            ? "blue.500"
            : validation.result === ValidationResultStatus.PASSING
              ? "green.500"
              : "red.500"
        }
      />
      { children }
    </ListItem>
  )
};
