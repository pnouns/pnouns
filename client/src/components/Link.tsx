import React, { ReactElement } from "react";
import { Link as ChakraLink, Button } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";

export interface CommonLinkProps {
  path: string;
  newTab?: boolean;
  external?: boolean;
}

export interface LinkProps extends CommonLinkProps {
  color?: string;
}

export const Link: React.FC<LinkProps> = ({
  path,
  newTab,
  external,
  color,
  children,
}) => {
  if(external) {
    return (
      <ChakraLink href={path} color={color} target={newTab ? "_blank" : undefined}>{ children }</ChakraLink>
    )
  }
  return (
    <ChakraLink
      as={RouterLink}
      to={path}
      color={color}
      target={newTab ? "_blank" : undefined}
    >
      { children }
    </ChakraLink>
  );
};

export interface ButtonLinkProps extends CommonLinkProps {
  colorScheme?: string;
  variant?: string;
  leftIcon?: ReactElement;
  rightIcon?: ReactElement;
}

export const ButtonLink: React.FC<ButtonLinkProps> = ({
  path,
  newTab,
  external,
  colorScheme,
  variant,
  leftIcon,
  rightIcon,
  children,
}) => {
  const props = {
    variant,
    colorScheme,
    leftIcon,
    rightIcon,
  }
  if(external) {
    return (
      <Button as="a" href={path} target={newTab ? "_blank" : undefined} {...props}>
        {children}
      </Button>
    );
  }
  return (
    <Button
      as={RouterLink}
      to={path}
      target={newTab ? "_blank" : undefined}
      {...props}
    >
      { children }
    </Button>
  );
}
