import { Badge } from "@chakra-ui/layout";
import { Tooltip } from "@chakra-ui/tooltip";
import React from "react";
import { StaffType, SupporterRole } from "../generated-types";
import { staffTypeText } from "../util/badge";
import { Link } from "./Link";

export const PageBadgeDisplay: React.FC<{
  staffRole?: StaffType;
  supporter?: SupporterRole;
}> = ({ staffRole, supporter }) => {
  const color = staffRole
    ? "cyan"
    : supporter === SupporterRole.GOLD || SupporterRole.PLATINUM
      ? "yellow"
      : "orange";
  const variant =
    staffRole
      ? "subtle"
      : supporter === SupporterRole.GOLD || SupporterRole.PLATINUM
        ? "subtle"
        : "solid";
  const text =
    staffRole
      ? staffTypeText(staffRole)
      : supporter === SupporterRole.GOLD || SupporterRole.PLATINUM
        ? "Supporter"
        : "Top Supporter";
  const tooltip =
    staffRole
      ? "This user works on pnouns.fyi"
      : supporter
        ? "This user financially supports pnouns.fyi on Patreon"
        : undefined;
  const link =
    supporter
      ? "https://patreon.com/pnouns"
      : undefined;
  const badge = (
    <Badge colorScheme={color} variant={variant} m="1">
      {text}
    </Badge>
  );
  const tooltipWrappedBadge = !tooltip
    ? badge
    : (
      <Tooltip
        hasArrow
        colorScheme="gray"
        label={tooltip}
      >
        {badge}
      </Tooltip>
    );
  const linkWrappedBadge =
    !link
      ? tooltipWrappedBadge
      : (
        <Link path={link} newTab external>
          {tooltipWrappedBadge}
        </Link>
      );
  return linkWrappedBadge;
};
