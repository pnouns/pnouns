import { useColorModeValue } from "@chakra-ui/react";
import React from "react";
import { CUIAutoComplete as AutoComplete } from "chakra-ui-autocomplete";
import { CommonName } from "../generated-types";

type CategoryItem = {
  value: CommonName;
  label: string;
};

export enum CategorySections {
  ENVIRONMENT = 1,
  SOCIAL,
  LANGUAGE,
}

export const CategoryAutocomplete: React.FC<{
  categories: CategorySections[];
  selectedItems?: CategoryItem[];
  exclude?: string[],
  onSelectedItemsChange?: (selected?: CategoryItem[]) => void;
  onCreateCategory?: (category: string) => void;
}> = ({
  categories,
  selectedItems,
  exclude,
  onSelectedItemsChange,
  onCreateCategory,
}) => {
  const bg = useColorModeValue("gray.100", "gray.800");
  const items = [
    ...(!categories.includes(CategorySections.ENVIRONMENT) ? [] : [
      { value: CommonName.WORK, label: "work" },
      { value: CommonName.SCHOOL, label: "school" },
      { value: CommonName.FRIENDS, label: "friends" },
      { value: CommonName.FAMILY, label: "family" },
      { value: CommonName.PROJECT, label: "project" },
      { value: CommonName.ONLINE, label: "online" },
      { value: CommonName.CODE, label: "code" },
      { value: CommonName.GIT, label: "git" },
    ]),
    ...(!categories.includes(CategorySections.SOCIAL) ? [] : [
      { value: CommonName.SOCIAL, label: "social" },
      { value: CommonName.TWITTER, label: "twitter" },
      { value: CommonName.DISCORD, label: "discord" },
      { value: CommonName.SLACK, label: "slack" },
      { value: CommonName.INSTAGRAM, label: "instagram" },
      { value: CommonName.GITHUB, label: "github" },
      { value: CommonName.GITLAB, label: "gitlab" },
    ]),
    ...(!categories.includes(CategorySections.LANGUAGE) ? [] : [
      { value: CommonName.EN, label: "en" },
      { value: CommonName.ES, label: "es" },
    ]),
  ]
    .filter(item => !exclude || !exclude.includes(item.label));
  const selectedItemsProp = selectedItems
    ? { selectedItems }
    : {};
  return (
    <AutoComplete
      label="Category"
      placeholder="Choose a category"
      items={items}
      listStyleProps={{ bg }}
      toggleButtonStyleProps={{ bg }}
      disableCreateItem={!onCreateCategory}
      onCreateItem={onCreateCategory && (i => onCreateCategory(i.label))}
      createItemRenderer={i => `Use '${i}' as a custom category`}
      onSelectedItemsChange={(onSelectedItemsChange && (changes => onSelectedItemsChange(changes.selectedItems))) ?? undefined}
      {...selectedItemsProp}
    />
  );
};
