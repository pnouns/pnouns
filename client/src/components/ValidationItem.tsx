import React from "react";
import {
  ListIcon,
  ListItem,
} from "@chakra-ui/react";
import { MdCancel, MdCheckCircle } from "react-icons/md";

/**
 * A list item (to be included in an `<List>`)
 * that displays a check/X depending on a condition.
 */
export const ValidationItem: React.FC<{
  test?: boolean,
}> = ({ test, children }) => {
  return (
    <ListItem>
      <ListIcon as={test ? MdCheckCircle : MdCancel} color={test ? "green.500" : "red.500"} />
      { children }
    </ListItem>
  );
}
