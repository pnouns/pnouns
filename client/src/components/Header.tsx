import React, { useEffect } from "react";
import {
  Badge,
  Button,
  chakra,
  Flex,
  HStack,
  Text,
  useColorMode,
  useColorModeValue,
} from "@chakra-ui/react";
import { Link } from "./Link";
import { useUserContext } from "../context/user.context";

export interface HeaderPageDetails {
  nickname: string;
  displayName: string;
  category?: string;
}

export const Header: React.FC<{
  page?: HeaderPageDetails;
  releaseNotes?: boolean;
  noAuth?: boolean;
  centerTitle?: boolean;
  onPublish?: () => void;
}> = ({
  page,
  releaseNotes,
  noAuth,
  centerTitle: forceCenterTitle,
  onPublish,
}) => {
  const { loggedIn } = useUserContext();
  const { toggleColorMode: toggleMode } = useColorMode();
  const text = useColorModeValue("dark", "light");
  const bg = useColorModeValue("white", "gray.500");

  useEffect(() => {
    const headway = (window as any).Headway;
    if(headway && typeof headway === "object" && typeof headway.init === "function") {
      headway.init();
    }
  }, [releaseNotes]);

  const centerTitle = !!page || forceCenterTitle;

  return (
    <chakra.header
      pos="sticky"
      top="0"
      zIndex="3"
      bg={bg}
      left="0"
      right="0"
      width="full"
      marginBottom="2"
    >
      <chakra.div height="4.5rem" mx="auto" maxW="8x1">
        <Flex w="100%" h="100%" px="6" align="center" justify="space-between">
          { ((!page && !centerTitle) || releaseNotes) && (
            <Flex align="center" flex="10">
              {
                (!page && !centerTitle) && (
                  <Link external path="https://pnouns.fyi/">
                    pnouns.fyi
                  </Link>
                )
              }
              {
                releaseNotes && (
                  <Badge marginLeft="1">
                    <HStack>
                      <span>Release Notes</span>
                      <span className="ReleaseNotesBadge" />
                    </HStack>
                  </Badge>
                )
              }
            </Flex>
          )}
          <Flex justifyContent="center" flex={centerTitle ? "20" : "1"}>
            {
              page && (
                <>
                  <Link external path={`https://${page.nickname}.pnouns.fyi/`}>{page.displayName}</Link>&nbsp;
                  { page.category && (
                    <><Text>{ page.category }</Text>&nbsp;</>
                  )}
                  <Link external path={`https://pnouns.fyi/`}>Pronouns (FYI)</Link>
                </>
              )
            }
            {
              centerTitle && !page && (
                <>
                  <Link external path="https://pnouns.fyi/">
                    pnouns.fyi
                  </Link>
                </>
              )
            }
          </Flex>
          <Flex justify="flex-end" flex="10" align="center">
            { !noAuth && loggedIn && (
              <>
                <Link path="/pages/">My Pages</Link>&nbsp;
              </>
            )}
            { !noAuth && (loggedIn
                ? <Link path="/logout/">Logout</Link>
                : <Link path="/login/">Login or Register</Link>
              )
            }
            {
              typeof onPublish === "function" && (
                <>
                  &nbsp;<Button colorScheme="green" onClick={onPublish}>Publish</Button>
                </>
              )
            }
          </Flex>
        </Flex>
      </chakra.div>
    </chakra.header>
  )
}
