import React from "react";
import { Box, HStack, Text, useColorModeValue } from "@chakra-ui/react";
import { JoiningWord, PageEditor_PageQuery, PasswordFormat } from "../generated-types";
import { joiningWordLabel } from "../util/joining-word";

const DEFAULT_CATEGORY_PLACEHOLDER = "school";

interface TextProperties {
  textDecoration?: "line-through",
};

function falseToUndefined(input: string | false | undefined): string | undefined {
  if(typeof input === "string") {
    return input;
  }
  return undefined;
}

const UrlSection: React.FC<{
  label?: string;
  placeholder?: string;
  text: string;
  textProperties?: TextProperties;
  color?: string;
}> = ({ label, text, placeholder, color, textProperties }) => {
  return (
    <Box marginInlineStart="0 !important">
      <Text
        borderWidth="1px"
        borderRadius="lg"
        borderColor={color ?? "transparent"}
        {...textProperties}
      >
        { text && text.length > 0 ? text : placeholder }
      </Text>
      <Text fontSize="xs" color={color}>{ label ?? <>&nbsp;</> }</Text>
    </Box>
  )
};

export interface UrlPreviewData {
  hideProtocol?: boolean;
  nickname: string;
  category?: string;
  categoryAsPath?: boolean;
  categoryJoiningWord?: JoiningWord;
  categoryJoiningWordFinal?: boolean;
  alwaysCategoryJoiningWord?: boolean;
  page?: string;
  pageJoiningWord?: JoiningWord;
  pageJoiningWordFinal?: boolean;
  alwaysPageJoiningWord?: boolean;
  password?: string;
  defaultPasswordFormat?: PasswordFormat;
};

export const UrlTextPreview: React.FC<{
  url: UrlPreviewData;
  strikeThrough?: boolean;
}> = ({ url, strikeThrough }) => {
  const nicknameColor = useColorModeValue("cyan.700", "cyan.300");
  const categoryColor = useColorModeValue("orange.600", "orange.200");
  const pageColor = useColorModeValue("green.800", "green.200");
  const joinColor = useColorModeValue("gray.900", "gray.100");
  const textProperties: TextProperties = {
    textDecoration: strikeThrough ? "line-through" : undefined,
  };
  return (
    <Box>
      <HStack>
        <UrlSection text="https://" textProperties={textProperties} />
        <UrlSection label="name" text={url.nickname} placeholder="jays" color={nicknameColor} textProperties={textProperties} />
        { url.category !== undefined && !url.categoryAsPath && (
          <>
            <UrlSection text="." textProperties={textProperties} />
            <UrlSection label="category" text={url.category} placeholder={DEFAULT_CATEGORY_PLACEHOLDER} color={categoryColor} textProperties={textProperties} />
          </>
        ) }
        <UrlSection text=".pnouns.fyi/" textProperties={textProperties} />
        {
          url.category !== undefined && url.categoryAsPath && (url.categoryJoiningWord || url.alwaysCategoryJoiningWord) && (
            <>
              <UrlSection
                label="join"
                text={(url.categoryJoiningWordFinal && url.categoryJoiningWord) ? joiningWordLabel(url.categoryJoiningWord) : ""}
                placeholder={falseToUndefined(!url.categoryJoiningWordFinal && (
                  url.categoryJoiningWord
                    ? joiningWordLabel(url.categoryJoiningWord)
                    : url.alwaysCategoryJoiningWord
                      ? joiningWordLabel(JoiningWord.AT)
                      : undefined
                ))}
                color={joinColor}
                textProperties={textProperties}
              />
              <UrlSection text="/" textProperties={textProperties} />
            </>
          )
        }
        {
          url.category !== undefined && url.categoryAsPath && (
            <>
              <UrlSection label="category" text={url.category} placeholder={DEFAULT_CATEGORY_PLACEHOLDER} color={categoryColor} textProperties={textProperties} />
              <UrlSection text="/" textProperties={textProperties} />
            </>
          )
        }
        {
          url.page !== undefined && (url.pageJoiningWord || url.alwaysPageJoiningWord) && (
            <>
              <UrlSection
                label="join"
                text={(url.pageJoiningWordFinal && url.pageJoiningWord) ? joiningWordLabel(url.pageJoiningWord) : ""}
                placeholder={falseToUndefined(!url.pageJoiningWordFinal && (
                  url.pageJoiningWord
                    ? joiningWordLabel(url.pageJoiningWord)
                    : url.alwaysPageJoiningWord
                      ? joiningWordLabel(JoiningWord.WITH)
                      : undefined
                  ))}
                color={joinColor}
                textProperties={textProperties}
              />
              <UrlSection text="/" textProperties={textProperties} />
            </>
          )
        }
        {
          url.page !== undefined && (
            <>
              <UrlSection
                label="page"
                text={url.page}
                placeholder="friends"
                color={pageColor}
                textProperties={textProperties}
              />
              <UrlSection text="/" textProperties={textProperties} />
            </>
          )
        }
      </HStack>
    </Box>
  );
};

export function formatUrl(data: UrlPreviewData): string {
  const isCategoryPage = data.page;
  const isCategory = !isCategoryPage && data.nickname;
  const isNicknamePage = !data.nickname;
  const params = new URLSearchParams();
  const useQuery = data.defaultPasswordFormat === PasswordFormat.QUERY_PASSWORD
    || data.defaultPasswordFormat === PasswordFormat.QUERY_P;
  /**
   * If the password would be in the domain, we'll need to force a query parameter
   * regardless of the user's choice.
   */
  const finalPathInDomain = isNicknamePage || (isCategory && !data.categoryAsPath);
  if(data.password && (useQuery || finalPathInDomain)) {
    if(data.defaultPasswordFormat === PasswordFormat.QUERY_P) {
      params.set("p", data.password);
    } else {
      params.set("password", data.password);
    }
  }
  const paramString = params.toString();

  return [
    data.hideProtocol ? "" : `https://`,
    data.nickname,
    data.categoryAsPath
      ? ""
      : `.${data.category}`,
    ".pnouns.fyi",
    data.category && data.categoryAsPath && data.categoryJoiningWord
      ? `/${joiningWordLabel(data.categoryJoiningWord)}`
      : "",
    data.category && data.categoryAsPath ?
      [
        `/${data.category}`,
        isCategoryPage && data.password && data.defaultPasswordFormat === PasswordFormat.IN_PAGE
          ? `+${data.password}`
          : "",
      ].join("")
      : "",
    data.page && data.pageJoiningWord
      ? `/${joiningWordLabel(data.pageJoiningWord)}`
      : "",
    data.page ?
      [
        `/${data.page}`,
        data.password && data.defaultPasswordFormat === PasswordFormat.IN_PAGE
          ? `+${data.password}`
          : "",
      ].join("")
      : "",
    (data.hideProtocol && paramString.length === 0) ? "" : "/",
    paramString.length > 0 ? `?${paramString}` : "",
  ].join("");
}

export function formatEditorUrl(
  page?: PageEditor_PageQuery,
  override: Partial<UrlPreviewData> = {},
): string {
  if(!page || !page.getPage) {
    return "";
  }
  const isCategoryPage = !!page.getPage.parent;
  const category = isCategoryPage ? page.getPage.parent : page.getPage;
  const categoryPage = isCategoryPage ? page.getPage : undefined;
  return formatUrl({
    nickname: page.getPage.nickname.id,
    category: category?.name ?? undefined,
    categoryAsPath: !category?.defaultSubdomain ?? undefined,
    categoryJoiningWord: category?.defaultJoiningWord ?? undefined,
    page: categoryPage?.name ?? undefined,
    pageJoiningWord: categoryPage?.defaultJoiningWord ?? undefined,
    password: page.getPage.password ?? undefined,
    defaultPasswordFormat: page.getPage.defaultPasswordFormat ?? undefined,
    ...override,
  });
}
