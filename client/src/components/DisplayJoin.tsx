import React from "react";
import { Text, Tooltip } from "@chakra-ui/react";

export const DisplayInlineJoin: React.FC<{
}> = ({ children }) => {
  return (
    <Tooltip
      hasArrow
      label="Multiple options can be used"
      bg="gray.300"
      color="black"
    >
      <Text
        display="inline"
        textDecoration="dashed underline"
      >
        { children }
      </Text>
    </Tooltip>
  )
}
