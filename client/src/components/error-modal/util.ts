import { CombinedError, Operation } from "urql";

export type GraphQLResponseError = {
  operation: Operation;
  errors: CombinedError;
  retry?: () => void;
}

export type ReportFunction = (operation: Operation, errors: CombinedError, retry?: () => void, preventOpen?: boolean) => void;
