import React, { useState } from "react";
import { Text, Heading, Link, Divider } from "@chakra-ui/layout";
import { Button, ButtonGroup } from "@chakra-ui/button";
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/modal";
import { GraphQLResponseError } from "./util";

export interface ErrorModalProps {
  isOpen: boolean;
  errors: GraphQLResponseError[];
  setErrors: (errors: GraphQLResponseError[]) => void;
  onClose: () => void;
};

const ErrorModal: React.FC<ErrorModalProps> = ({
  isOpen,
  errors,
  setErrors,
  onClose,
}) => {
  const [page, setPage] = useState(0);
  const error = errors[Math.min(page, errors.length - 1)];
  if(errors.length < 1 || !error) {
    return (
      <Modal isOpen={isOpen} onClose={onClose} size="xl">
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>No Errors</ModalHeader>
        </ModalContent>
      </Modal>
    );
  }

  const handleDismissError = (): void => {
    const lastError = errors.length === 1;
    setErrors(errors.filter((e, i) => i !== page));
    if(lastError) {
      onClose();
    }
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose} size="xl">
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>
          {error.operation.kind} failed with {error.errors.graphQLErrors.length} errors
        </ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          {error.errors.message}
          <Divider pv="2" />
          <Heading as="h3" fontSize="md" textAlign="center">Need a hand?</Heading>
          <Text>
            If you think that something is wrong or could use assistance, please let me know!<br />
            - Flyyn
          </Text>
          <Text>
            <Link textColor="teal.500" href="https://pnouns.freshdesk.com/support/tickets/new" target="_blank">Create a support ticket</Link>
          </Text>
          {errors.length > 1 && (
            <>
              <Divider pv="2" />
              <Text textColor="gray.500">Failed request {page + 1} of {errors.length}</Text>
              <ButtonGroup variant="outline" spacing="3">
                <Button disabled={page === 0} onClick={() => setPage(page - 1)}>Previous</Button>
                <Button colorScheme="cyan" onClick={handleDismissError}>Dismiss Error {page + 1}</Button>
                <Button disabled={page === errors.length - 1} onClick={() => setPage(page + 1)}>Next</Button>
              </ButtonGroup>
            </>
          )}
        </ModalBody>
        <ModalFooter>
          <Button
            colorScheme="orange"
            disabled={!error.retry}
            onClick={() => {
              error.retry && error.retry();
              handleDismissError();
            }}
          >
            Retry Request
          </Button>
          <Button onClick={handleDismissError} color="cyan">
            Close
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default ErrorModal;
