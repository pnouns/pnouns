import React, { ReactElement, useState } from "react";
import loadable from "@loadable/component";
import { GraphQLResponseError, ReportFunction } from "./util";

const ErrorModal = loadable(() => import("./modal"), {
  fallback: <></>,
});

export interface ErrorModalHookOutput {
  /**
   * If the modal is open.
   */
  open: boolean;

  /**
   * The modal that should be included in the page.
   */
  errorModal: ReactElement;

  report: ReportFunction;
}

export const useErrorModal = (): ErrorModalHookOutput => {
  const [open, setOpen] = useState(false);
  const [errors, setErrors] = useState<GraphQLResponseError[]>([]);
  const errorModal = <ErrorModal
    isOpen={open}
    errors={errors}
    setErrors={setErrors}
    onClose={() => setOpen(false)}
  />;

  const report: ReportFunction = (operation, operationErrors, retry, preventOpen = false) => {
    console.error(`Failed GraphQL ${operation.kind}: ${operationErrors.message}`, operation, operationErrors);
    setErrors([
      ...errors,
      {
        operation,
        retry,
        errors: operationErrors,
      },
    ]);
    if(!preventOpen) {
      setOpen(true);
    }
  };

  return {
    open,
    errorModal,
    report,
  };
};
