import React from "react";
import {
  Box,
  Flex,
  Heading,
  Spacer,
  Text,
} from "@chakra-ui/layout";
import { useColorModeValue } from "@chakra-ui/color-mode";
import { IconButton } from "@chakra-ui/button";
import { EditIcon } from "@chakra-ui/icons";
import { PublishedSection, PublishedValue } from "../generated-types";
import { strengthDisplay } from "../util/strength-display";
import { StrengthDisplay } from "./StrengthDisplay";

export const EditorContainer: React.FC<{
}> = ({ children }) => {
  const bg = useColorModeValue("gray.700", "gray.100");
  const text = useColorModeValue("white", "black");
  return (
    <Box borderWidth="1px" borderRadius="lg" overflow="hidden" bg={bg} textColor={text} p="1">
      {children}
    </Box>
  );
}

export const SectionContainer: React.FC<{
  title: string,
  onEdit?: () => void;
}> = ({ title, onEdit, children }) => {
  return (
    <EditorContainer>
      <Flex className="section-header">
        <Box p="2" flex="10">
          <Heading size="md">{title}</Heading>
        </Box>
        {
          typeof onEdit === "function" && (
            <Box justify="flex-end" flex="1">
              <IconButton aria-label="Edit" icon={<EditIcon />} onClick={onEdit} />
            </Box>
          )
        }
      </Flex>
      { children }
    </EditorContainer>
  );
}

export const SectionValueDisplay: React.FC<{
  value: PublishedValue,
}> = ({ value }) => {
  const strength = strengthDisplay(
    value.strength,
    value.customStrengthText || undefined,
    value.customStrengthIcon || undefined,
  );
  return (
    <Flex>
      <Box p="2">{ value.text }</Box>
      <Spacer />
      <StrengthDisplay
        strength={value.strength}
        customStrengthText={value.customStrengthText}
        customStrengthIcon={value.customStrengthIcon}
      />
      {/*<Box borderRadius="lg" textStyle={strength.s} layerStyle={strength.s} p="1">
        <Flex>
          <Center h="1.5em" paddingRight="0.5">
            <Icon as={strength.i} />
          </Center>
          <Center h="1.5em">
            <Text>
              { strength.t }
            </Text>
          </Center>
        </Flex>
      </Box>*/}
    </Flex>
  );
};

export const SectionDisplay: React.FC<{
  section: PublishedSection,
  onEdit?: () => void;
}> = ({
  section,
  onEdit,
}) => {
  const bg = useColorModeValue("gray.700", "gray.100");
  const text = useColorModeValue("white", "black");
  const sectionOrder = section.valueOrder;
  return (
    <SectionContainer title={section.name} onEdit={onEdit}>
      { section.subtitle && (<Heading size="sm">{ section.subtitle } </Heading>) }
      { section.lead && (<Text size="sm">{ section.lead }</Text>) }
      {
        section.values
          .sort((a,b) => sectionOrder.indexOf(a.id) - sectionOrder.indexOf(b.id))
          .map(value => (<SectionValueDisplay value={value} key={value.id} />))
      }
    </SectionContainer>
  );
};
