import React from "react";
import {
  Box,
  Flex,
  Heading,
  useColorModeValue,
} from "@chakra-ui/react";
import { Maybe } from "../generated-types";

export const BioDisplay: React.FC<{
  title?: Maybe<string>;
  subtitle?: Maybe<string>;
}> = ({
  title,
  subtitle,
}) => {
  const bg = useColorModeValue("gray.700", "gray.100");
  const text = useColorModeValue("white", "black");
  return (
    <Box
      borderWidth="1px"
      borderRadius="lg"
      overflow="hidden"
      bg={bg}
      textColor={text}
      p="1"
    >
      <Flex className="bio-header">
        <Box p="2" flex="10">
          <Heading size="md">{title}</Heading>
        </Box>
      </Flex>
      <Flex className="bio-subtitle">
        <Box p="2" flex="10">
          <Heading size="sm">{subtitle}</Heading>
        </Box>
      </Flex>
    </Box>
  );
}
