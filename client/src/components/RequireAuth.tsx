import React from "react";
import {
  Container,
  Heading,
  Text,
} from "@chakra-ui/react";
import { useUserContext } from "../context/user.context";
import { Header } from "./Header";

export const RequireAuth: React.FC<{
}> = ({
  children,
}) => {
  const { loggedIn } = useUserContext();
  if(!loggedIn) {
    return (
      <>
        <Header />
        <Container maxW="container.md">
          <Heading size="md" textAlign="center">Authentication Required</Heading>
          <Text>
            You must be logged in to view this page.
          </Text>
        </Container>
      </>
    );
  }
  return (
    <>
      { children }
    </>
  );
};
