import React from "react";
import {
  Select,
} from "@chakra-ui/react";
import { CommonName } from "../../generated-types";

export const CategorySelect: React.FC<{
  value?: CommonName;
  placeholder?: string;
  disabled?: boolean;
  size?: "xs" | "sm" | "md" | "lg";
  onChange?: (value: CommonName) => void;
}> = ({ size, placeholder, disabled, value, onChange }) => {
  return (
    <Select
      size={size}
      placeholder={placeholder}
      disabled={disabled}
      value={value}
      onChange={e => onChange && onChange(e.target.value as CommonName)}
    >
      { /* ENVIRONMENT */ }
      <option value={CommonName.WORK}>work</option>
      <option value={CommonName.SCHOOL}>school</option>
      <option value={CommonName.FRIENDS}>friends</option>
      <option value={CommonName.FAMILY}>family</option>
      <option value={CommonName.PROJECT}>project</option>
      <option value={CommonName.ONLINE}>online</option>
      <option value={CommonName.CODE}>code</option>
      { /* SOCIAL */ }
      <option value={CommonName.SOCIAL}>social</option>
      <option value={CommonName.TWITTER}>twitter</option>
      <option value={CommonName.DISCORD}>discord</option>
      <option value={CommonName.SLACK}>slack</option>
      <option value={CommonName.INSTAGRAM}>instagram</option>
      <option value={CommonName.GITHUB}>github</option>
      <option value={CommonName.GITLAB}>gitlab</option>
      { /* LANGUAGE */ }
      <option value={CommonName.EN}>en</option>
      <option value={CommonName.ES}>es</option>
    </Select>
  )
};
