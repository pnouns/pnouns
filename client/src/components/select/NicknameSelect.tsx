import React from "react";
import { Select } from "@chakra-ui/react";

export const NicknameSelect: React.FC<{
  nicknames: string[];
  value: string;
  size?: "xs" | "sm" | "md" | "lg";
  onChange: (nickname: string) => void;
}> = ({ nicknames, value, size, onChange }) => {
  return (
    <Select
      size={size}
      placeholder="Nickname"
      value={value}
      onChange={e => onChange && onChange(e.target.value)}
    >
      {
        nicknames.map(nickname => (
          <option key={nickname} value={nickname}>{nickname}</option>
        ))
      }
    </Select>
  )
};
