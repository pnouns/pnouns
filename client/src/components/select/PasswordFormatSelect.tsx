import React from "react";
import { FormControl, FormLabel, InputGroup, Select } from "@chakra-ui/react";
import { PasswordFormat } from "../../generated-types";

export const PasswordFormatSelect: React.FC<{
  id: string;
  password?: string;
  value?: PasswordFormat;
  allowInPage?: boolean;
  disabled?: boolean;
  size?: "xs" | "sm" | "md" | "lg";
  onChange?: (value: PasswordFormat) => void;
}> = ({
  id,
  password,
  value,
  allowInPage,
  disabled,
  size,
  onChange,
}) => {
  return (
    <FormControl id={id}>
      <FormLabel>Password Format</FormLabel>
      <InputGroup>
        <Select
          size={size}
          disabled={disabled}
          value={value}
          onChange={e => onChange && onChange(e.target.value as PasswordFormat)}
        >
          { allowInPage && (<option value={PasswordFormat.IN_PAGE}>In Path</option>) }
          <option value={PasswordFormat.QUERY_PASSWORD}>?password={password}</option>
          <option value={PasswordFormat.QUERY_P}>?p={password}</option>
        </Select>
      </InputGroup>
    </FormControl>
  );
};
