import React from "react";
import {
  Select,
} from "@chakra-ui/react";

export enum CategoryDefaultDisplay {
  SUBDOMAIN,
  PATH,
  PATH_WITH_JOIN,
}

export const CategoryDefaultDisplaySelect: React.FC<{
  nickname: string;
  category: string;
  value: CategoryDefaultDisplay;
  isCustomCategory?: boolean;
  disabled?: boolean;
  display?: "inline";
  size?: "xs" | "sm" | "md" | "lg";
  onChange?: (value: CategoryDefaultDisplay) => void;
}> = ({ nickname, category, isCustomCategory, display, size, disabled, value, onChange }) => {
  return (
    <Select
      display={display}
      size={size}
      placeholder="Default Display"
      disabled={disabled}
      value={value}
      onChange={e => onChange && onChange(parseInt(e.target.value) as CategoryDefaultDisplay)}
    >
      <option
        value={CategoryDefaultDisplay.SUBDOMAIN}
        disabled={isCustomCategory}
      >
        https://{nickname}.{category}.pnouns.fyi/
      </option>
      <option value={CategoryDefaultDisplay.PATH}>
        https://{nickname}.pnouns.fyi/{category}/
      </option>
      <option value={CategoryDefaultDisplay.PATH_WITH_JOIN}>
        https://{nickname}.pnouns.fyi/JOIN/{category}/
      </option>
    </Select>
  )
};
