import React, { useState } from "react";
import { useSelect, UseSelectStateChange } from "downshift";
import { Icon, List, ListItem, Select } from "@chakra-ui/react";
import { ChevronDownIcon } from "@chakra-ui/icons";
import { Maybe, Strength, usePageEditor_UpdateStrengthMutation } from "../../generated-types";
import { strengthDisplay } from "../../util/strength-display";
import { StrengthDisplay } from "../StrengthDisplay";

const strengths = [
  Strength.BEST,
  Strength.TESTING,
  Strength.GOOD,
  Strength.OK,
  Strength.JOKINGLY,
  Strength.CASUALLY_TESTING,
  Strength.ITS_COMPLICATED,
  Strength.UNSURE,
  Strength.AVOID,
  Strength.CONDITIONAL,
  Strength.NEVER,
  Strength.CUSTOM,
];

export const StrengthSelect: React.FC<{
  sectionValueId: string,
  strength: Strength,
  customStrengthText?: Maybe<string>;
  flex?: string,
}> = ({
  sectionValueId,
  strength,
  customStrengthText,
  flex,
}) => {
  const [newStrength, setNewStrength] = useState(strength);
  const [updating, setUpdating] = useState(false);

  const [, updateStrength] = usePageEditor_UpdateStrengthMutation();

  const handleSelectedItemChange = async ({ selectedItem }: UseSelectStateChange<Strength>) => {
    if(updating || !selectedItem) {
      return;
    }
    setNewStrength(selectedItem);
    setUpdating(true);
    await updateStrength({
      input: {
        sectionValueId: sectionValueId,
        strength: selectedItem,
      },
    });
    setUpdating(false);
  };

  const {
    isOpen,
    selectedItem,
    getToggleButtonProps,
    getLabelProps,
    getMenuProps,
    highlightedIndex,
    getItemProps,
  } = useSelect({
    items: strengths,
    selectedItem: updating ? newStrength : strength,
    onSelectedItemChange: handleSelectedItemChange,
  });
  return (
    <>
      <StrengthDisplay
        strength={selectedItem ?? Strength.UNSURE}
        customStrengthText={customStrengthText}
        rightIcon={<ChevronDownIcon />}
        flex={flex}
        {...getToggleButtonProps()}
      />
      <List
        width="100%"
        maxWidth="300"
        maxHeight="250"
        overflowY="auto"
        margin="0"
        borderTop="0"
        zIndex="1000"
        position="absolute"
        bg="gray.700"
        textColor="white"
        {...getMenuProps()}
      >
        {isOpen && strengths.map((strength, index) => (
          <ListItem
            key={strength}
            {...getItemProps({item: strength, index})}
          >
            <StrengthDisplay strength={strength} />
          </ListItem>
        ))}
      </List>
    </>
  );

}
