import React from "react";
import {
  Select,
} from "@chakra-ui/react";
import { JOINING_WORDS } from "../../util/page-matching";
import { JoiningWord } from "../../generated-types";

export const JoinerSelect: React.FC<{
  value?: JoiningWord;
  placeholder?: string;
  disabled?: boolean;
  size?: "xs" | "sm" | "md" | "lg";
  onChange?: (value: JoiningWord) => void;
}> = ({ size, placeholder, disabled, value, onChange }) => {
  return (
    <Select
      size={size}
      placeholder={placeholder}
      disabled={disabled}
      value={value}
      onChange={e => onChange && onChange(e.target.value as JoiningWord)}
    >
      <option value={JoiningWord.A}>a</option>
      <option value={JoiningWord.AN}>an</option>
      <option value={JoiningWord.AT}>at</option>
      <option value={JoiningWord.FOR}>for</option>
      <option value={JoiningWord.IN}>in</option>
      <option value={JoiningWord.AND}>and</option>
      <option value={JoiningWord.WITH}>with</option>
      <option value={JoiningWord.ON}>on</option>
    </Select>
  );
};
