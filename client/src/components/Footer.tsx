import React from "react";
import { Flex, Text } from "@chakra-ui/react";
import { Link } from "./Link";

export const Footer: React.FC<{
  displayName?: string;
}> = ({
  displayName,
}) => {
  return (
    <Flex my="4" pb="2">
      <Text flex="3">
        <Link path="https://pnouns.fyi/terms/" external color="white">Terms of Service</Link><br />
        <Link path="https://pnouns.fyi/privacy/" external color="white">Privacy Policy</Link>
      </Text>
      <Text flex="10" textAlign="center" textColor="gray.500">
        { displayName && `${displayName} pronoun page powered by `}
        <Link path="https://pnouns.fyi/" external color="white">pnouns.fyi</Link><br />
        <Link path="https://pnouns.fyi/" external>Create your own page for free</Link><br />
        Created by <Link path="https://twitter.com/GayFlyyn" external color="white">@GayFlyyn</Link>&nbsp;
        (<Link path="https://flyyns.project.pnouns.fyi/" external color="white">they/them</Link>)
      </Text>
      <Text flex="3" textAlign="right">
        <Link path="https://pnouns.freshdesk.com/support/solutions" external color="white">Knowledge Base</Link>
      </Text>
    </Flex>
  );
};
