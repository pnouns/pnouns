import { Center, Container } from "@chakra-ui/react";
import React from "react";

const Error404Page: React.FC<{}> = () => {
  return (
    <Container>
      <Center>
        <h1>404 Page Not Found</h1>
      </Center>
    </Container>
  );
}

export default Error404Page;
