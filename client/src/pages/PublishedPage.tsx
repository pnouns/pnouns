import React from "react";
import loadable from "@loadable/component";
import {
  Box,
  Container,
  Grid,
  GridItem,
} from "@chakra-ui/layout";
import { Header } from "../components/Header";
import { usePublishedPageQuery } from "../generated-types";
import { PublishedPageMatch } from "../util/page-matching";
import { SectionDisplay } from "../components/SectionDisplay";
import { Loading } from "./Loading";
import { Footer } from "../components/Footer";
import { BioDisplay } from "../components/BioDisplay";
import { PageBadgeDisplay } from "../components/PageBadgeDisplay";

const PublishedSubPageList = loadable(() => import("./published-page/PublishedSubPageList"), {
  fallback: <></>,
});

const PublishedPage: React.FC<{
  page: PublishedPageMatch,
}> = ({ page: pageMatch }) => {
  const { search } = window.location;
  const searchParams = new URLSearchParams(search);
  const [{data, fetching, error}, refetchPage] = usePublishedPageQuery({
    variables: {
      input: {
        namespace: pageMatch.namespace,
        category: pageMatch.category,
        page: pageMatch.page,
        password: pageMatch.password ?? searchParams.get("password") ?? searchParams.get("p"),
      },
    },
  });
  if(!data || !data.getPublishedPage || fetching || error) {
    return (
      <Loading />
    );
  }
  const page = data.getPublishedPage;

  const displayName = page.nicknameDisplay ?? page.nicknamePath ?? "My";
  const categoryDisplayName = page.categoryDisplay ?? page.categoryPath ?? "";

  const sectionOrder = page.sectionOrder;

  return (
    <>
      <Header
        noAuth
        page={{
          nickname: page.nicknamePath ?? "",
          displayName,
          category: categoryDisplayName,
        }}
      />
      <Container maxW="container.xl">
        { page.showBadges && (page.staffBadges.length > 0 || page.supporterRoleBadge) && (
          <Box p="2">
            { page.staffBadges.map(role => <PageBadgeDisplay staffRole={role} />) }
            { page.supporterRoleBadge && <PageBadgeDisplay supporter={page.supporterRoleBadge} /> }
          </Box>
        )}
        <Grid
          templateColumns="repeat(6, 1fr)"
          gap="2"
        >
          <GridItem colSpan={{ base: 6, xl: 2 }}>
            <BioDisplay title={page.pageBioTitle} subtitle={page.pageBioSubtitle} />
          </GridItem>
          <GridItem colSpan={{ base: 6, md: 3, xl: 2 }}>
            { page.nameSection && <SectionDisplay section={page.nameSection} /> }
          </GridItem>
          <GridItem colSpan={{ base: 6, md: 3, xl: 2 }}>
            { page.pronounsSection && <SectionDisplay section={page.pronounsSection} /> }
          </GridItem>
        </Grid>
        {page && page.listedSubPages && Array.isArray(page.listedSubPages) && page.listedSubPages.length > 0 && (
          <PublishedSubPageList page={data} />
        )}
        <Grid
          templateColumns="repeat(6, 1fr)"
          gap="2"
          paddingTop="3"
        >
          { page.languageSections
            .sort((a,b) => sectionOrder.indexOf(a.id) - sectionOrder.indexOf(b.id))
            .map(section => (
              <GridItem key={section.id} colSpan={{ base: 6, md: 3, xl: 2 }}>
                <SectionDisplay section={section} />
              </GridItem>
            ))
          }
        </Grid>
        <Footer displayName={displayName} />
      </Container>
    </>
  );
}

export default PublishedPage;
