import React, { useState } from "react";
import {
  Box,
  Button,
  Flex,
  Heading,
  HStack,
  Spacer,
  useColorModeValue,
} from "@chakra-ui/react";
import { ArrowBackIcon } from "@chakra-ui/icons";
import { useUserContext } from "../../../context/user.context";
import { WizardLoginScreen } from "./screens/WizardLoginScreen";
import { WizardNicknameListScreen } from "./screens/WizardNicknameListScreen";
import { useHistory, useLocation } from "react-router-dom";
import { WizardNicknameScreen } from "./screens/WizardNicknameScreen";
import CategoryWizardScreen from "./screens/CategoryWizardScreen";

enum WizardStage {
  NEEDS_LOGIN = 1,
  CHOOSE_NICKNAME,
  CHOOSE_CATEGORY,
  CHOOSE_PAGE,
};

interface WizardHeaderContext {
  nickname?: string;
}

const HomepageWizardHeaderBackButtonText: { [K in WizardStage]: string } = {
  [WizardStage.NEEDS_LOGIN]: "",
  [WizardStage.CHOOSE_NICKNAME]: "Logout",
  [WizardStage.CHOOSE_CATEGORY]: "Switch Nickname",
  [WizardStage.CHOOSE_PAGE]: "Switch Category",
};

const HomepageWizardHeaderTitle: { [K in WizardStage]: (context: WizardHeaderContext) => string } = {
  [WizardStage.NEEDS_LOGIN]: () => "Login",
  [WizardStage.CHOOSE_NICKNAME]: () => "Select Nickname",
  [WizardStage.CHOOSE_CATEGORY]: ({ nickname }) => `Nickname ${nickname}`,
  [WizardStage.CHOOSE_PAGE]: () => "Category",
};

const HomepageWizardHeader: React.FC<{
  stage: WizardStage;
  nickname?: string;
  onBack: () => void;
}> = ({ stage, nickname, onBack }) => {
  return (
    <Flex>
      <HStack spacing="24px">
        { stage !== WizardStage.NEEDS_LOGIN && (
          <Button onClick={onBack} size="lg" variant="ghost" leftIcon={<ArrowBackIcon w={8} h={8} />}>
            { HomepageWizardHeaderBackButtonText[stage] }
          </Button>
        )}
        <Heading size="lg">
          { HomepageWizardHeaderTitle[stage]({ nickname }) }
        </Heading>
      </HStack>
    </Flex>
  );
};

export const HomepageWizard: React.FC<{
}> = ({
}) => {
  const history = useHistory();
  const { search } = useLocation();
  const searchParams = new URLSearchParams(search);
  const { loggedIn, namespaces, updateJwt } = useUserContext();
  const bg = useColorModeValue("gray.100", "gray.600");
  const text = useColorModeValue("black", "white");
  const [nickname, setNickname] = useState<string | undefined>(searchParams.get("nickname") ?? undefined);
  const [categoryId, setCategoryId] = useState<string | undefined>(searchParams.get("category") ?? undefined);
  const stage = !loggedIn
    ? WizardStage.NEEDS_LOGIN
    : !(typeof nickname === "string" && nickname.length > 2)
      ? WizardStage.CHOOSE_NICKNAME
      : !(typeof categoryId === "string" && categoryId.length > 8)
        ? WizardStage.CHOOSE_CATEGORY
        : WizardStage.CHOOSE_PAGE;
  return (
    <Box borderWidth="1px" borderRadius="lg" overflow="hidden" bg={bg} textColor={text} p="1" marginBottom="2">
      <HomepageWizardHeader
        stage={stage}
        nickname={nickname}
        onBack={() => {
          if(stage === WizardStage.CHOOSE_NICKNAME) {
            updateJwt("");
          } else if(stage === WizardStage.CHOOSE_CATEGORY) {
            history.push(`/`);
            setNickname(undefined);
          } else if(stage === WizardStage.CHOOSE_PAGE) {
            history.push(`/?nickname=${nickname}`);
            setCategoryId(undefined);
          }
        }}
      />
      { stage === WizardStage.NEEDS_LOGIN && <WizardLoginScreen /> }
      { loggedIn && !(typeof nickname === "string" && nickname.length > 2) && <WizardNicknameListScreen
        onCreatedNickname={(nickname, jwt) => {
          updateJwt(jwt);
          history.push(`/?nickname=${nickname}`);
          setNickname(nickname);
        }}
        onNicknameSelected={nickname => {
          history.push(`/?nickname=${nickname}`);
          setNickname(nickname);
        }}
      />}
      { stage === WizardStage.CHOOSE_CATEGORY
        && typeof nickname === "string"
        && <WizardNicknameScreen
          nicknameId={nickname}
          onCategorySelected={categoryId => {
            history.push(`/?nickname=${nickname}&category=${categoryId}`);
            setCategoryId(categoryId);
          }}
          /> }
      { stage === WizardStage.CHOOSE_PAGE
        && typeof nickname === "string"
        && typeof categoryId === "string"
        && <CategoryWizardScreen
          nicknameId={nickname}
          categoryId={categoryId}
        />
      }
    </Box>
  );
};
