import React, { useRef, useState } from "react";
import { useLocation } from "react-router-dom";
import { EmailIcon, UnlockIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  Center,
  Container,
  Heading,
  Input,
  SimpleGrid,
  Stack,
  Text,
} from "@chakra-ui/react";
import HCaptcha from "@hcaptcha/react-hcaptcha";
import posthog from "posthog-js";
import {
  useEmailTokenMutation,
  useSubmitEmailedTokenMutation,
} from "../../../../generated-types";
import { useUserContext } from "../../../../context/user.context";
import { useErrorModal } from "../../../../components/error-modal";
import { FaDiscord } from "react-icons/fa";
import { ButtonLink, Link } from "../../../../components/Link";
import { WizardLoginScreenDiscordToken } from "./WizardLoginScreenDiscordToken";

export const WizardLoginScreen: React.FC<{
}> = ({
}) => {
  const { errorModal, report } = useErrorModal();
  const { updateJwt } = useUserContext();
  const { search } = useLocation();
  const searchParams = new URLSearchParams(search);
  const discordLoginToken = searchParams.get("discord_token");
  const emailParam = searchParams.get("email");
  const tokenParam = searchParams.get("token");
  const attemptedParamLogin = useRef(false);
  const [isEmailing, setIsEmailing] = useState(false);
  const [isSubmittingToken, setIsSubmittingToken] = useState(false);
  const [emailAddress, setEmailAddress] = useState(emailParam ?? "");
  const [captchaToken, setCaptchaToken] = useState<null | string>(null);
  const [registerToken, setRegisterToken] = useState(tokenParam ?? "");
  const [, emailToken] = useEmailTokenMutation();
  const [, submitEmailedToken] = useSubmitEmailedTokenMutation();

  const submitEmailToken = async (): Promise<void> => {
    if(isEmailing) {
      return;
    }
    setIsEmailing(true);
    posthog.capture("submit email", { email: emailAddress });
    const result = await emailToken({
      input: {
        email: emailAddress,
        captchaToken: captchaToken ?? "",
      },
    });
    if(result.error) {
      return report(result.operation, result.error, submitEmailToken);
    }
    setIsEmailing(false);
  };

  const handleLogin = async (): Promise<void> => {
    if(isSubmittingToken) {
      return;
    }
    setIsSubmittingToken(true);
    const email = emailAddress;
    posthog.capture("submit emailed token", {
      email,
      token: registerToken,
    });
    const result = await submitEmailedToken({
      input: {
        email,
        token: registerToken,
      },
    });
    if(result.error) {
      report(result.operation, result.error, handleLogin);
    }
    if(result.data?.submitEmailedToken) {
      posthog.people.set({ email });
      updateJwt(result.data.submitEmailedToken);
    }
    setIsSubmittingToken(false);
  };

  if(emailParam && tokenParam && emailParam.length > 0 && tokenParam.length > 0 && !attemptedParamLogin.current) {
    attemptedParamLogin.current = true;
    posthog.capture("using emailed token link", {
      emailParam,
      tokenParam,
    });
    handleLogin();
  }

  if(discordLoginToken && discordLoginToken.length > 5) {
    return (
      <WizardLoginScreenDiscordToken token={discordLoginToken} />
    )
  }


  return (
    <>
      <Container maxW="container.md">
        <Text fontSize="lg" textAlign="center">
          Welcome to pnouns.fyi!
        </Text>
        <Heading as="h3" size="lg" textAlign="center" py="1">
          Social Login
        </Heading>
        <Center>
          <Stack direction="row" spacing={4} align="center">
            <ButtonLink leftIcon={<FaDiscord />} colorScheme="discord" variant="solid" external path="/auth/discord/">
              Discord
            </ButtonLink>
          </Stack>
        </Center>
        <Heading as="h3" size="lg" textAlign="center" py="1">
          Login via Email
        </Heading>
        <Text fontSize="md" textAlign="center">
          To dive right in to creating a pronoun page, please enter your email below to register or login.<br />
          We'll email you an authentication token you'll need to copy/paste below.
        </Text>
        <Text fontSize="md" textAlign="center">
          We are having extreme issues with our email service.  Please consider logging in with Discord&nbsp;
          <Link color="#76E4F7" external path="/auth/discord/">via OAuth</Link> or by joining <Link color="#76E4F7" external path="https://discord.gg/QnNcwbnHgk">our server</Link>.
          <br />
          Follow <Link color="#76E4F7" path="https://pnouns.freshdesk.com/support/solutions/articles/69000690390-linking-an-email-and-discord-account" external newTab>our guide</Link>&nbsp;
          to add Discord to an existing account.
        </Text>
        <Box>
          <SimpleGrid columns={{ sm: 2 }} spacing="10px">
            <Input
              name="email-address"
              size="md"
              placeholder="Email Address"
              value={emailAddress}
              onChange={e => setEmailAddress(e.target.value)}
            />
            <HCaptcha
              sitekey="c1a6d2d8-836a-4933-86df-dfa9e4a3aff6"
              onVerify={setCaptchaToken}
            />
          </SimpleGrid>
          <Button
            leftIcon={<EmailIcon />}
            isLoading={isEmailing}
            colorScheme="teal"
            variant="solid"
            loadingText="Emailing..."
            disabled={emailAddress.length < 3 || !captchaToken}
            onClick={submitEmailToken}
          >
            Send Email
          </Button>
          <Heading as="h4" size="md" textAlign="center" py="1">
            Received a Token?
          </Heading>
          <Text>
            Once you get a token via email, please enter it below to either register or login.<br />
            Emailed tokens expire after 30 minutes.<br />
            If you have reloaded this page, please enter your email again above.
          </Text>
          <Input
            name="registration-token"
            size="md"
            placeholder="Token"
            value={registerToken}
            onChange={e => setRegisterToken(e.target.value)}
          />
          <Button
            leftIcon={<UnlockIcon />}
            isLoading={isSubmittingToken}
            colorScheme="teal"
            variant="solid"
            loadingText="Confirming token..."
            disabled={registerToken.length < 3 || emailAddress.length < 3}
            onClick={handleLogin}
          >
            Confirm Token
          </Button>
        </Box>
        <Heading as="h3" size="lg" textAlign="center" py="1">
          Discord Bot Login
        </Heading>
        <Text>
          You can login by DMing our Discord Bot.
          You will need to join our Discord server to message the bot; we've configured the server to have protections for new users so you won't be seen by others as easily.&nbsp;
          <Link path="https://pnouns.freshdesk.com/en/support/solutions/articles/69000504037-login-via-discord-bot" external newTab>Learn More</Link>
        </Text>
        <Center>
          <ButtonLink path="https://discord.gg/QnNcwbnHgk" external newTab>Join Discord Server</ButtonLink>
        </Center>
      </Container>
      { errorModal }
    </>
  );
};
