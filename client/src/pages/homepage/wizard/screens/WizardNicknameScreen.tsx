import React, { useEffect, useState } from "react";
import {
  Button,
  Checkbox,
  CheckboxGroup,
  Container,
  FormControl,
  FormLabel,
  Grid,
  GridItem,
  Heading,
  HStack,
  Input,
  InputGroup,
  InputRightElement,
  Radio,
  RadioGroup,
  Spinner,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import {
  CommonName,
  JoiningWord,
  useCreateCategoryMutation,
  useCreateNicknamePageMutation,
  useNicknameWizardQuery,
} from "../../../../generated-types";
import { WizardList, WizardListItem } from "../WizardList";
import { formatUrl, UrlTextPreview } from "../../../../components/UrlTextPreview";
import { CategoryAutocomplete, CategorySections } from "../../../../components/CategoryAutocomplete";
import { ValidationInputIndication } from "../../../../components/validation/ValidationInputIndication";
import { useValidations } from "../../../../components/validation/useValidations";
import { JoinerSelect } from "../../../../components/select/JoinerSelect";
import { useHistory, useLocation } from "react-router-dom";
import { categoryLabel } from "../../../../util/category-display";
import { ButtonLink, Link } from "../../../../components/Link";
import { WizardFooter } from "../WizardFooter";
import posthog from "posthog-js";
import { useErrorModal } from "../../../../components/error-modal";

function isString(val: any): val is string {
  return val && typeof val === "string";
}

export enum CategoryDefaultDisplay {
  SUBDOMAIN = 1,
  PATH,
  PATH_WITH_JOIN,
}

export const WizardNicknameScreen: React.FC<{
  nicknameId: string,
  onCategorySelected: (categoryId: string) => void;
}> = ({
  nicknameId,
  onCategorySelected,
}) => {
  const activeColor = useColorModeValue("blue.900", "blue.300");
  const { errorModal, report } = useErrorModal();
  const history = useHistory();
  const { search } = useLocation();
  const searchParams = new URLSearchParams(search);
  const foundPreset = searchParams.get("cat");
  const [categoryAutocompleteSections, setCategoryAutocompleteSections] = useState<string[]>([
    ""+CategorySections.ENVIRONMENT,
    ""+CategorySections.SOCIAL,
    ""+CategorySections.LANGUAGE,
  ]);
  const [newPresetCategory, setNewPresetCategory] = useState<{ value: CommonName, label: string } | undefined>(
    (foundPreset && CommonName[foundPreset])
      ? { value: CommonName[foundPreset], label: categoryLabel(CommonName[foundPreset]) }
      : undefined
  );
  const [newCategoryUseCustom, setNewCategoryUseCustom] = useState(searchParams.get("customCategory") !== null);
  const [newCustomCategory, setNewCustomCategory] = useState(searchParams.get("customCategory") ?? "");
  const [hasEnteredCustomCategory, setHasEnteredCustomCategory] = useState(searchParams.get("customCategory") !== null && searchParams.get("customCategory") !== "");
  const foundJoin = searchParams.get("join");
  const [categoryJoiningWord, setCategoryJoiningWord] = useState<JoiningWord | undefined>(
    (foundJoin && JoiningWord[foundJoin]) ?? undefined);
  const foundDisp = searchParams.get("disp");
  const [newCategoryDisplay, setNewCategoryDisplay] = useState<CategoryDefaultDisplay>(
    (foundDisp && parseInt(foundDisp, 10) && CategoryDefaultDisplay[CategoryDefaultDisplay[parseInt(foundDisp, 10)]])
    ?? undefined
  );

  useEffect(() => {
    let newSearch = `/?nickname=${nicknameId}`;
    if(newCategoryUseCustom) {
      newSearch = `${newSearch}&customCategory=${newCustomCategory}`;
    } else if(newPresetCategory) {
      newSearch = `${newSearch}&cat=${newPresetCategory.value}`;
    }
    if(newCategoryDisplay === CategoryDefaultDisplay.PATH_WITH_JOIN && categoryJoiningWord) {
      newSearch = `${newSearch}&join=${JoiningWord[categoryJoiningWord]}`
    }
    if(newCategoryDisplay) {
      newSearch = `${newSearch}&disp=${newCategoryDisplay}`
    }
    if(search !== newSearch) {
      history.push(newSearch);
    }
  }, [ newCategoryUseCustom, newCustomCategory, newPresetCategory, categoryJoiningWord, newCategoryDisplay ]);

  const validation = useValidations({
    longEnough: newCustomCategory.length > 0,
    characterSet: !!newCustomCategory.match(/^[a-zA-Z]*$/),
  });

  const handleCustomCategoryChange = (value: string) => {
    if(!hasEnteredCustomCategory) {
      setHasEnteredCustomCategory(true);
    }
    setNewCustomCategory(value);
  };

  const [{ data: nickname, fetching, error }, refetchNickname] = useNicknameWizardQuery({
    variables: {
      id: nicknameId,
    },
  });

  const blockCreatingCategory = (!newCategoryUseCustom && !newPresetCategory)
    || (newCategoryUseCustom && !validation.valid)
    || (newCategoryDisplay === CategoryDefaultDisplay.SUBDOMAIN && newCategoryUseCustom);

  const [isCreatingNicknamePage, setIsCreatingNicknamePage] = useState(false);
  const [, createNicknamePage] = useCreateNicknamePageMutation();
  const handleCreateNicknamePage = async () => {
    if(isCreatingNicknamePage) {
      return;
    }
    setIsCreatingNicknamePage(true);
    const _nicknameId = nicknameId;
    posthog.capture("try create nickname page", {
      nicknameId,
    });
    const result = await createNicknamePage({
      input: {
        nicknameId,
      },
    });
    setIsCreatingNicknamePage(false);
    if(result.error) {
      report(result.operation, result.error, handleCreateNicknamePage);
    }
    if(result.data && result.data.createNicknamePage) {
      posthog.capture("created nickname page", {
        nicknameId: _nicknameId,
        pageId: result.data.createNicknamePage.id,
      });
      history.push(`/page/${result.data.createNicknamePage.id}/`);
    }
  }

  const [isCreatingCategory, setIsCreatingCategory] = useState(false);
  const [, createCategory] = useCreateCategoryMutation();
  const handleCreateCategory = async () => {
    if(isCreatingCategory || blockCreatingCategory) {
      return;
    }
    setIsCreatingCategory(true);
    const presetName = !newCategoryUseCustom ? newPresetCategory?.value : undefined;
    const customName = newCategoryUseCustom ? newCustomCategory : undefined;
    posthog.capture("try create category", {
      nicknameId,
      presetName,
      customName,
      defaultJoiningWord: newCategoryDisplay === CategoryDefaultDisplay.PATH_WITH_JOIN ? categoryJoiningWord : undefined,
      defaultSubdomain: newCategoryDisplay === CategoryDefaultDisplay.SUBDOMAIN,
    });
    const result = await createCategory({
      input: {
        nicknameId,
        presetName,
        customName,
        defaultJoiningWord: newCategoryDisplay === CategoryDefaultDisplay.PATH_WITH_JOIN ? categoryJoiningWord : undefined,
        defaultSubdomain: newCategoryDisplay === CategoryDefaultDisplay.SUBDOMAIN,
        listed: false,
      },
    });
    setIsCreatingCategory(false);
    if(result.error) {
      report(result.operation, result.error, handleCreateCategory);
    }
    if(result.data) {
      posthog.capture("created category", {
        nicknameId,
        categoryId: result.data.createCategory.id,
        presetName,
        customName,
      });
      onCategorySelected(result.data.createCategory.id);
    }
  };

  const categoryText = newCategoryUseCustom ? newCustomCategory : (newPresetCategory?.label ?? "");

  const categories = fetching || !nickname || !nickname.nickname
    ? []
    : nickname.nickname.pages.filter(page => typeof page.name === "string" && !page.parent);
  return (
    <>
      <Container maxW="container.md">
        <Grid templateColumns="repeat(6, 1fr)" gap="2">
          <GridItem colSpan={{ base: 6, md: 3 }}>
            <Heading as="h3" size="md" textAlign="center" my="1">
              Nickname Page
            </Heading>
            <Text>
              You can have a top-level page for your nickname,
              with names, pronouns, and other language,
              and optionally list any sub-pages you'd like to link to.
            </Text>
            <Text>
              https://{nicknameId}.pnouns.fyi/
            </Text>
          </GridItem>
          <GridItem colSpan={{base: 6, md: 3}}>
            { fetching && (<Spinner />)}
            { !fetching && !nickname?.nickname.nicknamePage && (
              <>
                <Heading as="h4" size="sm" textAlign="center" my="1">
                  Create a Page
                </Heading>
                <Text textAlign="center">
                  You don't have a nickname page yet!
                </Text>
                <Button onClick={handleCreateNicknamePage}>
                  Create Nickname Page
                </Button>
              </>
            )}
            { !fetching && nickname?.nickname.nicknamePage && (
              <>
                <Heading as="h4" size="sm" textAlign="center" my="1">
                  Nickname Page
                </Heading>
                <ButtonLink path={`/page/${nickname.nickname.nicknamePage.id}/`}>Edit Nickname Page</ButtonLink>
              </>
            )}
          </GridItem>
        </Grid>
        <Heading as="h3" size="md" textAlign="center" my="1">
          Categories
        </Heading>
        <Text textAlign="center">
          You can create categories under your nickname in case you use different names,
          pronouns, or language in different situations.
        </Text>
        <WizardList>
          { (categories ?? []).map(category => {
            const link = formatUrl({
              nickname: nicknameId,
              category: category.name ?? undefined,
              categoryAsPath: !category.defaultSubdomain,
              categoryJoiningWord: category.defaultJoiningWord ?? undefined,
            });
            const extraButtons = (
              <>
                <ButtonLink path={`/page/${category.id}/`}>Edit</ButtonLink>
                {category.isPublished && (
                  <ButtonLink path={link} external>View</ButtonLink>
                )}
              </>
            );
            return (
              <WizardListItem
                key={category.id}
                displayName={category.displayName ?? category.name ?? category.id}
                id={category.id}
                label="Sub-Pages"
                info={link}
                infoLink={category.isPublished ? link : undefined}
                extraButtons={extraButtons}
                onSelect={() => onCategorySelected(category.id)}
              />
            );
          })}
        </WizardList>
        <Heading as="h3" size="md" textAlign="center" my="1">
          Add a Category
        </Heading>
        <Text textAlign="center">
          You can create as many categories under your nickname as you'd like.
        </Text>
        { !newCategoryUseCustom && !newPresetCategory && (
          <>
            <CheckboxGroup value={categoryAutocompleteSections} onChange={(e: string[]) => setCategoryAutocompleteSections(e)}>
              <HStack>
                <Checkbox value={""+CategorySections.ENVIRONMENT}>Environments</Checkbox>
                <Checkbox value={""+CategorySections.SOCIAL}>Social Medias</Checkbox>
                <Checkbox value={""+CategorySections.LANGUAGE}>Languages</Checkbox>
              </HStack>
            </CheckboxGroup>
            <CategoryAutocomplete
              categories={categoryAutocompleteSections.map(i => parseInt(i, 10) as CategorySections)}
              exclude={(nickname && nickname.nickname)
                  ? nickname.nickname.pages
                    .map(page => page.name)
                    .filter(isString)
                  : []
              }
              selectedItems={[]}
              onSelectedItemsChange={items => (items && items.length > 0) ? setNewPresetCategory(items[0]) : setNewPresetCategory(undefined)}
            />
            <Text>
              Are we missing a category that you and others would find useful?
              Please&nbsp;
              <Link
                path="https://pnouns.freshdesk.com/support/tickets/new"
                external
                newTab
                color={activeColor}
              >
                let us know
              </Link>!
            </Text>
            <Button
              variant="link"
              onClick={() => setNewCategoryUseCustom(true)}
            >
              Use a custom category instead
            </Button>
          </>
        )}
        { !newCategoryUseCustom && newPresetCategory && (
          <>
            <HStack>
              <Text>Selected Category:</Text>
              <Text>{newPresetCategory.label}</Text>
            </HStack>
            <HStack>
              <Button onClick={() => setNewPresetCategory(undefined)}>Change</Button>
              <Button onClick={() => setNewCategoryUseCustom(true)}>
                Use a custom category instead
              </Button>
            </HStack>
          </>
        )}
        { !!newCategoryUseCustom && (
          <>
            <FormControl id="nickname-screen-new-category-custom">
              <FormLabel>Custom Category Name</FormLabel>
              <InputGroup>
                <Input
                  name="nickname-page-new-custom-category-name"
                  value={newCustomCategory}
                  onChange={e => handleCustomCategoryChange(e.target.value)}
                />
                { hasEnteredCustomCategory && (
                  <InputRightElement children={<ValidationInputIndication validations={validation} />} />
                )}
              </InputGroup>
            </FormControl>
            <Button variant="link" onClick={() => setNewCategoryUseCustom(false)}>
              Use a default category
            </Button>
          </>
        )}
        <Heading as="h4" size="sm" textAlign="center">
          Default URL Format
        </Heading>
        <Text>
          You will be able to use any of the URLs listed below to access your page interchangeably.
          Please select the one you prefer most, and we'll use that when linking to your category.
        </Text>
        <RadioGroup value={newCategoryDisplay} onChange={value => setNewCategoryDisplay(parseInt(value, 10))}>
          <Stack>
            <Radio isInvalid={newCategoryUseCustom} disabled={newCategoryUseCustom} value={CategoryDefaultDisplay.SUBDOMAIN}>
              <UrlTextPreview
                url={{ nickname: nicknameId, category: categoryText }}
                strikeThrough={newCategoryUseCustom}
              />
              { newCategoryUseCustom && (
                <Text>You can only use this format with a default category.</Text>
              )}
            </Radio>
            <Radio value={CategoryDefaultDisplay.PATH}>
              <UrlTextPreview
                url={{
                  nickname: nicknameId,
                  category: categoryText,
                  categoryAsPath: true,
                }}
              />
            </Radio>
            <Radio value={CategoryDefaultDisplay.PATH_WITH_JOIN}>
              <UrlTextPreview
                url={{
                  nickname: nicknameId,
                  category: categoryText,
                  categoryAsPath: true,
                  categoryJoiningWord,
                  alwaysCategoryJoiningWord: true,
                }}
              />
            </Radio>
          </Stack>
        </RadioGroup>
        { newCategoryDisplay === CategoryDefaultDisplay.PATH_WITH_JOIN && (
          <FormControl>
            <FormLabel>Default Joining Word</FormLabel>
            <JoinerSelect value={categoryJoiningWord} onChange={setCategoryJoiningWord} />
          </FormControl>
        )}
      </Container>
      <WizardFooter
        urls={[
          {
            nickname: nicknameId,
            category: categoryText ?? "",
            categoryAsPath: newCategoryUseCustom || newCategoryDisplay !== CategoryDefaultDisplay.SUBDOMAIN,
            categoryJoiningWord: newCategoryDisplay === CategoryDefaultDisplay.PATH_WITH_JOIN
              ? (categoryJoiningWord ?? JoiningWord.AT)
              : undefined,
          },
          {
            nickname: nicknameId,
            category: categoryText ?? "",
            categoryAsPath: newCategoryUseCustom || newCategoryDisplay !== CategoryDefaultDisplay.SUBDOMAIN,
            categoryJoiningWord: newCategoryDisplay === CategoryDefaultDisplay.PATH_WITH_JOIN
              ? (categoryJoiningWord ?? JoiningWord.AT)
              : undefined,
            page: "",
          },
          {
            nickname: nicknameId,
            category: categoryText ?? "",
            categoryAsPath: newCategoryUseCustom || newCategoryDisplay !== CategoryDefaultDisplay.SUBDOMAIN,
            categoryJoiningWord: newCategoryDisplay === CategoryDefaultDisplay.PATH_WITH_JOIN
              ? (categoryJoiningWord ?? JoiningWord.AT)
              : undefined,
            page: "",
            pageJoiningWord: JoiningWord.WITH,
          },
        ]}
        label="Create Category"
        disabled={blockCreatingCategory}
        onNext={handleCreateCategory}
      />
      { errorModal }
    </>
  );
};
