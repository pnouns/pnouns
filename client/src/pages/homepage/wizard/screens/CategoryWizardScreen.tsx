import React, { useState } from "react";
import {
  Container,
  Stack,
  Text,
} from "@chakra-ui/layout";
import { JoiningWord, useCategoryWizardQuery, useCreateCategoryPageMutation } from "../../../../generated-types";
import { FormControl, FormLabel, Heading, Input, InputGroup, Radio, RadioGroup, Spinner } from "@chakra-ui/react";
import { WizardList, WizardListItem } from "../WizardList";
import { formatUrl, UrlTextPreview } from "../../../../components/UrlTextPreview";
import { ButtonLink } from "../../../../components/Link";
import { JoinerSelect } from "../../../../components/select/JoinerSelect";
import { WizardFooter } from "../WizardFooter";
import { useValidations } from "../../../../components/validation/useValidations";
import posthog from "posthog-js";
import { useErrorModal } from "../../../../components/error-modal";
import { useHistory } from "react-router-dom";

export enum SubpageDefaultDisplay {
  PATH = 2,
  PATH_WITH_JOIN,
}

const CategoryWizardScreen: React.FC<{
  nicknameId: string,
  categoryId: string;
}> = ({
  nicknameId,
  categoryId,
}) => {
  const { errorModal, report } = useErrorModal();
  const history = useHistory();
  const [subpageId, setSubpageId] = useState("");
  const [subpageJoiningWord, setSubpageJoiningWord] = useState<JoiningWord | undefined>(undefined);
  const [newSubpageDisplay, setNewSubpageDisplay] = useState(SubpageDefaultDisplay.PATH);
  const [{ data, fetching, error }, refetchCategory] = useCategoryWizardQuery({
    variables: {
      id: categoryId,
    },
  });

  const validation = useValidations({
    longEnough: subpageId.length >= 3,
    characterSet: !!subpageId.match(/^[a-zA-Z][a-zA-Z0-9_-]{0,29}[a-zA-Z0-9]$/),
  });

  const blockCreatingSubpage = !validation.valid;

  const [isCreatingSubpage, setIsCreatingSubpage] = useState(false);
  const [, createCategoryPage] = useCreateCategoryPageMutation();
  const handleCreateSubpage = async () => {
    if(isCreatingSubpage || blockCreatingSubpage) {
      return;
    }
    setIsCreatingSubpage(true);
    posthog.capture("try create category page", { nicknameId, categoryId, subpageId });
    const result = await createCategoryPage({
      input: {
        nicknameId,
        categoryId,
        name: subpageId,
        defaultJoiningWord: newSubpageDisplay === SubpageDefaultDisplay.PATH_WITH_JOIN
          ? subpageJoiningWord
          : undefined,
        listed: false,
      },
    });
    setIsCreatingSubpage(false);
    if(result.error) {
      report(result.operation, result.error, handleCreateSubpage);
    }
    if(result.data && result.data.createCategoryPage) {
      posthog.capture("created category page", { nicknameId, categoryId, subpageId });
      history.push(`/page/${result.data.createCategoryPage.id}/`);
    }
    // TODO:
  };

  if(!data || fetching || !data.getPage) {
    return <Spinner />;
  }

  const category = data.getPage;

  const link = formatUrl({
    nickname: nicknameId,
    category: category.name ?? undefined,
    categoryAsPath: !category.defaultSubdomain,
    categoryJoiningWord: category.defaultJoiningWord ?? undefined,
  });

  return (
    <>
      <Container maxW="container.md">
        <Heading as="h3" size="md" textAlign="center" my="1">
          Category Page
        </Heading>
        <Text>
          All categories have a webpage automatically created - it can contain your
          name, pronouns, and afirming language, as well as linking to any subpages that you'd like.
        </Text>
        <WizardList>
          <WizardListItem
            displayName={category.displayName ?? category.name ?? category.id}
            id={category.id}
            label="Edit"
            link={`/page/${category.id}`}
            info={link}
            infoLink={category.isPublished ? link : undefined}
            extraButtons={(
              <>
                { category.isPublished && (
                  <ButtonLink path={link} external>View</ButtonLink>
                )}
              </>
            )}
          />
        </WizardList>
        <Heading as="h3" size="md" textAlign="center" my="1">
          Sub-Pages
        </Heading>
        <Text textAlign="center">
          You can create sub-pages under this category, useful if you want to organize
          different translations of your pages, have different situations (e.g. Category: School, Sub-Page: With Friends),
          or plenty of other situations where another layer of pages could be useful.
        </Text>
        <WizardList>
          {
            category && category.subPageOptions && category.subPageOptions.map(page => {
              return (
                <WizardListItem
                  key={page.id}
                  displayName={page.displayName ?? page.name ?? page.id}
                  id={page.id}
                  label="Edit"
                  link={`/page/${page.id}`}
                />
              )
            })
          }
        </WizardList>
        <Heading as="h3" size="md" textAlign="center" my="1">
          Add a Sub-Page
        </Heading>
        <Text textAlign="center">
          You can create as many sub-pages as you'd like.
        </Text>
        <FormControl id="category-wizard-subpage-id" isRequired={true}>
          <FormLabel>Subpage Name</FormLabel>
          <InputGroup>
            <Input
              name="cateogry-wizard-subpage-id-value"
              value={subpageId}
              onChange={e => setSubpageId(e.target.value)}
            />
          </InputGroup>
        </FormControl>
        <FormControl id="category-wizard-subpage-format">
          <FormLabel>Default URL Format</FormLabel>
          <RadioGroup value={newSubpageDisplay} onChange={value => setNewSubpageDisplay(parseInt(value, 10))}>
            <Stack>
              <Radio
                value={SubpageDefaultDisplay.PATH}
              >
                <UrlTextPreview
                  url={{
                    nickname: nicknameId,
                    category: category.name ?? undefined,
                    categoryAsPath: !category.defaultSubdomain,
                    categoryJoiningWord: category.defaultJoiningWord ?? undefined,
                    page: subpageId,
                  }}
                />
              </Radio>
              <Radio
                value={SubpageDefaultDisplay.PATH_WITH_JOIN}
              >
                <UrlTextPreview
                  url={{
                    nickname: nicknameId,
                    category: category.name ?? undefined,
                    categoryAsPath: !category.defaultSubdomain,
                    categoryJoiningWord: category.defaultJoiningWord ?? undefined,
                    page: subpageId,
                    pageJoiningWord: subpageJoiningWord,
                    alwaysPageJoiningWord: true,
                  }}
                />
              </Radio>
            </Stack>
          </RadioGroup>
        </FormControl>
        {
          newSubpageDisplay === SubpageDefaultDisplay.PATH_WITH_JOIN && (
            <FormControl>
              <FormLabel>Default Joining Word</FormLabel>
              <JoinerSelect value={subpageJoiningWord} onChange={setSubpageJoiningWord} />
            </FormControl>
          )
        }
      </Container>
      <WizardFooter
        urls={[
          {
            nickname: nicknameId,
            category: category.name ?? undefined,
            categoryAsPath: !category.defaultSubdomain,
            categoryJoiningWord: category.defaultJoiningWord ?? undefined,
            page: subpageId,
          },
          {
            nickname: nicknameId,
            category: category.name ?? undefined,
            categoryAsPath: !category.defaultSubdomain,
            categoryJoiningWord: category.defaultJoiningWord ?? undefined,
            page: subpageId,
            pageJoiningWord: subpageJoiningWord,
            alwaysPageJoiningWord: true,
          }
        ]}
        label="Create Subpage"
        disabled={blockCreatingSubpage}
        onNext={handleCreateSubpage}
      />
      { errorModal }
    </>
  );
};

export default CategoryWizardScreen;
