import React, { useMemo, useRef, useState } from "react";
import loadable from "@loadable/component";
import { useDebouncedCallback } from "use-debounce";
import {
  Box,
  Center,
  Container,
  FormControl,
  FormLabel,
  Grid,
  GridItem,
  Heading,
  Input,
  InputGroup,
  InputRightElement,
  List,
  Spinner,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { useUserContext } from "../../../../context/user.context";
import { WizardList, WizardListItem } from "../WizardList";
import { useValidations } from "../../../../components/validation/useValidations";
import { ValidationInputIndication } from "../../../../components/validation/ValidationInputIndication";
import { ValidationItem } from "../../../../components/validation/ValidationItem";
import { WizardFooter } from "../WizardFooter";
import { JoiningWord, useCreateNicknameMutation } from "../../../../generated-types";
import { Link } from "../../../../components/Link";
import posthog from "posthog-js";
import { useErrorModal } from "../../../../components/error-modal";

const EnglishWordList = loadable.lib(() => import("an-array-of-english-words"));

const NicknameItem: React.FC<{
  nickname: string;
  onSelect: (nickname: string) => void;
}> = ({
  nickname,
  onSelect,
}) => {
  return (
    <WizardListItem displayName={nickname} id={nickname} onSelect={() => onSelect(nickname)} />
  );
}

export const WizardNicknameListScreen: React.FC<{
  onNicknameSelected: (nickname: string) => void;
  onCreatedNickname: (nickname: string, jwt: string) => void;
}> = ({
  onNicknameSelected,
  onCreatedNickname,
}) => {
  const activeColor = useColorModeValue("blue.900", "blue.300");
  const { report, errorModal } = useErrorModal();
  const [hasEnteredText, setHasEnteredText] = useState(false);
  const [newNickname, setNewNickname] = useState("");

  const wordList = useRef<Record<string,string>>();

  const validation = useValidations({
    longEnough: newNickname.length > 2,
    startsLetter: newNickname.length < 1 || !!newNickname[0].match(/[a-zA-Z]/),
    endsLetterNum: newNickname.length < 2 || !!newNickname[newNickname.length - 1].match(/[a-zA-Z0-9]/),
    characterSet: !!newNickname.match(/^[a-zA-Z0-9_-]*$/),
    notWord: {
      debounce: 200,
      test: nickname => {
        if(!wordList.current) {
          return false;
        }
        return Object.values(wordList.current).find((i: string) => i === nickname) === undefined;
      },
      maxWait: 1000,
    },
  });

  const handleNicknameChange = (value: string) => {
    if(!hasEnteredText) {
      setHasEnteredText(true);
    }
    setNewNickname(value);
    validation.passing.notWord.load(value);
  };

  const [, createNickname] = useCreateNicknameMutation();
  const [creatingNickname, setCreatingNickname] = useState(false);
  const handleNicknameCreate = async () => {
    const nickname = newNickname;
    if(!validation.valid || creatingNickname) {
      return;
    }
    setCreatingNickname(true);
    posthog.capture("try create nickname", {
      nickname,
    });
    const result = await createNickname({
      input: {
        nickname,
      },
    });
    if(result.error) {
      report(result.operation, result.error, handleNicknameCreate);
    }
    if(result.data?.createNickname.jwt) {
      posthog.capture("created nickname", {
        nickname,
      });
      onCreatedNickname(nickname, result.data?.createNickname.jwt);
    }
    setCreatingNickname(false);
  };

  const { namespaces } = useUserContext();
  return (
    <>
      <Container maxW="container.md">
        <Text fontSize="md" textAlign="center">
          Your account can have multiple nicknames.<br />
          Each nickname is fully independent from each other - this means you could have a private identity
          that can't be as easily connected back to you or other pages you might have.
        </Text>
        <Text my="2" textAlign="center">
          Accounts are limited to two nicknames by default.<br />
          Please&nbsp;
          <Link
            path="https://pnouns.freshdesk.com/support/tickets/new"
            external
            newTab
            color={activeColor}
          >
            contact us to increase your limit
          </Link>&nbsp;
          if needed.
        </Text>
        <Heading as="h3" size="md" textAlign="center" my="1">
          Existing Nicknames
        </Heading>
        <WizardList>
          { (namespaces ?? []).map(nickname => <NicknameItem key={nickname} nickname={nickname} onSelect={onNicknameSelected} />) }
          { (!namespaces || !Array.isArray(namespaces)) && (
            <Center>
              <Text>
                You don't have any nicknames.<br />
                Use the form below to create a nickname.
              </Text>
            </Center>
          )}
        </WizardList>
        <Heading as="h3" size="md" textAlign="center" my="1">
          Create a Nickname
        </Heading>
        <Text>
          We recommend you use the pluralized form of your name, e.g. "jays" if your nickname is "jay".
          This will make the URLs generated sound more natural.
        </Text>
        <Grid templateColumns="repeat(6, 1fr)" gap="2">
          <GridItem className="create-nickname-input" colSpan={{ base: 6, xl: 2 }}>
            <FormControl id="create-nickname-nickname">
              <FormLabel>Nickname:</FormLabel>
              <InputGroup>
                <Input
                  name="nickname-list-new-nickname"
                  value={newNickname}
                  onChange={e => handleNicknameChange(e.target.value)}
                />
                { hasEnteredText && (
                  <InputRightElement children={
                    <ValidationInputIndication validations={validation} />
                  } />
                )}
              </InputGroup>
            </FormControl>
          </GridItem>
          <GridItem className="create-nickname-validation" colSpan={{ base: 6, md: 3, xl: 2 }}>
            <List>
              <ValidationItem validation={validation.passing.longEnough}>At least 3 characters</ValidationItem>
              <ValidationItem validation={validation.passing.notWord}>Not an English word</ValidationItem>
              <ValidationItem validation={validation.passing.startsLetter}>
                Starts wtih a letter
              </ValidationItem>
            </List>
          </GridItem>
          <GridItem className="create-nickname-validation" colSpan={{ base: 6, md: 3, xl: 2 }}>
            <List>
              <ValidationItem validation={validation.passing.endsLetterNum}>
                Ends with a letter or number
              </ValidationItem>
              <ValidationItem validation={validation.passing.characterSet}>
                Only contains letters, numbers, dashes, and underscores
              </ValidationItem>
            </List>
          </GridItem>
        </Grid>
      </Container>
      <EnglishWordList ref={(list: unknown) => {
        wordList.current = list as Record<string,string>;
      }} />
      <WizardFooter
        urls={[
          { nickname: newNickname },
          { nickname: newNickname, category: "" },
          { nickname: newNickname, category: "", categoryAsPath: true, categoryJoiningWord: JoiningWord.AT },
        ]}
        label="Create Nickname"
        disabled={!validation.valid}
        onNext={handleNicknameCreate}
      />
      { errorModal }
    </>
  )
};
