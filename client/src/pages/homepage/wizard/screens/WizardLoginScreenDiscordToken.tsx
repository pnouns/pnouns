import { Container, Heading, Text } from "@chakra-ui/layout";
import React, { useEffect, useState } from "react";
import { useErrorModal } from "../../../../components/error-modal";
import { useUserContext } from "../../../../context/user.context";
import { useSubmitDiscordTokenMutation } from "../../../../generated-types";

export const WizardLoginScreenDiscordToken: React.FC<{
  token: string,
}> = ({ token }) => {
  const { updateJwt } = useUserContext();
  const { errorModal, report } = useErrorModal();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [, submitDiscordToken] = useSubmitDiscordTokenMutation();

  useEffect(() => {
    async function runSubmssion() {
      if(!token || token.length < 5 || isSubmitting) {
        return;
      }
      setIsSubmitting(true);
      const res = await submitDiscordToken({
        discordToken: token,
      });
      if(res.error) {
        report(res.operation, res.error, runSubmssion);
      }
      if(res.data?.submitDiscordToken) {
        updateJwt(res.data.submitDiscordToken);
      }
      setIsSubmitting(false);
    }
    runSubmssion();
  }, [ token ])

  return (
    <>
      <Container maxW="container.md">
        <Heading as="h3" size="lg" textAlign="center" py="1">
          Discord Login
        </Heading>
        <Text fontSize="lg" textAlign="center">
          Logging in via Discord magic link...
        </Text>
      </Container>
      { errorModal }
    </>
  )
}
