import { ChevronRightIcon } from "@chakra-ui/icons";
import { Box, Button, Flex, Heading, Spacer } from "@chakra-ui/react";
import React from "react";
import { UrlPreviewData, UrlTextPreview } from "../../../components/UrlTextPreview";

export const WizardFooter: React.FC<{
  urls?: UrlPreviewData[],
  label?: string,
  disabled?: boolean,
  onNext: () => void;
}> = ({
  urls,
  label,
  disabled,
  onNext,
}) => (
  <Flex w="100%" px="2" justify="space-between">
    <Box data-wizard-footer="urlPreviews">
      { urls && (
        <>
          <Heading size="sm">
            You'll be able to create pages like:
          </Heading>
          {urls.map((url, i) => <UrlTextPreview key={i} url={url} />)}
        </>
      )}
    </Box>
    <Spacer />
    <Button rightIcon={<ChevronRightIcon />} disabled={disabled} onClick={onNext}>
      { label ?? "Next" }
    </Button>
  </Flex>
);
