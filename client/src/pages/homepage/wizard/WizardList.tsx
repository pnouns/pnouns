import React, { ReactElement } from "react";
import {
  Box,
  Button,
  ButtonGroup,
  Center,
  Flex,
  Grid,
  Heading,
  SimpleGrid,
  Skeleton,
  SkeletonText,
  Spacer,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { ChevronRightIcon } from "@chakra-ui/icons";
import { ButtonLink, Link } from "../../../components/Link";

export const WizardListItemPlaceholder = Symbol("WizardListItemPlaceholder");

/**
 * A general utility for defining a list of options that will lead to the next screen of the wizard.
 * This is _not_ intended for the final options at the top of the screen that would take the user to future steps.
 */
export const WizardList: React.FC<{
}> = ({ children }) => {
  return (
    <Box my="2">
      {children}
    </Box>
  );
};

export const WizardListItem: React.FC<{
  displayName?: string | typeof WizardListItemPlaceholder;
  id?: string | typeof WizardListItemPlaceholder;
  info?: string | typeof WizardListItemPlaceholder;
  infoLink?: string;
  link?: string;
  label?: string;
  extraButtons?: ReactElement;
  onSelect?: () => void;
}> = ({
  displayName,
  id,
  info,
  infoLink,
  link,
  label,
  extraButtons,
  onSelect,
}): JSX.Element => {
  const activeColor = useColorModeValue("blue.900", "blue.300");
  const inactiveColor = useColorModeValue("gray.500", "gray.300");
  const actionText = label ?? `Use ${(displayName === WizardListItemPlaceholder ? null : displayName) ?? (id === WizardListItemPlaceholder ? null : id) ?? 'Select'}`;
  return (
    <SimpleGrid columns={{ sm: 2 }} spacing="20px" marginY="3">
      <Box className="identifier">
        { displayName !== undefined &&
          <SkeletonText isLoaded={displayName !== WizardListItemPlaceholder} noOfLines={1} fontSize="md" skeletonHeight="24px">
            <Heading fontSize="md">{ displayName }</Heading>
          </SkeletonText>
        }
        { id !== undefined &&
          <SkeletonText isLoaded={id !== WizardListItemPlaceholder} noOfLines={1} fontSize="md" skeletonHeight="24px">
            <Text fontSize="md">{ id }</Text>
          </SkeletonText>
        }
        { info !== undefined &&
          <SkeletonText isLoaded={info !== WizardListItemPlaceholder} noOfLines={1} fontSize="sm" skeletonHeight="20px">
            <Text fontSize="sm" color={inactiveColor}>
              {infoLink ? (<Link path={infoLink} external color={activeColor}>{info}</Link>) : info}
            </Text>
          </SkeletonText>
        }
      </Box>
      <Center className="actions">
        <Flex w="100%" justify="space-between">
          <Flex />
          <Spacer />
          <Flex justify="flex-end">
            <ButtonGroup>
              { extraButtons }
              { link && (
                <ButtonLink rightIcon={<ChevronRightIcon />} path={link}>
                  {actionText}
                </ButtonLink>
              ) }
              { !link && (
                <Button rightIcon={<ChevronRightIcon />} onClick={onSelect}>
                  {actionText}
                </Button>
              )}
            </ButtonGroup>
          </Flex>
        </Flex>
      </Center>
    </SimpleGrid>
  );
};
