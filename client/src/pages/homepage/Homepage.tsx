import React from "react";
import {
  Container,
  Heading,
  SimpleGrid,
  Text,
} from "@chakra-ui/react";
import { Header } from "../../components/Header";
import { SectionDisplay } from "../../components/SectionDisplay";
import {
  PublishedSection,
  Strength,
} from "../../generated-types";
import { Link } from "../../components/Link";
import { HomepageWizard } from "./wizard/HomepageWizard";
import { VERSION } from "../../version";
import { Footer } from "../../components/Footer";

const nameExample: PublishedSection = {
  id: "example-name",
  name: "Name",
  values: [
    {
      id: "example-name-1",
      text: "John",
      strength: Strength.BEST,
    },
    {
      id: "example-name-2",
      text: "Jane",
      strength: Strength.TESTING,
    },
  ],
  valueOrder: ["example-name-1", "example-name-2"],
};

const pronounExample: PublishedSection = {
  id: "example-pronouns",
  name: "Pronouns",
  values: [
    {
      id: "example-pn-1",
      text: "they/them",
      strength: Strength.BEST,
    },
    {
      id: "example-pn-2",
      text: "he/him",
      strength: Strength.JOKINGLY,
    },
    {
      id: "example-pn-3",
      text: "she/her",
      strength: Strength.NEVER,
    },
  ],
  valueOrder: ["example-pn-1", "example-pn-2", "example-pn-3"],
};

const textExample: PublishedSection = {
  id: "example-text",
  name: "Honorific",
  lead: "For use in place of sir/ma'am",
  values: [
    {
      id: "example-text-1",
      text: "None",
      strength: Strength.BEST,
    },
    {
      id: "example-text-2",
      text: "Capt.",
      strength: Strength.JOKINGLY,
    },
    /*{
      id: "example-text-3",
      text: "Yes please/no thank you",
      strength: Strength.OK,
      info: "e.g. using 'Yes please' instead of 'Yes sir'/'Yes ma'am'",
    },*/
    {
      id: "example-text-4",
      text: "Sir",
      strength: Strength.NEVER,
    },
    {
      id: "example-text-5",
      text: "Ma'am",
      strength: Strength.NEVER,
    },
  ],
  valueOrder: [1,2,3,4,5].map(i => `example-text-${i}`),
};

const Homepage: React.FC<{}> = () => {

  return (
    <>
      <Header releaseNotes noAuth centerTitle />
      <Container maxW="container.xl">
        <Heading as="h2" size="2xl" textAlign="center" paddingTop="2" paddingBottom="1">
          My Pronouns, FYI
        </Heading>
        <Text fontSize="xl" textAlign="center">
          Announce your name, pronouns, and affirming language.
        </Text>
        <Heading as="h3" size="xl" textAlign="center" paddingTop="2" paddingBottom="1">
          Example
        </Heading>
        <SimpleGrid columns={{ md: 3 }} spacing="20px">
          <SectionDisplay section={nameExample} />
          <SectionDisplay section={pronounExample} />
          <SectionDisplay section={textExample} />
        </SimpleGrid>
        <Heading as="h3" size="xl" textAlign="center" paddingTop="2" paddingBottom="1">
          Create your own page
        </Heading>
        <Text fontSize="xl" textAlign="center">
          You will be able to create URLs like <Link path="https://alexs.work.pnouns.fyi/with/customers/" external>alexs.work.pnouns.fyi/with/customers/</Link>.
        </Text>
        <HomepageWizard />
        <Text fontSize="small" textAlign="center">
          Running version {VERSION}.
        </Text>
        <Footer />
      </Container>
    </>
  );
};

export default Homepage;
