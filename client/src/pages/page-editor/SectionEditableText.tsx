import React from "react";
import { useEditorContext } from "../../context/editor.context";
import {
  SectionTextField,
  usePageEditor_EditSectionTextFieldMutation,
} from "../../generated-types";
import { EditableText } from "../../components/EditableText";

export const SectionEditableText: React.FC<{
  sectionId: string;
  field: SectionTextField;
  placeholder?: string;
  value?: string,
  disabled?: boolean;
  onSave?: () => void | Promise<void>;
}> = ({
  sectionId,
  field,
  placeholder,
  value,
  disabled,
  onSave,
}) => {
  const saveId = `${sectionId}-${field}`;
  const {
    isSaving,
    startSaving,
    report,
  } = useEditorContext();
  const [, save] = usePageEditor_EditSectionTextFieldMutation();

  const handleSave = async (value: string) => {
    if(isSaving) {
      return;
    }
    const done = startSaving(saveId);
    const result = await save({
      input: {
        sectionId,
        field,
        value,
      },
    });
    done();
    if(result.error) {
      report(result.operation, result.error, () => handleSave(value));
    }
    if(result.data?.editSectionTextField && typeof onSave === "function") {
      await Promise.resolve(onSave());
    }
  }

  return (
    <EditableText
      value={value}
      placeholder={placeholder}
      disabled={disabled || isSaving}
      onSave={handleSave}
    />
  );
}
