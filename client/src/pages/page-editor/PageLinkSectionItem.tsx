import React, { useState } from "react";
import { Box, Center, Flex, Grid, GridItem, Spacer, Text } from "@chakra-ui/layout";
import {
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  MenuItemOption,
  MenuOptionGroup,
} from "@chakra-ui/menu";
import { Tooltip } from "@chakra-ui/tooltip";
import { Button } from "@chakra-ui/button";
import { ChevronRightIcon, DeleteIcon, LockIcon, PlusSquareIcon, RepeatIcon, SettingsIcon } from "@chakra-ui/icons";
import {
  ListedSubPage,
  Page, PageEditor_PageQuery,
  UpdateLinkedPageInput,
  usePageEditor_DeleteLinkedPageMutation,
  usePageEditor_UpdateLinkedPageMutation,
} from "../../generated-types";
import { EditableText } from "../../components/EditableText";
import { formatUrl, UrlPreviewData } from "../../components/UrlTextPreview";
import { ButtonLink } from "../../components/Link";
import { useEditorContext } from "../../context/editor.context";

export const PageLinkSectionItem: React.FC<{
  page: PageEditor_PageQuery,
  item: Pick<ListedSubPage, "id" | "customName" | "showSubtitle" | "customSubtitle" | "caption" | "includePassword">,
  linked?: Pick<Page, "id" | "name" | "displayName" | "pageBioTitle" | "pageBioSubtitle" | "defaultSubdomain" | "defaultJoiningWord" | "password" | "defaultPasswordFormat">,
  refetchPage?: () => Promise<void>;
}> = ({
  page,
  item,
  linked,
  refetchPage,
}) => {
  const { report } = useEditorContext();
  const upstreamName = linked?.pageBioTitle ?? linked?.displayName ?? linked?.name ?? undefined;
  const [isSaving, setIsSaving] = useState(false);

  const [, updateLinkedPage] = usePageEditor_UpdateLinkedPageMutation();
  const [, deleteLinkedPage] = usePageEditor_DeleteLinkedPageMutation();

  const update = async (input: Omit<UpdateLinkedPageInput, "linkedSubPageId">): Promise<void> => {
    if(isSaving) {
      return;
    }
    setIsSaving(true);
    const res = await updateLinkedPage({
      input: {
        ...input,
        linkedSubPageId: item.id,
      },
    });
    if(res.error) {
      report(res.operation, res.error, () => update(input));
    }
    if(res.data && res.data.updateLinkedPage) {
      refetchPage && await refetchPage();
    }
    setIsSaving(false);
  };

  const handleRemove = async (): Promise<void> => {
    if(isSaving) {
      return;
    }
    setIsSaving(true);
    const res = await deleteLinkedPage({
      id: item.id,
    });
    if(res.error) {
      report(res.operation, res.error, handleRemove);
    }
    if(res.data && res.data.deleteListedSubPage) {
      refetchPage && await refetchPage();
    }
    setIsSaving(false);
  }

  const linkedIsCategorySubPage = !page.getPage.parent && page.getPage.name;

  const category = (linkedIsCategorySubPage || !linked) ? page.getPage : linked;

  const baseLink: UrlPreviewData = {
    nickname: page.getPage.nickname.id,
    category: category.name ?? undefined,
    categoryAsPath: !category.defaultSubdomain,
    categoryJoiningWord: category.defaultJoiningWord ?? undefined,
    page: linkedIsCategorySubPage ? linked?.name ?? undefined : undefined,
    pageJoiningWord: linkedIsCategorySubPage ? linked?.defaultJoiningWord ?? undefined : undefined,
    password: item.includePassword && linked && linked.password ? linked.password : undefined,
    defaultPasswordFormat: linked?.defaultPasswordFormat ?? undefined,
  };


  const linkedUrl = formatUrl({
    ...baseLink,
    hideProtocol: true,
  });
  const linkTarget = formatUrl(baseLink);
  return (
    <Box>
      <Flex mb="1">
        <Center>
          <Text fontWeight="extrabold" textColor="purple.500" textTransform="capitalize" fontSize="sm">
            Sub-Page
          </Text>
        </Center>
        <Spacer />
        <Center>
          <Text textColor="purple.500" fontSize="sm">{linkedUrl}</Text>
        </Center>
        <Menu>
          <MenuButton
            as={Button}
            size="sm"
            leftIcon={<SettingsIcon />}
          >
            Settings
          </MenuButton>
          <MenuList textColor="white">
            <MenuItem
              icon={<RepeatIcon />}
              disabled={isSaving}
              onClick={() => refetchPage && refetchPage()}
            >
              Refresh
            </MenuItem>
            <MenuOptionGroup
              type="checkbox"
              value={item.includePassword ? ["includePassword"] : []}
              onChange={val => update({
                includePassword: val.includes("includePassword"),
              })}
            >
              <MenuItemOption
                value="includePassword"
                isDisabled={isSaving}
                closeOnSelect={false}
                type="checkbox"
              >
                Include Password
              </MenuItemOption>
            </MenuOptionGroup>
            <MenuOptionGroup
              type="checkbox"
              value={item.showSubtitle ? ["showSubtitle"] : []}
              onChange={val => update({
                showSubtitle: val.includes("showSubtitle"),
              })}
            >
              <MenuItemOption
                value="showSubtitle"
                isDisabled={isSaving}
                closeOnSelect={false}
                type="checkbox"
              >
                Include Subtitle
              </MenuItemOption>
            </MenuOptionGroup>
            <MenuItem icon={<DeleteIcon />} onClick={handleRemove}>Remove Sub-Page</MenuItem>
          </MenuList>
        </Menu>
      </Flex>
      <Flex>
        <Grid flex="10" templateColumns="repeat(12, 1fr)" gap={2}>
          <GridItem colSpan={{
            base: 12,
            sm: item.showSubtitle ? 12 : 8,
            md: item.showSubtitle ? 6 : 9,
            lg: item.showSubtitle ? 6 : 10,
            xl: item.showSubtitle ? 6 : 11,
          }}>
            <EditableText
              placeholder="[Page Name]"
              value={item.customName ?? undefined}
              inheritedValue={{
                inheritValue: item.customName === null,
                upstreamValue: upstreamName,
                onOverride: () => update({ hasCustomName: true }),
                onUseUpstream: () => update({ hasCustomName: false }),
              }}
              onSave={customName => update({
                hasCustomName: true,
                customName,
              })}
              disabled={isSaving}
            />
          </GridItem>
          <GridItem colSpan={{
            base: 12,
            sm: item.showSubtitle ? 12 : 4,
            md: item.showSubtitle ? 6 : 3,
            lg: item.showSubtitle ? 6 : 2,
            xl: item.showSubtitle ? 6 : 1,
          }}>
            {
              item.showSubtitle
                ? (
                  <EditableText
                    placeholder="[Subtitle]"
                    value={item.customSubtitle ?? undefined}
                    inheritedValue={{
                      inheritValue: item.customSubtitle === null,
                      upstreamValue: linked?.pageBioSubtitle ?? undefined,
                      onOverride: () => update({ hasCustomSubtitle: true }),
                      onUseUpstream: () => update({ hasCustomSubtitle: false }),
                    }}
                    onSave={customSubtitle => update({
                      hasCustomSubtitle: true,
                      customSubtitle,
                    })}
                    disabled={isSaving}
                  />
                )
              : (
                <Button
                  colorScheme="blue"
                  variant="solid"
                  leftIcon={<PlusSquareIcon />}
                  disabled={isSaving}
                  onClick={() => update({ showSubtitle: true })}
                >
                  Subtitle
                </Button>
              )
            }
          </GridItem>
          <GridItem colSpan={12}>
            <EditableText
              placeholder="[caption]"
              value={item.caption ?? undefined}
              onSave={caption => update({
                caption: caption.length > 0 ? caption : undefined,
                hasCaption: caption.length === 0 ? false : undefined,
              })}
              disabled={isSaving}
            />
          </GridItem>
        </Grid>
        <Center>
          {
            (linked?.password && !item.includePassword)
              ? (
                <Tooltip
                  label="You will need to enter a password to view this page."
                >
                  <Button
                    leftIcon={<LockIcon />}
                  >
                    Open
                  </Button>
                </Tooltip>
              ) : (
                <ButtonLink
                  path={linkTarget}
                  rightIcon={<ChevronRightIcon />}
                  newTab
                  external
                >
                  Open
                </ButtonLink>
              )
          }
        </Center>
      </Flex>
    </Box>
  );
};
