import React, { useState } from "react";
import { Button, Center, Heading, Text } from "@chakra-ui/react";
import { SectionContainer } from "../../components/SectionDisplay";
import { PlusSquareIcon } from "@chakra-ui/icons";
import {
  StockInspiration,
  usePageEditor_CreateNameSectionMutation,
  usePageEditor_CreatePronounSectionMutation,
  usePageEditor_CreateSectionMutation,
} from "../../generated-types";
import { StockInspirationList, StockInspirationListItem } from "../../components/StockInspirationList";
import posthog from "posthog-js";
import { useEditorContext } from "../../context/editor.context";

export const CreateSection: React.FC<{
  pageId: string;
  title: string,
  names?: boolean;
  pronouns?: boolean;
  onCreate: (sectionId: string) => void;
}> = ({ pageId, title, names, pronouns, onCreate }) => {
  const { report } = useEditorContext();
  const [expandInspiration, setExpandInspiration] = useState(false);
  const [creating, setCreating] = useState(false);

  const [, createNameSection] = usePageEditor_CreateNameSectionMutation();
  const [, createPronounsSection] = usePageEditor_CreatePronounSectionMutation();
  const [, createSection] = usePageEditor_CreateSectionMutation();

  const handleCreate = async (): Promise<void> => {
    if(creating) {
      return;
    }
    if(names) {
      setCreating(true);
      const created = await createNameSection({
        input: {
          pageId,
          title,
        },
      });
      setCreating(false);
      if(created.error) {
        report(created.operation, created.error, handleCreate);
      }
      if(created.data?.addNameSection) {
        posthog.capture("create section", {
          specialSection: "name",
          pageId,
          sectionId: created.data.addNameSection.id,
        });
        onCreate(created.data.addNameSection.id);
      }
    } else if(pronouns) {
      setCreating(true);
      const created = await createPronounsSection({
        input: {
          pageId,
          title,
        },
      });
      setCreating(false);
      if(created.error) {
        report(created.operation, created.error, handleCreate);
      }
      if(created.data?.addPronounSection) {
        posthog.capture("created section", {
          specialSection: "pronoun",
          pageId,
          sectionId: created.data.addPronounSection.id,
        });
        onCreate(created.data.addPronounSection.id);
      }
    } else {
      setCreating(true);
      const created = await createSection({
        input: {
          pageId,
          title,
        },
      });
      setCreating(false);
      if(created.error) {
        report(created.operation, created.error, handleCreate);
      }
      if(created.data?.addSection) {
        posthog.capture("created section", {
          pageId,
          title,
          sectionId: created.data.addSection.id,
        });
        onCreate(created.data.addSection.id);
      }
    }
  };

  const handleCreateInspired = async (
    newTitle: string,
    inspiration: StockInspiration,
  ) => {
    if(creating) {
      return;
    }
    setCreating(true);
    const created = await createSection({
      input: {
        pageId,
        title: newTitle,
        inspiration: [inspiration],
      },
    });
    setCreating(false);
    if(created.error) {
      report(created.operation, created.error, handleCreate);
    }
    if(created.data?.addSection) {
      posthog.capture("created section", {
        primaryInspiration: inspiration,
        primaryInspirationName: StockInspiration[inspiration],
        pageId,
        title: newTitle,
        inspiration,
        sectionId: created.data.addSection.id,
      });
      onCreate(created.data.addSection.id);
    }
  };

  return (
    <SectionContainer title={title}>
      <Center minHeight="20">
        <Button aria-label="Create" leftIcon={<PlusSquareIcon />} size="lg" onClick={() => handleCreate()}>
          Create Blank Section
        </Button>
      </Center>
      {
        !names && !pronouns && (
          <>
            <Heading as="h4" size="sm" textAlign="center">Section Templates</Heading>
            <Text fontSize="sm" textAlign="center">
              Create a new section with prefilled items.
              You can modify, delete, and add items to the template.
            </Text>
            <StockInspirationList>
              <StockInspirationListItem
                name="Prefix"
                example="(e.g. Mx., Mr., Ms.)"
                item={StockInspiration.PREFIX_MX_MR_MS}
                onSelect={handleCreateInspired}
              />
              <StockInspirationListItem
                name="Honorific"
                example="(e.g. boss, sir, ma'am)"
                item={StockInspiration.HONORIFIC_SIR_MAAM}
                onSelect={handleCreateInspired}
              />
              <StockInspirationListItem
                name="Slang"
                example="(e.g. dude, buddy, bro)"
                item={StockInspiration.REFERENCES_FRIEND_SLANG}
                onSelect={handleCreateInspired}
              />
              <StockInspirationListItem
                name="Compliments"
                example="(e.g. beautiful, pretty, cute)"
                item={StockInspiration.COMPLIMENTS}
                onSelect={handleCreateInspired}
              />
              {
                expandInspiration ? (
                  <>
                    <StockInspirationListItem
                      name="References"
                      example="(e.g. person, man, woman)"
                      item={StockInspiration.REFERENCES_PERSON}
                      onSelect={handleCreateInspired}
                    />
                    { /*
                    <StockInspirationListItem
                      name="Parent"
                      example="(e.g. dad, mom)"
                      item={StockInspiration.RELATIONSHIP_PARENT}
                      onSelect={handleCreateInspired}
                    />
                    */ }
                    <StockInspirationListItem
                      name="Child"
                      example="(e.g. child, son, daughter)"
                      item={StockInspiration.RELATIONSHIP_CHILD}
                      onSelect={handleCreateInspired}
                    />
                    <StockInspirationListItem
                      name="Partner"
                      example="(e.g. partner, joy/boy/girl-friend)"
                      item={StockInspiration.RELATIONSHIP_PARTNER}
                      onSelect={handleCreateInspired}
                    />
                    <Center>
                      <Button size="small" onClick={() => setExpandInspiration(false)}>
                        Less
                      </Button>
                    </Center>
                  </>
                ) : (
                  <Center>
                    <Button size="small" onClick={() => setExpandInspiration(true)}>
                      More
                    </Button>
                  </Center>
                )
              }
            </StockInspirationList>
          </>
        )
      }
    </SectionContainer>
  );
};
