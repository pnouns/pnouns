import React from "react";
import { usePageEditor_EditSectionValueTextMutation } from "../../../generated-types";
import { EditableText } from "../../../components/EditableText";
import { useEditorContext } from "../../../context/editor.context";

export const SectionValueEditableText: React.FC<{
  sectionValueId: string;
  value?: string;
  disabled?: boolean;
  onSave?: () => void | Promise<void>;
}> = ({
  sectionValueId,
  value,
  disabled,
  onSave,
}) => {
  const saveId = `${sectionValueId}-text`;

  const {
    isSaving,
    startSaving,
    report,
  } = useEditorContext();

  const [, save] = usePageEditor_EditSectionValueTextMutation();

  const handleSave = async (value: string) => {
    if(isSaving) {
      return;
    }
    const done = startSaving(saveId);
    const result = await save({
      input: {
        sectionValueId,
        text: value,
      },
    });
    done();
    if(result.error) {
      report(result.operation, result.error, () => handleSave(value));
    }
    if(result.data?.editSectionValueText && typeof onSave === "function") {
      await Promise.resolve(onSave());
    }
  }


  return (
    <EditableText
      value={value}
      placeholder="Item"
      disabled={disabled || isSaving}
      onSave={handleSave}
    />
  );
}
