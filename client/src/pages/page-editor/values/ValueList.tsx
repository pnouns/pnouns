import React, { useMemo, useRef, useState } from "react";
import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Box,
  Button,
  CircularProgress,
  Flex,
  IconButton,
  Input,
  useDisclosure,
} from "@chakra-ui/react";
import {
  DeleteIcon,
  DragHandleIcon, PlusSquareIcon,
} from "@chakra-ui/icons";
import { DragDropContext, Droppable, Draggable, DropResult } from "react-beautiful-dnd";
import { OperationContext } from "urql";
import {
  PageEditor_GetSectionQuery,
  usePageEditor_CreateSectionValueMutation,
  usePageEditor_DeleteSectionValueMutation,
  usePageEditor_UpdateSectionValueOrderMutation,
} from "../../../generated-types";
import { StrengthSelect } from "../../../components/select/StrengthSelect";
import { SectionValueEditableText } from "./SectionValueEditableText";
import { useEditorContext } from "../../../context/editor.context";

interface ValueOrder {
  /**
   * id->index
   */
  orderMap: Record<string, number>;

  indexToId: Record<number, string>;

  /**
   * the very last element
   */
  last?: string;
}

const ValueList: React.FC<{
  sectionResponse?: PageEditor_GetSectionQuery,
  refetchSection?: (opts?: Partial<OperationContext>) => void;
}> = ({
  sectionResponse,
  refetchSection,
}): JSX.Element => {
  const { report } = useEditorContext();
  const deleteConfirmation = useDisclosure();
  const deleteConfirmationCancel = useRef<HTMLButtonElement | null>(null);
  const [deletionId, setDeletionId] = useState("");
  const [deletionText, setDeletionText] = useState("");
  const section = sectionResponse?.section;
  const [newItem, setNewItem] = useState("");
  const [creatingSectionValue, setCreatingSectionValue] = useState(false);

  const [, createSectionValue] = usePageEditor_CreateSectionValueMutation();
  //const [, moveSectionValue] = usePageEditor_MoveSectionValueMutation();
  const [, updateOrder] = usePageEditor_UpdateSectionValueOrderMutation();
  const [, deleteSectionValue] = usePageEditor_DeleteSectionValueMutation();

  const values = section?.values;
  const valueOrder = section ? [...section.valueOrder] : [];

  /*const itemOrderMap: ValueOrder = useMemo(() => {
    if(!values) {
      return {
        orderMap: {},
        last: undefined,
      } as ValueOrder;
    }
    let added = 0;
    const order: ValueOrder = {
      orderMap: {},
      indexToId: {},
      last: undefined,
    };
    const nextMap: Record<string, string | undefined> = {};
    const previousMap: Record<string, string> = {};
    values
      .map(value => {
        nextMap[value.id] = value.next?.id;
        if(value.next) {
          previousMap[value.next.id] = value.id;
        }
        return value;
      })
      .filter(value => !previousMap[value.id])
      .sort((a, b) => a.id < b.id ? -1 : a.id > b.id ? 1 : 0)
      .map(value => {
        console.log(value);
        order.indexToId[added] = value.id;
        order.orderMap[value.id] = added++;
        order.last = value.id;
        return value;
      })
      .map(value => {
        let i = value.id;
        while(i && nextMap[i]) {
          const next = nextMap[i];
          if(typeof next !== "string" || order.orderMap[next]) {
            break;
          }
          i = next;
          order.indexToId[added] = i;
          order.orderMap[i] = added++;
          order.last = i;
        }
      });
    console.log("nextMap", nextMap);
    console.log("previousMap", previousMap);
    console.log("orderMap", order.orderMap);
    console.log("indexToId", order.indexToId);
    return order;
  }, [ values ]);*/

  const handleCreateSectionValue = async () => {
    if(creatingSectionValue) {
      return;
    }
    setCreatingSectionValue(true);
    const res = await createSectionValue({
      input: {
        sectionId: section?.id ?? "",
        text: newItem,
        //previous: itemOrderMap.last,
      },
    });
    if(res.error) {
      report(res.operation, res.error, handleCreateSectionValue);
    }
    if(res.data && res.data.createSectionValue) {
      setNewItem("");
    }
    typeof refetchSection === "function" && refetchSection({
      requestPolicy: "network-only",
    });
    setCreatingSectionValue(false);
  }

  const handleDeleteValue = async () => {
    const res = await deleteSectionValue({
      id: deletionId,
    });
    if(res.error) {
      report(res.operation, res.error, handleDeleteValue);
    }
    if(res.data && res.data.deleteSectionValue) {
      setDeletionId("");
      setDeletionText("");
    }
    typeof refetchSection === "function" && refetchSection({
      requestPolicy: "network-only",
    });
    deleteConfirmation.onClose();
  };


  if(!section) {
    return <CircularProgress isIndeterminate />;
  }
  const dropId = `${section.id}-drop`;
  const onDragEnd = async (result: DropResult) => {
    if(!result.destination) {
      return;
    }
    const startIndex = result.source.index;
    const endIndex = result.destination.index;

    const updated = [...section.valueOrder];
    const [removed] = updated.splice(startIndex, 1);
    updated.splice(endIndex, 0, removed);
    const res = await updateOrder({
      input: {
        sectionId: section.id,
        order: updated,
      },
    });
    if(res.error) {
      report(res.operation, res.error);
    }
    typeof refetchSection === "function" && refetchSection({
      requestPolicy: "network-only",
    });

    /*console.log(`Source: ${sourceIndex} Destination: ${destinationIndex}`)
    const sectionValueId = itemOrderMap.indexToId[sourceIndex];
    const newPrevious = itemOrderMap.indexToId[destinationIndex];
    console.log(`Item ${sectionValueId} used to be at ${sourceIndex}`);
    console.log(`Moving after item #${destinationIndex}, ${newPrevious}`);
    await moveSectionValue({
      input: {
        sectionValueId,
        newPrevious,
      },
    });
    */
    return;
  };
  return (
    <>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId={dropId}>
          {(provided, snapshot) => (
            <Box
              {...provided.droppableProps}
              ref={provided.innerRef}
            >
              { section.values
                .sort((a, b) => valueOrder.indexOf(a.id) - valueOrder.indexOf(b.id))
                .map((value, index) => {
                  return (
                    <Draggable key={value.id} draggableId={value.id} index={valueOrder.indexOf(value.id) ?? index}>
                      {(provided, snapshot) => (
                        <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                          <Flex>
                            <DragHandleIcon flex="1" />
                            <Box flex="10">
                              <SectionValueEditableText
                                sectionValueId={value.id}
                                value={value.text}
                                onSave={() => refetchSection && refetchSection({
                                  requestPolicy: "network-only",
                                })}
                              />
                            </Box>
                            <StrengthSelect
                              sectionValueId={value.id}
                              strength={value.strength}
                              customStrengthText={value.customStrengthText}
                              flex="1"
                              />
                            <IconButton flex="1" aria-label="Delete Section" icon={<DeleteIcon />} onClick={() => {
                              setDeletionId(value.id);
                              setDeletionText(value.text);
                              deleteConfirmation.onOpen();
                            }} />
                          </Flex>
                        </div>
                      )}
                    </Draggable>
                  );
                }) }
              {/* section.values.sort((a, b) => itemOrderMap.orderMap[a.id] - itemOrderMap.orderMap[b.id]).map((value, index) => {
                return (
                  <Draggable key={value.id} draggableId={value.id} index={itemOrderMap.orderMap[value.id] ?? index}>
                    {(provided, snapshot) => (
                      <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                        <Flex>
                          <DragHandleIcon flex="1" />
                          <Box flex="10">
                            <EditableText value={value.text} />
                          </Box>
                          <StrengthSelect
                            sectionValueId={value.id}
                            strength={value.strength}
                            customStrengthText={value.customStrengthText}
                            flex="1"
                            />
                        </Flex>
                      </div>
                    )}
                  </Draggable>
                );
              }) */}
              { provided.placeholder }
            </Box>
          )}
        </Droppable>
      </DragDropContext>
      <Flex>
        <Input
          flex="10"
          placeholder="New Item"
          variant="filled"
          value={newItem}
          onChange={e => setNewItem(e.target.value)}
          bg="teal.200"
        />
        <IconButton
          flex="1"
          aria-label="Add new item"
          icon={<PlusSquareIcon />}
          onClick={handleCreateSectionValue}
        />
      </Flex>
      <AlertDialog
        isOpen={deleteConfirmation.isOpen}
        leastDestructiveRef={deleteConfirmationCancel}
        onClose={deleteConfirmation.onClose}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Delete Value {deletionText}
            </AlertDialogHeader>
            <AlertDialogBody>
              Are you sure you want to permanantly delete this value?
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button ref={deleteConfirmationCancel} onClick={deleteConfirmation.onClose}>
                Cancel
              </Button>
              <Button colorScheme="red" onClick={handleDeleteValue} ml={3}>
                Delete
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </>
  );
};

export default ValueList;
