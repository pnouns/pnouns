import { DragHandleIcon } from "@chakra-ui/icons";
import { Box, Flex } from "@chakra-ui/react";
import React from "react";
import { DragDropContext, Draggable, Droppable, DropResult } from "react-beautiful-dnd";
import { OperationContext } from "urql";
import { useEditorContext } from "../../context/editor.context";
import { PageEditor_PageQuery, usePageEditor_UpdateSectionOrderMutation } from "../../generated-types";

export const SectionOrdering: React.FC<{
  page?: PageEditor_PageQuery;
  refetchPage?: (opts?: Partial<OperationContext>) => void;
}> = ({
  page,
  refetchPage,
}) => {
  const { report } = useEditorContext();
  const [, updateSectionOrder] = usePageEditor_UpdateSectionOrderMutation();
  if(!page || !page.getPage) {
    return <Box />;
  }

  const sectionOrder: string[] = page.getPage.sectionOrder;

  const sections = page.getPage.sections
    .filter(section => !section.type);

  const handleDragEnd = async (result: DropResult) => {
    if(!result.destination) {
      return;
    }
    const startIndex = result.source.index;
    const endIndex = result.destination.index;

    const updated = [...sectionOrder];
    const [removed] = updated.splice(startIndex, 1);
    updated.splice(endIndex, 0, removed);
    const res = await updateSectionOrder({
      input: {
        pageId: page.getPage.id,
        order: updated,
      },
    });
    if(res.error) {
      report(res.operation, res.error);
    }
    typeof refetchPage === "function" && refetchPage({
      requestPolicy: "network-only",
    });
  };

  return (
    <DragDropContext onDragEnd={handleDragEnd}>
      <Droppable droppableId="section-ordering-droppable">
        {(provided, snapshot) => (
          <Box
            {...provided.droppableProps}
            ref={provided.innerRef}
          >
            {
              sections
                .sort((a, b) => sectionOrder.indexOf(a.id) - sectionOrder.indexOf(b.id))
                .map((value, index) => {
                  return (
                    <Draggable
                      key={value.id}
                      draggableId={value.id}
                      index={sectionOrder.indexOf(value.id) !== -1 ? sectionOrder.indexOf(value.id) : index}
                    >
                      {(provided, snapshot) => (
                        <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                          <Flex>
                            <DragHandleIcon flex="1" />
                            <Box flex="10">
                              { value.title }
                            </Box>
                          </Flex>
                        </div>
                      )}
                    </Draggable>
                  )
                })
            }
            { provided.placeholder }
          </Box>
        )}
      </Droppable>
    </DragDropContext>
  );
};
