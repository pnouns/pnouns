import React from "react";
import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Badge,
  Box,
  Container,
  HStack,
  Spacer,
  Text,
} from "@chakra-ui/react";
import { useEditorContext } from "../../context/editor.context";
import { HeaderPasswordSetting } from "./header-settings/password";
import { PageEditor_PageQuery } from "../../generated-types";
import { HeaderBadgeSetting } from "./header-settings/badges";

export const HeaderSettings: React.FC<{
  page?: PageEditor_PageQuery,
  refetchPage?: () => Promise<void>,
}> = ({
  page,
  refetchPage,
}) => {
  const {
    displayName,
    categoryDisplayName,
  } = useEditorContext();

  const showBadges = page && page.getPage.showBadges;
  const nicknameShowBadgesByDefault = page && page.getPage.nickname.showBadgesByDefault;
  const hasPassword = page && page.getPage.password && page.getPage.password.length >= 2;

  return (
    <Container maxW="container.xl">
    <Accordion allowToggle>
      <AccordionItem>
        <h2>
          <AccordionButton>
            <Box flex="1" textAlign="left">
              Editing {displayName} {categoryDisplayName} Page
            </Box>
            <Spacer />
            <HStack textAlign="right">
              {
                showBadges === true
                  ? (<Badge colorScheme="blue">Badges Shown</Badge>)
                  : showBadges === false
                    ? (<Badge colorScheme="gray">Badges Hidden</Badge>)
                    : nicknameShowBadgesByDefault === true
                      ? (<Badge colorScheme="blue">Badges Shown (inherited)</Badge>)
                      : (<Badge colorScheme="gray">Badges Hidden (inherited)</Badge>)
              }
              { hasPassword
                ? (<Badge colorScheme="blue">Password Protected</Badge>)
                : (<Badge colorScheme="yellow">No Password</Badge>)
              }
              <Text>Settings</Text>
            </HStack>
            <AccordionIcon />
          </AccordionButton>
        </h2>
        <AccordionPanel pb={4}>
          { page && (
            <Container maxW="container.xl">
              <HeaderPasswordSetting page={page} refetchPage={refetchPage} />
              <HeaderBadgeSetting page={page} refetchPage={refetchPage} />
            </Container>
          )}
        </AccordionPanel>
      </AccordionItem>
    </Accordion>
    </Container>
  );
}
