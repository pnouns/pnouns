import React from "react";
import {
  Box,
  Flex,
  Heading,
  useColorModeValue,
} from "@chakra-ui/react";
import {
  Maybe,
  PageEditor_PageQuery,
  usePageEditor_UpdatePageBioMutation,
} from "../../generated-types";
import { EditableText } from "../../components/EditableText";
import { useEditorContext } from "../../context/editor.context";

export const BioEditor: React.FC<{
  page?: PageEditor_PageQuery,
  onSave?: () => void | Promise<void>;
}> = ({
  page,
  onSave,
}) => {
  const {
    isSaving,
    startSaving,
    report,
  } = useEditorContext();
  const bg = useColorModeValue("gray.700", "gray.100");
  const text = useColorModeValue("white", "black");
  const placeholderTextColor = useColorModeValue("gray.400", "gray.400");

  const [, save] = usePageEditor_UpdatePageBioMutation();

  const getTextColor = (value?: Maybe<string>): string | undefined => {
    if(!value || value.length < 1) {
      return placeholderTextColor;
    }
    return undefined;
  };

  const handleSave = async (title?: Maybe<string>, subtitle?: Maybe<string>) => {
    if(isSaving) {
      return;
    }
    const done = startSaving(`${page?.getPage.id}-bio`);
    const result = await save({
      input: {
        pageId: page?.getPage.id ?? '',
        title,
        subtitle,
      },
    });
    if(result.error) {
      report(result.operation, result.error, () => handleSave(title, subtitle));
    }
    done();
    if(typeof onSave === "function") {
      onSave();
    }
  };

  if(!page) {
    return <></>;
  }

  return (
    <>
      <Box
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
        bg={bg}
        textColor={text}
        p="1"
      >
        <Flex className="bio-header">
          <Box p="2" flex="10">
            <Heading size="md" textColor={getTextColor(page?.getPage?.pageBioTitle)}>
              <EditableText
                placeholder="[Name]"
                value={page?.getPage?.pageBioTitle ?? undefined}
                disabled={isSaving}
                onSave={newTitle => handleSave(newTitle, page?.getPage?.pageBioSubtitle)}
              />
            </Heading>
          </Box>
        </Flex>
        <Flex className="bio-subtitle">
          <Box p="2" flex="10">
            <Heading size="sm" textColor={getTextColor(page?.getPage?.pageBioSubtitle)}>
              <EditableText
                placeholder="[Subtitle]"
                value={page?.getPage?.pageBioSubtitle ?? undefined}
                disabled={isSaving}
                onSave={newSubtitle => handleSave(page?.getPage?.pageBioTitle, newSubtitle)}
              />
            </Heading>
          </Box>
        </Flex>
      </Box>
    </>
  );
};
