import React, { useState } from "react";
import { Center, CircularProgress } from "@chakra-ui/react";
import { SectionContainer } from "../../components/SectionDisplay";

export const LoadingSection: React.FC<{
}> = ({}) => {
  return (
    <SectionContainer title="Loading">
      <Center minHeight="20">
        <CircularProgress isIndeterminate />
      </Center>
    </SectionContainer>
  );
};
