import React, { useState } from "react";
import { Badge, Box, Center, Flex, Heading, Spacer } from "@chakra-ui/layout";
import { Tooltip } from "@chakra-ui/tooltip";
import { IconButton } from "@chakra-ui/button";
import { Select } from "@chakra-ui/select";
import { DragHandleIcon, PlusSquareIcon } from "@chakra-ui/icons";
import {
  DragDropContext,
  Draggable,
  Droppable,
  DropResult,
} from "react-beautiful-dnd";
import {
  PageEditor_PageQuery,
  usePageEditor_AddLinkedPageMutation,
  usePageEditor_UpdateLinkedPageOrderMutation,
} from "../../generated-types";
import { useEditorContext } from "../../context/editor.context";
import { PageLinkSectionItem } from "./PageLinkSectionItem";
import { EditorContainer } from "../../components/SectionDisplay";

/**
 *  [Drag]  [Name, flex=1]     [Subtitle, flex=3]   [Refresh] [Delete]
 *          [caption]
 *          [url]                                   [security] [info=need to republish if changing child security]
 */

export const PageLinkSection: React.FC<{
  page: PageEditor_PageQuery;
  refetchPage?: () => Promise<void>;
}> = ({ page, refetchPage }) => {
  const { report } = useEditorContext();
  const [selectedSubPage, setSelectedSubPage] = useState<string | undefined>();
  const [addingSubPage, setAddingSubPage] = useState(false);

  const pages = page.getPage.listedSubPages;
  const pageOrder = page.getPage.listedSubPageOrder;

  const [, addLinkedPage] = usePageEditor_AddLinkedPageMutation();
  const [, updateOrder] = usePageEditor_UpdateLinkedPageOrderMutation();

  const handleAddSubPage = async (): Promise<void> => {
    if(addingSubPage || !selectedSubPage) {
      return;
    }
    setAddingSubPage(true);
    const res = await addLinkedPage({
      input: {
        pageId: page.getPage.id,
        linkedPageId: selectedSubPage,
      },
    });
    if(res.error) {
      report(res.operation, res.error, () => handleAddSubPage);
    }
    if(res.data && res.data.addLinkedPage) {
      refetchPage && await refetchPage();
    }
    setAddingSubPage(false);
  };

  const onDragEnd = async (result: DropResult) => {
    if(!result.destination) {
      return;
    }
    const startIndex = result.source.index;
    const endIndex = result.destination.index;

    const updated = [...pageOrder];
    const [removed] = updated.splice(startIndex, 1);
    updated.splice(endIndex, 0, removed);
    const res = await updateOrder({
      input: {
        pageId: page.getPage.id,
        order: updated,
      },
    });
    if(res.error) {
      report(res.operation, res.error);
    }
    refetchPage && await refetchPage();
  };


  return (
    <>
      <Flex mt="3">
        <Heading flex="6" as="h3" fontSize="md" color={pages.length > 0 ? "white" : "gray.500"}>
          Sub-Pages
          { pages.length === 0 && (
            <>
              &nbsp;
              <Tooltip
                hasArrow
                label="This heading is hidden because there are no sub-pages listed."
                bg="gray.800"
                color="white"
              >
                <Badge
                  colorScheme="yellow"
                >
                  Hidden
                </Badge>
              </Tooltip>
            </>
          )}
        </Heading>
        <Spacer />
        <Select
          placeholder="Add Existing Page"
          flex="4"
          value={selectedSubPage}
          onChange={(e) => setSelectedSubPage(e.target.value)}
        >
          { page.getPage.subPageOptions
            .filter(option => !pages.find(page => page.linked.id === option.id))
            .map(option => (
              <>
                <option value={option.id}>{option.name}</option>
              </>
            ))}
        </Select>
        <Box>
          <IconButton
            aria-label="Add Selected Existing Page"
            icon={<PlusSquareIcon />}
            colorScheme="green"
            variant="outline"
            ml="2"
            disabled={!selectedSubPage}
            isLoading={addingSubPage}
            onClick={handleAddSubPage}
          />
        </Box>
      </Flex>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="page-editor-sub-pages-dropable">
          {(provided, snapshot) => (
            <Box
              {...provided.droppableProps}
              ref={provided.innerRef}
            >
              {
                pages
                  .sort((a,b) => pageOrder.indexOf(a.id) - pageOrder.indexOf(b.id))
                  .map((value, index) => {
                    return (
                      <Draggable key={value.id} draggableId={value.id} index={pageOrder.indexOf(value.id) ?? index}>
                        {(provided, snapshot) => (
                          <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                            <Box my="2">
                              <EditorContainer>
                                <Flex>
                                  <Center px="2">
                                    <DragHandleIcon />
                                  </Center>
                                  <Box flex="9">
                                    <PageLinkSectionItem
                                      page={page}
                                      item={value}
                                      linked={page.getPage.subPageOptions.find(i => i.id === value.linked.id)}
                                      refetchPage={refetchPage}
                                    />
                                  </Box>
                                </Flex>
                              </EditorContainer>
                            </Box>
                          </div>
                        )}
                      </Draggable>
                    )
                  })
              }
              { provided.placeholder }
            </Box>
          )}
        </Droppable>
      </DragDropContext>
    </>
  );
};
