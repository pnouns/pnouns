import React, { useState } from "react";
import { useRouteMatch } from "react-router-dom";
import {
  Badge,
  Box,
  Button,
  Container,
  Grid,
  GridItem,
  Heading,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useColorModeValue,
  useDisclosure,
} from "@chakra-ui/react";
import { Header } from "../../components/Header";
import { HeaderSettings } from "./HeaderSettings";
import {
  PublishedSection,
  SupporterRole,
  usePageEditor_PageQuery,
  usePageEditor_PublishMutation,
} from "../../generated-types";
import { CreateSection } from "./CreateSection";
import { LoadingSection } from "./LoadingSection";
import { EditSection } from "./EditSection";
import { EditorProvider } from "../../context/editor.context";
import { ButtonLink, Link } from "../../components/Link";
import { SectionOrdering } from "./SectionOrdering";
import { Footer } from "../../components/Footer";
import { BioEditor } from "./BioEditor";
import { PublishWarning } from "./PublishWarning";
import { formatEditorUrl } from "../../components/UrlTextPreview";
import posthog from "posthog-js";
import { useErrorModal } from "../../components/error-modal";
import { PageLinkSection } from "./PageLinkSection";
import { staffTypeText } from "../../util/badge";
import { PageBadgeDisplay } from "../../components/PageBadgeDisplay";
import { useUserContext } from "../../context/user.context";

interface BadgeInfo {
  color: "orange" | "gray";
  variant: "outline" | "solid" | "subtle";
  text: string;
  staff?: true;
  supporter?: true;
}

const PageEditor: React.FC<{}> = ({}) => {
  const { loggedIn } = useUserContext();
  const { errorModal, report } = useErrorModal();
  const publishModal = useDisclosure();
  const orderModal = useDisclosure();
  const [isPublishing, setIsPublishing] = useState(false);
  const [publishSuccess, setPublishSuccess] = useState(false);
  const { params: { pageId } } = useRouteMatch<{ pageId: string }>();

  const [{ data: page, fetching, error }, refetchPage] = usePageEditor_PageQuery({
    variables: {
      id: pageId,
    },
  });

  const [, publish] = usePageEditor_PublishMutation();

  const displayName = page?.getPage?.nickname.displayName ?? page?.getPage?.nickname.id ?? "My";
  const categoryDisplayName = page?.getPage?.displayName ?? page?.getPage?.name ?? "";

  const handlePublish = async () => {
    if(isPublishing) {
      return;
    }
    setIsPublishing(true);
    const res = await publish({
      id: pageId,
    });
    if(res.error) {
      report(res.operation, res.error, handlePublish);
    }
    if(res.data && res.data.publish) {
      posthog.capture("published page", {
        pageId,
        publishedPageId: res.data?.publish?.id,
      });
      setPublishSuccess(true);
    }
    setIsPublishing(false);
  };

  const sections = page && page.getPage && Array.isArray(page.getPage.sections)
    ? page.getPage.sections
    : undefined;

  const sectionOrder = page && page.getPage ? page.getPage.sectionOrder : undefined;

  const link = formatEditorUrl(page);

  const isNamespacePage = page && page.getPage && page.getPage.name === null;
  const isCategoryPage = page && page.getPage && !isNamespacePage && !page.getPage.parent;
  const isSubPage = page && page.getPage && !isNamespacePage && !isCategoryPage;

  const pageShowBadges = page && page.getPage.showBadges;
  const nicknameShowBadgesByDefault = page && page.getPage.nickname.showBadgesByDefault;
  const showBadges = pageShowBadges === true || (pageShowBadges !== false && nicknameShowBadgesByDefault === true);
  const staffBadges = page?.getPage.nickname.staffType ?? [];
  const supporterBadge = page?.getPage.nickname.supporterRole;

  const bg = useColorModeValue("gray.100", "gray.600");
  const text = useColorModeValue("black", "white");

  if(!loggedIn) {
    return (

      <Container maxW="container.sm" mt="4">
        <Box borderWidth="1px" borderRadius="lg" overflow="hidden" bg={bg} textColor={text} p="1" marginBottom="2">
          <Heading as="h3" size="md" textAlign="center" my="1">
            Not Logged In
          </Heading>
          <ButtonLink path="/">Login</ButtonLink>
        </Box>
      </Container>
    )
  }

  return (
    <EditorProvider
      pageId={pageId}
      displayName={displayName}
      categoryDisplayName={categoryDisplayName}
      report={report}
    >
      <Header
        page={{
          nickname: page?.getPage?.nickname.id ?? "",
          displayName,
          category: categoryDisplayName,
        }}
        noAuth
        onPublish={() => {
          setPublishSuccess(false);
          publishModal.onOpen();
        }}
      />
      <HeaderSettings page={page} refetchPage={async () => refetchPage({
        requestPolicy: "network-only",
      })} />
      <Container maxW="container.xl">
        <PublishWarning />
        { showBadges && (staffBadges.length > 0 || supporterBadge) && (
          <Box p="2">
            { staffBadges.map(role => <PageBadgeDisplay staffRole={role} />) }
            { supporterBadge && <PageBadgeDisplay supporter={supporterBadge} /> }
          </Box>
        )}
        <Grid
          templateColumns="repeat(6, 1fr)"
          gap="2"
        >
          <GridItem colSpan={{ base: 6, xl: 2 }}>
            <BioEditor
              page={page}
              onSave={() => refetchPage({ requestPolicy: "network-only" })}
            />
          </GridItem>
          <GridItem colSpan={{ base: 6, md: 3, xl: 2 }}>
            {
              !page || !page.getPage && (
                <LoadingSection />
              )
            }
            {
              page && !fetching && page.getPage && !page.getPage.nameSection && (
                <CreateSection
                  pageId={pageId}
                  title="Names"
                  names
                  onCreate={() => refetchPage({
                    requestPolicy: "network-only",
                  })}
                />
              )
            }
            {
              page && page.getPage && page.getPage.nameSection && (
                <EditSection
                  pageId={pageId}
                  sectionId={page.getPage.nameSection.id}
                />
              )
            }
          </GridItem>
          <GridItem colSpan={{ base: 6, md: 3, xl: 2 }}>
          {
              !page || !page.getPage && (
                <LoadingSection />
              )
            }
            {
              page && !fetching && page.getPage && !page.getPage.pronounSection && (
                <CreateSection
                  pageId={pageId}
                  title="Pronouns"
                  pronouns
                  onCreate={() => refetchPage({
                    requestPolicy: "network-only",
                  })}
                />
              )
            }
            {
              page && page.getPage && page.getPage.pronounSection && (
                <EditSection
                  pageId={pageId}
                  sectionId={page.getPage.pronounSection.id}
                />
              )
            }
          </GridItem>
        </Grid>
        {
          page && page.getPage && (isNamespacePage || isCategoryPage) && (<PageLinkSection
            page={page}
            refetchPage={async () => refetchPage({
              requestPolicy: "network-only",
            })}
          />)
        }
        <Box>
          <Button
            variant="ghost"
            onClick={orderModal.onOpen}
          >
            Order Sections
          </Button>
        </Box>
        <Grid
          marginTop="4"
          templateColumns="repeat(6, 1fr)"
          gap="2"
        >
          {
            sections && sectionOrder && sections
              .filter(section => !section.type)
              .sort((a, b) => sectionOrder.indexOf(a.id) - sectionOrder.indexOf(b.id))
              .map(section => (
                <GridItem key={section.id} colSpan={{ base: 6, md: 3, xl: 2 }}>
                  <EditSection pageId={pageId} sectionId={section.id} />
                </GridItem>
              ))
          }
          <GridItem colSpan={{ base: 6, md: 3, xl: 2 }}>
            <CreateSection
              pageId={pageId}
              title="New Section"
              onCreate={() => refetchPage({
                requestPolicy: "network-only",
              })}
            />
          </GridItem>
        </Grid>
        <Footer displayName={displayName} />
      </Container>
      <Modal isOpen={publishModal.isOpen} onClose={publishModal.onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Publish Page</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Text>Ready to publish your site?</Text>
            <Text>
              If you haven't published yet, your page is not visible to anyone else.
            </Text>
            <Text>
              Each time you make changes to your page, make sure you come re-publish again.
            </Text>
            <Text>
              Changes you make will not show up until you publish again -
              unless in very rare situations we re-publish pages if there are major site changes made.
            </Text>
            { publishSuccess && (
              <>
                <Heading size="sm">Published!</Heading>
                <Text>Your site is now live:</Text>
                <Link path={link} external>{ link }</Link>
              </>
            )}
          </ModalBody>
          <ModalFooter>
            <Button variant="ghost" mr={3} onClick={publishModal.onClose}>
              Cancel
            </Button>
            <Button colorScheme="blue" onClick={handlePublish} isLoading={isPublishing} loadingText="Publishing...">
              Publish
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
      <Modal isOpen={orderModal.isOpen} onClose={orderModal.onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Order Sections</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <SectionOrdering page={page} />
          </ModalBody>
          <ModalFooter>
            <Button variant="ghost" mr={3} onClick={orderModal.onClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
      { errorModal }
    </EditorProvider>
  );
}

export default PageEditor;
