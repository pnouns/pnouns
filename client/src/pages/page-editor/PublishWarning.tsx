import React from "react";
import { Box, Heading, Text } from "@chakra-ui/layout";
import { useColorModeValue } from "@chakra-ui/system";

export const PublishWarning: React.FC<{}> = ({}) => {
  const bg = useColorModeValue("orange.400", "orange.600");
  const text = useColorModeValue("black", "white");
  return (
    <Box borderWidth="1px" borderRadius="lg" overflow="hidden" bg={bg} textColor={text} p="1" my="2">
      <Heading size="sm">Don't forget to publish!</Heading>
      <Text>
        Make sure you use the "Publish" button in the top-right to publish your page.
      </Text>
    </Box>
  )
}
