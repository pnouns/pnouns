import React, { Suspense, useRef } from "react";
import { lazy } from "@loadable/component";
import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Box,
  Button,
  Center,
  CircularProgress,
  Flex,
  Heading,
  IconButton,
  Text,
  useColorModeValue,
  useDisclosure,
} from "@chakra-ui/react";
import { SectionContainer } from "../../components/SectionDisplay";
import {
  Maybe,
  SectionTextField,
  usePageEditor_DeleteSectionMutation,
  usePageEditor_GetSectionQuery,
} from "../../generated-types";
import { SectionEditableText } from "./SectionEditableText";
import { DeleteIcon } from "@chakra-ui/icons";
import { OperationContext } from "urql";
import { useEditorContext } from "../../context/editor.context";

const ValueList = lazy(() => import("./values/ValueList"));

export const EditSection: React.FC<{
  pageId: string;
  sectionId: string;
  refetchPage?: (opts?: Partial<OperationContext>) => void;
}> = ({ pageId, sectionId, refetchPage }) => {
  const { report } = useEditorContext();
  const deleteConfirmation = useDisclosure();
  const deleteConfirmationCancel = useRef<HTMLButtonElement | null>(null);
  const bg = useColorModeValue("gray.700", "gray.100");
  const text = useColorModeValue("white", "black");
  const placeholderTextColor = useColorModeValue("gray.400", "gray.400");

  const [, deleteSection] = usePageEditor_DeleteSectionMutation();

  const [{ data, fetching, error }, refetchSection] = usePageEditor_GetSectionQuery({
    variables: {
      id: sectionId,
    }
  });
  const section = data?.section;

  if(!section || error) {
    return (
      <SectionContainer title="Loading">
        <Center minHeight="20">
          <CircularProgress isIndeterminate />
        </Center>
      </SectionContainer>
    );
  }

  const handleDeleteSection = async () => {
    if(!section) {
      return;
    }
    const res = await deleteSection({
      input: sectionId,
    });
    deleteConfirmation.onClose();
    if(res.error) {
      report(res.operation, res.error, handleDeleteSection);
    }
    typeof refetchPage === "function" && refetchPage({
      requestPolicy: "network-only",
    });
  };

  const getTextColor = (value?: Maybe<string>): string | undefined => {
    if(!value || value.length < 1) {
      return placeholderTextColor;
    }
    return undefined;
  };

  return (
    <>
      <Box
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
        bg={bg}
        textColor={text}
        p="1"
        data-section={sectionId}
      >
        <Flex className="section-header">
          <Box p="2" flex="10">
            <Heading size="md" textColor={getTextColor(section.title)}>
              <SectionEditableText
                sectionId={sectionId}
                field={SectionTextField.TITLE}
                placeholder="[Title]"
                value={section.title}
              />
            </Heading>
          </Box>
          <IconButton flex="1" aria-label="Delete Section" icon={<DeleteIcon />} onClick={deleteConfirmation.onOpen} />
        </Flex>
        <Box p="2" flex="10">
          <Heading size="sm" data-section-field="subtitle" textColor={getTextColor(section.subtitle)}>
            <SectionEditableText
              sectionId={sectionId}
              field={SectionTextField.SUBTITLE}
              placeholder="[Subtitle]"
              value={section.subtitle ?? undefined}
            />
          </Heading>
        </Box>
        <Box p="2" flex="10">
          <Text size="sm" textColor={getTextColor(section.lead)}>
            <SectionEditableText
              sectionId={sectionId}
              field={SectionTextField.LEAD}
              placeholder="[Lead]"
              value={section.lead ?? undefined}
            />
          </Text>
        </Box>
        <Suspense fallback={<CircularProgress isIndeterminate />}>
          <ValueList sectionResponse={data} refetchSection={refetchSection} />
        </Suspense>
      </Box>
      <AlertDialog
        isOpen={deleteConfirmation.isOpen}
        leastDestructiveRef={deleteConfirmationCancel}
        onClose={deleteConfirmation.onClose}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Delete Section {section.title}
            </AlertDialogHeader>
            <AlertDialogBody>
              Are you sure you want to permanantly delete this section?
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button ref={deleteConfirmationCancel} onClick={deleteConfirmation.onClose}>
                Cancel
              </Button>
              <Button colorScheme="red" onClick={handleDeleteSection} ml={3}>
                Delete
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </>
  );
};
