import React, { useState } from "react";
import { Grid, GridItem } from "@chakra-ui/layout";
import {
  Button,
  Checkbox,
  FormControl,
  FormHelperText,
  FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  Text,
} from "@chakra-ui/react";
import { useValidations } from "../../../components/validation/useValidations";
import { ValidationInputIndication } from "../../../components/validation/ValidationInputIndication";
import { PasswordFormatSelect } from "../../../components/select/PasswordFormatSelect";
import {
  PageEditor_PageQuery,
  PasswordFormat,
  usePageEditor_UpdatePagePasswordMutation,
} from "../../../generated-types";
import { formatEditorUrl } from "../../../components/UrlTextPreview";
import { useEditorContext } from "../../../context/editor.context";

export const HeaderPasswordSetting: React.FC<{
  page: PageEditor_PageQuery;
  refetchPage?: () => Promise<void>;
}> = ({
  page,
  refetchPage,
}) => {
  const { report } = useEditorContext();
  const savedPassword = page.getPage.password;
  const savedPasswordActive = !!savedPassword && savedPassword.length >= 2;
  const savedPasswordFormat = page.getPage.defaultPasswordFormat;
  const [headerPasswordActive, setHeaderPasswordActive] = useState(savedPasswordActive);
  const [pagePassword, setPagePassword] = useState(savedPassword ?? "");
  const [passwordFormat, setPasswordFormat] = useState<PasswordFormat | undefined>(savedPasswordFormat ?? undefined);

  const [updatingPassword, setUpdatingPassword] = useState(false);
  const [, updatePagePassword] = usePageEditor_UpdatePagePasswordMutation();

  const dataToSave = savedPasswordActive !== headerPasswordActive
    || (headerPasswordActive && pagePassword !== savedPassword)
    || (headerPasswordActive && passwordFormat !== savedPasswordFormat);

  const passwordValidation = useValidations({
    longEnough: pagePassword.length > 1,
    shortEnough: pagePassword.length < 7,
    characterSet: !!pagePassword.match(/^[a-zA-Z0-9]*$/),
  });

  const handleUpdatePagePassword = async () => {
    if(updatingPassword || !dataToSave || (headerPasswordActive && !passwordValidation.valid)) {
      return;
    }
    setUpdatingPassword(true);
    const res = await updatePagePassword({
      input: {
        pageId: page.getPage.id,
        password: headerPasswordActive ? pagePassword : undefined,
        defaultPasswordFormat: headerPasswordActive ? passwordFormat : undefined,
      },
    });
    if(res.error) {
      report(res.operation, res.error, handleUpdatePagePassword);
    }
    refetchPage && await refetchPage();
    setUpdatingPassword(false);
  };

  const link = formatEditorUrl(page, {
    password: pagePassword,
    defaultPasswordFormat: passwordFormat,
  });

  const allowInPage = page.getPage.name !== null
    && (page.getPage.parent !== null || page.getPage.defaultSubdomain !== true);

  return (
    <Grid templateColumns="repeat(12, 1fr)" gap="2" data-setting-row="password">
      <GridItem colSpan={{ base: 12, lg: 3 }}>
        <FormControl id="page-editor-header-password-active">
          <FormLabel>Password Security</FormLabel>
          <InputGroup>
            <Checkbox
              isChecked={headerPasswordActive}
              onChange={e => {
                setHeaderPasswordActive(e.target.checked);
                if(e.target.checked && !passwordFormat) {
                  setPasswordFormat(allowInPage ? PasswordFormat.IN_PAGE : PasswordFormat.QUERY_PASSWORD);
                }
              }}
            >
              Password Protection
            </Checkbox>
          </InputGroup>
        </FormControl>
      </GridItem>
      <GridItem colSpan={{ base: 12, md: 4, lg: 3 }}>
        <FormControl id="page-editor-header-password-value" isRequired={headerPasswordActive}>
          <FormLabel>Page Password</FormLabel>
          <InputGroup>
            <Input
              name="page-editor-password-value"
              value={pagePassword}
              disabled={!headerPasswordActive}
              onChange={e => setPagePassword(e.target.value)}
            />
            {headerPasswordActive && (
              <InputRightElement children={<ValidationInputIndication validations={passwordValidation} />} />
            )}
          </InputGroup>
          {headerPasswordActive && (
            <FormHelperText>
              2-6 characters, only letters and numbers
            </FormHelperText>
          )}
        </FormControl>
      </GridItem>
      <GridItem colSpan={{ base: 12, md: 4, lg: 3 }}>
        <PasswordFormatSelect
          id="page-editor-header-password-format"
          password={pagePassword}
          value={passwordFormat}
          allowInPage={allowInPage}
          disabled={!headerPasswordActive}
          onChange={setPasswordFormat}
        />
      </GridItem>
      <GridItem colSpan={{ base: 12, md: 4, lg: 3 }}>
        <FormControl id="page-editor-header-password-save">
          <FormLabel>Save</FormLabel>
          <InputGroup>
            <Button
              disabled={!dataToSave || !passwordValidation.valid || updatingPassword}
              isLoading={updatingPassword}
              loadingText="Updating Password..."
              onClick={handleUpdatePagePassword}
            >
              Update Password
            </Button>
          </InputGroup>
          {!dataToSave && (
            <FormHelperText>
              Up to date.
            </FormHelperText>
          )}
          { dataToSave && !passwordValidation.valid && (
            <FormHelperText textColor="red.500">
              Enter a valid password.
            </FormHelperText>
          ) }
        </FormControl>
      </GridItem>
      <GridItem colSpan={{ base: 12 }}>
        { !headerPasswordActive && (
          <Text>No password required.</Text>
        ) }
        { headerPasswordActive && (
          <Text>Default URL: { link }</Text>
        )}
      </GridItem>
    </Grid>
  );
};
