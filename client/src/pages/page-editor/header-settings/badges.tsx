import { FormControl, FormLabel } from "@chakra-ui/form-control";
import { InputGroup } from "@chakra-ui/input";
import { Grid, GridItem } from "@chakra-ui/layout";
import { Select } from "@chakra-ui/select";
import React, { useState } from "react";
import { useEditorContext } from "../../../context/editor.context";
import { Maybe, PageEditor_PageQuery, usePageEditor_UpdatePageShowBadgesMutation } from "../../../generated-types";

enum BoolStrValue {
  TRUE = "true",
  FALSE = "false",
  UNDEFINED = "undefined",
}

function getBoolStrValue(val: Maybe<boolean> | undefined): BoolStrValue {
  if(val === true) {
    return BoolStrValue.TRUE;
  } else if(val === false) {
    return BoolStrValue.FALSE;
  } else {
    return BoolStrValue.UNDEFINED;
  }
}

export const HeaderBadgeSetting: React.FC<{
  page: PageEditor_PageQuery;
  refetchPage?: () => Promise<void>;
}> = ({ page, refetchPage }) => {
  const [updatingBadgeSetting, setUpdatingBadgeSetting] = useState(false);
  const { report } = useEditorContext();

  const [, updatePageShowBadges] = usePageEditor_UpdatePageShowBadgesMutation();

  const handleUpdateBadgeSetting = async (val: BoolStrValue) => {
    if(updatingBadgeSetting) {
      return;
    }
    setUpdatingBadgeSetting(true);
    const res = await updatePageShowBadges({
      input: {
        pageId: page.getPage.id,
        inherit: val === BoolStrValue.UNDEFINED,
        showBadges: val === BoolStrValue.TRUE
          ? true
          : val === BoolStrValue.FALSE
            ? false
            : undefined,
      },
    });
    if(res.error) {
      report(res.operation, res.error, () => handleUpdateBadgeSetting(val));
    }
    refetchPage && await refetchPage();
    setUpdatingBadgeSetting(false);
  }
  return (
    <Grid template="repeat(12, 1fr)" gap="2" data-setting-row="badges">
      <GridItem colSpan={{ base: 12, md: 12 }}>
        <FormControl id="page-editor-header-badges-value">
          <FormLabel>Badges</FormLabel>
          <InputGroup>
            <Select
              value={getBoolStrValue(page.getPage.showBadges)}
              onChange={e => handleUpdateBadgeSetting(e.target.value as BoolStrValue)}
            >
              <option value={BoolStrValue.UNDEFINED}>Inherit</option>
              <option value={BoolStrValue.TRUE}>Show Badges</option>
              <option value={BoolStrValue.FALSE}>Hide Badges</option>
            </Select>
          </InputGroup>
        </FormControl>
      </GridItem>
    </Grid>
  )
};
