Terms of Service
================

_Last Updated: July 10, 2021_

## The Gist
We (the folks at [pnouns.fyi][] and [identities.fyi][])
are here to help our queer/LGBTQIA+ community members be respected, via publishing platforms, and resources for community members and allies.
These Terms of Service ("Terms") describe our commitments to you, and your rights and responsibilities when using our services.
Please read them carefully and reach out to us if you have any questions.

These terms are available under a [Creative Commons Sharealike license](https://creativecommons.org/licenses/by-sa/4.0/),
which means that you're more than welcome to copy them, adapt them, and repurpose them for your own use.


## Terms of Service

These Terms govern your access to and use of the products and services we provide through or for [pnouns.fyi][] and [identities.fyi][] (collectively, "Services").
These Terms also govern visitors' access to and use of any websites that use our Services,
like the websites that our users create on [pnouns.fyi][].

Please read these Terms carefully before accessing or using our Services. By accessing or using any part of our Services, you agree to be bound by all of the Terms and all other operating rules, policies, and procedures that we may publish via the Services from time to time (collectively, the “Agreement”). You also agree that we may automatically change, update, or add on to our Services, and this Agreement will apply to any changes.

## 1. Who’s Who

“You” means any individual or entity using our Services. If you use our Services on behalf of another person or entity, you represent and warrant that you’re authorized to accept the Agreement on that person’s or entity’s behalf, that by using using our Services you're accepting the Agreement on behalf of that person or entity, and that if you, or that person or entity, violates the Agreement, you and that person or entity agree to be responsible to us.

We refer to the creator of pnouns.fyi and identities.fyi as pnouns or “we” throughout these Terms.

## 2. Your Account

When using our Services requires an account, you agree to provide us with complete and accurate information and to keep the information current so that we can communicate with you about your account. We may need to send you emails about notable updates (like changes to our Terms of Service or Privacy Policy), or to let you know about legal inquiries or complaints we receive about the ways you use our Services so you can make informed choices in response.

We realize that many of our users have changed their name (legally or not), are trying different names, or are questioning.
So are we!
Please use your name (whether or not it matches your legal name) or any other text on your profile and website;
we do not require you to list a legal name/birthname/deadname on your profile or website.

We may limit your access to our Services until we’re able to verify your account information, like your email address.

You’re solely responsible and liable for all activity under your account. You’re also fully responsible for maintaining the security of your account (which includes keeping your password secure). We’re not liable for any acts or omissions by you, including any damages of any kind incurred as a result of your acts or omissions. If you get fired because of a line on a profile you write about your boss, that’s on you.

Don’t share or misuse your access credentials. And notify us immediately of any unauthorized uses of your account, store, or website, or of any other breach of security. If we believe your account has been compromised, we may suspend or disable it.
If you'd like to learn about how we handle the data you provide us, please see our [Privacy Policy](https://pnouns.fyi/privacy/).

## 3. Minimum Age Requirements

Our Services are not directed to children. You’re not allowed to access or use our Services if you’re under the age of 13 (or 16 in Europe). If you register as a user or otherwise use our Services, you represent that you’re at least 13 (or 16 in Europe). You may use our Services only if you can legally form a binding contract with us. In other words, if you’re under 18 years of age (or the legal age of majority where you live), you can only use our Services under the supervision of a parent or legal guardian who agrees to the Agreement.

## 4. Responsibility of Visitors and Users

We allow our users to create profiles, websites, and pages (collectively, "user-made content").

We do not intend for our services to be used in a commercial manner - that is,
to advertise commercial content or sell content directly from our website (collectively, "commercial use").
We do welcome our users to link to their profiles on other sites and describe their interests, hobbies, and career in their user-made content if they wish, and welcome users to link coworkers to their user-made content, but do not allow direct commercial activity by our users on our platforms.

We haven’t reviewed, and can’t review, all of the content (like text, photo, video, audio, code, computer software, items for sale, and other materials) posted to or made available through our Services by users or anyone else (“Content”) or on websites that link to, or are linked from, our Services. We’re not responsible for any use or effects of Content or third-party websites. So, for example:

* We don’t have any control over third-party websites.
* A link to or from one of our Services does not represent or imply that we endorse any third-party website.
* We don’t endorse any Content or represent that Content is accurate, useful, or not harmful. Content could be offensive, indecent, or objectionable; include technical inaccuracies, typographical mistakes, or other errors; or violate or infringe the privacy, publicity rights, intellectual property rights, or other proprietary rights of third parties.
* You’re fully responsible for the Content available on your profile and any published pages, and any harm resulting from that Content. It’s your responsibility to ensure that your website’s Content abides by applicable laws and by the Agreement.
* We aren’t responsible for any harm resulting from anyone’s access, use, purchase, or downloading of Content, or for any harm resulting from third-party websites. You’re responsible for taking the necessary precautions to protect yourself and your computer systems from viruses, worms, Trojan horses, and other harmful or destructive content.
* We do not allow commercial use of our websites.  Any content or services that’s for sale through any of our Services is not allowed, and you must look solely to the seller for any damages that result from your purchase or use of Content.
* We are not a party to, and will have no responsibility or liability for, any communications, transactions, interactions, or disputes between you and the provider of any Content.

## 5. Feedback
We love hearing from you and are always looking to improve our Services. When you share comments, ideas, or feedback with us, you agree that we're free to use them without any restriction or compensation to you.

## 6. General Representation and Warranty

Our mission is to make the web a better place, and our Services are designed to give you control and ownership over your websites. We encourage you to express yourself freely, subject to a few requirements. In particular, you represent and warrant that your use of our Services:

* Will be in strict accordance with the Agreement;
* Will comply with all applicable laws and regulations (including, without limitation, all applicable laws regarding online conduct and acceptable content, privacy, data protection, the transmission of technical data exported from the United States or the country in which you reside, the use or provision of financial services, notification and consumer protection, unfair competition, and false advertising);
* Will not be for any unlawful purposes, to publish illegal content, or in furtherance of illegal activities;
* Will not infringe or misappropriate the intellectual property rights of Automattic or any third party;
* Will not overburden or interfere with our systems or impose an unreasonable or disproportionately large load on our infrastructure, as determined by us in our sole discretion;
* Will not disclose the personal information of others;
* Will not be used to send spam or bulk unsolicited messages;
* Will not interfere with, disrupt, or attack any service or network;
* Will not be used to create, distribute, or enable material that is, facilitates, or operates in conjunction with, malware, spyware, adware, or other malicious programs or code;
* Will not involve reverse engineering, decompiling, disassembling, deciphering, or otherwise attempting to derive the source code for the Services or any related technology that is not open source; and
* Will not involve renting, leasing, loaning, selling, or reselling the Services or related data without our consent.

## 7. Specific Service Terms

### a. pnouns.fyi Websites and Accounts

[pnouns.fyi][] enables you to create profile pages and websites (user-made content), and we would love for you to use it.

[pnouns.fyi][]'s service is currently free, without plans to monetize the service.
If we do add paid services, we will update our agreement to lay out the terms and your rights.

We don't own your content, and you retain all ownership rights you have in the content you post to your website. However, be responsible in what you publish. In particular, make sure that nothing prohibited (like spam, viruses, or serious threats of violence) appears on your website,
and that you do not use our services for commercial use.

If you find user content that you believe violates these Terms, please [let us know][report-content].

**Your website/page URL.**
If you create content on [pnouns.fyi][] you get use of a namespace - which appears as a subdomain (e.g. `alexs.pnouns.fyi`).
You must not engage in “domain squatting,” claim an unreasonable number of subdomains (as determined by us), or sell access to any subdomains.

You may create as many categories and pages in the namespace (e.g. `alexs.work.pnouns.fyi` or `alexs.pnouns.fyi/at/work/`) as you wish,
as long as all Terms are being followed.

We may allocate URLs (subdomains and paths) for your content; this does not grant ownership of subdomains or websites to our users.

**License.**
By uploading or sharing Content, you grant us a worldwide, royalty-free, transferable, sub-licensable, and non-exclusive license
to use, reproduce, modify, distribute, adapt, publicly display,
and publish the Content solely for the purpose of providing and improving our products and Services and promoting your website.

**Removing Content.**
If you delete Content, we’ll use reasonable efforts to remove it from public view (or in the case of a private website,
from view by the authorized visitors) on pnouns.fyi,
but you acknowledge that cached versions of the Content or references to the Content may not be immediately unavailable.

**Web Traffic.**
We may use internal services to measure pnouns.fyi's audience and usage,
that will measure usage both of our editing application as well as the usage of visitors viewing user-made Content.
Data will be stored according to our [privacy policy][]; web traffic is not stored by third party services.

**Prohibited Uses.**
Your Content and conduct must not violate the [pnouns.fyi Content Guidelines](https://pnouns.fyi/content-guidelines/).

**Advertisements.**
We may display advertisements on your website unless you have purchased a plan that includes the removal of ads.

**Attribution.**
We may display attribution text or links in your website footer or toolbar,
noting that your website is powered by pnouns.fyi and providing a link for others to create their own pages, for example.

## 8. Copyright Infringement and DMCA Policy

As we ask others to respect our intellectual property rights, we respect the intellectual property rights of others.
If you believe any Content violates your copyright,
please see our [Digital Millennium Copyright Act (“DMCA”) Policy][dmca-notice] and send us a notice.

## 9. Intellectual Property

The Agreement doesn’t transfer any pnouns or third-party intellectual property to you, and all right, title, and interest in and to such property remains (as between pnouns and you) solely with pnouns.  [pnouns.fyi][], [identities.fyi][], pnouns, and all other trademarks, service marks, graphics, and logos used in connection with our websites or Services are trademarks or registered trademarks of pnouns (or pnouns' licensors). Other trademarks, service marks, graphics, and logos used in connection with our Services may be the trademarks of other third parties. Using our Services doesn’t grant you any right or license to reproduce or otherwise use any pnouns or third-party trademarks.

## 10. Changes

We may update, change, or discontinue any aspect of our Services at any time. Since we’re constantly updating our Services, we sometimes have to change the legal terms under which they’re offered. The Agreement may only be modified by a written amendment signed by an authorized executive of pnouns, or if pnouns posts a revised version. We’ll let you know when there are changes: we’ll post them here and update the “Last Updated” date, and we may also post on one of our blogs or changelogs or send you an email or other communication before the changes become effective. Your continued use of our Services after the new terms take effect will be subject to the new terms, so if you disagree with the changes in the new terms, you should stop using our Services. To the extent you have an existing subscription, you may be eligible for a refund.

## 11. Termination

We may terminate your access to all or any part of our Services at any time,
with or without cause, with or without notice, effective immediately.

We have the right (though not the obligation) to, in our sole discretion,
(i) reclaim your username or website’s URL due to prolonged inactivity,
(ii) refuse or remove any content that, in our reasonable opinion, violates any the Agreement or any pnouns policy, or is in any way harmful or objectionable,
(iii) ask you to make adjustments, restrict the resources your website uses, or terminate your access to the Services, if we believe your website’s storage or bandwidth usage burdens our systems (which is rare and typically only occurs when a website is used for file sharing or storage), or
(iv) terminate or deny access to and use of any of our Services to any individual or entity for any reason. We will have no obligation to provide a refund of any fees previously paid.

You can stop using our Services at any time, or, if you use a Paid Service, you can cancel at any time, subject to the Fees, Payment, and Renewal section of these Terms.

## 12. Disclaimers

Our Services are provided “as is.” pnouns and its suppliers and licensors hereby disclaim all warranties of any kind, express or implied, including, without limitation, the warranties of merchantability, fitness for a particular purpose and non-infringement. Neither Automattic, nor its suppliers and licensors, makes any warranty that our Services will be error free or that access thereto will be continuous or uninterrupted. You understand that you download from, or otherwise obtain content or services through, our Services at your own discretion and risk.

## 13. Jurisdiction and Applicable Law.

Except to the extent any applicable law provides otherwise,
the Agreement and any access to or use of our Services will be governed by the laws of the Commonwealth of Massachusetts, U.S.A.,
excluding its conflict of law provisions.
The proper venue for any disputes arising out of or relating to the Agreement and any access to or use of our Services will be the state and federal courts located in the Commonwealth of Massachusetts.

## 14. Limitation of Liability

In no event will pnouns, or its suppliers, partners, or licensors,
be liable with respect to any subject matter of the Agreement under any contract, negligence, strict liability or other legal or equitable theory for:
(i) any special, incidental or consequential damages;
(ii) the cost of procurement for substitute products or services;
(iii) for interruption of use or loss or corruption of data;
or (iv) for any amounts that exceed $50 or the fees paid by you to pnouns under the Agreement during the twelve (12) month period prior to the cause of action, whichever is greater.
pnouns shall have no liability for any failure or delay due to matters beyond its reasonable control.
The foregoing shall not apply to the extent prohibited by applicable law.

## 15. Indemnification

You agree to indemnify and hold harmless pnouns, its contractors, and its licensors, and their respective directors, officers, employees, and agents from and against any and all losses, liabilities, demands, damages, costs, claims, and expenses, including attorneys’ fees, arising out of or related to your use of our Services, including but not limited to your violation of the Agreement or any agreement with a provider of third-party services used in connection with the Services, and any Content that you post.

## 16. US Economic Sanctions

You may not use the Services if such use is inconsistent with U.S. sanctions law or if you are on any list maintained by a U.S. government authority relating to designated, restricted or prohibited persons.

## 17. Translation

These Terms were originally written in English (US). We may translate these terms into other languages, and in the event of a conflict between a translated version of these Terms and the English version, the English version will control.

## 18. Miscellaneous

The Agreement (together with any other terms we provide that apply to any specific Service)
constitutes the entire agreement between pnouns and you concerning our Services.
If any part of the Agreement is unlawful, void, or unenforceable, that part is severable from the Agreement, and does not affect the validity or enforceability of the rest of the Agreement. A waiver by either party of any term or condition of the Agreement or any breach thereof, in any one instance, will not waive such term or condition or any subsequent breach thereof.
pnouns may assign its rights under the Agreement without condition.
You may only assign your rights under the Agreement with our prior written consent.

[pnouns.fyi]: https://pnouns.fyi
[identities.fyi]: https://identities.fyi
[privacy policy]: https://pnouns.fyi/privacy
[dmca-notice]: https://pnouns.fyi/dmca-notice/
[report-content]: https://pnouns.atlassian.net/servicedesk/customer/portal/1/create/19
