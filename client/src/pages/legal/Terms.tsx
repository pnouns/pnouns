import React, { useMemo } from "react";
import { Parser, ProcessNodeDefinitions } from "html-to-react";
import { Container, Heading } from "@chakra-ui/layout";
import { Link } from "../../components/Link";

import terms from "./terms.md";
import privacy from "./privacy.md";
import dmca from "./dmca.md";
import { LegalPage } from "./LegalPage";
import { Footer } from "../../components/Footer";

const headerSize = {
  1: "2xl",
  2: "xl",
  3: "lg",
  4: "md",
  5: "sm",
  6: "xs",
};

type HEADER_VALUE = "h1" | "h2" | "h3" | "h4" | "h5" | "h6";
function asHeader(level: number): HEADER_VALUE {
  return `h${level}` as HEADER_VALUE;
}

const Legal: React.FC<{
  page: LegalPage,
}> = ({ page }) => {
  const parser = useMemo(() => new Parser(), []);
  const isValidNode = () => true;
  const processNodeDefs = new ProcessNodeDefinitions(React);
  const text = page === LegalPage.TERMS
    ? terms
    : page === LegalPage.PRIVACY
      ? privacy
      : dmca;
  const component = parser.parseWithInstructions(text, isValidNode, [
    {
      shouldProcessNode: node => node.name === "a"
        && node.attribs
        && node.attribs.href
        && !node.attribs["data-identity"]
        && !node.attribs["data-glossary"],
      processNode: (node, children, index) => {
        return <Link path={node.attribs.href} external color="cyan.500">{ children }</Link>;
      },
    },
    {
      shouldProcessNode: node => node.name && node.name.length > 0 && node.name[0] === "h",
      processNode: (node, children, index) => {
        const level = parseInt(node.name[1], 10);
        return <Heading as={asHeader(level)} size={headerSize[level]}>{children}</Heading>
      },
    },
    {
      shouldProcessNode: node => true,
      processNode: processNodeDefs.processDefaultNode,
    },
  ]);
  return (
    <Container maxW="container.lg">
      {component}
      <Footer />
    </Container>
  );
}

export default Legal;
