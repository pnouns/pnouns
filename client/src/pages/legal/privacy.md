Privacy Policy
==============

Your privacy is critically important to us. At pnouns, we have a few fundamental principles:

*   We are thoughtful about the personal information we ask you to provide and the personal information that we collect about you through the operation of our services.
*   We help protect you from overreaching government demands for your personal information.
*   We aim for full transparency on how we gather, use, and share your personal information.

Below is our Privacy Policy, which incorporates and clarifies these principles.

### Who We Are and What This Policy Covers

Howdy!  We are the folks behind a variety of products and services designed to help
our queer/LGBTQIA+ community members be respected,
via publishing platforms, and resources for community members and allies. This Privacy Policy applies to information that we collect about you when you use:

*   Our websites (including pnouns.fyi and identities.fyi);
*   Communications and sent by our services, including authentication confirmations;
*   Other users’ websites that use our Services, while you are logged in to your account with us.

Below we explain how we collect, use, and share information about you, along with the choices that you have with respect to that information.

### Creative Commons Sharealike License

We’ve decided to make this Privacy Policy available under a [Creative Commons Sharealike](https://creativecommons.org/licenses/by-sa/4.0/) license.
You’re more than welcome to copy it, adapt it, and repurpose it for your own use.
Just make sure to revise the language so that your policy reflects your actual practices.

### Information We Collect

We only collect information about you if we have a reason to do so — for example, to provide our Services, to communicate with you, or to make our Services better. We collect this information from three sources: if and when you provide information to us, automatically through operating our Services, and from outside sources. Let’s go over the information that we collect.

#### _Information You Provide to Us_

It’s probably no surprise that we collect information that you provide to us directly. Here are some examples:

* **Basic account information:**
  We ask for basic information from you in order to set up your account.
  For example, we require individuals who sign up for a pnouns.fyi account to provide an email address,
  or details of an account on another platform to login and that’s it.
  You may provide us with more information — like your name and other information you want to share -
  but we don’t require that information to create a pnouns.fyi account.
* **Nicknames:**
  We allow each account to have multiple nicknames - effectively granting you multiple usernames that are not tied together.
  The fact that a nickname has been taken is public, although not tied back to your user account.
  The list of nicknames associated with your account is visible inside the editor.
  If you wish to not tie nicknames together, you must ensure your account is secure and ensure people can't physically see the page editor
  inside your account.
* **Personal information on pages:**
  Some page builders, such as the system at pnouns.fyi, is designed to share personal information -
  you may choose to list legal or personal names, and details in your profile.
  These details can be published publicly, so please keep that in mind when deciding what information you would like to include.
* **Website token ("password") protection:**
  Some of our platforms include features to protect your site with a weak "password" or token,
  such as the page builder at pnouns.fyi.
  These passwords provide some security, but have limits and are not designed to be cryptographically secure:
  * Due to URL limitations, passwords are limited to a smaller set of allowed characters;
  * Page tokens are stored in plaintext, allowing us to provide you links to view your published page;
  * The token is included in the page URL, which may be potentially stored in your browser and in access logs;
  * The token is designed to be shared with the content so others may use it; you have limited control to prevent re-sharing of the token;
  * Your nickname and category pages can optionally share sub-page passwords with users (this is an opt-in setting in the editor).

  Page "passwords" (tokens) may provide enough security for your use-case.  They make it harder to guess category page names,
  especially as we suggest a pre-defined list of categories.

  Please note that we take security seriously, and are sharing these limitations up-front to ensure that everyone is on the same page
  about what page tokens can and can't do.

  If you may be put in an unsafe or uncomfortable position if the page token was shared or otherwise obtained,
  we recommend you do not rely on page tokens for safety's sake.

  Members of our pnouns team do use page "password" tokens, and rely on passwords for personal comfort of coming out,
  but do not depend on passwords for safety,
  but this is a judgement call that members of our team have individually made.
* **Communications with us (hi there!):**
  You may also provide us with information when you respond to surveys,
  communicate with our helpdesk with a support question or suggestion,
  or sign up for a newsletter.
  When you communicate with us via form, email, phone, or otherwise, we store a copy of our communications
  (including any call recordings as permitted by applicable law).

#### _Information We Collect Automatically_

We also collect some information automatically:

* **Log information:**
  Like most online service providers, we collect information that web browsers, mobile devices, and servers typically make available,
  including the browser type, IP address, unique device identifiers, language preference, referring site, the date and time of access,
  operating system, and mobile network information.
  We collect log information when you use our Services — for example, when you create or make changes to your website or pages on pnouns.fyi.
* **Usage information:**
  We collect information about your usage of our Services.
  For example, we collect information about the actions that site administrators and users perform on a site using our pnouns.fyi -
  in other words, who did what and when (e.g., \[pnouns.fyi user id\] published “\[name of page\]” at \[time/date\]).
  We also collect information about what happens when you use our Services
  (e.g., page views, support document searches, features enabled for your website, interactions with other parts of our Services)
  along with information about your device (e.g., screen size, name of cellular network, and mobile device manufacturer).
  We use this information to, for example, provide our Services to you,
  get insights on how people use our Services so we can make our Services better, and understand and make predictions about user retention.
* **Location information:**
  We may determine the approximate location of your device from your IP address.
  We collect and use this information to, for example, calculate how many people visit our Services from certain geographic regions.
* **Interactions with other users’ sites:**
  We collect some information about your interactions with other users’ sites while you are logged in to your account with us,
  and connect this activity to your user account.
  This is used to separate out known users from new users discovering pnouns.fyi pages for the first time, and similar analytics.
* **Information from first-party cookies & other technologies:**
  A cookie is a string of information that a website stores on a visitor’s computer,
  and that the visitor’s browser provides to the website each time the visitor returns.
  We use cookies and similar browser storage technologies to store login tokens (so you can reload or return to the site and still be logged in).
  We also may use cookies and similar storage to store preferences locally, such as if you've seen news announcements, or marked your preferences in regards to cookies and other privacy dialogs.
  Finally, we use cookies and other technologies to help us identify and track visitors, usage, and access preferences for our Services.
* **Information about third-party cookies & other technologies:**
  Services we use on our site and externally for support requests,
  showing website status and informing users of downtime/maintenance,
  showing product announcements, and other services,
  may use their own cookies or other technology that is out of our control.
  We do not link any details from pnouns.fyi with any of these third-party services,
  but third-party services may already have information about you,
  such as if you create an account with our support portal provider.
* **Off-website tracking pixels & other technologies:**
  Pixel tags (also called web beacons) are small blocks of code placed on websites and emails.
  Pnouns uses pixel tags and other technologies to help us identify and track visitors, usage, and access preferences via email campaigns
  and other off-website systems.

For more information about our use of cookies and other technologies for tracking,
including how you can control the use of cookies, please see our [Cookie Policy][].

#### _Information We Collect from Other Sources_

We may also get information about you from other sources.
For example, if you create or log in to your pnouns.fyi account through another service (like Google or Discord),
we’ll receive information from that service (e.g., your username, basic profile information) via the authorization procedures for that service.
The information we receive depends on which services you use or authorize and what options are available.
Third-party services may also give us information, like mailing addresses for individuals who are not yet our users (but we hope will be!).
We use this information for marketing purposes like postcards and other mailers advertising our Services.

### How and Why We Use Information

#### _Purposes for Using Information_

We use information about you for the purposes listed below:

* **To provide our Services.**
  For example, to set up and maintain your account, host your website and pages,
  backup and restore your website, provide customer service, and verify user information.
* **To ensure quality, maintain safety, and improve our Services.**
  For example, by providing automatic upgrades and new versions of our Services.
  Or, for example, by monitoring and analyzing how users interact with our Services
  so we can create new features that we think our users will enjoy and that will help them
  create and manage websites and pages more efficiently or make our Services easier to use.
* **To market our Services and measure, gauge, and improve the effectiveness of our marketing.**
  For example, by targeting our marketing messages to groups of our users
  (like those who have a particular plan with us or have been users for a certain length of time),
  advertising our Services, analyzing the results of our marketing campaigns
  (like how many people purchased a paid plan after receiving a marketing message),
  and understanding and forecasting user retention.
* **To protect our Services, our users, and the public.**
  For example, by detecting security incidents; detecting and protecting against malicious, deceptive, fraudulent, or illegal activity;
  fighting spam; complying with our legal obligations; and protecting the rights and property of pnouns and others,
  which may result in us, for example, declining a transaction or terminating Services.
* **To fix problems with our Services.**
  For example, by monitoring, debugging, repairing, and preventing issues.
* **To customize the user experience.**
  For example, to remember your cookie preferences, or mark that you have read notes about our newest features.
* **To communicate with you.**
  For example, by emailing you to ask for your feedback, share tips for getting the most out of our products,
  or keep you up to date on pnouns;

#### _Legal Bases for Collecting and Using Information_

A note here for those in the European Union about our legal grounds for processing information about you under EU data protection laws, which is that our use of your information is based on the grounds that: (1) The use is necessary in order to fulfill our commitments to you under the applicable terms of service or other agreements with you or is necessary to administer your account — for example, in order to enable access to our website on your device or charge you for a paid plan; or (2) The use is necessary for compliance with a legal obligation; or (3) The use is necessary in order to protect your vital interests or those of another person; or (4) We have a legitimate interest in using your information — for example, to provide and update our Services; to improve our Services so that we can offer you an even better user experience; to safeguard our Services; to communicate with you; to measure, gauge, and improve the effectiveness of our advertising; and to understand our user retention and attrition; to monitor and prevent any problems with our Services; and to personalize your experience; or (5) You have given us your consent — for example before we place certain cookies on your device and access and analyze them later on, as described in our [Cookie Policy][].

### Sharing Information

#### _How We Share Information_

We share information about you in limited circumstances, and with appropriate safeguards on your privacy. These are spelled out below:

* **Subsidiaries and independent contractors:**
  We may disclose information about you to our subsidiaries and independent contractors who need the information to help us provide our Services
  or process the information on our behalf.
  We require our subsidiaries and independent contractors to follow this Privacy Policy for any personal information that we share with them.
* **Third-party vendors:**
  We may share information about you with third-party vendors who need the information in order to provide their services to us.
  This includes vendors that
    help us provide our Services to you,
    cloud storage services,
    postal and email delivery services that help us stay in touch with you,
    customer chat and email support services that help us communicate with you,
    those that make tools to help us run our operations (like programs that help us with task management, scheduling, word processing, email and other communications, and collaboration among our teams).
    We require vendors to agree to privacy commitments in order to share information with them.
    Other vendors are listed in our more specific policies (e.g., our [Cookie Policy][]).
* **Legal and regulatory requirements:**
  We may disclose information about you in response to a subpoena, court order, or other governmental request.
  For more information on how we respond to requests for information about pnouns.fyi users, please see our [Legal Guidelines][legal-sharing].
* **To protect rights, property, and others:**
  We may disclose information about you when we believe in good faith that disclosure is reasonably necessary to protect the property or rights of pnouns, third parties, or the public at large. For example, if we have a good faith belief that there is an imminent danger of death or serious physical injury, we [may disclose information related to the emergency without delay][legal-sharing].
* **Business transfers:**
  In connection with any merger, sale of company assets, or acquisition of all or a portion of our business by another company,
  or in the unlikely event that Pnouns goes out of business or enters bankruptcy,
  user information would likely be one of the assets that is transferred or acquired by a third party.
  If any of these events were to happen, this Privacy Policy would continue to apply to your information and the party receiving your information may continue to use your information, but only consistent with this Privacy Policy.
* **Aggregated or de-identified information:** We may share information that has been aggregated or de-identified, so that it can no longer reasonably be used to identify you. For instance, we may publish aggregate statistics about the use of our Services, or share a hashed version of your email address to facilitate customized ad campaigns on other platforms.
* **Published support requests:** If you send us a request for assistance (for example, via a support email or one of our other feedback mechanisms), we reserve the right to publish that request in order to clarify or respond to your request, or to help us support other users.

We have a long-standing policy that we do not sell our users' data. We aren't a data broker, we don't sell your personal information to data brokers, and we don't sell your information to other companies that want to spam you with marketing emails.

#### _Information Shared Publicly_

Information that you choose to make public is — you guessed it — disclosed publicly.
That means information like your public profile, pages, other content that you make public on your website, are all available to others — and we hope they get a lot of views!
Public information may also be indexed by search engines or used by third parties. Please keep all of this in mind when deciding what you would like to share publicly.

As stated above, we support features such as page "passwords" (tokens) that may limit some access to your published materials.
We will never intentionally share your password with others, but tokens have inherent risks and shouldn't be treated as if they will
prevent any unauthorized sharing of your website content.

### How Long We Keep Information

We generally discard information about you when it’s no longer needed for the purposes for which we collect and use it.
We may continue storing data that you mark as deleted for a short period, in case you would like to change your mind.

### Security

While no online service is 100% secure, we work very hard to protect information about you
against unauthorized access, use, alteration, or destruction, and take reasonable measures to do so.
We monitor our Services for potential vulnerabilities and attacks.

### Choices

You have several choices available when it comes to information about you:

* **Limit the information that you provide:**
  If you have an account with us, you can choose not to provide the optional account information and information listed on your pages.
* **Opt out of marketing communications:**
  You may opt out of receiving promotional communications from us.
  Just follow the instructions in those communications or let us know.
  If you opt out of promotional communications, we may still send you other communications, like those about your account and legal notices.
* **Set your browser to reject cookies:**
  At this time, pnouns does not respond to “do not track” signals across all of our Services.
  However, you can [usually choose](https://automattic.com/cookies/#controlling-cookies) to set your browser to remove or reject browser cookies
  before using pnouns' websites, with the drawback that certain features of pnouns' websites may not function properly without the aid of cookies,
  including parts of user authentication.
* **Close your account:**
  While we’d be very sad to see you go, you can close your account if you no longer want to use our Services.
  ([Here][close-account] are account closure instructions for pnouns.fyi accounts.)
  Please keep in mind that we may continue to retain your information after closing your account,
  as described in How Long We Keep Information above — for example, when that information is reasonably needed to comply with (or demonstrate our compliance with) legal obligations such as law enforcement requests, or reasonably needed for our legitimate business interests.

### Your Rights

If you are located in certain parts of the world, including California and countries that fall under the scope of the European General Data Protection Regulation (aka the “GDPR”), you may have certain rights regarding your personal information, like the right to request access to or deletion of your data.

#### _European General Data Protection Regulation (GDPR)_

If you are located in a country that falls under the scope of the GDPR, data protection laws give you certain rights with respect to your personal data, subject to any exemptions provided by the law, including the rights to:

*   Request access to your personal data;
*   Request correction or deletion of your personal data;
*   Object to our use and processing of your personal data;
*   Request that we limit our use and processing of your personal data; and
*   Request portability of your personal data.

You also have the right to make a complaint to a government supervisory authority.

#### _California Consumer Privacy Act (CCPA)_

The California Consumer Privacy Act (“CCPA”) requires us to provide California residents with some additional information about the categories of personal information we collect and share, where we get that personal information, and how and why we use it. The CCPA also requires us to provide a list of the “categories” of personal information we collect, as that term is defined in the law, so, here it is. In the last 12 months, we collected the following categories of personal information from California residents, depending on the Services used:

*   Identifiers (like your name, contact information, and device and online identifiers);
*   Commercial information (your billing information and purchase history, for example);
*   Characteristics protected by law (for example, you might provide your gender as part of a research survey for us);
*   Internet or other electronic network activity information (such as your usage of our Services, like the actions you take as an administrator of a WordPress.com site);
*   Geolocation data (such as your location based on your IP address);
*   Audio, electronic, visual or similar information (such as your profile picture, if you uploaded one);
*   Professional or employment-related information (for example, your company and team information if you are a Happy Tools user, or information you provide in a job application); and
*   Inferences we make (such as likelihood of retention or attrition).

You can find more information about what we collect and sources of that information in the "Information We Collect" section above.
We collect personal information for the business and commercial purposes described in the "How and Why We Use Information" section.
And we share this information with the categories of third parties described in the "Sharing Information" section.
If you are a California resident, you have additional rights under the CCPA, subject to any exemptions provided by the law, including the right to:

*   Request to know the categories of personal information we collect, the categories of business or commercial purpose for collecting and using it, the categories of sources from which the information came, the categories of third parties we share it with, and the specific pieces of information we collect about you;
*   Request deletion of personal information we collect or maintain;
*   Opt out of any sale of personal information; and
*   Not receive discriminatory treatment for exercising your rights under the CCPA.

#### _Contacting Us About These Rights_

You can usually access, correct, or delete your personal data using your account settings and tools that we offer, but if you aren’t able to or you’d like to contact us about one of the other rights, scroll down to "How to Reach Us" to, well, find out how to reach us. When you contact us about one of your rights under this section, we’ll need to verify that you are the right person before we disclose or delete anything. For example, if you are a user, we will need you to contact us from the email address associated with your account. You can also designate an authorized agent to make a request on your behalf by giving us written authorization. We may still require you to verify your identity with us.

### How to Reach Us

If you have a question about this Privacy Policy,
or you would like to contact us about any of the rights mentioned in the "Your Rights" section above,
please contact us through our [web form][contact-form] or via [email](mailto:fly@pnouns.fyi).

### Other Things You Should Know (Keep Reading!)

#### _Transferring Information_

Because pnouns Services are offered worldwide, the information about you that we process when you use the Services in the EU may be used, stored, and/or accessed by individuals operating outside the European Economic Area (EEA) who work for us, other members of our group of companies, or third-party data processors. This is required for the purposes listed in the "How and Why We Use Information" section above. When providing information about you to entities outside the EEA, we will take appropriate measures to ensure that the recipient protects your personal information adequately in accordance with this Privacy Policy as required by applicable law. These measures include entering into European Commission approved standard contractual arrangements with entities based in countries outside the EEA. You can ask us for more information about the steps we take to protect your personal information when transferring it from the EU.

#### _Ads and Analytics Services Provided by Others_

Ads appearing on any of our Services may be delivered by advertising networks. Other parties may also provide analytics services via our Services. These ad networks and analytics providers may set tracking technologies (like cookies) to collect information about your use of our Services and across other websites and online services. These technologies allow these third parties to recognize your device to compile information about you or others who use your device. This information allows us and other companies to, among other things, analyze and track usage, determine the popularity of certain content, and deliver ads that may be more targeted to your interests. Please note this Privacy Policy only covers the collection of information by Automattic and does not cover the collection of information by any third-party advertisers or analytics providers.

#### _Visitors to Our Users’ Websites_

We also process information about visitors to our users’ websites, on behalf of our users and in accordance with our user agreements.

### Privacy Policy Changes

Although most changes are likely to be minor, pnouns may change its Privacy Policy from time to time.
pnouns encourages visitors to frequently check this page for any changes to its Privacy Policy.
If we make changes, we will notify you by revising the change log below, and, in some cases, we may provide additional notice (like adding a statement to our homepage or the our changelog, or sending you a notification through email or your dashboard). Your further use of the Services after a change to our Privacy Policy will be subject to the updated policy.

### Translation

Our Privacy Policy was originally written in English (US). We may translate it into other languages.
In the event of a conflict between a translated version of our Privacy Policy and the English version, the English version will control.
That’s it! Thanks for reading.

## Changelog

_July 10, 2021_ Published

[Cookie Policy]: https://pnouns.fyi/cookies/
[legal-sharing]: https://pnouns.fyi/legal-guidelines/
[close-account]: https://pnouns.freshdesk.com/en/support/solutions/articles/69000502583-close-my-account
[contact-form]: https://pnouns.freshdesk.com/support/tickets/new
