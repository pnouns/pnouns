import React from "react";
import {
  Badge,
  Box,
  Container,
  Heading,
} from "@chakra-ui/react";
import { Header } from "../../components/Header";
import { RequireAuth } from "../../components/RequireAuth";
import { usePageList_MyPagesQuery } from "../../generated-types";
import { Link } from "../../components/Link";

const PageList: React.FC<{}> = ({}) => {
  const [{ data, fetching, error }, refetchPages] = usePageList_MyPagesQuery();
  const pages = data?.myPages ?? [];
  return (
    <RequireAuth>
      <Header noAuth />
      <Container
        maxW="container.lg"
      >
        <Heading size="lg">Pages</Heading>
        { pages.map(page => (
          <Box w="full" borderWidth="1px" borderRadius="lg" overflow="hidden">
            <Box className="page-list-card-details" p="6">
              <Box className="page-list-page-header" d="flex" alignItems="baseline">
                <Badge borderRadius="full" p="2" colorScheme="teal">
                  {page.nickname.displayName ?? page.nickname.id}
                </Badge>
              </Box>
              <Box mt="1" fontWeight="semibold" as="h4" lineHeight="tight" isTruncated>
                { page.displayName ?? page.name }
              </Box>
              <Link path={`/page/${page.id}/`}>Edit</Link>
            </Box>
          </Box>
        ))}
      </Container>
    </RequireAuth>
  );
};

export default PageList;
