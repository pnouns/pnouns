import React from "react";
import {
  Center,
  CircularProgress,
} from "@chakra-ui/react";

export const Loading: React.FC<{}> = () => {
  return (
    <Center h="100%">
      <CircularProgress isIndeterminate />
    </Center>
  );
};
