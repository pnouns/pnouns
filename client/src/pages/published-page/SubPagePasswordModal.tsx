import React, { useState } from "react";
import { Button } from "@chakra-ui/button";
import { FormControl, FormLabel } from "@chakra-ui/form-control";
import { Input } from "@chakra-ui/input";
import { Text } from "@chakra-ui/layout";
import { Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay } from "@chakra-ui/modal";

const SubPagePasswordModal: React.FC<{
  isOpen: boolean,
  url: string,
  onClose: () => void,
}> = ({
  isOpen,
  url,
  onClose,
}) => {
  const [password, setPassword] = useState("");

  const tryOpen = () => {
    const target = `${url}?password=${password}`;
    const opened = window.open(target, "_blank");
    opened && opened.focus();
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Enter Password</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Text>This page requires authentication.  Please enter the password:</Text>
          <FormControl>
            <FormLabel>Page Password:</FormLabel>
            <Input value={password} onChange={e => setPassword(e.target.value)} />
          </FormControl>
        </ModalBody>
        <ModalFooter>
          <Button variant="ghost" mr={3} onClick={onClose}>Close</Button>
          <Button colorScheme="blue" onClick={tryOpen}>Submit Password</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default SubPagePasswordModal;
