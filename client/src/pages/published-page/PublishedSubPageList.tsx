import React, { useState } from "react";
import loadable from "@loadable/component";
import { useDisclosure } from "@chakra-ui/hooks";
import { Box, Center, Flex, Grid, GridItem, Heading, Text } from "@chakra-ui/layout";
import { PublishedPageQuery } from "../../generated-types";
import { EditorContainer } from "../../components/SectionDisplay";
import { Button } from "@chakra-ui/button";
import { ChevronRightIcon, LockIcon } from "@chakra-ui/icons";
import { ButtonLink } from "../../components/Link";

const SubPagePasswordModal = loadable(() => import("./SubPagePasswordModal"), {
  fallback: <></>,
});

const PublishedSubPageList: React.FC<{
  page: PublishedPageQuery,
}> = ({ page: response }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [passwordModalUrl, setPasswordModalUrl] = useState("");
  if(!response || !response.getPublishedPage) {
    return <></>;
  }
  const page = response.getPublishedPage;
  if(!page.listedSubPages || !Array.isArray(page.listedSubPages) || page.listedSubPages.length === 0) {
    return <></>;
  }

  const anyPageNeedsPassword = page.listedSubPages.find(i => i.needsPassword);

  const order = page.listedSubPageOrder ?? [];

  return (
    <>
      <Heading as="h2" size="lg">Sub-Pages</Heading>
      {
        page.listedSubPages
          .sort((a,b) => order.indexOf(a.id) - order.indexOf(b.id))
          .map(subPage => (
            <Box my="4">
              <EditorContainer>
                <Flex>
                  <Box flex="1">
                    <Grid templateColumns="repeat(12, 1fr)" gap="2">
                      <GridItem colSpan={!subPage.subtitle ? { base: 12} : {
                        base: 12,
                        md: 6,
                      }}>
                        <Heading as="h3" size="md" flex="6">{ subPage.title }</Heading>
                      </GridItem>
                      {subPage.subtitle && (
                        <GridItem colSpan={{
                          base: 12,
                          md: 6,
                        }}>
                          <Heading size="md" flex="6" color="gray.300">{ subPage.subtitle }</Heading>
                        </GridItem>
                      )}
                    </Grid>
                    {subPage.caption && (
                      <Text>{ subPage.caption }</Text>
                    )}
                    <Text fontSize="sm" color="purple.300" mt="1">
                      { subPage.url }
                    </Text>
                  </Box>
                  <Center>
                    {
                      subPage.needsPassword
                        ? <Button leftIcon={<LockIcon />} onClick={() => {
                          setPasswordModalUrl(subPage.url);
                          onOpen();
                        }}>Open</Button>
                        : <ButtonLink path={subPage.url} rightIcon={<ChevronRightIcon />} newTab external>Open</ButtonLink>
                    }
                  </Center>
                </Flex>
              </EditorContainer>
            </Box>
          ))
      }
      { anyPageNeedsPassword && <SubPagePasswordModal isOpen={isOpen} onClose={onClose} url={passwordModalUrl} /> }
    </>
  );

};

export default PublishedSubPageList;
