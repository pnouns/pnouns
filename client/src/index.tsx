import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import { ChakraProvider } from "@chakra-ui/react";
import { createClient as createUrqlClient, makeOperation, Exchange, Operation, Provider as UrqlProvider, fetchExchange, dedupExchange, cacheExchange } from "urql";
import { authExchange } from "@urql/exchange-auth";
import { devtoolsExchange } from "@urql/devtools";
import posthog from "posthog-js";
import { theme } from "./theme";
import { UserProvider, loadJwt } from "./context/user.context";
import { Loading } from "./pages/Loading";
import { matchPublishedPage } from "./util/page-matching";

type GraphClientAuth = {
  token: string;
};

type GraphClientAuthStored = GraphClientAuth | null;

async function graphqlClientGetAuth(
  { authState }: { authState: GraphClientAuthStored },
): Promise<GraphClientAuthStored> {
  if(!authState) {
    const token = loadJwt();
    if(token) {
      return { token };
    }
    return null;
  }
  return null;
}

function graphqlClientAddAuthToOperation(
  { authState, operation }: { authState: GraphClientAuthStored, operation: Operation },
): Operation {
  /*if(!authState || !authState.token) {
    return operation;
  }
  token = authState.token;
  */
  const token = loadJwt();
  if(!token) {
    return operation;
  }
  const fetchOptions = typeof operation.context.fetchOptions === "function"
    ? operation.context.fetchOptions()
    : operation.context.fetchOptions || {};

  return makeOperation(operation.kind, operation, {
    ...operation.context,
    fetchOptions: {
      ...fetchOptions,
      headers: {
        ...fetchOptions.headers,
        Authorization: `Bearer ${token}`,
      },
    },
  });
}

const graphClient = createUrqlClient({
  url: (window as any).PNOUNS_GRAPHQL ?? "/graphql",
  exchanges: [
    devtoolsExchange as any as Exchange,
    dedupExchange,
    cacheExchange,
    authExchange<GraphClientAuth>({
      getAuth: graphqlClientGetAuth,
      addAuthToOperation: graphqlClientAddAuthToOperation,
    }) as any as Exchange,
    fetchExchange,
  ],
});

const PublishedPage = React.lazy(() => import("./pages/PublishedPage"));
const BaseApp = React.lazy(() => import("./BaseApp"));
const Error404Page = React.lazy(() => import("./pages/404"));

const Router: React.FC<{}> = () => {
  const { hostname, pathname } = window.location;
  const resolvedHost = hostname.replace("localhost.pnouns.fyi", "pnouns.fyi");
  const url = `${resolvedHost}${pathname}`;
  const published = matchPublishedPage(url);
  if(published) {
    return (
      <Suspense fallback={<Loading />}>
        <PublishedPage page={published} />
      </Suspense>
    );
  }
  if(resolvedHost === "pnouns.fyi") {
    return (
      <Suspense fallback={<Loading />}>
        <BaseApp />
      </Suspense>
    );
  }
  console.error(`Unknown url: ${url}`);
  return (
    <Suspense fallback={<Loading />}>
      <Error404Page />
    </Suspense>
  );
};

const App: React.FC<{}> = () => {
  posthog.init("phc_fpKd5wySVaKTL28A3LwUneTs5h4xuej00ZOwrc9aNqJ", {
    api_host: "https://hog.pnouns.fyi",
  });
  return (
    <UrqlProvider value={graphClient}>
      <UserProvider>
        <ChakraProvider theme={theme}>
          <Router />
        </ChakraProvider>
      </UserProvider>
    </UrqlProvider>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
