import React, { Suspense } from "react";
import { lazy } from "@loadable/component";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Loading } from "./pages/Loading";
import Homepage from "./pages/homepage/Homepage";
import { LegalPage } from "./pages/legal/LegalPage";

const About = lazy(() => import("./pages/About"));
const PageEditor = lazy(() => import("./pages/page-editor/PageEditor"));
const PageList = lazy(() => import("./pages/page-list/PageList"));
const Legal = lazy(() => import("./pages/legal/Terms"));

/**
 * BaseApp - for pages on the core 'pnouns.fyi' domain.
 * Split out from page rendering for improved performance, so end pages can load directly without react-router.
 */

const BaseApp: React.FC<{}> = () => {
  return (
    <Router>
      <Suspense fallback={<Loading />}>
        <Switch>
          <Route exact path="/about" component={About} />
          <Route exact path="/terms" render={() => <Legal page={LegalPage.TERMS} />} />
          <Route exact path="/privacy" render={() => <Legal page={LegalPage.PRIVACY} />} />
          <Route exact path="/dmca" render={() => <Legal page={LegalPage.DMCA} />} />
          <Route exact path="/" component={Homepage} />
          <Route exact path="/pages/" component={PageList} />
          <Route exact path="/page/:pageId/" component={PageEditor} />
        </Switch>
      </Suspense>
    </Router>
  );
};

export default BaseApp;
