export default {
  typescript: {
    extensions: [
      "ts",
      "tsx",
    ],
    rewritePaths: {
      "src/": "out-test/src/",
      "test/": "out-test/test/",
    },
    compile: "tsc",
  },
  files: [
    "test/**/*",
  ],
};
