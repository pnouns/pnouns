import test, { Macro } from "ava";
import { matchPublishedPage } from "../../../../src/util/page-matching";

type MatchValue = {
  namespace: string;
  category?: string;
  page?: string;
}

const matches: Macro<[string, undefined | MatchValue]> = (t, url, expected) => {
  const res = matchPublishedPage(url);
  if(expected === undefined) {
    return t.true(res === undefined, `expected ${url} to not match.`);
  }
  if(!res || typeof res !== "object") {
    return t.fail(`Got back ${res}, expected ${JSON.stringify(expected)}`);
  }
  t.log(`found namespace=${res.namespace} category=${res.category} page=${res.page}`);
  t.is(res.category, expected.category);
  t.is(res.namespace, expected.namespace);
  t.is(res.page, expected.page);
};
matches.title = (given, url, expected) => given || ((expected && typeof expected === "object" && expected !== undefined) ?
  `${url} parsed to namespace=${expected.namespace} category=${expected.category} page=${expected.page}`
  : `${url} to not match`);

test(matches, "flyyns.work.pnouns.fyi/acme-corp/", {
  namespace: "flyyns",
  category: "work",
  page: "acme-corp",
});

test(matches, "flyyns.work.pnouns.fyi/", {
  namespace: "flyyns",
  category: "work",
});

test(matches, "flyyns.pnouns.fyi/for/work/at/acme-corp", {
  namespace: "flyyns",
  category: "work",
  page: "acme-corp",
});

test(matches, "flyyns.pnouns.fyi/for/jones/family", {
  namespace: "flyyns",
  category: "jones",
  page: "family",
});

test(matches, "flyyns.pnouns.fyi/at/school/", {
  namespace: "flyyns",
  category: "school",
});

test(matches, "flyyns.pnouns.fyi/work/acme-corp/", {
  namespace: "flyyns",
  category: "work",
  page: "acme-corp",
});

test(matches, "flyyns.pnouns.fyi/family/", {
  namespace: "flyyns",
  category: "family",
});

test(matches, "flyyns.pnouns.fyi", {
  namespace: "flyyns",
});
