const PnpWebpackPlugin = require("pnp-webpack-plugin");
const RemarkHTML = require("remark-html");
//const ModernNpmPlugin = require("webpack-plugin-modern-npm");

module.exports = {

  entry: "./src/index.tsx",

  output: {
    filename: "[name].bundle.js",
    chunkFilename: "[name].bundle.js",
    path: __dirname + "/dist",
    publicPath: "/dist/",
  },

  resolve: {
    plugins: [
      PnpWebpackPlugin,
    ],
    extensions: [ ".ts", ".tsx", ".js", ".jsx", ".json", ".css", ".sass", ".md" ],
  },

  resolveLoader: {
    plugins: [
      PnpWebpackPlugin.moduleLoader(module),
    ],
  },

  plugins: [
    //new ModernNpmPlugin(),
  ],

  module: {
    rules: [

      {
        test: /\.md$/,
        rules: [
          {
            loader: "html-loader",
          },
          {
            loader: "remark-loader",
            options: {
              remarkOptions: {
                plugins: [RemarkHTML],
              },
            },
          },
        ],
      },

      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        options: {
          configFile: "tsconfig.browser.json",
        },
      },

      {
        test: /\.s?css$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
      },

      /*{
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader",
      },*/

      {
        test: /\.(woff|woff2|eot|ttf|svg)$/,
        //include: /node_modules/,
        loader: 'file-loader',
        options: {
          limit: 1024,
          name: '[name].[ext]',
          publicPath: '/dist/assets/',
          outputPath: 'assets/'
        },
      },

      {
        test: /\.js$/,
        exclude: /@babel(?:\/|\\{1,2})runtime/,
        loader: require.resolve("babel-loader"),
        options: {
          babelrc: false,
          configFile: false,
          compact: false,
          presets: [
            "@babel/preset-env",
            [
              require.resolve("babel-preset-react-app/dependencies"),
              { helpers: true },
            ],
            "@babel/preset-typescript",
          ],
          cacheDirectory: true,
          cacheCompression: false,
        }
      },

    ],
  },

  externals: {
    //"react": "React",
    //"react-dom": "ReactDOM",
    //"reflect-metadata": "Reflect",
  },

};
