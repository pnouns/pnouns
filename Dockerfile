FROM node:14-alpine
WORKDIR /usr/src
RUN apk --no-cache add curl

HEALTHCHECK \
  --start-period=25s \
  --interval=5s \
  --timeout=3s \
  --retries=3 \
  CMD curl -f http://localhost:9510/index.html || exit 1

COPY . .
RUN rm client/index.html && \
  cp client/index.prod.html client/index.html && \
  yarn && \
  yarn workspace @pronounsfyi/client build && \
  yarn workspace @pronounsfyi/server build

EXPOSE 9510
CMD [ "yarn", "workspace", "@pronounsfyi/server", "start" ]
