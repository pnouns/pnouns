import { Field, InputType } from "@nestjs/graphql";
import { Length, Matches, IsNotIn } from "class-validator";
import { IsNotWord } from "../../util/IsNotWord.decorator";
import { RESERVED_NICKNAMES } from "../util/reserved_nicknames";

@InputType()
export class CreateNicknameInput {

  @Field()
  @Length(3, 31, {
    message: "Nickname must be between 3 and 31 characters.",
  })
  @Matches(/^[a-zA-Z][a-zA-Z0-9_-]{1,29}[a-zA-Z0-9]$/)
  @IsNotWord({
    message: "Nickname cannot be a common English word.",
  })
  @IsNotIn(RESERVED_NICKNAMES, {
    message: "That nickname is reserved.",
  })
  nickname: string;

}
