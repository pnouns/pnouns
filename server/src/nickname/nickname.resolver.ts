import {
  UnauthorizedException,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { Args, ID, Mutation, Parent, Query, ResolveField, Resolver } from "@nestjs/graphql";
import { GraphIP } from "../tracking/GraphIP.decorator";
import { EventLoggerService } from "../tracking/event-logger.service";
import { ParseJWTGuard } from "../user/parse-jwt.guard";
import { UserId } from "../util/gql-context";
import { NicknameService } from "./nickname.service";
import { CreateNicknameInput } from "./dto/CreateNicknameInput.input";
import { UserService } from "../user/user.service";
import { NicknameCreationOutputModel } from "./model/nickname-creation-output.model";
import { NicknameModel } from "./model/nickname.model";
import { User } from "../user/entities/user.entity";
import { Nickname } from "./entities/nickname.entity";
import { TrackedEvent } from "../tracking/enum/TrackedEvent.enum";
import { StaffType } from "../user/enum/StaffType";
import { SupporterRole } from "../user/enum/SupporterRole";

@Resolver(() => NicknameModel)
export class NicknameResolver {

  public constructor(
    private readonly configService: ConfigService,
    private readonly eventLoggerService: EventLoggerService,
    private readonly userService: UserService,
    private readonly nicknameService: NicknameService,
  ) {}

  @UseGuards(ParseJWTGuard)
  @Query(() => NicknameModel, {
    description: "Get one of your own nicknames.",
  })
  @UsePipes(new ValidationPipe({ transform: true }))
  public async nickname(
    @UserId() userId: string,
    @Args("nickname", { type: () => ID }) nicknameId: string,
  ) {
    const [nickname] = await this.getAuthenticatedNickname(userId, nicknameId);
    return nickname;
  }

  @Query(() => Boolean)
  public async nicknameTaken(
    @Args("nickname") nickname: string,
  ): Promise<boolean> {
    return this.nicknameService.nicknameInUse(nickname);
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => NicknameCreationOutputModel)
  @UsePipes(new ValidationPipe({ transform: true }))
  public async createNickname(
    @GraphIP() ip: string,
    @UserId() userId: string,
    @Args("input", { type: () => CreateNicknameInput }) input: CreateNicknameInput,
  ): Promise<NicknameCreationOutputModel> {
    if(!userId) {
      throw new UnauthorizedException("You must be logged in.");
    }
    const user = await this.userService.getUserById(userId);
    if(!user) {
      throw new Error(`Couldn't find user ${userId}`);
    }
    const existing = await this.nicknameService.getById(input.nickname);
    if(existing) {
      throw new ReferenceError(`Nickname already in use.`);
    }
    const nicknames = await user.nicknames;
    const isFirstNickname = nicknames.length === 0;
    const nicknameLimit = user.nicknameLimit ?? parseInt(this.configService.get<string>("NICKNAME_COUNT", "2"), 10);
    if(nicknames.length >= nicknameLimit) {
      throw new Error(`Already at the nickname limit (have ${nicknames.length} of ${nicknameLimit})`);
    }
    const nickname = await this.nicknameService.createNickname(user, input.nickname);
    const updatedUser = await this.userService.getUserById(userId);
    if(!updatedUser) {
      throw new Error(`Couldn't find user ${userId}.  (This should never happen.)`);
    }
    const allNicknames = await updatedUser.nicknames;
    this.eventLoggerService.logUserEvent(userId, TrackedEvent.USER_NICKNAME_CREATE, ip, {
      nicknameId: nickname.id,
      isFirstNickname,
      nicknameCount: allNicknames.length,
      nicknames: allNicknames.map(nickname => nickname.id),
      $set: {
        nicknameCount: allNicknames.length,
        nicknames: allNicknames.map(nickname => nickname.id),
      }
    });
    const jwt = await this.userService.createJwt(updatedUser);
    return {
      nickname: nickname as any as NicknameModel,
      jwt,
    };
  }

  @ResolveField()
  async staffType(@Parent() nickname: Nickname): Promise<StaffType[]> {
    const user = await nickname.user;
    return user.staff ?? [];
  }

  @ResolveField()
  async supporterRole(@Parent() nickname: Nickname): Promise<SupporterRole | undefined> {
    const user = await nickname.user;
    return user.supporter;
  }

  private async getAuthenticatedNickname(
    userId: string,
    nicknameId: string,
  ): Promise<[Nickname, User]> {
    if(!userId) {
      throw new UnauthorizedException("You must be logged in.");
    }
    const nickname = await this.nicknameService.getById(nicknameId);
    const nicknameOwner = nickname && await nickname.user;
    if(!nickname || !nicknameOwner || nicknameOwner.id !== userId) {
      throw new UnauthorizedException("Nickname not found or you lack permission for this nickname.");
    }
    return [nickname, nicknameOwner];
  }

}
