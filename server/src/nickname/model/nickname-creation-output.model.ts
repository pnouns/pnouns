import { Field, ObjectType } from "@nestjs/graphql";
import { NicknameModel } from "./nickname.model";

@ObjectType("NicknameCreationOutput", {
  description: "The returned output from creating a new nickname (see `createNickname`)",
})
export class NicknameCreationOutputModel {

  @Field(() => NicknameModel)
  public nickname: NicknameModel;

  @Field({
    description: "The updated JWT.",
  })
  public jwt: string;

}
