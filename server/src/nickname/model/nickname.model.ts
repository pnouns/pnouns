import { Field, ID, ObjectType } from "@nestjs/graphql";
import { PageModel } from "../../page/model/page.model";
import { StaffType } from "../../user/enum/StaffType";
import { SupporterRole } from "../../user/enum/SupporterRole";

@ObjectType("Nickname", {
  description: "A namespace to publish users"
})
export class NicknameModel {

  @Field(() => ID)
  id: string;

  @Field({
    description: "A human-readable display name.",
    nullable: true,
  })
  displayName?: string;

  @Field(() => [StaffType], {
    description: "The applicable labels of staff for this user.",
  })
  staffType: StaffType[];

  @Field(() => SupporterRole, {
    description: "The level of financial support.",
    nullable: true,
  })
  supporterRole?: SupporterRole;

  @Field({
    description: "If staff/supporter badges should be shown by default for sub-pages.",
    nullable: true,
  })
  showBadgesByDefault?: boolean;

  @Field(() => [PageModel], {
    description: "Get all pages in this nickname.",
  })
  pages: PageModel[];

  @Field(() => PageModel, {
    description: "Get the primary page for this nickname",
    nullable: true,
  })
  nicknamePage?: PageModel;

}
