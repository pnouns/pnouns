import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
} from "typeorm";
import { Page } from "../../page/entities/page.entity";
import { User } from "../../user/entities/user.entity";

@Entity()
export class Nickname {

  /**
   * The nickname to use - used for URLs, etc.
   */
  @PrimaryColumn("varchar", { length: 31 })
  id: string;

  /**
   * The human-readable name to display.
   * Default to 'id' if not set.
   */
  @Column("varchar", { length: 31, nullable: true })
  displayName?: string;

  /**
   * If 'true', pages will show badges by default.
   */
  @Column("bool", {
    nullable: true,
  })
  showBadgesByDefault?: boolean;

  @ManyToOne(() => User, user => user.nicknames)
  user: Promise<User>;

  @OneToMany(() => Page, page => page.nickname, {
    onDelete: "CASCADE",
  })
  pages: Promise<Page[]>;

  /**
   * The primary page for this nickname - e.g. served at https://<nickname>.pnouns.fyi/
   * Not required to exist.
   */
  @OneToOne(() => Page, {
    nullable: true,
    onDelete: "CASCADE",
  })
  @JoinColumn()
  nicknamePage?: Promise<Page>;

  @CreateDateColumn()
  createdAt: Date;

}
