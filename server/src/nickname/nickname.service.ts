import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Page } from "../page/entities/page.entity";
import { User } from "../user/entities/user.entity";
import { Nickname } from "./entities/nickname.entity";

@Injectable()
export class NicknameService {

  public constructor(
    @InjectRepository(Nickname)
    private readonly nicknameRepository: Repository<Nickname>,
  ) {}

  public async getById(id: string): Promise<Nickname | undefined> {
    return this.nicknameRepository.findOne(id);
  }

  public async nicknameInUse(
    name: string,
  ): Promise<boolean> {
    const existing = await this.nicknameRepository.findOne(name, {
      select: [],
      relations: [],
    });
    return existing !== undefined;
  }

  public async createNickname(
    user: User,
    name: string,
  ): Promise<Nickname> {
    const nickname = new Nickname();
    nickname.id = name;
    nickname.user = Promise.resolve(user);
    return this.nicknameRepository.save(nickname);
  }

  public async addNicknamePage(
    nickname: Nickname,
    nicknamePage: Page,
  ): Promise<Nickname> {
    nickname.nicknamePage = Promise.resolve(nicknamePage);
    return this.nicknameRepository.save(nickname);
  }

}
