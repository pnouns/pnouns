import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { TrackingModule } from "../tracking/tracking.module";
import { ParseJwtImports, UserModule } from "../user/user.module";
import { Nickname } from "./entities/nickname.entity";
import { NicknameService } from "./nickname.service";
import { NicknameResolver } from "./nickname.resolver";

@Module({
  imports: [
    ...ParseJwtImports,
    TrackingModule,
    TypeOrmModule.forFeature([
      Nickname,
    ]),
    UserModule,
  ],
  providers: [
    NicknameService,
    NicknameResolver,
  ],
  exports: [
    NicknameService,
  ],
})
export class NicknameModule {}
