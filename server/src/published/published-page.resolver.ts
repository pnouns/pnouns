import { UnauthorizedException, UsePipes, ValidationPipe } from "@nestjs/common";
import { Args, Parent, Query, ResolveField, Resolver } from "@nestjs/graphql";
import { StaffType } from "../user/enum/StaffType";
import { SupporterRole } from "../user/enum/SupporterRole";
import { UserService } from "../user/user.service";
import { PublishedPageResolverArgs } from "./dto/page.args";
import { PublishedPageModel } from "./model/published-page.model";
import { PublishedPageService } from "./published-page.service";

@Resolver(() => PublishedPageModel)
export class PublishedPageResolver {

  public constructor(
    private readonly publishedPageService: PublishedPageService,
    private readonly userService: UserService,
  ) {}

  @Query(() => PublishedPageModel)
  @UsePipes(new ValidationPipe({ transform: true }))
  async getPublishedPage(
    @Args("input", { type: () => PublishedPageResolverArgs }) input: PublishedPageResolverArgs,
  ): Promise<PublishedPageModel> {
    const {
      namespace,
      category,
      page: pagePath,
      password,
    } = input;

    const page = await this.publishedPageService.getPageByPath(namespace, category, pagePath);
    if(!page || (page.password && page.password !== password)) {
      throw new UnauthorizedException(`Cannot find page by ID, or incorrect password entered.`);
    }
    const content = JSON.parse(page.content);
    return content;
  }

  @ResolveField()
  async staffBadges(@Parent() page: PublishedPageModel): Promise<StaffType[]> {
    const uid = page.userId;
    if(!page.showBadges || !uid) {
      return [];
    }
    const user = await this.userService.getUserById(uid);
    if(!user) {
      return [];
    }
    return user.staff ?? [];
  }

  @ResolveField()
  async supporterRoleBadge(@Parent() page: PublishedPageModel): Promise<SupporterRole | undefined> {
    const uid = page.userId;
    if(!page.showBadges || !uid) {
      return undefined;
    }
    const user = await this.userService.getUserById(uid);
    return user?.supporter;
  }

}
