import {
  Field,
  ID,
  ObjectType,
} from "@nestjs/graphql";
import {
  Length,
  Matches,
  IsNotIn,
  IsOptional,
  IsString,
  IsUUID,
  ValidateNested,
} from "class-validator";
import { Strength } from "../../section/enum/strength.enum";

@ObjectType("PublishedValue", {
  description: "An option to include as part of a section",
})
export class PublishedValueModel {

  @Field(() => ID)
  @IsString()
  @IsUUID("4")
  id: string;

  @Field({
    description: "The name of the item.",
  })
  @IsString()
  @Length(1,64)
  text: string;

  @Field(() => Strength, {
    description: "How the author feels about this value.",
  })
  strength: Strength;

  @Field({
    description: "If the user uses a custom strength, the text to display.",
    nullable: true,
  })
  @IsOptional()
  @IsString()
  customStrengthText?: string;

  @Field({
    description: "If the user uses a custom strength, the icon to display.",
    nullable: true,
  })
  @IsOptional()
  @IsString()
  customStrengthIcon?: string;

  @Field({
    description: "A short (1-line) description to attach to the value.",
    nullable: true,
  })
  @IsOptional()
  @IsString()
  shortDescription?: string;

  @Field({
    description: "A long description for the item.",
    nullable: true,
  })
  @IsOptional()
  @IsString()
  info?: string;

}
