import {
  Field,
  ID,
  ObjectType,
} from "@nestjs/graphql";
import {
  Length,
  IsOptional,
  IsString,
  IsUUID,
  IsBoolean,
} from "class-validator";

@ObjectType("PublishListedSubPage", {
  description: "A link to a sub-page in the same namespace.",
})
export class PublishListedSubPageModel {

  @Field(() => ID)
  @IsUUID()
  id: string;

  @Field()
  @IsString()
  title: string;

  @Field({
    nullable: true,
  })
  @IsOptional()
  @IsString()
  subtitle?: string;

  @Field({
    nullable: true,
  })
  @IsOptional()
  @IsString()
  caption?: string;

  @Field()
  @IsString()
  @Length(8)
  url: string;

  @Field()
  @IsBoolean()
  needsPassword: boolean;

}
