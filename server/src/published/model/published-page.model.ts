import {
  Field,
  ID,
  ObjectType,
} from "@nestjs/graphql";
import {
  Length,
  Matches,
  IsNotIn,
  IsOptional,
  IsString,
  IsUUID,
  ValidateNested,
  IsBoolean,
} from "class-validator";
import { RESERVED_NICKNAMES } from "../../nickname/util/reserved_nicknames";
import { IsNotWord } from "../../util/IsNotWord.decorator";
import { PublishedNamespaceModel } from "./published-namespace.model";
import { PublishedSectionModel } from "./published-section.model";
import { PublishListedSubPageModel } from "./published-listed-sub-page.model";
import { StaffType } from "../../user/enum/StaffType";
import { SupporterRole } from "../../user/enum/SupporterRole";

export class StoredPublishedPage {

  @Field(() => ID)
  @IsUUID("4")
  id: string;

  @Field(() => ID, {
    nullable: true,
  })
  @IsOptional()
  @IsUUID("4")
  userId?: string;

  @IsString()
  @Length(3, 31, {
    message: "Nickname must be between 3 and 31 characters.",
  })
  @Matches(/^[a-zA-Z][a-zA-Z0-9_-]{1,29}[a-zA-Z0-9]$/)
  @IsNotWord({
    message: "Nickname cannot be a common English word.",
  })
  @IsNotIn(RESERVED_NICKNAMES, {
    message: "That nickname is reserved.",
  })
  nicknamePath: string;

  @IsOptional()
  @IsString()
  nicknameDisplay?: string;

  @IsOptional()
  @IsString()
  @Matches(/^[a-zA-Z]+$/)
  categoryPath?: string;

  @IsOptional()
  @IsString()
  categoryDisplay?: string;

  @IsOptional()
  @IsString()
  pageBioTitle?: string;

  @IsOptional()
  @IsString()
  pageBioSubtitle?: string;

  @IsOptional()
  @IsString()
  @Matches(/^[a-zA-Z][a-zA-Z0-9_-]{0,29}[a-zA-Z0-9]$/)
  pagePath?: string;

  @IsOptional()
  @ValidateNested()
  nameSection?: PublishedSectionModel;

  @IsOptional()
  @ValidateNested()
  pronounsSection?: PublishedSectionModel;

  @ValidateNested()
  languageSections: PublishedSectionModel[];

  sectionOrder: string[];

  @IsOptional()
  @ValidateNested()
  listedSubPages?: PublishListedSubPageModel[];

  listedSubPageOrder?: string[];

  @IsOptional()
  @IsBoolean()
  showBadges?: boolean;
}

@ObjectType("PublishedPage")
export class PublishedPageModel extends StoredPublishedPage {

  @Field(() => ID)
  id: string;

  @Field(() => ID, {
    description: "The nickname/name for this account.",
  })
  nicknamePath: string;

  @Field({
    description: "A human-readable version of the nickname.",
    nullable: true,
  })
  nicknameDisplay?: string;

  @Field({
    description: "The name of the category under the nickname.  Null if this is a top-level page.",
    nullable: true,
  })
  categoryPath?: string;

  @Field({
    description: "A more human-readable version of the category name/path.  Default to 'categoryPath'.",
    nullable: true,
  })
  categoryDisplay?: string;

  @Field({
    description: "A title in the bio section.",
    nullable: true,
  })
  pageBioTitle?: string;

  @Field({
    description: "A subtitle in the bio section.",
    nullable: true,
  })
  pageBioSubtitle?: string;

  @Field({
    description: "The name of the page under a category.  Null if this is a top-level page or category page.",
    nullable: true,
  })
  pagePath?: string;

  @Field(() => PublishedSectionModel, {
    nullable: true,
  })
  nameSection?: PublishedSectionModel;

  @Field(() => PublishedSectionModel, {
    nullable: true,
  })
  pronounsSection?: PublishedSectionModel;

  @Field(() => [PublishedSectionModel], {
    description: "The sections of different language that should be used.",
  })
  languageSections: PublishedSectionModel[];

  @Field(() => [String], {
    description: "The list of section IDs (excluding name/pronoun) in the sorted order.",
  })
  sectionOrder: string[];

  @Field(() => [PublishListedSubPageModel], {
    description: "A list of pages to link to.",
    nullable: true,
  })
  listedSubPages?: PublishListedSubPageModel[];

  @Field(() => [String], {
    description: "The list of sub-page IDs in the sorted order.",
    nullable: true,
  })
  listedSubPageOrder?: string[];

  @Field({
    description: "If badges should be shown for this page.",
    nullable: true,
  })
  showBadges?: boolean;

  @Field(() => PublishedNamespaceModel, {
    description: "[COMPUTE] Get the parent namespace.  Requires more fetching than other fields.",
  })
  namespace: PublishedNamespaceModel;

  @Field(() => PublishedPageModel, {
    description: "[COMPUTE] Get the parent page.  Requires more fetching than other fields.",
    nullable: true,
  })
  parentCategory?: PublishedPageModel;

  @Field(() => [StaffType], {
    description: "[COMPUTE] Get the staff badges to show.  Empty if no staff rules or if hidden.",
  })
  staffBadges: StaffType[];

  @Field(() => SupporterRole, {
    description: "[COMPUTE] Get a supporter role to show.  Null if no supporter role or if hidden.",
    nullable: true,
  })
  supporterRoleBadge?: SupporterRole;

}
