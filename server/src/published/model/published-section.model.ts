import {
  Field,
  ID,
  ObjectType,
} from "@nestjs/graphql";
import {
  Length,
  IsOptional,
  IsString,
  IsUUID,
  ValidateNested,
} from "class-validator";
import { PublishedValueModel } from "./published-value.model";

@ObjectType("PublishedSection", {
  description: "The contents of a published section",
})
export class PublishedSectionModel {

  @Field(() => ID)
  @IsUUID()
  id: string;

  @Field()
  @IsString()
  @Length(1, 64)
  name: string;

  @Field({
    nullable: true,
  })
  @IsOptional()
  @IsString()
  subtitle?: string;

  @Field({
    nullable: true,
  })
  @IsOptional()
  @IsString()
  lead?: string;

  @Field(() => [PublishedValueModel], {
    description: "The items to list for this section.",
  })
  @ValidateNested()
  values: PublishedValueModel[];

  @Field(() => [String], {
    description: "The list of value IDs in the sorted order."
  })
  valueOrder: string[];

}
