import {
  Field,
  ID,
  ObjectType,
} from "@nestjs/graphql";
import { PublishedPageModel } from "./published-page.model";

@ObjectType("PublishedNamespacePage", {
  description: "A summary of a public page linked from a namespace.",
})
export class PublishedNamespacePageModel {

  @Field(() => ID, {
    description: "The backend ID of the page.",
  })
  id: string;

  @Field(() => PublishedPageModel, {
    description: "[COMPUTE] Get the contents of the page.  Requires more fetching than other fields.",
  })
  page: PublishedPageModel;

}

@ObjectType("PublishedNamespace")
export class PublishedNamespaceModel {

  @Field(() => ID, {
    description: "A human-readable ID for the namespace.",
  })
  id: string;

  @Field(() => [PublishedNamespacePageModel], {
    description: "A summary of the public pages in this namespace.",
  })
  publicPages: PublishedNamespacePageModel[];

}
