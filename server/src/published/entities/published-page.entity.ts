import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Page } from "../../page/entities/page.entity";

@Index(["nicknamePath", "categoryPath", "pagePath"], { unique: true })
@Entity()
export class PublishedPage {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  /**
   * The ID of the namespace/nickname.
   * Used for lookup.
   */
  @Column("varchar", { length: 31 })
  nicknamePath: string;

  /**
   * The path of the category.
   * Null if this is the top page for a nickname.
   */
  @Column("varchar", { length: 255, nullable: true })
  categoryPath?: string;

  /**
   * The path of the page, directly under the category.
   */
  @Column("varchar", { length: 255, nullable: true })
  pagePath?: string;

  @Column("varchar", { length: 255, nullable: true })
  password?: string;

  @Column("mediumtext")
  content: string;

  @OneToOne(() => Page, page => page.published)
  page: Promise<Page>;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

}
