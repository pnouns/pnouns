import { Field, InputType } from "@nestjs/graphql";

@InputType()
export class PublishedPageResolverArgs {

  @Field()
  namespace: string;

  @Field({
    nullable: true,
  })
  category?: string;

  @Field({
    nullable: true,
  })
  page?: string;

  @Field({
    nullable: true,
  })
  password?: string;

}
