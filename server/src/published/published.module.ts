import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { PublishedPageResolver } from "./published-page.resolver";
import { PublishedPage } from "./entities/published-page.entity";
import { PublishedPageService } from "./published-page.service";
import { UserModule } from "../user/user.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([
      PublishedPage,
    ]),
    UserModule,
  ],
  providers: [
    PublishedPageResolver,
    PublishedPageService,
  ],
  exports: [
    PublishedPageService,
  ],
})
export class PublishedModule {}
