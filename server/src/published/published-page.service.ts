import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { IsNull, Repository } from "typeorm";
import { v4 as uuid } from "uuid";
import { validateOrReject } from "class-validator";
import { joiningWordLabel } from "../util/joining-word";
import { Page } from "../page/entities/page.entity";
import { PublishedPage } from "./entities/published-page.entity";
import { StoredPublishedPage } from "./model/published-page.model";
import { Section } from "../section/entities/section.entity";
import { PublishedSectionModel } from "./model/published-section.model";
import { SectionValue } from "../section/entities/section-value.entity";
import { PublishedValueModel } from "./model/published-value.model";
import { ListedSubPage } from "../page/entities/listed-sub-page.entity";
import { PublishListedSubPageModel } from "./model/published-listed-sub-page.model";
import { PasswordFormat } from "../page/enum/PasswordFormat";

@Injectable()
export class PublishedPageService {

  public constructor(
    @InjectRepository(PublishedPage)
    private readonly pageRepository: Repository<PublishedPage>,
  ) {}

  public getPageByPath(
    nickname: string,
    category?: string,
    page?: string,
  ): Promise<PublishedPage | undefined> {
    return this.pageRepository.findOne({
      nicknamePath: nickname,
      categoryPath: category ? category : IsNull(),
      pagePath: page ? page : IsNull(),
    });
  }

  public async createOrUpdatePage(
    page: Page,
  ): Promise<PublishedPage> {
    if(page.published && !!(await page.published)) {
      console.log(`Page ${page.id} already published.  Updating the page.`);
      return this.updatePage(page);
    } else {
      console.log(`Page ${page.id} not published (${page.published}, ${page.published && !!(await page.published)})`);
      return this.createPage(page);
    }
  }

  public async createPage(
    page: Page,
  ): Promise<PublishedPage> {
    const id = uuid();
    const stored = await this.compilePage(id, page);
    const published = new PublishedPage();
    published.id = id;
    published.nicknamePath = stored.nicknamePath;
    published.categoryPath = stored.categoryPath;
    published.pagePath = stored.pagePath;
    published.password = page.password;
    published.content = JSON.stringify(stored);
    const inserted = this.pageRepository.save(published);
    return inserted;
  }

  public async updatePage(page: Page): Promise<PublishedPage> {
    if(!page.published) {
      throw new Error("Can't update an unpublished page.");
    }
    const published = await page.published;
    const stored = await this.compilePage(published.id, page);
    console.log(stored.languageSections[0]);
    published.password = page.password;
    published.content = JSON.stringify(stored);
    await this.pageRepository.update(published.id, {
      password: page.password,
      content: JSON.stringify(stored),
    });
    return (await this.pageRepository.findOne(published.id)) ?? published;
  }

  private async compilePage(
    id: string,
    page: Page,
  ): Promise<StoredPublishedPage> {
    const nickname = await page.nickname;
    const parent = await page.parent;
    const stored = new StoredPublishedPage();
    stored.id = id;
    stored.userId = (await page.user).id;
    stored.nicknamePath = nickname.id;
    stored.nicknameDisplay = nickname.displayName;
    const isNicknamePage = !page.name;
    const isCategoryPage = !parent;
    const isCategorySubPage = !!parent;
    const category = isCategoryPage ? page : parent;
    if(isCategoryPage || isCategorySubPage) {
      stored.categoryPath = category?.name;
      stored.categoryDisplay = category?.displayName;
    }
    if(isCategorySubPage) {
      stored.pagePath = page.name;
    }
    stored.pageBioTitle = page.pageBioTitle;
    stored.pageBioSubtitle = page.pageBioSubtitle;
    stored.nameSection = await this.compileSection(await page.nameSection);
    stored.pronounsSection = await this.compileSection(await page.pronounSection);
    const languageSections = (await page.sections).filter(section => {
      console.log(`${section.title} ${!section.type}`);
      return !section.type
    });
    const sectionCompilation = languageSections.map(section => this.compileSection(section));
    const compiledSections = await Promise.all(sectionCompilation);
    console.log(`Adding ${languageSections.length} sections. ${compiledSections.length}`);
    stored.languageSections = compiledSections.filter<PublishedSectionModel>((i): i is PublishedSectionModel => i !== undefined);
    stored.sectionOrder = page.sectionOrder;
    stored.listedSubPages = await Promise.all((await page.listedSubPages).map(page => this.compileListedSubPage(page)));
    stored.listedSubPageOrder = page.listedSubPageOrder;
    stored.showBadges = page.showBadges;
    try {
      await validateOrReject(stored);
    } catch (err) {
      if(Array.isArray(err) && err.length === 1) {
        throw err[0];
      }
      throw err;
    }
    return stored;
  }

  private async compileListedSubPage(
    page: ListedSubPage,
  ): Promise<PublishListedSubPageModel> {
    const parent = await page.parent;
    const child = await page.linked;
    const nickname = await child.nickname;

    const grandparent = await (parent.parent ?? Promise.resolve());
    const isCategorySubPage = !grandparent && parent.name;

    const category = isCategorySubPage ? parent : child;
    const categorySubPage = isCategorySubPage ? child : undefined;

    const compiled = new PublishListedSubPageModel();
    compiled.id = page.id;
    compiled.title = page.customName ?? child.pageBioTitle ?? child.displayName ?? child.name ?? "";
    compiled.subtitle = page.showSubtitle ? page.customSubtitle ?? child.pageBioSubtitle : undefined;
    compiled.caption = page.caption;

    const categoryAsPath = !category.defaultSubdomain;

    compiled.url = [
      `https://`,
      nickname.id,
      category.defaultSubdomain ? `.${category.name}` : "",
      ".pnouns.fyi",
      categoryAsPath && category.defaultJoiningWord
        ? `/${joiningWordLabel(category.defaultJoiningWord)}`
        : "",
      categoryAsPath ? `/${category.name}` : "",
      categoryAsPath && !isCategorySubPage && category.password && page.includePassword && category.defaultPasswordFormat === PasswordFormat.IN_PAGE
        ? `+${category.password}`
        : "",
      categorySubPage && categorySubPage.defaultJoiningWord
        ? `/${joiningWordLabel(categorySubPage.defaultJoiningWord)}`
        : "",
      categorySubPage
        ? `/${categorySubPage.name}`
        : "",
      categorySubPage && categorySubPage.password && page.includePassword && categorySubPage.defaultPasswordFormat === PasswordFormat.IN_PAGE
        ? `+${categorySubPage.password}`
        : "",
      "/",
      page.includePassword && child.password && child.defaultPasswordFormat !== PasswordFormat.IN_PAGE
        ? `?${child.defaultPasswordFormat === PasswordFormat.QUERY_P ? "p" : "password"}=${child.password}`
        : ""
    ].join("");

    compiled.needsPassword = !page.includePassword && !!child.password;

    return compiled;
  }

  private async compileSection(
    section: Section,
  ): Promise<PublishedSectionModel | undefined> {
    if(!section) {
      return undefined;
    }
    const compiled = new PublishedSectionModel();
    compiled.id = section.id;
    compiled.name = section.title;
    compiled.subtitle = section.subtitle;
    compiled.lead = section.lead;
    compiled.values = await Promise.all((await section.values).map(value => this.compileValue(value)));
    compiled.valueOrder = section.valueOrder;
    await validateOrReject(compiled);
    return compiled;
  }

  private async compileValue(
    value: SectionValue,
  ): Promise<PublishedValueModel> {
    const compiled = new PublishedValueModel();
    compiled.id = value.id;
    compiled.text = value.text;
    compiled.strength = value.strength;
    compiled.customStrengthText = value.customStrengthText;
    await validateOrReject(compiled);
    return compiled;
  }

}
