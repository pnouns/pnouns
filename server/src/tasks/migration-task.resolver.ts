import { ForbiddenException } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { Args, Mutation, Resolver } from "@nestjs/graphql";
import { join } from "path";
import { exec } from "child-process-promise";
import { ProcessResponseModel } from "./process-response.model";

@Resolver()
export class MigrationTaskResolver {

  constructor(
    private readonly configService: ConfigService,
  ) {}

  @Mutation(() => ProcessResponseModel)
  public async runMigrateTask(
    @Args("password") password: string,
  ): Promise<ProcessResponseModel> {
    const setPassword = this.configService.get<string>("MIGRATE_TASK_PASSWORD");
    if(!setPassword || setPassword.length < 4 || password !== setPassword) {
      throw new ForbiddenException("Only administrators can run database migrations.");
    }
    const res = await exec("yarn typeorm migration:run", {
      cwd: join(__dirname, "../../../"),
    });
    return {
      stdout: res.stdout,
      stderr: res.stderr,
    };
  }

}
