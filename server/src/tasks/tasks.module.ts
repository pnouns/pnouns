import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { MigrationTaskResolver } from "./migration-task.resolver";

@Module({
  imports: [
    ConfigModule,
  ],
  providers: [
    MigrationTaskResolver,
  ],
  exports: [
  ],
})
export class TasksModule {}
