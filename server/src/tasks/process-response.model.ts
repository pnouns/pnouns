import { Field, ObjectType } from "@nestjs/graphql";

@ObjectType("TaskShellResponse", {
  description: "The output of a shell command run as a task.",
})
export class ProcessResponseModel {

  @Field()
  stderr: string;

  @Field()
  stdout: string;

}
