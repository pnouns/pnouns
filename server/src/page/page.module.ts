import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { NicknameModule } from "../nickname/nickname.module";
import { PublishedModule } from "../published/published.module";
import { SectionModule } from "../section/section.module";
import { TrackingModule } from "../tracking/tracking.module";
import { ParseJwtImports, UserModule } from "../user/user.module";
import { ListedSubPage } from "./entities/listed-sub-page.entity";
import { Page } from "./entities/page.entity";
import { PageResolver } from "./page.resolver";
import { PageService } from "./page.service";

@Module({
  imports: [
    ...ParseJwtImports,
    TrackingModule,
    TypeOrmModule.forFeature([
      ListedSubPage,
      Page,
    ]),
    UserModule,
    NicknameModule,
    SectionModule,
    PublishedModule,
  ],
  providers: [
    PageService,
    PageResolver,
  ],
  exports: [
    PageService,
  ],
})
export class PageModule {}
