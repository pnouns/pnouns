import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Page } from "./page.entity";

@Entity()
export class ListedSubPage {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @ManyToOne(() => Page, page => page.listedSubPages)
  parent: Promise<Page>;

  @ManyToOne(() => Page, page => page.listedOnParent)
  linked: Promise<Page>;

  @Column("varchar", {
    length: 255,
    nullable: true,
  })
  customName?: string;

  @Column("bool")
  showSubtitle: boolean;

  @Column("varchar", {
    length: 255,
    nullable: true,
  })
  customSubtitle?: string;

  @Column("varchar", {
    length: 2048,
    nullable: true,
  })
  caption?: string;

  @Column("bool")
  includePassword: boolean;

}
