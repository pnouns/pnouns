import { Field, ID, ObjectType } from "@nestjs/graphql";
import { NicknameModel } from "../../nickname/model/nickname.model";
import { SectionModel } from "../../section/model/section.model";
import { ListedSubPageModel } from "./listed-sub-page.model";
import { JoiningWord } from "../enum/JoiningWord";
import { PasswordFormat } from "../enum/PasswordFormat";

@ObjectType("Page", {
  description: "A publishable page - can be a namespace page, category page, or sub-page."
})
export class PageModel {

  @Field(() => ID)
  id: string;

  @Field({
    description: "The name of this page/category.  'null' if this is the page for the nickname.",
    nullable: true,
  })
  name?: string;

  @Field({
    description: "A non-URL-safe name for this page, to be shown in the header, etc.  Default to 'name'.",
    nullable: true,
  })
  displayName?: string;

  @Field({
    nullable: true,
  })
  pageBioTitle?: string;

  @Field({
    nullable: true,
  })
  pageBioSubtitle?: string;

  @Field({
    description: "If this is a category, if the default URL should be a category subdomain.",
    nullable: true,
  })
  defaultSubdomain?: boolean;

  @Field(() => JoiningWord, {
    description: "The default word to insert before this page.  Ignore if this page is a category and 'defaultSubdomain' is set, or if this is a nickname page.",
    nullable: true,
  })
  defaultJoiningWord?: JoiningWord;

  @Field({
    description: "A password to protect this page from casual scraping.  Null if no password is required.",
    nullable: true,
  })
  password?: string;

  @Field(() => PasswordFormat, {
    description: "The default format for inserting a password into the URL.",
    nullable: true,
  })
  defaultPasswordFormat?: PasswordFormat;

  @Field({
    description: "If staff and/or supporter badges should be shown on this page.  If 'null', uses the setting from the associated nickname.",
    nullable: true,
  })
  showBadges?: boolean;

  @Field({
    description: "If this page has been published",
  })
  isPublished: boolean;

  @Field(() => NicknameModel, {
    description: "The nickname this page is under.",
  })
  nickname: NicknameModel;

  @Field(() => SectionModel, {
    description: "The special section for name options.",
    nullable: true,
  })
  nameSection?: SectionModel;

  @Field(() => SectionModel, {
    description: "The special section for pronouns.",
    nullable: true,
  })
  pronounSection?: SectionModel;

  @Field(() => [SectionModel], {
    description: "A list of sections - including the special sections.",
  })
  sections: SectionModel[];

  @Field(() => [String], {
    description: "A list of the section IDs in order.  Long-term we'll use next/previous on the sections.",
  })
  sectionOrder: string[];

  @Field(() => [ListedSubPageModel], {
    description: "A list of sub-pages to include on the page.",
  })
  listedSubPages: ListedSubPageModel[];

  @Field(() => [String], {
    description: "A list of the sub-page listing IDs in order.  Long-term we'll use next/previous.",
  })
  listedSubPageOrder: string[];

  @Field(() => [PageModel], {
    description: "The pages that can be linked as sub-pages.",
  })
  subPageOptions: PageModel[];

  @Field(() => PageModel, {
    description: "The parent page for this page - null for nickname pages and category pages.",
    nullable: true,
  })
  parent?: PageModel;

}
