import { Field, ID, ObjectType } from "@nestjs/graphql";
import { PageModel } from "./page.model";

@ObjectType("ListedSubPage", {
  description: "A sub-page listed on a parent page.",
})
export class ListedSubPageModel {

  @Field(() => ID)
  id: string;

  @Field(() => PageModel, {
    description: "The page being linked."
  })
  linked: PageModel;

  @Field({
    description: "The name to display.  If null, fallback to the page's pageBioTitle/displayName/name.",
    nullable: true,
  })
  customName?: string;

  @Field({
    description: "If true, shows either a custom or inherited subtitle.  Otherwise leaves blank.",
  })
  showSubtitle: boolean;

  @Field({
    description: "A subtitle to list.  If null, fallback to the page's pageBioSubtitle",
    nullable: true,
  })
  customSubtitle?: string;

  @Field({
    description: "A (optionally multi-line) caption to display under the title/subtitle",
    nullable: true,
  })
  caption?: string;

  @Field({
    description: "If the page password should be embedded.  Otherwise will prompt on the published site.",
  })
  includePassword: boolean;

}
