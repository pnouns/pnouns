import { registerEnumType } from "@nestjs/graphql";
import stripIndent from "strip-indent";

export enum PasswordFormat {
  /**
   * For pages that have the category or page as the final
   * path segment, add the password to the category as the default URL.
   *
   * e.g. for category "friends" and password "a4":
   * https://alexs.pnouns.fyi/with/friends+a4/
   */
  IN_PAGE = 1,

  /**
   * Default to providing URLs with a `?password=a4` parameter.
   */
  QUERY_PASSWORD,

  /**
   * Default to providing URLs with a `?p=a4` parameter.
   */
  QUERY_P,
}

const IN_PAGE_DESCRIPTION = `\
For pages that have the category or page as the final path segment,
add the password into the category parameter as the default URL.

e.g. for category \`friends\` and password \`a4\`,
https://alexs.pnouns.fyi/with/friends+a4/
`;

registerEnumType(PasswordFormat, {
  name: "PasswordFormat",
  description: "Sets the default format for URLs when linking to this entity.",
  valuesMap: {
    IN_PAGE: {
      description: IN_PAGE_DESCRIPTION,
    },
    QUERY_PASSWORD: {
      description: "Default to providing URLs with a `?password=a4` parameter.",
    },
    QUERY_P: {
      description: "Default to providing URLs with a `?=a4` parameter.",
    },
  },
});
