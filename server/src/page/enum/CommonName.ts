import { registerEnumType } from "@nestjs/graphql";

export enum CommonName {
  ENVIRONMENT = 1,
  WORK,
  SCHOOL,
  FRIENDS,
  FAMILY,
  PROJECT,
  ONLINE,
  CODE,
  GIT,
  SOCIAL = 1000,
  TWITTER,
  DISCORD,
  SLACK,
  INSTAGRAM,
  GITHUB,
  GITLAB,
  LANGUAGE = 2000,
  EN,
  ES,
}

registerEnumType(CommonName, {
  name: "CommonName",
});

export function getCommonName(name: CommonName): string {
  switch (name) {
    /* ENVIRONMENT */
    case CommonName.ENVIRONMENT:
      return "environment";
    case CommonName.WORK:
      return "work";
    case CommonName.SCHOOL:
      return "school";
    case CommonName.FRIENDS:
      return "friends";
    case CommonName.FAMILY:
      return "family";
    case CommonName.PROJECT:
      return "project";
    case CommonName.ONLINE:
      return "online";
    case CommonName.CODE:
      return "code";
    case CommonName.GIT:
      return "git";
    /* SOCIAL */
    case CommonName.SOCIAL:
      return "social";
    case CommonName.TWITTER:
      return "twitter";
    case CommonName.DISCORD:
      return "discord";
    case CommonName.SLACK:
      return "slack";
    case CommonName.INSTAGRAM:
      return "instagram";
    case CommonName.GITHUB:
      return "github";
    case CommonName.GITLAB:
      return "gitlab";
    /* LANGUAGE */
    case CommonName.LANGUAGE:
      return "language";
    case CommonName.EN:
      return "en";
    case CommonName.ES:
      return "es";
  }
}
