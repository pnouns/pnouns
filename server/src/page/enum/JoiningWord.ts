import { registerEnumType } from "@nestjs/graphql";

export enum JoiningWord {
  A = 1,
  AN,
  AT,
  FOR,
  IN,
  AND,
  WITH,
  ON,
}

registerEnumType(JoiningWord, {
  name: "JoiningWord",
});
