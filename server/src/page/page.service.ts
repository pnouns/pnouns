import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Nickname } from "../nickname/entities/nickname.entity";
import { NicknameService } from "../nickname/nickname.service";
import { PublishedPage } from "../published/entities/published-page.entity";
import { Section } from "../section/entities/section.entity";
import { User } from "../user/entities/user.entity";
import { CreateCategoryInput } from "./dto/create-category.input";
import { CreateCategoryPageInput } from "./dto/create-category-page.input";
import { CreateNicknamePageInput } from "./dto/create-nickname-page.input";
import { UpdateLinkedPageInput } from "./dto/update-linked-page.input";
import { UpdatePageBioInput } from "./dto/update-page-bio.input";
import { UpdatePagePasswordInput } from "./dto/update-page-password.input";
import { ListedSubPage } from "./entities/listed-sub-page.entity";
import { Page } from "./entities/page.entity";
import { getCommonName } from "./enum/CommonName";

@Injectable()
export class PageService {

  public constructor(
    private readonly nicknameService: NicknameService,
    @InjectRepository(Page)
    private readonly pageRepository: Repository<Page>,
    @InjectRepository(ListedSubPage)
    private readonly listedSubPageRepository: Repository<ListedSubPage>,
  ) {}

  public async getById(id: string): Promise<Page | undefined> {
    return this.pageRepository.findOne(id);
  }

  public async getByUser(user: User): Promise<Page[]> {
    return this.pageRepository.find({
      where: {
        user,
      },
    });
  }

  public async getListedSubPageById(id: string): Promise<ListedSubPage | undefined> {
    return this.listedSubPageRepository.findOne(id);
  }

  public async createNicknamePage(
    user: User,
    nickname: Nickname,
    input: CreateNicknamePageInput,
  ): Promise<Page> {
    const page = new Page();
    page.name = undefined; // Marks as a nickname page
    page.user = Promise.resolve(user);
    page.nickname = Promise.resolve(nickname);
    page.listed = false; // No effect
    page.sectionOrder = [];
    page.children = Promise.resolve([]);
    page.sections = Promise.resolve([]);
    page.listedSubPageOrder = [];
    const created = await this.pageRepository.save(page);
    this.nicknameService.addNicknamePage(nickname, created);
    const refetched = await this.getById(created.id);
    if(!refetched) {
      throw new Error("Couldn't find page after setting nicknamePage.");
    }
    return refetched;
  }

  public async createCategory(
    user: User,
    nickname: Nickname,
    input: CreateCategoryInput,
  ): Promise<Page> {
    const page = new Page();
    page.name = input.presetName ? getCommonName(input.presetName) : input.customName;
    page.commonName = input.presetName;
    page.user = Promise.resolve(user);
    page.nickname = Promise.resolve(nickname);
    page.defaultSubdomain = input.defaultSubdomain;
    page.defaultJoiningWord = input.defaultJoiningWord;
    page.children = Promise.resolve([]);
    page.sections = Promise.resolve([]);
    page.sectionOrder = [];
    page.listedSubPageOrder = [];
    page.listed = input.listed;
    return this.pageRepository.save(page);
  }

  public async createCategoryPage(
    user: User,
    nickname: Nickname,
    category: Page,
    input: CreateCategoryPageInput,
  ): Promise<Page> {
    const page = new Page();
    page.name = input.name;
    page.user = Promise.resolve(user);
    page.nickname = Promise.resolve(nickname);
    page.parent = Promise.resolve(category);
    page.defaultJoiningWord = input.defaultJoiningWord;
    page.children = Promise.resolve([]);
    page.sections = Promise.resolve([]);
    page.sectionOrder = [];
    page.listedSubPageOrder = [];
    page.listed = input.listed;
    return this.pageRepository.save(page);
  }

  public async updatePageBio(
    page: Page,
    info: UpdatePageBioInput,
  ): Promise<Page> {
    page.pageBioTitle = info.title;
    page.pageBioSubtitle = info.subtitle;
    return this.pageRepository.save(page);
  }

  public async addNameSection(
    page: Page,
    section: Section,
  ): Promise<Page> {
    const existingSections = await page.sections;
    page.sections = Promise.resolve([
      ...(existingSections ?? []),
      section,
    ]);
    page.nameSection = Promise.resolve(section);
    return this.pageRepository.save(page);
  }

  public async addPronounSection(
    page: Page,
    section: Section,
  ): Promise<Page> {
    const existingSections = await page.sections;
    page.sections = Promise.resolve([
      ...(existingSections ?? []),
      section,
    ]);
    page.pronounSection = Promise.resolve(section);
    return this.pageRepository.save(page);
  }

  public async addSection(
    page: Page,
    section: Section,
  ): Promise<Page> {
    const existingSections = await page.sections;
    page.sections = Promise.resolve([
      ...(existingSections ?? []),
      section,
    ]);
    page.sectionOrder = [...page.sectionOrder, section.id];
    return this.pageRepository.save(page);
  }

  public async updatePassword(
    page: Page,
    input: UpdatePagePasswordInput,
  ): Promise<Page> {
    const { password, defaultPasswordFormat } = input;
    const hasPassword = password && password.length >= 2;
    page.password = hasPassword ? password : (null as any as undefined);
    page.defaultPasswordFormat = hasPassword ? defaultPasswordFormat : (null as any as undefined);
    return this.pageRepository.save(page);
  }

  public async updateShowBadges(
    page: Page,
    inherit: boolean,
    showBadges?: boolean,
  ): Promise<Page> {
    page.showBadges = inherit ? null as any as undefined : showBadges;
    return this.pageRepository.save(page);
  }

  public async updateListedSubPage(
    subPage: ListedSubPage,
    input: UpdateLinkedPageInput,
  ): Promise<ListedSubPage> {
    if(input.hasCustomName === false) {
      if(input.customName) {
        throw new BadRequestException(input.customName, "Invalid condition: hasCustomName=false and provided customName.");
      }
      subPage.customName = null as any as undefined;
    } else if(input.customName) {
      subPage.customName = input.customName;
    } else if(input.hasCustomName === true && !subPage.customName) {
      subPage.customName = "";
    }

    if(input.showSubtitle === true || input.showSubtitle === false) {
      subPage.showSubtitle = input.showSubtitle;
    }

    if(input.hasCustomSubtitle === false) {
      if(input.customSubtitle) {
        throw new BadRequestException(input.customSubtitle, "Invalid condition: hasCustomSubtitle=false and provided customSubtitle.");
      }
      subPage.customSubtitle = null as any as undefined;
    } else if(input.customSubtitle) {
      subPage.customSubtitle = input.customSubtitle;
    } else if(input.hasCustomSubtitle === true && !subPage.customSubtitle) {
      subPage.customSubtitle = "";
    }

    if(input.hasCaption === false) {
      if(input.caption) {
        throw new BadRequestException(input.caption, "Invalid condition: hasCaption=false and provided caption");
      }
      subPage.caption = null as any as undefined;
    } else if(input.caption) {
      subPage.caption = input.caption;
    } else if(input.hasCaption === true && !subPage.caption) {
      subPage.caption = "";
    }

    if(input.includePassword === true || input.includePassword === false) {
      subPage.includePassword = input.includePassword;
    }

    return this.listedSubPageRepository.save(subPage);
  }

  public setPublishedPage(
    page: Page,
    published: PublishedPage,
  ): Promise<Page> {
    page.published = Promise.resolve(published);
    return this.pageRepository.save(page);
  }

  public async setSectionOrder(
    page: Page,
    sectionOrder: string[],
  ): Promise<Page> {
    page.sectionOrder = sectionOrder;
    const updated = await this.pageRepository.save(page);
    return updated;
  }

  public async setListedSubPageOrder(
    page: Page,
    subPageOrder: string[],
  ): Promise<Page> {
    page.listedSubPageOrder = subPageOrder;
    const updated = await this.pageRepository.save(page);
    return updated;
  }

  public async deleteListedSubPage(
    subPage: ListedSubPage,
  ): Promise<void> {
    await this.listedSubPageRepository.delete(subPage.id);
  }

  public async createListedSubPage(page: Page, linkedPage: Page): Promise<ListedSubPage> {
    const subPage = new ListedSubPage();
    subPage.parent = Promise.resolve(page);
    subPage.linked = Promise.resolve(linkedPage);
    subPage.showSubtitle = true;
    subPage.includePassword = false;
    return this.listedSubPageRepository.save(subPage);
  }

  public addListedSubPage(page: Page, subPage: ListedSubPage): Promise<Page> {
    page.listedSubPageOrder = [
      ...(page.listedSubPageOrder ?? []),
      subPage.id,
    ];
    return this.pageRepository.save(page);
  }

}
