import { Field, ID, InputType } from "@nestjs/graphql";
import { Matches } from "class-validator";
import { JoiningWord } from "../enum/JoiningWord";

@InputType()
export class CreateCategoryPageInput {

  @Field(() => ID, {
    description: "The ID of the nickname this page is stored under.",
  })
  nicknameId: string;

  @Field(() => ID, {
    description: "The ID of the category this page is stored under.",
  })
  categoryId: string;

  @Field()
  @Matches(/^[a-zA-Z][a-zA-Z0-9_-]{0,29}[a-zA-Z0-9]$/)
  name: string;

  @Field(() => JoiningWord, {
    description: "If set, defines a word that will be inserted before this page in the URL.",
    nullable: true,
  })
  defaultJoiningWord?: JoiningWord;

  @Field({
    description: "If 'true', category will be listed in the category page.",
  })
  listed: boolean;

}
