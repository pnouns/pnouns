import { Field, ID, InputType } from "@nestjs/graphql";
import { IsOptional, Length } from "class-validator";

@InputType()
export class UpdatePageShowBadgesInput {

  @Field(() => ID)
  pageId: string;

  @Field({
    description: "'true' if the page should inherit this from the category.",
  })
  inherit: boolean;

  @Field({
    description: "if the badges should be shown.  Leave undefined if inheriting.",
    nullable: true,
  })
  showBadges: boolean;

}
