import { Field, ID, InputType } from "@nestjs/graphql";

@InputType()
export class UpdatePageOrderInput {

  @Field(() => ID)
  pageId: string;

  @Field(() => [String], {
    description: "A list of section IDs that should be ordered.",
  })
  order: string[];

}
