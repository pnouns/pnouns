import { Field, ID, InputType } from "@nestjs/graphql";
import { Length } from "class-validator";

@InputType()
export class UpdatePageBioInput {

  @Field(() => ID)
  pageId: string;

  @Field({
    nullable: true,
  })
  title?: string;

  @Field({
    nullable: true,
  })
  subtitle?: string;

}
