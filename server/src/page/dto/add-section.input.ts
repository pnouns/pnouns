import { Field, ID, InputType } from "@nestjs/graphql";
import { StockInspiration } from "../../section/enum/stock-inspiration.enum";

@InputType()
export class AddSectionInput {

  @Field(() => ID, {
    description: "The ID of the page to update.",
  })
  pageId: string;

  @Field({
    description: "The title of the section.",
  })
  title: string;

  @Field(() => [StockInspiration], {
    description: "Any stock lists this section should be inspired by.",
    nullable: true,
  })
  inspiration?: StockInspiration[];

}
