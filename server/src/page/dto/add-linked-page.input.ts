import { Field, ID, InputType } from "@nestjs/graphql";

@InputType()
export class AddLinkedPageInput {

  @Field(() => ID, {
    description: "The ID of the parent page that should list a child-page.",
  })
  pageId: string;

  @Field(() => ID, {
    description: "The ID of the sub-page that should be listed.",
  })
  linkedPageId: string;

}
