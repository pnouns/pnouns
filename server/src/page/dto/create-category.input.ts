import { Field, ID, InputType } from "@nestjs/graphql";
import { Length, IsOptional } from "class-validator";
import { CommonName } from "../enum/CommonName";
import { JoiningWord } from "../enum/JoiningWord";

@InputType()
export class CreateCategoryInput {

  @Field(() => ID, {
    description: "The ID of the nickname this page is stored under.",
  })
  nicknameId: string;

  @Field(() => CommonName, {
    nullable: true,
  })
  @IsOptional()
  presetName?: CommonName;

  @Field({
    nullable: true,
  })
  @IsOptional()
  @Length(1, 20, {
    message: "Name must be between 1-20 characters long.",
  })
  customName?: string;

  @Field({
    description: "If 'true', the URL to this category will default to the 'url' pattern.  Ignored if 'customName' is set.",
  })
  defaultSubdomain: boolean;

  @Field(() => JoiningWord, {
    description: "If set, defines a word that will be inserted before this category in the URL.  Ignored if 'defaultSubdomain'.",
    nullable: true,
  })
  defaultJoiningWord?: JoiningWord;

  @Field({
    description: "If 'true', category will be listed in the top-level nickname page.",
  })
  listed: boolean;

}
