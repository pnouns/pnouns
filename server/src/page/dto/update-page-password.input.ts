import { Field, ID, InputType } from "@nestjs/graphql";
import { IsOptional, Length } from "class-validator";
import { PasswordFormat } from "../enum/PasswordFormat";

@InputType()
export class UpdatePagePasswordInput {

  @Field(() => ID)
  pageId: string;

  @Field({
    description: "Set to 'null' to not require a password.",
    nullable: true,
  })
  @IsOptional()
  @Length(2, 6)
  password?: string;

  @Field(() => PasswordFormat, {
    description: "The default format for inserting a password into the URL.",
    nullable: true,
  })
  @IsOptional()
  defaultPasswordFormat?: PasswordFormat;

}
