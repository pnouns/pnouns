import { Field, ID, InputType } from "@nestjs/graphql";

@InputType()
export class UpdateLinkedPageInput {

  @Field(() => ID, {
    description: "The ID of the page listing.",
  })
  linkedSubPageId: string;

  @Field({
    description: "Control if the field uses a custom name.  Leave 'null' to keep current value.",
    nullable: true,
  })
  hasCustomName?: boolean;

  @Field({
    description: "Set the current field.  Leave 'null' to keep current value (use hasCustomName=false to clear)",
    nullable: true,
  })
  customName?: string;

  @Field({
    description: "Set if the subtitle is visible.  Leave 'null' to keep current value.",
    nullable: true,
  })
  showSubtitle?: boolean;

  @Field({
    description: "Control if the field uses a custom subtitle.  Leave 'null' to keep current value.",
    nullable: true,
  })
  hasCustomSubtitle?: boolean;

  @Field({
    description: "Set the subtitle.  Leave 'null' to keep current value (use hasCustomSubtitle=false to clear)",
    nullable: true,
  })
  customSubtitle?: string;

  @Field({
    description: "Control if a caption is displayed.  Leave 'null' to keep current value.",
    nullable: true,
  })
  hasCaption?: boolean;

  @Field({
    description: "Set the caption.  Leave 'null' to keep current value (use hasCaption=false to clear)",
    nullable: true,
  })
  caption?: string;

  @Field({
    description: "Control if the linked page's password is included.  Leave 'null' to keep current value.",
    nullable: true,
  })
  includePassword?: boolean;

}
