import { Field, ID, InputType } from "@nestjs/graphql";

@InputType()
export class CreateNicknamePageInput {

  @Field(() => ID, {
    description: "The ID of the nickname this page is stored under.",
  })
  nicknameId: string;

}
