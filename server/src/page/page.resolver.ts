import {
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { Args, ID, Mutation, Parent, Query, ResolveField, Resolver } from "@nestjs/graphql";
import { ParseJWTGuard } from "../user/parse-jwt.guard";
import { UserId } from "../util/gql-context";
import { NicknameService } from "../nickname/nickname.service";
import { PageModel } from "./model/page.model";
import { PageService } from "./page.service";
import { CreateCategoryInput } from "./dto/create-category.input";
import { CreateNicknamePageInput } from "./dto/create-nickname-page.input";
import { AddSectionInput } from "./dto/add-section.input";
import { UpdatePagePasswordInput } from "./dto/update-page-password.input";
import { Page } from "./entities/page.entity";
import { SectionModel } from "../section/model/section.model";
import { SectionService } from "../section/section.service";
import { SectionType } from "../section/enum/section-type.enum";
import { Section } from "../section/entities/section.entity";
import { PublishedPageModel } from "../published/model/published-page.model";
import { PublishedPageService } from "../published/published-page.service";
import { PublishedPage } from "../published/entities/published-page.entity";
import { UpdatePageOrderInput } from "./dto/update-page-order.input";
import { UpdatePageBioInput } from "./dto/update-page-bio.input";
import { AddLinkedPageInput } from "./dto/add-linked-page.input";
import { UpdateLinkedPageInput } from "./dto/update-linked-page.input";
import { UpdatePageShowBadgesInput } from "./dto/update-page-show-badges.input";
import { User } from "../user/entities/user.entity";
import { UserService } from "../user/user.service";
import { Nickname } from "../nickname/entities/nickname.entity";
import { EventLoggerService } from "../tracking/event-logger.service";
import { GraphIP } from "../tracking/GraphIP.decorator";
import { TrackedEvent } from "../tracking/enum/TrackedEvent.enum";
import { ListedSubPageModel } from "./model/listed-sub-page.model";
import { ListedSubPage } from "./entities/listed-sub-page.entity";
import { CreateCategoryPageInput } from "./dto/create-category-page.input";

const PAGE_NOT_FOUND_ERROR_MESSAGE = "Page not found or you lack permission to view this page.";
const NICKNAME_NOT_FOUND_ERROR_MESSAGE = "Cannot find nickname, or you are trying to write to a nickname you don't own.";

@Resolver(() => PageModel)
export class PageResolver {

  public constructor(
    private readonly configService: ConfigService,
    private readonly eventLoggerService: EventLoggerService,
    private readonly userService: UserService,
    private readonly nicknameService: NicknameService,
    private readonly sectionService: SectionService,
    private readonly pageService: PageService,
    private readonly publishedPageService: PublishedPageService,
  ) {}

  @UseGuards(ParseJWTGuard)
  @Query(() => [PageModel])
  public async myPages(
    @UserId() userId: string,
  ): Promise<Page[]> {
    if(!userId) {
      throw new UnauthorizedException("You must login for this query.");
    }
    const user = await this.userService.getUserById(userId);
    if(!user) {
      throw new UnauthorizedException("User not found.");
    }
    return this.pageService.getByUser(user);
  }

  @UseGuards(ParseJWTGuard)
  @Query(() => PageModel)
  public async getPage(
    @UserId() userId: string,
    @Args("id", { type: () => ID }) id: string,
  ): Promise<Page> {
    const found = await this.pageService.getById(id);
    if(!found) {
      throw new NotFoundException(PAGE_NOT_FOUND_ERROR_MESSAGE);
    }
    const owner = await found.user;
    if(owner.id !== userId) {
      throw new NotFoundException(PAGE_NOT_FOUND_ERROR_MESSAGE);
    }
    return found;
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => PageModel)
  @UsePipes(new ValidationPipe({ transform: true }))
  public async createNicknamePage(
    @GraphIP() ip: string,
    @UserId() userId: string,
    @Args("input", { type: () =>  CreateNicknamePageInput }) input: CreateNicknamePageInput,
  ): Promise<Page> {
    const [nickname, user] = await this.getAuthenticatedNickname(userId, input.nicknameId);
    const page = await this.pageService.createNicknamePage(user, nickname, input);
    this.eventLoggerService.logUserEvent(userId, TrackedEvent.USER_CREATE_NICKNAME_PAGE, ip, {
      nicknameId: nickname.id,
      pageId: page.id,
    });
    return page;
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => PageModel)
  @UsePipes(new ValidationPipe({ transform: true }))
  public async createCategory(
    @GraphIP() ip: string,
    @UserId() userId: string,
    @Args("input", { type: () => CreateCategoryInput }) input: CreateCategoryInput,
  ): Promise<Page> {
    if(!userId) {
      throw new UnauthorizedException("You must login before creating categories.");
    }
    const foundNickname = await this.nicknameService.getById(input.nicknameId);
    if(!foundNickname) {
      throw new NotFoundException(NICKNAME_NOT_FOUND_ERROR_MESSAGE);
    }
    const nicknameOwner = await foundNickname.user;
    if(nicknameOwner.id !== userId) {
      throw new NotFoundException(NICKNAME_NOT_FOUND_ERROR_MESSAGE);
    }
    const hasCommonName = !!input.presetName;
    const hasCustomName = !!input.customName;
    const names = (hasCommonName ? 1 : 0) + (hasCustomName ? 1 : 0);
    if(names !== 1) {
      throw new ReferenceError("You must provide either hasCommonName or hasCustomName, and not both.");
    }
    const page = await this.pageService.createCategory(nicknameOwner, foundNickname, input);
    this.eventLoggerService.logUserEvent(userId, TrackedEvent.USER_CREATE_CATEGORY, ip, {
      nicknameId: foundNickname.id,
      pageId: page.id,
      pageName: page.name,
      request: input,
    });
    return page;
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => PageModel)
  @UsePipes(new ValidationPipe({ transform: true }))
  public async createCategoryPage(
    @GraphIP() ip: string,
    @UserId() userId: string,
    @Args("input", { type: () => CreateCategoryPageInput }) input: CreateCategoryPageInput,
  ): Promise<Page> {
    const [nickname, user] = await this.getAuthenticatedNickname(userId, input.nicknameId);
    const [category] = await this.getAuthenticatedPage(userId, input.categoryId);
    const page = await this.pageService.createCategoryPage(user, nickname, category, input);
    this.eventLoggerService.logUserEvent(userId, TrackedEvent.USER_CREATE_CATEGORY_PAGE, ip, {
      nicknameId: nickname.id,
      categoryId: category.id,
      pageId: page.id,
      pageName: page.name,
      request: input,
    });
    return page;
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => PageModel)
  public async updatePageBio(
    @UserId() userId: string,
    @Args("input", { type: () => UpdatePageBioInput }) input: UpdatePageBioInput,
  ): Promise<Page> {
    const [page] = await this.getAuthenticatedPage(userId, input.pageId);
    return this.pageService.updatePageBio(page, input);
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => SectionModel)
  @UsePipes(new ValidationPipe({ transform: true }))
  public async addNameSection(
    @UserId() userId: string,
    @Args("input", { type: () => AddSectionInput }) input: AddSectionInput,
  ): Promise<Section> {
    const [page, pageOwner] = await this.getAuthenticatedPage(userId, input.pageId);
    const existingSection = await page.nameSection;
    if(existingSection) {
      throw new ReferenceError("Page already has a 'name' section.");
    }
    const section = await this.sectionService.createSection({
      user: pageOwner,
      title: input.title,
      page,
      type: SectionType.NAME,
    });
    const updated = await this.pageService.addNameSection(page, section);
    return updated.nameSection;
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => SectionModel)
  @UsePipes(new ValidationPipe({ transform: true }))
  public async addPronounSection(
    @UserId() userId: string,
    @Args("input", { type: () => AddSectionInput }) input: AddSectionInput,
  ): Promise<Section> {
    const [page, pageOwner] = await this.getAuthenticatedPage(userId, input.pageId);
    const existingSection = await page.pronounSection;
    if(existingSection) {
      throw new ReferenceError("Page already has a 'pronoun' section.");
    }
    const section = await this.sectionService.createSection({
      user: pageOwner,
      title: input.title,
      page,
      type: SectionType.PRONOUNS,
    });
    const updated = await this.pageService.addPronounSection(page, section);
    return updated.pronounSection;
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => SectionModel)
  @UsePipes(new ValidationPipe({ transform: true }))
  public async addSection(
    @UserId() userId: string,
    @Args("input", { type: () => AddSectionInput }) input: AddSectionInput,
  ): Promise<Section> {
    const [page, pageOwner] = await this.getAuthenticatedPage(userId, input.pageId);
    const section = await this.sectionService.createSection({
      user: pageOwner,
      title: input.title,
      page,
      inspiration: input.inspiration,
    });
    const updated = await this.pageService.addSection(page, section);
    return section;
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => PageModel)
  public async updatePageOrder(
    @UserId() userId: string,
    @Args("input", { type: () => UpdatePageOrderInput }) { pageId, order }: UpdatePageOrderInput,
  ): Promise<Page> {
    const [page] = await this.getAuthenticatedPage(userId, pageId);
    const existingSections = await page.sections;
    for(const listedSection of order) {
      if(existingSections.find(i => i.id === listedSection) === undefined) {
        throw new ReferenceError(`Cannot include value '${listedSection}' - doesn't exist in this page.`);
      }
    }
    return this.pageService.setSectionOrder(page, order);
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => ListedSubPageModel)
  @UsePipes(new ValidationPipe({ transform: true }))
  public async addLinkedPage(
    @GraphIP() ip: string,
    @UserId() userId: string,
    @Args("input", { type: () => AddLinkedPageInput }) input: AddLinkedPageInput,
  ): Promise<ListedSubPage> {
    const [page] = await this.getAuthenticatedPage(userId, input.pageId);
    const [linkedPage] = await this.getAuthenticatedPage(userId, input.linkedPageId);
    const isNicknamePage = page.name === null;
    const isCategoryPage = !isNicknamePage && !page.parent || !(await Promise.resolve(page.parent));
    if(isNicknamePage) {
      const nickname = await page.nickname;
      const children = await nickname.pages;
      const foundChild = children.find(c => c.id === linkedPage.id);
      if(!foundChild) {
        throw new NotFoundException(`Page ${page.id} doesn't have a child ${linkedPage.id}`);
      }
    } else if(isCategoryPage) {
      const children = await page.children;
      const foundChild = children.find(c => c.id === linkedPage.id);
      if(!foundChild) {
        throw new NotFoundException(`Page ${page.id} doesn't have a child ${linkedPage.id}`);
      }
    } else {
      throw new ReferenceError(`Page ${page.id} cannot have sub-pages.`);
    }
    const listedSubPage = await this.pageService.createListedSubPage(page, linkedPage);
    await this.pageService.addListedSubPage(page, listedSubPage);
    const found = await this.pageService.getListedSubPageById(listedSubPage.id);
    if(!found) {
      throw new InternalServerErrorException(undefined, "Couldn't find newly created sub-page.");
    }
    this.eventLoggerService.logUserEvent(userId, TrackedEvent.USER_EDITOR_ADD_LINKED_SUB_PAGE, ip, {
      pageId: page.id,
      pageName: page.name,
      subPageId: linkedPage.id,
      subPageName: linkedPage.id,
      request: input,
    });
    return found;
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => PageModel)
  public async updateListedSubPageOrder(
    @UserId() userId: string,
    @Args("input", { type: () => UpdatePageOrderInput }) { pageId, order }: UpdatePageOrderInput,
  ): Promise<Page> {
    const [page] = await this.getAuthenticatedPage(userId, pageId);
    const existingSubPages = await page.listedSubPages;
    for(const listedSubPage of order) {
      if(existingSubPages.find(i => i.id === listedSubPage) === undefined) {
        throw new ReferenceError(`Cannot include value '${listedSubPage}' - doesn't exist in this page.`);
      }
    }
    return this.pageService.setListedSubPageOrder(page, order);
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => ListedSubPageModel)
  @UsePipes(new ValidationPipe({ transform: true }))
  public async updateLinkedPage(
    @UserId() userId: string,
    @Args("input", { type: () => UpdateLinkedPageInput }) input: UpdateLinkedPageInput,
  ): Promise<ListedSubPage> {
    const [subPage] = await this.getAuthenticatedSubPage(userId, input.linkedSubPageId);
    return await this.pageService.updateListedSubPage(subPage, input);
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => PageModel)
  public async deleteListedSubPage(
    @UserId() userId: string,
    @Args("id", { type: () => ID }) listedSubPageId: string,
  ): Promise<Page> {
    const [subPage, page] = await this.getAuthenticatedSubPage(userId, listedSubPageId);
    await this.pageService.deleteListedSubPage(subPage);
    return page;
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => PageModel)
  public async updatePagePassword(
    @GraphIP() ip: string,
    @UserId() userId: string,
    @Args("input", { type: () => UpdatePagePasswordInput }) input: UpdatePagePasswordInput,
  ): Promise<Page> {
    const { pageId, password, defaultPasswordFormat } = input;
    const [page] = await this.getAuthenticatedPage(userId, pageId);
    const updated = await this.pageService.updatePassword(page, input);
    this.eventLoggerService.logUserEvent(userId, TrackedEvent.USER_EDITOR_UPDATE_PASSWORD, ip, {
      pageId,
      pageName: page.name,
      hasPassword: password && password.length > 1,
      passwordLength: password?.length,
      defaultPasswordFormat,
    });
    return updated;
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => PageModel)
  public async updatePageShowBadges(
    @GraphIP() ip: string,
    @UserId() userId: string,
    @Args("input", { type: () => UpdatePageShowBadgesInput }) input: UpdatePageShowBadgesInput,
  ): Promise<Page> {
    const { pageId, inherit, showBadges } = input;
    const [page] = await this.getAuthenticatedPage(userId, pageId);
    const updated = await this.pageService.updateShowBadges(page, inherit, showBadges);
    this.eventLoggerService.logUserEvent(userId, TrackedEvent.USER_EDITOR_CHANGE_PAGE_SHOW_BADGES, ip, {
      pageId,
      pageName: page.name,
      inherit,
      showBadges,
    });
    return updated;
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => PublishedPageModel)
  public async publish(
    @GraphIP() ip: string,
    @UserId() userId: string,
    @Args("id", { type: () => ID }) pageId: string,
  ): Promise<PublishedPage> {
    const [page] = await this.getAuthenticatedPage(userId, pageId);
    const published = await this.publishedPageService.createOrUpdatePage(page);
    const previouslyPublished = page.published && await page.published;
    this.eventLoggerService.logUserEvent(userId, TrackedEvent.USER_PUBLISH_PAGE, ip, {
      pageId,
      pageName: page.name,
      firstPublication: !previouslyPublished,
    });
    if(!page.published || !previouslyPublished || previouslyPublished.id !== published.id) {
      await this.pageService.setPublishedPage(page, published);
    }
    return published;
  }

  @ResolveField()
  async isPublished(@Parent() page: Page) {
    if(!page.published) {
      return false;
    }
    const publishedData = await page.published;
    if(!publishedData) {
      return false;
    }
    return true;
  }

  @ResolveField()
  public async subPageOptions(@Parent() page: Page): Promise<Page[]> {
    const isNicknamePage = page.name === null;
    const isCategoryPage = !isNicknamePage && !page.parent || !(await Promise.resolve(page.parent));
    if(isNicknamePage) {
      const nickname = await page.nickname;
      return nickname.pages;
    } else if(isCategoryPage) {
      return page.children;
    } else {
      return [];
    }
  }

  private async getAuthenticatedNickname(
    userId: string,
    nicknameId: string,
  ): Promise<[Nickname, User]> {
    if(!userId) {
      throw new UnauthorizedException("You must be logged in to perform this action.");
    }
    const nickname = await this.nicknameService.getById(nicknameId);
    const nicknameOwner = nickname && await nickname.user;
    if(!nickname || !nicknameOwner || nicknameOwner.id !== userId) {
      throw new UnauthorizedException("Nickname not found or you do not have permissions to access this nickname.");
    }
    return [nickname, nicknameOwner];
  }

  private async getAuthenticatedPage(
    userId: string,
    pageId: string,
  ): Promise<[Page, User]> {
    if(!userId) {
      throw new UnauthorizedException("You must login before updating pages.");
    }
    const page = await this.pageService.getById(pageId);
    const pageOwner = page && await page.user;
    if(!page || !pageOwner || pageOwner.id !== userId) {
      throw new UnauthorizedException(PAGE_NOT_FOUND_ERROR_MESSAGE);
    }
    return [page, pageOwner];
  }

  private async getAuthenticatedSubPage(
    userId: string,
    linkedSubPageId: string,
  ): Promise<[ListedSubPage, Page, User]> {
    if(!userId) {
      throw new UnauthorizedException("You must login before updating sub-pages.");
    }
    const subPage = await this.pageService.getListedSubPageById(linkedSubPageId);
    const page = subPage && await subPage.parent;
    const pageOwner = page && await page.user;
    if(!subPage || !page || !pageOwner || pageOwner.id !== userId) {
      throw new UnauthorizedException("Sub-page not found or you do not have permission to access this entity.");
    }
    return [subPage, page, pageOwner];
  }

}
