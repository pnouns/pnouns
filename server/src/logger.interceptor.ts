import {
  CallHandler,
  ExecutionContext,
  Injectable,
  LoggerService,
  NestInterceptor,
} from "@nestjs/common";
import { WinstonLogger } from "nest-winston";
import { Logger } from "winston";
import { GqlContextType, GqlExecutionContext } from "@nestjs/graphql";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { Request } from "express";
import { getGraphQLContext } from "./util/gql-context";
import { ShipLog } from "./util/log";

@Injectable()
export class LoggingInterceptor implements NestInterceptor {

  private readonly winstonLogger: Logger;

  constructor(
    private nestLogger: LoggerService,
  ) {
    // Fine type coercion:
    const nestWinstonLogger = nestLogger as WinstonLogger;
    // Get a private property:
    const _nestWinstonLogger = nestWinstonLogger as any;
    this.winstonLogger = _nestWinstonLogger.logger as Logger;
  }

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    if(context.getType() === "http") {
      const ctx = context.switchToHttp();
      const request = ctx.getRequest<Request>();
      // TODO: Do stuff
    } else if(context.getType<GqlContextType>() === "graphql") {
      const ctx = getGraphQLContext(context);
      const req = context.switchToHttp().getRequest() as Request;
      const logger = new ShipLog(this.winstonLogger);
      logger.setRequestId(req.headers["x-request-id"]);
      ctx.setLogger(logger);
      // TODO: Do stuff
    }

    return next.handle().pipe(
      tap({
        next: (value) => {
          // TODO: Log response
        },
        error: (err) => {
          // TODO: Log error
        },
        complete: () => {
          // TODO: Log complete
        },
      }),
    );
  }

}
