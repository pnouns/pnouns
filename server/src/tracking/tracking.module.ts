import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { EventLoggerService } from "./event-logger.service";

@Module({
  imports: [
    ConfigModule,
  ],
  providers: [
    EventLoggerService,
  ],
  exports: [
    EventLoggerService,
  ],
})
export class TrackingModule {}
