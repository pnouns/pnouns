export enum TrackedEvent {
  USER_AUTHENTICATED = "server:UserAuthenticated",
  USER_NICKNAME_CREATE = "server:UserNicknameCreate",
  USER_CREATE_NICKNAME_PAGE = "server:UserPageCreateNicknamePage",
  USER_CREATE_CATEGORY = "server:UserPageCreateCategory",
  USER_CREATE_CATEGORY_PAGE = "server:UserPageCreateCategoryPage",
  USER_EDITOR_UPDATE_PASSWORD = "server:UserEditorUpdatePassword",
  USER_EDITOR_ADD_LINKED_SUB_PAGE = "server:UserEditorAddLinkedSubPage",
  USER_PUBLISH_PAGE = "server:UserPagePublish",
  USER_EDITOR_CHANGE_PAGE_SHOW_BADGES = "server:UserEditorChangePageShowBadges",
  SERVER_GRAPHQL_ERROR = "server:GraphQLError",
}
