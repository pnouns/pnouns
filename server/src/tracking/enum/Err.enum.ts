export enum Err {
  /**
   * Server-critical, major effects to multiple users.
   */
  CRITICAL = 10,

  /**
   * Major effect to one user.
   */
  SEVERE = 100,

  /**
   * Issue for a user.
   */
  ERROR = 200,

  WARN = 300,

  NOTE = 1000,

  LOG = 2000,

  DEBUG = 5000,

  TRACE = 6000,

  VERBOSE = 9000,
}

export const ERR_LABELS: Record<Err, string> = {
  [Err.CRITICAL]: "critical",
  [Err.SEVERE]: "severe",
  [Err.ERROR]: "error",
  [Err.WARN]: "warn",
  [Err.NOTE]: "note",
  [Err.LOG]: "log",
  [Err.DEBUG]: "debug",
  [Err.TRACE]: "trace",
  [Err.VERBOSE]: "verbose",
}
