import {
  createParamDecorator,
  ExecutionContext,
} from "@nestjs/common";
import { getGraphQLContext } from "../util/gql-context";
import * as requestIp from "request-ip";

export const GraphIP = createParamDecorator((data: unknown, context: ExecutionContext) => {
  const ctx = getGraphQLContext(context);
  return requestIp.getClientIp(ctx.req);
});
