import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import PostHog from "posthog-node";
import { IssuerEnvironment } from "../user/enum/IssuerEnvironment";
import { TrackedEvent } from "./enum/TrackedEvent.enum";
import { Err } from "./enum/Err.enum";
import { ResolverEventLogger } from "./resolver-event-logger";
import { VERSION } from "../version";

@Injectable()
export class EventLoggerService {

  private readonly env: IssuerEnvironment;

  /**
   * Attached with requests - allows non-genuine-server requests to be filtered out.
   */
  private readonly posthogKey: string;

  private readonly hog: PostHog;

  public constructor(
    private readonly configService: ConfigService,
  ) {
    this.env = configService.get<IssuerEnvironment>("ISSUER", IssuerEnvironment.DEVELOPMENT)
    const postHogServer = configService.get("POSTHOG_SERVER", "https://hog.pnouns.fyi");
    const apiKey = configService.get("POSTHOG_API_KEY", "");
    this.hog = new PostHog(apiKey, {
      host: postHogServer,
    });
    this.posthogKey = configService.get("POSTHOG_SECRET_KEY", "DefaultServerKey");
  }

  public getResolverEventLogger(
    resolver: Function,
    method: string,
    commonData: object = {},
  ): ResolverEventLogger {
    return new ResolverEventLogger(
      this.hog,
      resolver.name,
      method,
      commonData,
    );
  }

  public logResolverEvent(
    resolver: Function,
    method: string,
    channel: Err,
    event: string,
    data: object = {},
  ) {
    this.hog.capture({
      distinctId: "0",
      event: `server:${event}`,
      properties: {
        ...data,
        resolverEvent: event,
        resolver: resolver.name,
        service: resolver.name,
        method: `${resolver.name}#${method}`,
        level: channel,
        levelRank: channel,
      },
    });
  }

  public logUserEvent(
    userId: string,
    eventName: TrackedEvent,
    ip: string,
    data: object = {},
  ) {
    this.hog.capture({
      distinctId: userId,
      event: eventName,
      properties: {
        $ip: ip,
        env: this.env,
        serverKey: this.posthogKey,
        serverClientIp: ip,
        serverVersion: VERSION,
        ...data,
      },
    });
  }

}
