import PostHog from "posthog-node";
import { Err, ERR_LABELS } from "./enum/Err.enum";

export class ResolverEventLogger {

  public constructor(
    private readonly hog: PostHog,
    private readonly resolverName: string,
    private readonly methodName: string,
    private readonly commonData: object = {},
  ) {}

  public catch(event: string, level: Err = Err.ERROR, data: object = {}) {
    return (e: Error) => {
      this.event(level, `server:${event}`, {
        ...data,
        error: e,
      });
      throw e;
    };
  }

  public error(event: string, data: object = {}) {
    this.event(Err.ERROR, event, data);
  }

  private event(level: Err, event: string, data: object = {}) {
    this.hog.capture({
      distinctId: "0",
      event: `server:${event}`,
      properties: {
        ...this.commonData,
        ...data,
        resolverEvent: event,
        resolver: this.resolverName,
        service: this.resolverName,
        method: `${this.resolverName}#${this.methodName}`,
        level: level,
        levelRank: ERR_LABELS[level],
      },
    });
  }

}
