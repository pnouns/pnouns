import { NestFactory } from "@nestjs/core";
import { utilities as nestWinstonModuleUtilities, WinstonModule } from "nest-winston";
import { format, transports } from "winston";
import session from "express-session";
import passport from "passport";
import { AppModule } from "./app.module";
import { NotFoundExceptionFilter } from "./catch-all.filter";
import { addRequestId } from "./util/add-request-id.middleware";

const HTTP_PORT = 9510;

export async function bootstrap() {

  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger({
      format: format.combine(
        format.timestamp(),
        format.ms(),
        format.colorize({ all: true }),
      ),
      transports: [
        new transports.File({ filename: "error.log", level: "error" }),
        new transports.File({ filename: "pnouns.log" }),
        new transports.Console({
          format: format.combine(
            format.timestamp(),
            format.ms(),
            nestWinstonModuleUtilities.format.nestLike(),
          ),
        }),
      ],
    }),
  });
  app.use(addRequestId);
  /*app.use(session({
    cookie: {
      maxAge: 86400000,
    },
    secret: process.env.SESSION_SECRET ?? "secret",
  }))*/
  app.use(passport.initialize());
  //app.use(passport.session());
  app.useGlobalFilters(new NotFoundExceptionFilter());
  await app.listen(HTTP_PORT);
  console.info(`Started Pronoun server on port ${HTTP_PORT}`);
}
