import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { GqlContextType } from "@nestjs/graphql";
import { Logger } from "winston";
import { Request } from "express";
import { getGraphQLContext } from "./gql-context";

/**
 * Per-request logger.
 */
export class ShipLog {

  private requestId: string | null;
  private userId: string;

  constructor(
    private readonly logger: Logger,
  ) {}

  public setRequestId(requestId?: string | string[]): void {
    if(!requestId) {
      return;
    }
    this.requestId = Array.isArray(requestId) ? requestId[0] : requestId;
  }

  public setUserId(userId: string): void {
    this.userId = userId;
  }

  public log(message: string, context: object = {}): void {
    this.logger.log(message, {
      ...this.metadata(),
      context,
    });
  }

  private metadata() {
    return {
      reqId: this.requestId,
      uid: this.userId,
    };
  }

}

export const RequestLogger = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    if(context.getType() === "http") {
      return;
    } else if(context.getType<GqlContextType>() === "graphql") {
      const ctx = getGraphQLContext(context);
      const logger = ctx.getLogger();
      if(!logger) {
        return;
      }
      logger.setRequestId(context.switchToHttp().getRequest().headers["x-request-id"]);
      return logger;
    }
  },
);
