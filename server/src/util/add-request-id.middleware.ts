import { Request, Response, NextFunction } from "express";
import { v4 as uuid } from "uuid";

export function addRequestId(req: Request, res: Response, next: NextFunction) {
  if(!req.headers["x-request-id"]) {
    req.headers["x-request-id"] = uuid();
  }
  next();
}
