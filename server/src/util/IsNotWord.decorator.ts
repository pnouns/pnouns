import { registerDecorator, ValidationOptions, ValidationArguments } from "class-validator";
import wordList from "an-array-of-english-words";

export function IsNotWord(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'isNotWord',
      target: object.constructor,
      propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          return typeof value === "string" && !wordList.includes(value);
        }
      }
    })
  }
}
