import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { GqlExecutionContext } from "@nestjs/graphql";
import { IncomingMessage } from "http";
import { JWT } from "../user/dto/jwt.dto";
import { ShipLog } from "./log";

/*export interface BaseGraphQLContext {

  req: IncomingMessage;

}*/

export class PronounsGraphQLContext {

  public readonly req: IncomingMessage;

  private hasAuthenticated: boolean | null;

  private sub: string | null;

  private jwt: JWT | null;

  private log: ShipLog | null;

  public constructor(req: IncomingMessage) {
    this.req = req;
    this.hasAuthenticated = null;
    this.sub = null;
    this.jwt = null;
    this.log = null;
  }

  public setHasAuthenticated(status: boolean): void {
    this.hasAuthenticated = status;
  }

  public getHasAuthenticated(): boolean | null {
    return this.hasAuthenticated;
  }

  public setSub(sub: string): void {
    this.sub = sub;
  }

  public getSub(): string | null {
    return this.sub;
  }

  public setJwt(jwt: JWT): void {
    this.jwt = jwt;
  }

  public getJwt(): JWT | null {
    return this.jwt;
  }

  public setLogger(log: ShipLog) {
    this.log = log;
  }

  public getLogger(): ShipLog | null {
    return this.log;
  }

}

export function defaultGraphQLContext(req: IncomingMessage): PronounsGraphQLContext {
  return new PronounsGraphQLContext(req);
}

export function getGraphQLContext(executionContext: ExecutionContext): PronounsGraphQLContext {
  const gqlCtx = GqlExecutionContext.create(executionContext);
  const ctx = gqlCtx.getContext<PronounsGraphQLContext>();
  return ctx;
}

export const UserId = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    const ctx = getGraphQLContext(context);
    return ctx.getSub();
  },
);
