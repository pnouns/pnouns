import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Page } from "../../page/entities/page.entity";
import { User } from "../../user/entities/user.entity";
import { SectionType } from "../enum/section-type.enum";
import { StockInspiration } from "../enum/stock-inspiration.enum";
import { SectionValue } from "./section-value.entity";

@Entity()
export class Section {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @ManyToOne(() => User, user => user.sections)
  user: Promise<User>;

  /**
   * If this is a specific special type of section.
   */
  @Column({
    type: "enum",
    enum: SectionType,
    nullable: true,
  })
  type?: SectionType;

  /**
   * The title of this section.
   */
  @Column("varchar", { length: 63 })
  title: string;

  /**
   * A subtitle for this section
   */
  @Column("varchar", { length: 127, nullable: true })
  subtitle?: string;

  /**
   * A lead sentence/paragraph for this section.
   */
  @Column("varchar", { length: 1024, nullable: true })
  lead?: string;

  /**
   * This is hackish.  Ordering elements is being a pain.
   * The proper implementation will be next/back.
   * Instead, just create an array in the entity.
   */
  @Column("simple-array")
  valueOrder: string[];

  /**
   * The items inside this section.
   */
  @OneToMany(() => SectionValue, value => value.section, {
    onDelete: "CASCADE",
  })
  values: Promise<SectionValue[]>;

  @ManyToOne(() => Page, page => page.sections)
  page: Promise<Page>;

  /**
   * If this section is an un-modified mirror of another section.
   * No editing of this section is allowed if this is set.
   */
  @ManyToOne(() => Section, section => section.aliases, {
    nullable: true,
  })
  aliasOf?: Promise<Section>;

  /**
   * Sections this section uses as value sources.
   * Note that this doesn't actually add anything automatically, just provides autocomplete values.
   */
  @ManyToMany(() => Section, section => section.inspiredSections, {
    cascade: ["insert", "update"],
  })
  @JoinTable()
  sectionInspiration: Promise<Section[]>;

  /**
   * Sets of values (pre-built included in the client) that could auto-complete for this section.
   */
  @Column({
    type: "set",
    enum: StockInspiration,
    default: [],
  })
  stockInspiration: StockInspiration[];

  /**
   * Other sections that mirror this one.
   * This section can be updated, but any sections included in this array are not editable.
   */
  @OneToMany(() => Section, section => section.aliasOf)
  aliases: Promise<Section[]>;

  /**
   * Sections that use this section to suggest autocomplete options.
   */
  @ManyToMany(() => Section, section => section.sectionInspiration)
  inspiredSections: Promise<Section[]>;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

}
