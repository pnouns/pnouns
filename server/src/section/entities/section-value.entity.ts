import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { StockInspiration } from "../enum/stock-inspiration.enum";
import { Section } from "./section.entity";
import { Strength } from "../enum/strength.enum";

@Entity()
export class SectionValue {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  /**
   * The text of the item.
   */
  @Column("varchar", { length: 127 })
  text: string;

  @OneToOne(() => SectionValue, value => value.previous, {
    nullable: true,
  })
  @JoinColumn()
  next?: Promise<SectionValue>;

  @OneToOne(() => SectionValue, value => value.next, {
    nullable: true,
  })
  previous?: Promise<SectionValue>;

  /**
   * How the user feels about this item.
   */
  @Column({
    type: "enum",
    enum: Strength,
    default: Strength.UNSURE,
  })
  strength: Strength;

  @Column("varchar", {
    length: 31,
    nullable: true,
  })
  customStrengthText?: string;

  /**
   * The section that this item is in.
   */
  @ManyToOne(() => Section, section => section.values)
  section: Promise<Section>;

  /**
   * If this was an auto-complete suggestion from another section.
   */
  @ManyToOne(() => Section, {
    nullable: true,
  })
  inspiredFromSection?: Promise<Section>;

  /**
   * If this was an auto-complete suggestion from a pre-compiled list.
   */
  @Column({
    type: "enum",
    enum: StockInspiration,
    nullable: true,
  })
  inspiredFromStock?: StockInspiration;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

}
