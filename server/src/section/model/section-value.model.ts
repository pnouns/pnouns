import { Field, ID, ObjectType } from "@nestjs/graphql";
import { Strength } from "../enum/strength.enum";

@ObjectType("SectionValue", {
  description: "A single value in a section",
})
export class SectionValueModel {

  @Field(() => ID)
  id: string;

  @Field(() => SectionValueModel, {
    nullable: true,
  })
  next?: SectionValueModel;

  @Field(() => SectionValueModel, {
    nullable: true,
  })
  previous?: SectionValueModel;

  @Field()
  text: string;

  @Field(() => Strength)
  strength: Strength;

  @Field({
    nullable: true,
  })
  customStrengthText: string;

}
