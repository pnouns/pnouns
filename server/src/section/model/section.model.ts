import { Field, ID, ObjectType } from "@nestjs/graphql";
import { SectionType } from "../enum/section-type.enum";
import { SectionValueModel } from "./section-value.model";

@ObjectType("Section", {
  description: "A list of options on a page.",
})
export class SectionModel {

  @Field(() => ID)
  id: string;

  @Field(() => SectionType, {
    nullable: true,
  })
  type?: SectionType;

  @Field()
  title: string;

  @Field({
    nullable: true,
  })
  subtitle?: string;

  @Field({
    nullable: true,
  })
  lead?: string;

  @Field(() => [String], {
    description: "A list of the value IDs in order.  Long-term we'll use next/previous on the values.",
  })
  valueOrder: string[];

  @Field(() => [SectionValueModel])
  values: SectionValueModel[];

}
