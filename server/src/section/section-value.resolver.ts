import { UnauthorizedException, UseGuards, UsePipes, ValidationPipe } from "@nestjs/common";
import { Args, ID, Mutation, Resolver } from "@nestjs/graphql";
import { ParseJWTGuard } from "../user/parse-jwt.guard";
import { SectionValueService } from "./section-value.service";
import { UserId } from "../util/gql-context";
import { CreateSectionValueInput } from "./dto/create-section-value.input";
import { SectionValueModel } from "./model/section-value.model";
import { SectionService } from "./section.service";
import { Strength } from "./enum/strength.enum";
import { UpdateStrengthInput } from "./dto/update-strength.input";
import { MoveSectionValueInput } from "./dto/move-section-value.input";
import { EditSectionValueTextInput } from "./dto/edit-section-value-text.input";
import { SectionValue } from "./entities/section-value.entity";
import { User } from "../user/entities/user.entity";
import { SectionModel } from "./model/section.model";
import { Section } from "./entities/section.entity";

@Resolver(() => SectionValueModel)
export class SectionValueResolver {

  constructor(
    private readonly sectionService: SectionService,
    private readonly sectionValueService: SectionValueService,
  ) {}

  @UseGuards(ParseJWTGuard)
  @Mutation(() => SectionValueModel)
  @UsePipes(new ValidationPipe({ transform: true }))
  public async createSectionValue(
    @UserId() userId: string,
    @Args("input", { type: () => CreateSectionValueInput }) input: CreateSectionValueInput,
  ) {
    if(!userId) {
      throw new UnauthorizedException("You must login before creating categories.");
    }
    const foundSection = await this.sectionService.getById(input.sectionId);
    const sectionOwner = foundSection && await foundSection.user;
    if(!foundSection || !sectionOwner || sectionOwner.id !== userId) {
      throw new UnauthorizedException("Section not found, or you lack permission to view the section.");
    }
    return this.sectionValueService.createSectionValue(foundSection, input.text, Strength.UNSURE, input.previous, input.next);
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => SectionModel)
  public async deleteSectionValue(
    @UserId() userId: string,
    @Args("id", { type: () => ID }) id: string,
  ): Promise<Section> {
    if(!userId) {
      throw new UnauthorizedException("You must login.");
    }
    const [sectionValue] = await this.getSectionValueAuthenticated(userId, id);
    return this.sectionValueService.deleteSectionValue(sectionValue);
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => SectionValueModel)
  public async editSectionValueText(
    @UserId() userId: string,
    @Args("input", { type: () => EditSectionValueTextInput }) input: EditSectionValueTextInput,
  ) {
    const [sectionValue] = await this.getSectionValueAuthenticated(userId, input.sectionValueId);
    return this.sectionValueService.updateText(sectionValue, input.text);
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => SectionValueModel)
  public async editSectionValueStrength(
    @UserId() userId: string,
    @Args("input", { type: () => UpdateStrengthInput }) input: UpdateStrengthInput,
  ) {
    const [sectionValue] = await this.getSectionValueAuthenticated(userId, input.sectionValueId);
    return this.sectionValueService.updateStrength(sectionValue, input.strength);
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => SectionValueModel)
  public async moveSectionValue(
    @UserId() userId: string,
    @Args("input", { type: () => MoveSectionValueInput }) input: MoveSectionValueInput,
  ) {
    const [sectionValue] = await this.getSectionValueAuthenticated(userId, input.sectionValueId);
    const newPrevious = input.newPrevious && (await this.getSectionValueAuthenticated(userId, input.newPrevious))[0];
    const newPreviousNext = newPrevious && await Promise.resolve(newPrevious.next);
    const newNext = input.newNext && (await this.getSectionValueAuthenticated(userId, input.newNext))[0];
    const oldNext = await Promise.resolve(sectionValue.next);
    const oldPrevious = await Promise.resolve(sectionValue.previous);
    await this.sectionValueService.removeNext(sectionValue);
    if(oldPrevious) {
      console.log("Remove old previous");
      await this.sectionValueService.removeNext(oldPrevious);
    }
    if(newPrevious) {
      console.log("Remove new previous");
      await this.sectionValueService.removeNext(newPrevious);
    }
    if(oldPrevious && oldNext) {
      await this.sectionValueService.connect(oldPrevious, oldNext);
    }
    if(newPrevious && newNext) {
      await this.sectionValueService.connect(newPrevious, sectionValue);
      await this.sectionValueService.connect(sectionValue, newNext);
    } else if(newPrevious && newPreviousNext) {
      await this.sectionValueService.connect(newPrevious, sectionValue);
      await this.sectionValueService.connect(sectionValue, newPreviousNext);
    } else if(newPrevious) {
      await this.sectionValueService.connect(newPrevious, sectionValue);
    } else if(newNext) {
      await this.sectionValueService.connect(sectionValue, newNext);
    }
    return sectionValue;
  }

  private async getSectionValueAuthenticated(
    userId: string,
    sectionValueId: string,
  ): Promise<[SectionValue, User]> {
    if(!userId) {
      throw new UnauthorizedException("You must be logged in.");
    }
    const sectionValue = await this.sectionValueService.getById(sectionValueId);
    const user = sectionValue && await (await sectionValue.section).user;
    if(!sectionValue || !user || user.id !== userId) {
      throw new UnauthorizedException("Section value not found, or you do not have permission to view this page.");
    }
    return [sectionValue, user];
  }

}
