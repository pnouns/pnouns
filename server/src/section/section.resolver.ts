import { UnauthorizedException, UseGuards } from "@nestjs/common";
import { Args, ID, Mutation, Query, Resolver } from "@nestjs/graphql";
import { ParseJWTGuard } from "../user/parse-jwt.guard";
import { UserId } from "../util/gql-context";
import { SectionModel } from "./model/section.model";
import { SectionService } from "./section.service";
import { EditSectionTextFieldInput } from "./dto/edit-section-text-field.input";
import { UpdateSectionValueOrderInput } from "./dto/update-section-value-order.input";
import { SectionValueService } from "./section-value.service";
import { Section } from "./entities/section.entity";
import { PageModel } from "../page/model/page.model";
import { Page } from "../page/entities/page.entity";
import { User } from "../user/entities/user.entity";

@Resolver(() => SectionModel)
export class SectionResolver {

  public constructor(
    private readonly sectionService: SectionService,
  ) {}

  @UseGuards(ParseJWTGuard)
  @Query(() => SectionModel)
  public async section(
    @UserId() userId: string,
    @Args("id", { type: () => ID }) id: string,
  ) {
    if(!userId) {
      throw new UnauthorizedException("You must be logged in.");
    }
    const section = await this.sectionService.getById(id);
    const user = section && await (await section.page).user;
    if(!section || !user || user.id !== userId) {
      throw new UnauthorizedException("Section not found, or you do not have permission to view this page.");
    }
    return section;
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => SectionModel)
  public async editSectionTextField(
    @UserId() userId: string,
    @Args("input", { type: () => EditSectionTextFieldInput }) input: EditSectionTextFieldInput,
  ) {
    if(!userId) {
      throw new UnauthorizedException("You must be logged in.");
    }
    const section = await this.sectionService.getById(input.sectionId);
    const user = section && await (await section.page).user;
    if(!section || !user || user.id !== userId) {
      throw new UnauthorizedException("Section not found, or you do not have permission to view this page.");
    }
    return this.sectionService.editTextField(section, input);
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => SectionModel)
  public async updateSectionValueOrder(
    @UserId() userId: string,
    @Args("input", { type: () => UpdateSectionValueOrderInput }) input: UpdateSectionValueOrderInput,
  ): Promise<Section> {
    const [section] = await this.getAuthenticatedSection(userId, input.sectionId);
    const order = input.order;
    const existingValues = await section.values;
    for (const listedValue of order) {
      if(existingValues.find(i => i.id === listedValue) === undefined) {
        throw new ReferenceError(`Can't include value '${listedValue}' - not in this section.`);
      }
    }
    return this.sectionService.setValueOrder(section, order);
  }

  @UseGuards(ParseJWTGuard)
  @Mutation(() => PageModel)
  public async deleteSection(
    @UserId() userId: string,
    @Args("id", { type: () => ID }) sectionId: string,
  ): Promise<Page> {
    const [section] = await this.getAuthenticatedSection(userId, sectionId);
    const page = await section.page;
    await this.sectionService.deleteSection(section);
    return page;
  }

  private async getAuthenticatedSection(
    userId: string,
    sectionId: string,
  ): Promise<[Section, User]> {
    if(!userId) {
      throw new UnauthorizedException("You must be logged in.");
    }
    const section = await this.sectionService.getById(sectionId);
    const user = section && await (await section.page).user;
    if(!section || !user || user.id !== userId) {
      throw new UnauthorizedException("Section not found, or you do not have permission to view this page.");
    }
    return [section, user];
  }

}
