import { forwardRef, Inject, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Page } from "../page/entities/page.entity";
import { CreateSectionOptions } from "./dto/create-section";
import { EditSectionTextFieldInput } from "./dto/edit-section-text-field.input";
import { SectionValue } from "./entities/section-value.entity";
import { Section } from "./entities/section.entity";
import { SectionTextField } from "./enum/section-text-fields.enum";
import { SectionType } from "./enum/section-type.enum";
import { stockInspirationValues } from "./enum/stock-inspiration.enum";
import { Strength } from "./enum/strength.enum";
import { SectionValueService } from "./section-value.service";

@Injectable()
export class SectionService {

  public constructor(
    @Inject(forwardRef(() => SectionValueService))
    private readonly sectionValueService: SectionValueService,
    @InjectRepository(Page)
    private readonly pageRepository: Repository<Page>,
    @InjectRepository(Section)
    private readonly sectionRepository: Repository<Section>,
  ) {}

  public getById(id: string): Promise<Section | undefined> {
    return this.sectionRepository.findOne(id);
  }

  public async createSection({
    title,
    page,
    user,
    type,
    inspiration,
  }: CreateSectionOptions): Promise<Section> {
    const section = new Section();
    section.title = title;
    section.page = Promise.resolve(page);
    section.user = Promise.resolve(user);
    section.type = type;
    section.valueOrder = [];
    section.stockInspiration = inspiration ?? [];
    const created = await this.sectionRepository.save(section);
    if(inspiration) {
      for (const stockInspiration of inspiration) {
        const values = stockInspirationValues[stockInspiration];
        for (const value of values) {
          await this.sectionValueService.createSectionValue(
            created,
            value,
            Strength.UNSURE,
          );
        }
      }
    }
    return created;
  }

  public editTextField(
    section: Section,
    { field, value }: EditSectionTextFieldInput,
  ): Promise<Section> {
    if(field === SectionTextField.TITLE) {
      section.title = value;
    } else if(field === SectionTextField.SUBTITLE) {
      section.subtitle = value;
    } else if(field === SectionTextField.LEAD) {
      section.lead = value;
    } else {
      throw new ReferenceError(`Unknown text field: ${field}`);
    }
    return this.sectionRepository.save(section);
  }

  public async appendValueOrder(
    section: Section,
    value: SectionValue,
  ): Promise<Section> {
    return this.setValueOrder(section, [...section.valueOrder, value.id]);
  }

  public async setValueOrder(
    section: Section,
    valueOrder: string[],
  ): Promise<Section> {
    section.valueOrder = valueOrder;
    const updated = await this.sectionRepository.save(section);
    return updated;
  }

  public async deleteSection(
    section: Section,
  ): Promise<void> {
    if(section.type === SectionType.NAME) {
      const page = await section.page;
      page.nameSection
      await this.pageRepository.createQueryBuilder()
        .relation(Page, "nameSection")
        .of(page)
        .set(null);
    }
    if(section.type === SectionType.PRONOUNS) {
      const page = await section.page;
      await this.pageRepository.createQueryBuilder()
        .relation(Page, "pronounSection")
        .of(page)
        .set(null);
    }
    await this.sectionRepository.delete(section.id);
  }

}
