import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Section } from "./entities/section.entity";
import { SectionService } from "./section.service";
import { SectionValueService } from "./section-value.service";
import {SectionResolver } from "./section.resolver";
import { ParseJwtImports, UserModule } from "../user/user.module";
import { SectionValue } from "./entities/section-value.entity";
import { SectionValueResolver } from "./section-value.resolver";
import { Page } from "../page/entities/page.entity";

@Module({
  imports: [
    ...ParseJwtImports,
    TypeOrmModule.forFeature([
      Section,
      SectionValue,
      Page,
    ]),
    UserModule,
  ],
  providers: [
    SectionService,
    SectionValueService,
    SectionResolver,
    SectionValueResolver,
  ],
  exports: [
    SectionService,
    SectionValueService,
  ],
})
export class SectionModule {}
