import { Field, ID, InputType } from "@nestjs/graphql";

@InputType()
export class UpdateSectionValueOrderInput {

  @Field(() => ID)
  sectionId: string;

  @Field(() => [String], {
    description: "A list of section value IDs that should be ordered.",
  })
  order: string[];

}
