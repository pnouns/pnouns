import { Field, ID, InputType } from "@nestjs/graphql";

@InputType()
export class EditSectionValueTextInput {

  @Field(() => ID)
  sectionValueId: string;

  @Field()
  text: string;

}
