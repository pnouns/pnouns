import { Page } from "../../page/entities/page.entity";
import { User } from "../../user/entities/user.entity";
import { SectionType } from "../enum/section-type.enum";
import { StockInspiration } from "../enum/stock-inspiration.enum";

export interface CreateSectionOptions {

  title: string;

  page: Page;

  user: User;

  type?: SectionType;

  inspiration?: StockInspiration[];

}
