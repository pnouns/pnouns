import { Field, ID, InputType } from "@nestjs/graphql";
import { Length } from "class-validator";

@InputType()
export class CreateSectionValueInput {

  @Field(() => ID)
  sectionId: string;

  @Field()
  @Length(1, 100)
  text: string;

  @Field(() => ID, {
    nullable: true,
  })
  previous?: string;

  @Field(() => ID, {
    nullable: true,
  })
  next?: string;

}
