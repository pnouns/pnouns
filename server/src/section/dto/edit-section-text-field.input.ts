import { Field, ID, InputType } from "@nestjs/graphql";
import { SectionTextField } from "../enum/section-text-fields.enum";

@InputType()
export class EditSectionTextFieldInput {

  @Field(() => ID)
  sectionId: string;

  @Field(() => SectionTextField)
  field: SectionTextField;

  @Field()
  value: string;

}
