import { Field, ID, InputType } from "@nestjs/graphql";
import { Strength } from "../enum/strength.enum";

@InputType()
export class UpdateStrengthInput {

  @Field(() => ID)
  sectionValueId: string;

  @Field(() => Strength)
  strength: Strength;

}
