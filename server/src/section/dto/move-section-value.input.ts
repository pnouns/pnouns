import { Field, ID, InputType } from "@nestjs/graphql";

@InputType()
export class MoveSectionValueInput {

  @Field(() => ID)
  sectionValueId: string;

  /**
   * The new value this will be in front of.
   */
  @Field(() => ID, {
    nullable: true,
  })
  newPrevious?: string;

  /**
   * The new value after this one.
   * If not provided, attempts to use 'newPrevious.next'
   */
  @Field(() => ID, {
    nullable: true,
  })
  newNext?: string;

}
