import { registerEnumType } from "@nestjs/graphql";

export enum SectionType {
  NAME = 1,
  PRONOUNS,
}

registerEnumType(SectionType, {
  name: "SectionType",
  description: "Marks special types of sections.",
});
