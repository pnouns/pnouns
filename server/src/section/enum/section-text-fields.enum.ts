import { registerEnumType } from "@nestjs/graphql";

export enum SectionTextField {
  TITLE = 1,
  SUBTITLE,
  LEAD,
}

registerEnumType(SectionTextField, {
  name: "SectionTextField",
  description: "One of the regular text fields in a section.",
});
