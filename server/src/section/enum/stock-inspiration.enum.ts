import { registerEnumType } from "@nestjs/graphql";

/**
 * Built-in "inspiration" for sections - the front-end can use these as
 * general suggestions, e.g. "Terms for Parent", "Terms for Child", "Honorifics"
 */
export enum StockInspiration {
  PRONOUNS = 1,
  PREFIX_MX_MR_MS = 100,
  HONORIFIC_SIR_MAAM,
  /**
   * person, man, woman, lady, boy, girl
   */
  REFERENCES_PERSON,
  /**
   * buddy, pal, dude, bud, bro, sis
   */
  REFERENCES_FRIEND_SLANG,
  COMPLIMENTS,
  RELATIONSHIP_PARENT = 200,
  RELATIONSHIP_CHILD,
  RELATIONSHIP_PARTNER,
}

registerEnumType(StockInspiration, {
  name: "StockInspiration",
});

export const stockInspirationValues: Record<StockInspiration, string[]> = {
  [StockInspiration.PRONOUNS]: [],
  [StockInspiration.PREFIX_MX_MR_MS]: [
    "Mx.",
    "Mr.",
    "Ms.",
  ],
  [StockInspiration.HONORIFIC_SIR_MAAM]: [
    "Sir",
    "Ma'am",
    "Friend",
    "Captain",
    "Boss",
  ],
  [StockInspiration.REFERENCES_PERSON]: [
    "Person",
    "Man",
    "Woman",
    "Lady",
    "Boy",
    "Girl",
    "Guy",
  ],
  [StockInspiration.REFERENCES_FRIEND_SLANG]: [
    "Buddy",
    "Friend",
    "Pal",
    "Dude",
    "Bud",
    "Bro",
    "Sis",
  ],
  [StockInspiration.COMPLIMENTS]: [
    "Beautiful",
    "Handsome",
    "Pretty",
    "Cute",
    "Hot",
  ],
  [StockInspiration.RELATIONSHIP_PARENT]: [],
  [StockInspiration.RELATIONSHIP_CHILD]: [
    "Child",
    "Son",
    "Daughter",
  ],
  [StockInspiration.RELATIONSHIP_PARTNER]: [
    "Partner",
    "Joyfriend",
    "Boyfriend",
    "Girlfriend",
  ],
};
