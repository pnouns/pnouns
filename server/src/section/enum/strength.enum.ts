import { registerEnumType } from "@nestjs/graphql";

export enum Strength {
  BEST = 1,
  TESTING,
  GOOD,
  OK,
  JOKINGLY,
  CASUALLY_TESTING,
  ITS_COMPLICATED,
  UNSURE,
  AVOID,
  CONDITIONAL,
  NEVER,
  CUSTOM = 100,
}

registerEnumType(Strength, {
  name: "Strength",
  description: "How the user feels about a specific option.",
});
