import { forwardRef, Inject, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { createDecipher } from "crypto";
import { Repository } from "typeorm";
import { SectionValue } from "./entities/section-value.entity";
import { Section } from "./entities/section.entity";
import { Strength } from "./enum/strength.enum";
import { SectionService } from "./section.service";

@Injectable()
export class SectionValueService {

  public constructor(
    @InjectRepository(SectionValue)
    private readonly sectionValueRepository: Repository<SectionValue>,
    @Inject(forwardRef(() => SectionService))
    private readonly sectionService: SectionService,
  ) {}

  public getById(id: string): Promise<SectionValue | undefined> {
    return this.sectionValueRepository.findOne(id);
  }

  public async createSectionValue(
    section: Section,
    text: string,
    strength: Strength,
    previous?: string,
    next?: string,
  ): Promise<SectionValue> {
    //const nextValue = next ? await this.getById(next) : undefined;
    //const previousValue = previous ? await this.getById(previous) : undefined;
    const value = new SectionValue();
    value.text = text;
    value.strength = strength;
    value.section = Promise.resolve(section);
    /*if(nextValue && previousValue) {
      console.log(`Appending new value ${value.text} between ${previousValue.id} and ${nextValue.id}`);
      value.next = Promise.resolve(nextValue);
      const created = await this.sectionValueRepository.save(value);
      this.connect(previousValue, created);
      return created;
    } else if(previousValue) {
      console.log(`Appending new value ${value.text} after ${previousValue.id}`);
      const previousValueOldNext = previousValue.next ? await previousValue.next : undefined;
      if(previousValueOldNext) {
        value.next = Promise.resolve(previousValueOldNext);
      }
      const created = await this.sectionValueRepository.save(value);
      this.connect(previousValue, created);
      return created;
    } else if(nextValue) {
      console.log(`Appending new value ${value.text} before ${nextValue.id}`);
      value.next = Promise.resolve(nextValue);
    }*/
    const created = await this.sectionValueRepository.save(value);
    await this.sectionService.appendValueOrder(section, created);
    return created;
  }

  public async deleteSectionValue(
    sectionValue: SectionValue,
  ): Promise<Section> {
    const section = await sectionValue.section;
    const id = sectionValue.id;
    await this.sectionValueRepository.delete(sectionValue.id);
    return this.sectionService.setValueOrder(section, section.valueOrder.filter(v => v !== id));
  }

  public updateStrength(
    value: SectionValue,
    strength: Strength,
  ): Promise<SectionValue> {
    value.strength = strength;
    return this.sectionValueRepository.save(value);
  }

  public updateText(
    value: SectionValue,
    text: string,
  ): Promise<SectionValue> {
    value.text = text;
    return this.sectionValueRepository.save(value);
  }

  public async connect(
    value: SectionValue,
    next: SectionValue,
  ): Promise<void> {
    console.log(`Setting 'next' value of '${value.id}' (${value.text}) to point at '${next.id}' (${next.text})`);
    value.next = Promise.resolve(next);
    await this.sectionValueRepository.save(value);
  }

  public async removeNext(
    value: SectionValue,
  ): Promise<void> {
    console.log(`Removing 'next' value from ${value.id} (${value.text})`);
    value.next = undefined;
    await this.sectionValueRepository.save(value);
  }

}
