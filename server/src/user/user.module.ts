import { resolve } from "path";
import { readFileSync } from "fs";
import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ServerType } from "./enum/ServerType";
import { IssuerEnvironment } from "./enum/IssuerEnvironment";
import { EmailToken } from "./entities/email-token.entity";
import { DiscordToken } from "./entities/discord-token.entity";
import { EmailVerificationService } from "./email-verification.service";
import { DiscordAuthService } from "./discord-auth.service";
import { UserResolver } from "./user.resolver";
import { UserService } from "./user.service";
import { User } from "./entities/user.entity";
import { ParseJWTGuard } from "./parse-jwt.guard";
import { TrackingModule } from "../tracking/tracking.module";
import { AuthController } from "./auth.controller";
import { OAuth } from "./entities/oauth.entity";
import { GoogleStrategy } from "./oauth/google.strategy";
import { DiscordStrategy } from "./oauth/discord.strategy";

function getAudiences(env: IssuerEnvironment): string[] {
  if(env === IssuerEnvironment.PRODUCTION) {
    return [
      IssuerEnvironment.PRODUCTION,
      IssuerEnvironment.DEVELOPMENT,
      IssuerEnvironment.DEMO,
    ];
  } else if(env === IssuerEnvironment.DEMO) {
    return [
      IssuerEnvironment.DEVELOPMENT,
      IssuerEnvironment.DEMO,
    ];
  } else {
    return [
      IssuerEnvironment.DEVELOPMENT,
    ];
  }
}

function getAllowedIssuers(serverType: ServerType): string[] {
  if(serverType === ServerType.CLOUD || serverType === ServerType.LOCAL) {
    return [
      `pronouns-${ServerType.CLOUD}`,
      `pronouns-${ServerType.LOCAL}`,
    ];
  }
  return [
    `pronouns-${ServerType.CLOUD}`,
    `pronouns-${ServerType.LOCAL}`,
    `pronouns-${ServerType.DEV}`,
    `pronouns-${ServerType.UNKNOWN}`,
  ];
}

export const ParseJwtImports = [
  ConfigModule,
  JwtModule.registerAsync({
    imports: [ ConfigModule ],
    inject: [ ConfigService ],
    useFactory: async (configService: ConfigService) => {
      const serverType = configService.get<ServerType>("SERVER_TYPE", ServerType.DEV);
      const issuerEnvironment = configService.get<IssuerEnvironment>("ISSUER", IssuerEnvironment.DEVELOPMENT);
      return {
        publicKey: readFileSync(resolve(__dirname, "../../../", configService.get<string>("JWT_PUBLIC_KEY", "")), "utf-8"),
        privateKey: readFileSync(resolve(__dirname, "../../../", configService.get<string>("JWT_PRIVATE_KEY", "")), "utf-8"),
        signOptions: {
          issuer: `pronouns-${serverType}`,
          audience: getAudiences(issuerEnvironment),
          expiresIn: "14 days",
          algorithm: "RS256",
        },
        verifyOptions: {
          algorithms: [ "RS256" ],
          issuer: getAllowedIssuers(serverType),
          audience: [issuerEnvironment],
        }
      };
    },
  }),
]

@Module({
  imports: [
    ...ParseJwtImports,
    PassportModule,
    TrackingModule,
    TypeOrmModule.forFeature([
      EmailToken,
      DiscordToken,
      OAuth,
      User,
    ]),
  ],
  providers: [
    EmailVerificationService,
    UserService,
    GoogleStrategy,
    DiscordStrategy,
    UserResolver,
    ParseJWTGuard,
    DiscordAuthService,
  ],
  controllers: [
    AuthController,
  ],
  exports: [
    EmailVerificationService,
    UserService,
    ParseJWTGuard,
  ]
})
export class UserModule {}
