import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy, Profile } from "passport-discord";
import { User } from "../entities/user.entity";
import { UserService } from "../user.service";

@Injectable()
export class DiscordStrategy extends PassportStrategy(Strategy, "discord") {

  public constructor(
    private configService: ConfigService,
    private userService: UserService,
  ) {
    super({
      clientID: configService.get<string>("DISCORD_CLIENT_ID"),
      clientSecret: configService.get<string>("DISCORD_CLIENT_SECRET"),
      callbackURL: configService.get<string>("DISCORD_CALLBACK", "http://pnouns.fyi:9510/auth/discord/callback"),
      scope: [ "identify" ],
    });
  }

  async validate(accessToken, refreshToken, profile: Profile): Promise<User> {
    console.log(profile);
    const user = await this.userService.findOrCreateByDiscord(profile.id, `${profile.username}#${profile.discriminator}`);
    console.log("Found user:", user);
    return user;
  }

}
