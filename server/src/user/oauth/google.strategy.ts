import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { OAuth2Strategy } from "passport-google-oauth";
import { User } from "../entities/user.entity";
import { UserService } from "../user.service";

@Injectable()
export class GoogleStrategy extends PassportStrategy(OAuth2Strategy, "google") {

  public constructor(
    private configService: ConfigService,
    private userService: UserService,
  ) {
    super({
      clientID: configService.get<string>("GOOGLE_CLIENT_ID"),
      clientSecret: configService.get<string>("GOOGLE_CLIENT_SECRET"),
      callbackURL: configService.get<string>("GOOGLE_CALLBACK", "http://pnouns.fyi:9510/auth/google/callback"),
      scope: [ "email", "profile" ],
    });
  }

  async validate(accessToken, refreshToken, profile): Promise<User> {
    const user = this.userService.findOrCreateByGoogleId(
      profile.id,
      profile.name,
      profile.emails[0].value,
    );
    return user;
  }

}
