import {
  BadRequestException,
  CanActivate,
  ExecutionContext,
  Injectable,
} from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { getGraphQLContext } from "../util/gql-context";
import { JWT } from "./dto/jwt.dto";

@Injectable()
export class ParseJWTGuard implements CanActivate {

  public constructor(
    private jwtService: JwtService,
  ) {}

  public canActivate(context: ExecutionContext): boolean {
    const ctx = getGraphQLContext(context);
    const authHeader = ctx.req.headers.authorization;
    if(!authHeader) {
      ctx.setHasAuthenticated(false);
      return true;
    }
    const auth = authHeader.split(" ");
    if(auth.length !== 2) {
      throw new BadRequestException("Invalid 'Authorization' header.");
    }
    const [method, token] = auth;
    if(method.toLowerCase() !== "Bearer".toLowerCase()) {
      throw new BadRequestException("Unknown 'Authorization' method.");
    }
    try {
      const jwt = this.jwtService.verify<JWT>(token);
      ctx.setHasAuthenticated(true);
      ctx.setSub(jwt.sub);
      ctx.setJwt(jwt);
    } catch (err) {
      console.error("JWT unverifiable: ", err, token);
      throw new BadRequestException("Invalid JWT provided.");
    }
    return true;
  }

}
