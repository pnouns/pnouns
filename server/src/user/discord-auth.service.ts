import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import Discord from "discord.js";
import { DateTime } from "luxon";
import { UserService } from "./user.service";
import { DISCORD_TOKEN_MAX_AGE } from "./discord-config";

/**
 * Rate limit users to `DISCORD_TOKEN_MAX_ACTIVE` token requests
 * every `DISCORD_TOKEN_MAX_ACTIVE_AGE` minutes.
 */
const DISCORD_TOKEN_MAX_ACTIVE = 3;
const DISCORD_TOKEN_MAX_ACTIVE_AGE = 5;

function isTextChannel(channel: Discord.GuildChannel | Discord.ThreadChannel): channel is Discord.TextChannel {
  return channel.type === "GUILD_TEXT";
}

interface SentLogin {
  id: string;
  time: DateTime;
}

@Injectable()
export class DiscordAuthService {

  public readonly enabled: boolean;
  private readonly client?: Discord.Client;
  /**
   * Tracks the logins sent - limits users to 3 links/10 minutes
   */
  private sentLogins: SentLogin[];

  public constructor(
    private readonly configService: ConfigService,
    private readonly userService: UserService,
  ) {
    this.sentLogins = [];
    const token = configService.get<string>("DISCORD_APP_TOKEN");
    this.enabled = !!token;
    if(this.enabled) {
      this.client = new Discord.Client({
        intents: [ "GUILDS", "GUILD_MESSAGES", "GUILD_MEMBERS" ],
      });
      this.client.on("messageCreate", message => {
        this.handleMessage(message);
      });
      this.client.on("interactionCreate", i => {
        if(i.isButton() && i.customId === "send_auth_dm") {
          this.handleSendAuthDM(i);
        }
        if(i.isButton() && i.customId === "send_auth") {
          this.handleSendAuth(i);
        }
        if(i.isButton() && i.customId === "start_verification") {
          this.handleStartVerification(i);
        }
      });
      this.client.on("guildMemberAdd", async member => {
        console.log(`${member.id} joined the Discord server!`);
        const sent = await member.send({
          content:
            "Welcome to pnouns.fyi!  Click the button below to generate a login link that will sign you directly in to pnouns.fyi.\n" +
            "For safety, new members are invisible on the pnouns.fyi Discord server. " +
            "If you would like to join, please click 'Join Server' to start the verification process.",
          components: [
            new Discord.MessageActionRow()
              .addComponents(
                new Discord.MessageButton()
                  .setCustomId("send_auth_dm")
                  .setLabel("Get Login Link")
                  .setStyle("PRIMARY"),
                new Discord.MessageButton()
                  .setURL("https://pnouns.fyi/")
                  .setLabel("pnouns.fyi")
                  .setStyle("LINK"),
                new Discord.MessageButton()
                  .setCustomId("start_verification")
                  .setLabel("Join Server")
                  .setStyle("SECONDARY"),
              ),
          ],
        });
        await sent.pin();
      });
      this.client.login(token);
    }
  }

  private async handleMessage(message: Discord.Message) {
    /**
     * 'post <channel id>'
     */
    if(
      message.author.id === this.configService.get<string>("DISCORD_ADMIN_ID")
      && message.content.startsWith("post")
      && this.client
    ) {
      const [, channelId] = message.content.split(" ");
      const guild = message.guild;
      const channel = guild?.channels.resolve(channelId as any);
      if(channel && isTextChannel(channel)) {
        console.log(`Posting link to ${channel.id}`);
        const row = new Discord.MessageActionRow()
          .addComponents(
            new Discord.MessageButton()
              .setCustomId("send_auth")
              .setLabel("Get Login Link")
              .setStyle("PRIMARY")
          );
        await channel.send({
          content: "pnouns.fyi authentication",
          components: [row],
        });
      } else {
        message.channel.send(`Couldn't find channel ${channelId}.`);
      }
    }
  }

  private async handleSendAuth(i: Discord.ButtonInteraction) {
    i.user.send("You requested a login.");
  }

  private async handleSendAuthDM(i: Discord.ButtonInteraction) {
    const userId = i.user.id;
    const loginsSent = this.getLoginsSent(userId);
    if(loginsSent >= DISCORD_TOKEN_MAX_ACTIVE) {
      await i.reply({
        content: `You have requested ${loginsSent} within the last ${DISCORD_TOKEN_MAX_ACTIVE_AGE} minutes.  Please wait to send another.`,
        ephemeral: true,
      });
      return;
    }
    const domain = this.configService.get<string>("SERVER_DOMAIN", "http://localhost.pnouns.fyi:9510");
    const token = await this.userService.getDiscordBotLoginLink(userId, `${i.user.username}#${i.user.discriminator}`);
    const url = `${domain}${token}`;
    await i.reply({
      content: `Magic login link: ${url}.  This link will expire in ${DISCORD_TOKEN_MAX_AGE} minutes, and will only work once.`,
      components: [
        new Discord.MessageActionRow()
          .addComponents(
            new Discord.MessageButton()
              .setURL(url)
              .setLabel("Login to pnouns.fyi")
              .setStyle("LINK"),
          ),
      ],
      ephemeral: true,
    });
    this.sentLogins.push({
      id: userId,
      time: DateTime.now(),
    });
  }

  private async handleStartVerification(i: Discord.ButtonInteraction) {
    if(!this.client) {
      return;
    }
    const guilds = await this.client.guilds.fetch();
    const tempGuild = guilds.find(guild => guild.id === this.configService.get<string>("DISCORD_GUILD_ID"));
    if(!tempGuild) {
      i.reply({
        content: "Error: unable to find guild.",
        ephemeral: true,
      });
      return;
    }
    const guild = await tempGuild.fetch();
    const verificationRoleId = this.configService.get<string>("DISCORD_VERIFICATION_ROLE", "");
    const roles = await guild.roles.fetch();
    const role = roles.find(role => role.id === verificationRoleId);
    if(!role) {
      i.reply({
        content: "Error: unable to find the verification role.",
        ephemeral: true,
      });
      return;
    }
    const members = await guild.members.fetch();
    const foundMember = members.find(member => member.user.id === i.user.id);
    if(!foundMember) {
      i.reply({
        content: "Error: you are not in the pnouns.fyi serve.r",
        ephemeral: true,
      });
      return;
    }
    const member = await foundMember.fetch();
    console.log(`Granting member ${member.nickname ?? member.displayName} (${member.id}) verification role '${role.name}' (${role.id})`);
    await member.roles.add(role, "User requested to start verification process.");
    const components = i.message.components;
    if(components) {
      for (const row of components) {
        if(row.type === "ACTION_ROW") {
          for(const component of row.components) {
            if(component.type === "BUTTON" && component.customId === "start_verification") {
              component.setDisabled(true);
            }
          }
        }
      }
    }
    i.reply({
      content: "Please check the pnouns.fyi server to start the verification process.",
      ephemeral: true,
    });
  }

  private getLoginsSent(userId: string) {
    this.cleanSentLogins();
    return this.sentLogins.filter(i => i.id === userId).length;
  }

  private cleanSentLogins(): void {
    const oldestAllowed = DateTime.now().minus({ minutes: DISCORD_TOKEN_MAX_ACTIVE_AGE });
    this.sentLogins = this.sentLogins.filter(login => login.time > oldestAllowed);
  }

}
