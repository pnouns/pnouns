import { ConfigService } from "@nestjs/config";
import { Args, Mutation, Resolver } from "@nestjs/graphql";
import { verify } from "hcaptcha";
import { EmailTokenInput } from "./dto/EmailTokenInput";
import { EmailedTokenInput } from "./dto/EmailedTokenInput";
import { EmailVerificationService } from "./email-verification.service";
import { UserService } from "./user.service";
import { EventLoggerService } from "../tracking/event-logger.service";
import { TrackedEvent } from "../tracking/enum/TrackedEvent.enum";
import { GraphIP } from "../tracking/GraphIP.decorator";
import { Err } from "../tracking/enum/Err.enum";

@Resolver()
export class UserResolver {

  public constructor(
    private readonly config: ConfigService,
    private readonly eventLoggerService: EventLoggerService,
    private readonly emailVerification: EmailVerificationService,
    private readonly userService: UserService,
  ) {}

  @Mutation(() => String)
  async emailToken(
    @Args("input", { type: () => EmailTokenInput }) input: EmailTokenInput,
  ): Promise<string> {
    await verify(this.config.get<string>("HCAPTCHA_SECRET", ""), input.captchaToken)
      .catch(err => {
        this.eventLoggerService.logResolverEvent(UserResolver, "emailToken", Err.NOTE, "failed captcha", {
          unverifiedEmail: input.email,
        });
        throw err;
      });
    await this.emailVerification
      .sendEmail(input.email)
      .then(v => {
        this.eventLoggerService.logResolverEvent(UserResolver, "emailToken", Err.TRACE, "verification email sent", {
          unverifiedEmail: input.email,
        });
        return v;
      })
      .catch(e => {
        this.eventLoggerService.logResolverEvent(UserResolver, "emailToken", Err.ERROR, "verification email failed", {
          unverifiedEmail: input.email,
        });
        throw e;
      });
    return "sent";
  }

  @Mutation(() => String)
  async submitEmailedToken(
    @GraphIP() ip: string,
    @Args("input", { type: () => EmailedTokenInput }) input: EmailedTokenInput,
  ): Promise<string> {
    const log = this.eventLoggerService.getResolverEventLogger(UserResolver, "submitEmailedToken");
    const verified = await this.emailVerification.verifyEmailedToken(input.email, input.token);
    if(!verified) {
      throw new Error("Token not found.");
    }
    const previouslyExisting = await this.userService.findByEmail(input.email)
      .catch(log.catch("couldn't find user for email", Err.NOTE, { unverifiedEmail: input.email }));
    const user = previouslyExisting ?? await this.userService.findOrCreateByEmail(input.email)
      .catch(log.catch("creating user by email failed", Err.ERROR, { unverifiedEmail: input.email }));
    const nicknames = await user.nicknames;
    const oauth = await user.oauth;
    this.eventLoggerService.logUserEvent(user.id, TrackedEvent.USER_AUTHENTICATED, ip, {
      userCreated: !previouslyExisting,
      $set: {
        email: user.email,
        oauth,
        nicknameLimit: user.nicknameLimit,
        nicknameCount: nicknames.length,
        nicknames: nicknames.map(nickname => nickname.id),
      },
    });
    const jwt = this.userService.createJwt(user);
    return jwt;
  }

  @Mutation(() => String)
  async submitDiscordToken(
    @GraphIP() ip: string,
    @Args("discordToken") discordToken: string,
  ) {
    const verified = await this.userService.verifyDiscordToken(discordToken);
    if(!verified) {
      throw new Error("Token not found.");
    }
    const previouslyExisting = await this.userService.getDiscordBotUser(verified.discordId);
    const user = previouslyExisting ?? await this.userService.findOrCreateDiscordBotUser(verified);
    const nicknames = await user.nicknames;
    this.eventLoggerService.logUserEvent(user.id, TrackedEvent.USER_AUTHENTICATED, ip, {
      userCreated: !previouslyExisting,
      discordToken,
      $set: {
        email: `${user.discordId}@discord-bot`,
        discordId: user.discordId,
        discordUsername: user.discordUsername,
        nicknameLimit: user.nicknameLimit,
        nicknameCount: nicknames.length,
        nicknames: nicknames.map(nickname => nickname.id),
      },
    });
    const jwt = this.userService.createJwt(user);
    return jwt;
  }

}
