export enum ServerType {
  UNKNOWN = "unknown",
  CLOUD = "cloud",
  LOCAL = "local",
  DEV = "dev",
}
