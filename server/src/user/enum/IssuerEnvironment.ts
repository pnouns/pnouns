/**
 * Configuration to separate JWTs between production
 * and development environments.
 *
 * JWTs are accepted if they share server.key - so with configurations like
 * the production environment, admin review/dev environment tokens look like valid admin
 * tokens on production.
 *
 * An issuer check is implemented against the IssuerEnvironment, so demo
 * details can't be used in production environments (which have protected PII).
 *
 * Production tokens can work on any instance - enables being lazy in Postman,
 * or as an emergency token.
 */
export enum IssuerEnvironment {
  PRODUCTION = "production",
  DEVELOPMENT = "development",
  DEMO = "demo",
}
