import { registerEnumType } from "@nestjs/graphql";

export enum SupporterRole {
  GOLD = "GOLD",
  PLATINUM = "PLATINUM",
  DIAMOND = "DIAMOND",
}

registerEnumType(SupporterRole, {
  name: "SupporterRole",
});
