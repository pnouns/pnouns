import { registerEnumType } from "@nestjs/graphql";

export enum StaffType {
  FOUNDER = "FOUNDER",
  LEAD_DEV = "LEAD_DEV",
}

registerEnumType(StaffType, {
  name: "StaffType",
});
