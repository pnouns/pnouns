export interface JWT {
  sub: string;
  namespaces: string[];
}
