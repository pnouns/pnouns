import { Field, InputType } from "@nestjs/graphql";

@InputType()
export class EmailedTokenInput {

  @Field()
  email: string;

  @Field()
  token: string;

}
