import { Field, InputType } from "@nestjs/graphql";

@InputType()
export class EmailTokenInput {

  @Field()
  email: string;

  @Field()
  captchaToken: string;

}
