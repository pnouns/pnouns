import { join } from "path";
import { readFile } from "fs-extra";
import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { InjectRepository } from "@nestjs/typeorm";
import { MoreThan, Repository } from "typeorm";
import { DateTime } from "luxon";
import nodemailer from "nodemailer";
import { generator } from "rand-token";
import { EmailToken } from "./entities/email-token.entity";
import SMTPTransport from "nodemailer/lib/smtp-transport";

@Injectable()
export class EmailVerificationService {

  private readonly sendTokenTemplate: Promise<string>;

  private readonly transporter: nodemailer.Transporter<SMTPTransport.SentMessageInfo>;

  public constructor(
    private readonly config: ConfigService,
    @InjectRepository(EmailToken)
    private readonly emailTokenRepository: Repository<EmailToken>,
  ) {
    this.sendTokenTemplate = readFile(join(__dirname, "../../../email/out/send-token.html"), "utf8");
    this.transporter = nodemailer.createTransport({
      service: "Mailjet",
      auth: {
        user: config.get<string>("MAILJET_APIKEY_PUBLIC", ""),
        pass: config.get<string>("MAILJET_APIKEY_PRIVATE", ""),
      },
    });
  }

  public async sendEmail(address: string): Promise<EmailToken> {
    const template = await this.sendTokenTemplate;
    const code = generator({ chars: "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" }).generate(8);
    const token = new EmailToken();
    token.email = address;
    token.token = code;
    token.used = false;
    const saved = await this.emailTokenRepository.save(token);
    const expiresAt = DateTime.fromJSDate(saved.sent).plus({ minutes: 30 }).toISO();
    const htmlMessage = template
      .replace(/\{\{email\}\}/g, address)
      .replace(/\{\{token\}\}/g, code)
      .replace(/\{\{tokenExpiration\}\}/g, expiresAt);
    await this.transporter.sendMail({
      from: {
        name: "pnouns.fyi",
        address: this.config.get<string>("EMAIL_SENDER", ""),
      },
      to: address,
      subject: `Login to pnouns.fyi (${address})`,
      text: `Hello ${address},\nSomeone (hopefully you) has attempted to login to https://pnouns.fyi/.\n\nIf this was you, please enter the code ${code} on https://pnouns.fyi/#login.\n\nIf this was not you, you can safely ignore this email.`,
      html: htmlMessage,
    });
    return saved;
  }

  public async verifyEmailedToken(email: string, token: string): Promise<boolean> {
    const found = await this.emailTokenRepository.findOne({
      email,
      token,
      used: false,
      sent: MoreThan(DateTime.now().minus({ minutes: 30 }).toJSDate()),
    });
    return found !== undefined;
  }

}
