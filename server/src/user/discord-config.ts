/**
 * Number of minutes until tokens expire.
 */
export const DISCORD_TOKEN_MAX_AGE = 10;
