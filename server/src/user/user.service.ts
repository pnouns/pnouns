import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectRepository } from "@nestjs/typeorm";
import { MoreThan, Repository } from "typeorm";
import { generator } from "rand-token";
import { User } from "./entities/user.entity";
import { JWT } from "./dto/jwt.dto";
import { OAuth } from "./entities/oauth.entity";
import { OAuthIssuer } from "./enum/OAuthIssuer";
import { DiscordToken } from "./entities/discord-token.entity";
import { DateTime } from "luxon";
import { DISCORD_TOKEN_MAX_AGE } from "./discord-config";

@Injectable()
export class UserService {

  public constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(OAuth)
    private readonly oauthRepository: Repository<OAuth>,
    @InjectRepository(DiscordToken)
    private readonly discordTokenRepository: Repository<DiscordToken>,
    private readonly jwtService: JwtService,
  ) {}

  public getUserById(id: string): Promise<User | undefined> {
    return this.userRepository.findOne(id);
  }

  public findByEmail(email: string): Promise<User | undefined> {
    return this.userRepository.findOne({ email });
  }

  public async findOrCreateByEmail(email: string): Promise<User> {
    const user = await this.userRepository.findOne({ email });
    if(user) {
      return user;
    }
    const reg = new User();
    reg.email = email;
    const created = await this.userRepository.save(reg);
    return created;
  }

  public async createJwt(user: User): Promise<string> {
    const payload: JWT = {
      sub: user.id,
      namespaces: (await user.nicknames).map(nickname => nickname.id),
    };
    return this.jwtService.signAsync(payload);
  }

  public async findOrCreateByGoogleId(
    userId: string,
    name: string,
    email: string,
  ): Promise<User> {
    const existing = await this.oauthRepository.findOne({ provider: OAuthIssuer.GOOGLE, providerId: userId });
    if(existing) {
      return existing.user;
    }
    const user = new User();
    user.email = email;
    const createdUser = await this.userRepository.save(user);
    const oauth = new OAuth();
    oauth.provider = OAuthIssuer.GOOGLE;
    oauth.providerId = userId;
    oauth.user = Promise.resolve(createdUser);
    const createdOAuth = await this.oauthRepository.save(oauth);
    return createdOAuth.user;
  }

  /**
   * OAuth handler for Discord.
   */
  public async findOrCreateByDiscord(
    providerId: string,
    username: string,
  ): Promise<User> {
    const existing = await this.oauthRepository.findOne({ provider: OAuthIssuer.DISCORD, providerId });
    if(existing) {
      if(existing.providerUsername !== username) {
        existing.providerUsername = username;
        const updated = await this.oauthRepository.save(existing);
        return updated.user;
      }
      return existing.user;
    }
    const user = new User();
    const createdUser = await this.userRepository.save(user);
    const oauth = new OAuth();
    oauth.provider = OAuthIssuer.DISCORD;
    oauth.providerId = providerId;
    oauth.providerUsername = username;
    oauth.user = Promise.resolve(createdUser);
    const createdOAuth = await this.oauthRepository.save(oauth);
    return createdOAuth.user;
  }

  public async getDiscordBotLoginLink(
    discordId: string,
    discordUsername: string,
  ): Promise<string> {
    const token = await this.createDiscordToken(discordUsername, discordId);
    return `/?discord_token=${token.token}`;
  }

  public async verifyDiscordToken(token: string): Promise<DiscordToken | undefined> {
    const found = await this.discordTokenRepository.findOne({
      token,
      used: false,
      sent: MoreThan(DateTime.now().minus({ minutes: DISCORD_TOKEN_MAX_AGE }).toJSDate()),
    });
    return found;
  }

  public async getDiscordBotUser(discordId: string): Promise<User | undefined> {
    return this.userRepository.findOne({ discordId });
  }

  public async findOrCreateDiscordBotUser(input: DiscordToken): Promise<User> {
    const existing = await this.getDiscordBotUser(input.discordId);
    if(existing) {
      return existing;
    }
    const user = new User();
    user.discordId = input.discordId;
    user.discordUsername = input.discordUsername;
    return this.userRepository.save(user);
  }

  private async createDiscordToken(
    discordUsername: string,
    discordId: string,
  ): Promise<DiscordToken> {
    const code = generator({ chars: "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" }).generate(20);
    const token = new DiscordToken();
    token.discordId = discordId;
    token.discordUsername = discordUsername;
    token.token = code;
    token.used = false;
    return this.discordTokenRepository.save(token);
  }

}
