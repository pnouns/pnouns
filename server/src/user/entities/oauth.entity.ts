import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { OAuthIssuer } from "../enum/OAuthIssuer";
import { User } from "./user.entity";

/**
 * Details of an OAuth authentication.
 */
@Entity()
export class OAuth {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @ManyToOne(() => User, user => user.oauth)
  user: Promise<User>;

  @Column({
    type: "enum",
    enum: OAuthIssuer,
  })
  provider: OAuthIssuer;

  @Column("varchar", { length: 128 })
  providerId: string;

  /**
   * The username on the remote service - use for display (/manual verification) only.
   */
  @Column("varchar", { length: 128 })
  providerUsername: string;

  @CreateDateColumn()
  firstAuth: Date;

}
