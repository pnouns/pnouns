import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from "typeorm";

/**
 * A stored token that was DM'd to a user to register or login.
 */
@Entity()
export class DiscordToken {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column("varchar", { length: 32 })
  discordId: string;

  @Column("varchar", { length: 64 })
  discordUsername: string;

  @Column("varchar", { length: 31 })
  token: string;

  @Column("bool")
  used: boolean;

  @CreateDateColumn()
  sent: Date;

}
