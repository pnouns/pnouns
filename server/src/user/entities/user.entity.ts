import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Nickname } from "../../nickname/entities/nickname.entity";
import { Page } from "../../page/entities/page.entity";
import { Section } from "../../section/entities/section.entity";
import { OAuth } from "./oauth.entity";
import { StaffType } from "../enum/StaffType";
import { SupporterRole } from "../enum/SupporterRole";

@Entity()
export class User {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column("varchar", { length: "255", nullable: true })
  email?: string;

  @Column("varchar", { length: 32, nullable: true })
  discordId?: string;

  @Column("varchar", { length: 64, nullable: true })
  discordUsername?: string;

  @OneToMany(() => OAuth, oauth => oauth.user, {
    onDelete: "CASCADE",
  })
  oauth: Promise<OAuth[]>;

  @Column({
    type: "set",
    enum: StaffType,
    nullable: true,
  })
  staff?: StaffType[];

  @Column({
    type: "enum",
    enum: SupporterRole,
    nullable: true,
  })
  supporter?: SupporterRole;

  @OneToMany(() => Nickname, nickname => nickname.user, {
    onDelete: "CASCADE",
    eager: true,
  })
  nicknames: Promise<Nickname[]>;

  /**
   * A custom limit of nicknames for this user.
   * Null if the user doesn't have a manually changed limit.
   */
  @Column("tinyint", {
    unsigned: true,
    nullable: true,
  })
  nicknameLimit?: number;

  @OneToMany(() => Page, page => page.user, {
    onDelete: "CASCADE",
  })
  pages: Promise<Page[]>;

  @OneToMany(() => Section, section => section.user, {
    onDelete: "CASCADE",
  })
  sections: Promise<Section[]>;

  @CreateDateColumn()
  created: Date;

}
