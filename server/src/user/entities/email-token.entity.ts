import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from "typeorm";

/**
 * A stored token that was emailed to a user to register or login.
 */
@Entity()
export class EmailToken {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column("varchar", { length: 255 })
  email: string;

  @Column("varchar", { length: 31 })
  token: string;

  @Column("bool")
  used: boolean;

  @CreateDateColumn()
  sent: Date;

}
