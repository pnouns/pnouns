import { Controller, Get, Req, Res, UseGuards } from "@nestjs/common";
import { UserService } from "./user.service";
import { GoogleAuthGuard } from "./oauth/google.guard";
import { DiscordAuthGuard } from "./oauth/discord.guard";

@Controller("auth")
export class AuthController {

  public constructor(
    private readonly userService: UserService,
  ) {}

  @Get("google")
  @UseGuards(GoogleAuthGuard)
  async googleAuth() {}

  @Get("google/callback")
  @UseGuards(GoogleAuthGuard)
  async googleAuthRedirect(@Req() req, @Res() res) {
    const user = req.user;
    const jwt = await this.userService.createJwt(user);
    return res.redirect(`/?jwt=${jwt}`);
  }

  @Get("discord")
  @UseGuards(DiscordAuthGuard)
  async discordAuth() {}

  @Get("discord/callback")
  @UseGuards(DiscordAuthGuard)
  async discordAuthRedirect(@Req() req, @Res() res) {
    const user = req.user;
    const jwt = await this.userService.createJwt(user);
    return res.redirect(`/?jwt=${jwt}`);
  }

}
