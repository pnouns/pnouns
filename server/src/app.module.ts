import { join } from "path";
import { APP_INTERCEPTOR } from "@nestjs/core";
import { Logger, Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ServeStaticModule } from "@nestjs/serve-static";
import { GraphQLModule } from "@nestjs/graphql";
import { PublishedModule } from "./published/published.module";
import { UserModule } from "./user/user.module";
import { NicknameModule } from "./nickname/nickname.module";
import { SectionModule } from "./section/section.module";
import { PageModule } from "./page/page.module";
import { TasksModule } from "./tasks/tasks.module";
import { defaultGraphQLContext } from "./util/gql-context";
import { LoggingInterceptor } from "./logger.interceptor";
import { TrackingModule } from "./tracking/tracking.module";
import { IssuerEnvironment } from "./user/enum/IssuerEnvironment";
import PostHog from "posthog-node";
import { TrackedEvent } from "./tracking/enum/TrackedEvent.enum";
import { VERSION } from "./version";

const ormConfig = require(join(__dirname, "../../ormconfig"));

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [
        join(__dirname, "../../../defaults.env"),
        join(__dirname, "../../../secrets.env"),
      ],
    }),
    TrackingModule,
    TypeOrmModule.forRoot(ormConfig),
    GraphQLModule.forRootAsync({
      imports: [ ConfigModule ],
      inject: [ ConfigService ],
      useFactory: async (configService: ConfigService) => {
        const env = configService.get<IssuerEnvironment>("ISSUER", IssuerEnvironment.DEVELOPMENT);
        const postHogServer = configService.get("POSTHOG_SERVER", "https://hog.pnouns.fyi");
        const postHogAPIKey = configService.get("POSTHOG_API_KEY", "");
        const hog = new PostHog(postHogAPIKey, {
          host: postHogServer,
        });
        const posthogKey = configService.get("POSTHOG_SECRET_KEY", "DefaultServerKey");
        const corsRegex = configService.get<string | null>("CORS_REGEX", null);
        const origin = corsRegex
          ? new RegExp(`${corsRegex.replace('.', '\\.')}$`)
          : [
            configService.get<string>("CORS_ORIGIN_0") ?? "http://localhost.pnouns.fyi",
            configService.get<string>("CORS_ORIGIN_1") ?? "http://localhost.pnouns.fyi:9510",
          ];
        return {
          cors: {
            origin,
            methods: [ "GET", "POST" ],
          },
          autoSchemaFile: join(__dirname, "../../schema.gql"),
          context: ({ req }) => {
            const ctx = defaultGraphQLContext(req);
            return ctx;
          },
          fieldResolverEnhancers: [ "interceptors", "guards" ],
          formatError: error => {
            console.error('GraphQL Error', error);
            hog.capture({
              distinctId: "0",
              event: TrackedEvent.SERVER_GRAPHQL_ERROR,
              properties: {
                env,
                serverKey: posthogKey,
                serverVersion: VERSION,
                name: error.name,
                error: error.toString(),
              },
            })
            return error;
          },
        };
      },
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, "../../../client"),
      renderPath: /(?:dist\/.*)|(?:index.html)/,
      serveStaticOptions: {
        cacheControl: true,
        maxAge: 5 * 60 * 1000,
        index: [ "index.html" ],
      },
    }),
    UserModule,
    TasksModule,
    PublishedModule,
    NicknameModule,
    SectionModule,
    PageModule,
  ],
  providers: [
    Logger,
    /*{
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },*/
  ],
})
export class AppModule {}
