const fs = require("fs");
const path = require("path");

function getEnv(option) {
  return typeof process.env[option] === "string" && process.env[option].length > 0
    ? process.env[option]
    : undefined;
}

const DEBUG = process.env.DEBUG && process.env.DEBUG !== "";

function envVariable(option, defaultValue) {
  const file = getEnv(`${option}_FILE`);
  const arg = getEnv(option);
  if(file) {
    const readData = fs.readFileSync(file, "utf8");
    DEBUG && console.log(`Loaded ${option} from file (${readData.length} characters)`);
    return readData;
  } else if(arg) {
    DEBUG && console.log(`Loaded ${option} (${arg.length} characters)`);
    return arg;
  } else {
    return defaultValue;
  }
}

module.exports = {
  type: "mariadb",
  host: process.env.DB_HOST ?? "host_parent",
  port: process.env.DB_PORT ?? 3306,
  username: envVariable("MYSQL_USERNAME", "pronoundb").trim(),
  password: envVariable("MYSQL_PASSWORD", "all_the_pronouns").trim(),
  database: envVariable("MYSQL_DATABASE", "pronoundb").trim(),
  entities: [
    path.join(__dirname, "out/**/*.entity.js"),
  ],
  migrations: [
    path.join(__dirname, "out/migrations/*.js"),
  ],
  cli: {
    migrationsDir: "migrations",
  },
  synchronize: false,
};
