import {MigrationInterface, QueryRunner} from "typeorm";

export class addOauthUsername1631062980171 implements MigrationInterface {
    name = 'addOauthUsername1631062980171'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `o_auth` ADD `providerUsername` varchar(128) NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `o_auth` DROP COLUMN `providerUsername`");
    }

}
