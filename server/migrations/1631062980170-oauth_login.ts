import {MigrationInterface, QueryRunner} from "typeorm";

export class oauthLogin1631062980170 implements MigrationInterface {
    name = 'oauthLogin1631062980170'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `o_auth` (`id` varchar(36) NOT NULL, `provider` enum ('1', '2') NOT NULL, `providerId` varchar(128) NOT NULL, `firstAuth` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `userId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `discord_token` (`id` varchar(36) NOT NULL, `discordId` varchar(32) NOT NULL, `discordUsername` varchar(64) NOT NULL, `token` varchar(31) NOT NULL, `used` tinyint NOT NULL, `sent` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `user` CHANGE `email` `email` varchar(255) NULL");
        await queryRunner.query("ALTER TABLE `o_auth` ADD CONSTRAINT `FK_26949195275599aed148a7f28de` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `o_auth` DROP FOREIGN KEY `FK_26949195275599aed148a7f28de`");
        await queryRunner.query("ALTER TABLE `user` CHANGE `email` `email` varchar(255) NOT NULL");
        await queryRunner.query("DROP TABLE `discord_token`");
        await queryRunner.query("DROP TABLE `o_auth`");
    }

}
