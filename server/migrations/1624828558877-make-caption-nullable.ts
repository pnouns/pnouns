import {MigrationInterface, QueryRunner} from "typeorm";

export class makeCaptionNullable1624828558877 implements MigrationInterface {
    name = 'makeCaptionNullable1624828558877'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `listed_sub_page` CHANGE `caption` `caption` varchar(2048) NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `listed_sub_page` CHANGE `caption` `caption` varchar(2048) NOT NULL");
    }

}
