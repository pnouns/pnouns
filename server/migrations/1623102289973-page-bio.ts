import {MigrationInterface, QueryRunner} from "typeorm";

export class pageBio1623102289973 implements MigrationInterface {
    name = 'pageBio1623102289973'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` ADD `pageBioTitle` varchar(64) NULL");
        await queryRunner.query("ALTER TABLE `page` ADD `pageBioSubtitle` varchar(128) NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `pageBioSubtitle`");
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `pageBioTitle`");
    }

}
