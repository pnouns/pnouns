import {MigrationInterface, QueryRunner} from "typeorm";

export class nicknameDisplay1622908098690 implements MigrationInterface {
    name = 'nicknameDisplay1622908098690'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `nickname` ADD `displayName` varchar(31) NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `nickname` DROP COLUMN `displayName`");
    }

}
