import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1622486839287 implements MigrationInterface {
    name = 'initial1622486839287'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `email_token` (`id` varchar(36) NOT NULL, `email` varchar(255) NOT NULL, `token` varchar(31) NOT NULL, `used` tinyint NOT NULL, `sent` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP TABLE `email_token`");
    }

}
