import {MigrationInterface, QueryRunner} from "typeorm";

export class discordBot1631062980170 implements MigrationInterface {
    name = 'discordBot1631062980170'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `user` ADD `discordId` varchar(32) NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `discordUsername` varchar(64) NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `discordUsername`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `discordId`");
    }

}
