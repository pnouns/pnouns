import {MigrationInterface, QueryRunner} from "typeorm";

export class improvePublish1622916236760 implements MigrationInterface {
    name = 'improvePublish1622916236760'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `published_page` (`id` varchar(36) NOT NULL, `nicknamePath` varchar(31) NOT NULL, `categoryPath` varchar(255) NULL, `pagePath` varchar(255) NULL, `password` varchar(255) NULL, `content` mediumtext NOT NULL, UNIQUE INDEX `IDX_e06f4b97ad620ea9cfc3020444` (`nicknamePath`, `categoryPath`, `pagePath`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `page` ADD `password` varchar(64) NULL");
        await queryRunner.query("ALTER TABLE `page` ADD `publishedId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `page` ADD UNIQUE INDEX `IDX_d401a0b1b8d2b325acef222d5d` (`publishedId`)");
        await queryRunner.query("CREATE UNIQUE INDEX `REL_d401a0b1b8d2b325acef222d5d` ON `page` (`publishedId`)");
        await queryRunner.query("ALTER TABLE `page` ADD CONSTRAINT `FK_d401a0b1b8d2b325acef222d5d4` FOREIGN KEY (`publishedId`) REFERENCES `published_page`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` DROP FOREIGN KEY `FK_d401a0b1b8d2b325acef222d5d4`");
        await queryRunner.query("DROP INDEX `REL_d401a0b1b8d2b325acef222d5d` ON `page`");
        await queryRunner.query("ALTER TABLE `page` DROP INDEX `IDX_d401a0b1b8d2b325acef222d5d`");
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `publishedId`");
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `password`");
        await queryRunner.query("DROP INDEX `IDX_e06f4b97ad620ea9cfc3020444` ON `published_page`");
        await queryRunner.query("DROP TABLE `published_page`");
    }

}
