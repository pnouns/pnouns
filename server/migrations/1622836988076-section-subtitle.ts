import {MigrationInterface, QueryRunner} from "typeorm";

export class sectionSubtitle1622836988076 implements MigrationInterface {
    name = 'sectionSubtitle1622836988076'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP INDEX `IDX_dd45a2dd37ff2620227f1b723a` ON `page`");
        await queryRunner.query("DROP INDEX `IDX_43aa1f139e2fb422aee5a20096` ON `page`");
        await queryRunner.query("ALTER TABLE `section` ADD `subtitle` varchar(127) NULL");
        await queryRunner.query("ALTER TABLE `section` ADD `lead` varchar(1024) NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `section` DROP COLUMN `lead`");
        await queryRunner.query("ALTER TABLE `section` DROP COLUMN `subtitle`");
        await queryRunner.query("CREATE UNIQUE INDEX `IDX_43aa1f139e2fb422aee5a20096` ON `page` (`pronounSectionId`)");
        await queryRunner.query("CREATE UNIQUE INDEX `IDX_dd45a2dd37ff2620227f1b723a` ON `page` (`nameSectionId`)");
    }

}
