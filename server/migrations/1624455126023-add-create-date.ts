import {MigrationInterface, QueryRunner} from "typeorm";

export class addCreateDate1624455126023 implements MigrationInterface {
    name = 'addCreateDate1624455126023'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `published_page` ADD `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `published_page` ADD `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `section_value` ADD `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `section_value` ADD `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `section` ADD `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `section` ADD `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `page` ADD `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `page` ADD `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `nickname` ADD `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `nickname` DROP COLUMN `createdAt`");
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `updatedAt`");
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `createdAt`");
        await queryRunner.query("ALTER TABLE `section` DROP COLUMN `updatedAt`");
        await queryRunner.query("ALTER TABLE `section` DROP COLUMN `createdAt`");
        await queryRunner.query("ALTER TABLE `section_value` DROP COLUMN `updatedAt`");
        await queryRunner.query("ALTER TABLE `section_value` DROP COLUMN `createdAt`");
        await queryRunner.query("ALTER TABLE `published_page` DROP COLUMN `updatedAt`");
        await queryRunner.query("ALTER TABLE `published_page` DROP COLUMN `createdAt`");
    }

}
