import {MigrationInterface, QueryRunner} from "typeorm";

export class betterPages1622681055782 implements MigrationInterface {
    name = 'betterPages1622681055782'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` ADD `defaultSubdomain` tinyint NULL");
        await queryRunner.query("ALTER TABLE `page` ADD `defaultJoiningWord` enum ('1', '2', '3', '4', '5', '6', '7') NULL");
        await queryRunner.query("ALTER TABLE `page` ADD `listed` tinyint NOT NULL");
        await queryRunner.query("ALTER TABLE `page` CHANGE `commonName` `commonName` enum ('1', '2', '3', '4', '5') NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` CHANGE `commonName` `commonName` enum ('1', '2', '3') NULL");
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `listed`");
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `defaultJoiningWord`");
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `defaultSubdomain`");
    }

}
