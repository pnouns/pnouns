import {MigrationInterface, QueryRunner} from "typeorm";

export class addValuePrevious1622925905936 implements MigrationInterface {
    name = 'addValuePrevious1622925905936'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP INDEX `IDX_6da1996c84b83a47e4f723087b` ON `section_value`");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE UNIQUE INDEX `IDX_6da1996c84b83a47e4f723087b` ON `section_value` (`nextId`)");
    }

}
