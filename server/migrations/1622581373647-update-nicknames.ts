import {MigrationInterface, QueryRunner} from "typeorm";

export class updateNicknames1622581373647 implements MigrationInterface {
    name = 'updateNicknames1622581373647'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `user` ADD `nicknameLimit` tinyint UNSIGNED NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `nicknameLimit`");
    }

}
