import {MigrationInterface, QueryRunner} from "typeorm";

export class usersAndPages1622491432108 implements MigrationInterface {
    name = 'usersAndPages1622491432108'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `section_value` (`id` varchar(36) NOT NULL, `text` varchar(127) NOT NULL, `strength` enum ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '100') NOT NULL DEFAULT '8', `customStrengthText` varchar(31) NULL, `inspiredFromStock` enum ('1', '2', '3', '4') NULL, `sectionId` varchar(36) NULL, `inspiredFromSectionId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `section` (`id` varchar(36) NOT NULL, `type` enum ('1', '2') NULL, `title` varchar(63) NOT NULL, `stockInspiration` set ('1', '2', '3', '4') NOT NULL DEFAULT '', `pageId` varchar(36) NULL, `aliasOfId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user` (`id` varchar(36) NOT NULL, `email` varchar(255) NOT NULL, `created` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `page` (`id` varchar(36) NOT NULL, `name` varchar(255) NULL, `commonName` enum ('1', '2', '3') NULL, `userId` varchar(36) NULL, `nicknameId` varchar(31) NULL, `parentId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `nickname` (`id` varchar(31) NOT NULL, `userId` varchar(36) NULL, `nicknamePageId` varchar(36) NULL, UNIQUE INDEX `REL_88b11cd78f4ef3b0df5164a6ca` (`nicknamePageId`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `section_section_inspiration_section` (`sectionId_1` varchar(36) NOT NULL, `sectionId_2` varchar(36) NOT NULL, INDEX `IDX_90fc259d7c3a3cb7771d6c556d` (`sectionId_1`), INDEX `IDX_528ae54b2734391d5d0af7605c` (`sectionId_2`), PRIMARY KEY (`sectionId_1`, `sectionId_2`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `section_value` ADD CONSTRAINT `FK_880d56a2927d1acb20331c44c98` FOREIGN KEY (`sectionId`) REFERENCES `section`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `section_value` ADD CONSTRAINT `FK_6e7a4c343e4e19aa8785cb44f13` FOREIGN KEY (`inspiredFromSectionId`) REFERENCES `section`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `section` ADD CONSTRAINT `FK_73a502c62159d28900594ba710e` FOREIGN KEY (`pageId`) REFERENCES `page`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `section` ADD CONSTRAINT `FK_603f1001d847bad1ce3dcec9969` FOREIGN KEY (`aliasOfId`) REFERENCES `section`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `page` ADD CONSTRAINT `FK_ae1d917992dd0c9d9bbdad06c4a` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `page` ADD CONSTRAINT `FK_20e1a5336f5c15b3987ade2e4f6` FOREIGN KEY (`nicknameId`) REFERENCES `nickname`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `page` ADD CONSTRAINT `FK_1c6d434d60b856dc8572e7b7b57` FOREIGN KEY (`parentId`) REFERENCES `page`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `nickname` ADD CONSTRAINT `FK_abb0a9f3185899f66cfa8d2b281` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `nickname` ADD CONSTRAINT `FK_88b11cd78f4ef3b0df5164a6cad` FOREIGN KEY (`nicknamePageId`) REFERENCES `page`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `section_section_inspiration_section` ADD CONSTRAINT `FK_90fc259d7c3a3cb7771d6c556d4` FOREIGN KEY (`sectionId_1`) REFERENCES `section`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `section_section_inspiration_section` ADD CONSTRAINT `FK_528ae54b2734391d5d0af7605c4` FOREIGN KEY (`sectionId_2`) REFERENCES `section`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `section_section_inspiration_section` DROP FOREIGN KEY `FK_528ae54b2734391d5d0af7605c4`");
        await queryRunner.query("ALTER TABLE `section_section_inspiration_section` DROP FOREIGN KEY `FK_90fc259d7c3a3cb7771d6c556d4`");
        await queryRunner.query("ALTER TABLE `nickname` DROP FOREIGN KEY `FK_88b11cd78f4ef3b0df5164a6cad`");
        await queryRunner.query("ALTER TABLE `nickname` DROP FOREIGN KEY `FK_abb0a9f3185899f66cfa8d2b281`");
        await queryRunner.query("ALTER TABLE `page` DROP FOREIGN KEY `FK_1c6d434d60b856dc8572e7b7b57`");
        await queryRunner.query("ALTER TABLE `page` DROP FOREIGN KEY `FK_20e1a5336f5c15b3987ade2e4f6`");
        await queryRunner.query("ALTER TABLE `page` DROP FOREIGN KEY `FK_ae1d917992dd0c9d9bbdad06c4a`");
        await queryRunner.query("ALTER TABLE `section` DROP FOREIGN KEY `FK_603f1001d847bad1ce3dcec9969`");
        await queryRunner.query("ALTER TABLE `section` DROP FOREIGN KEY `FK_73a502c62159d28900594ba710e`");
        await queryRunner.query("ALTER TABLE `section_value` DROP FOREIGN KEY `FK_6e7a4c343e4e19aa8785cb44f13`");
        await queryRunner.query("ALTER TABLE `section_value` DROP FOREIGN KEY `FK_880d56a2927d1acb20331c44c98`");
        await queryRunner.query("DROP INDEX `IDX_528ae54b2734391d5d0af7605c` ON `section_section_inspiration_section`");
        await queryRunner.query("DROP INDEX `IDX_90fc259d7c3a3cb7771d6c556d` ON `section_section_inspiration_section`");
        await queryRunner.query("DROP TABLE `section_section_inspiration_section`");
        await queryRunner.query("DROP INDEX `REL_88b11cd78f4ef3b0df5164a6ca` ON `nickname`");
        await queryRunner.query("DROP TABLE `nickname`");
        await queryRunner.query("DROP TABLE `page`");
        await queryRunner.query("DROP TABLE `user`");
        await queryRunner.query("DROP TABLE `section`");
        await queryRunner.query("DROP TABLE `section_value`");
    }

}
