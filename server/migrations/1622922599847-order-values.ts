import {MigrationInterface, QueryRunner} from "typeorm";

export class orderValues1622922599847 implements MigrationInterface {
    name = 'orderValues1622922599847'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP INDEX `IDX_d401a0b1b8d2b325acef222d5d` ON `page`");
        await queryRunner.query("ALTER TABLE `section_value` ADD `nextId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `section_value` ADD UNIQUE INDEX `IDX_6da1996c84b83a47e4f723087b` (`nextId`)");
        await queryRunner.query("CREATE UNIQUE INDEX `REL_6da1996c84b83a47e4f723087b` ON `section_value` (`nextId`)");
        await queryRunner.query("ALTER TABLE `section_value` ADD CONSTRAINT `FK_6da1996c84b83a47e4f723087ba` FOREIGN KEY (`nextId`) REFERENCES `section_value`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `section_value` DROP FOREIGN KEY `FK_6da1996c84b83a47e4f723087ba`");
        await queryRunner.query("DROP INDEX `REL_6da1996c84b83a47e4f723087b` ON `section_value`");
        await queryRunner.query("ALTER TABLE `section_value` DROP INDEX `IDX_6da1996c84b83a47e4f723087b`");
        await queryRunner.query("ALTER TABLE `section_value` DROP COLUMN `nextId`");
        await queryRunner.query("CREATE UNIQUE INDEX `IDX_d401a0b1b8d2b325acef222d5d` ON `page` (`publishedId`)");
    }

}
