import {MigrationInterface, QueryRunner} from "typeorm";

export class categories21623026576122 implements MigrationInterface {
    name = 'categories21623026576122'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` CHANGE `commonName` `commonName` enum ('1', '2', '3', '4', '5', '6', '7', '8', '9', '1000', '1001', '1002', '1003', '1004', '1005', '1006', '2000', '2001', '2002') NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` CHANGE `commonName` `commonName` enum ('1', '2', '3', '4', '5') NULL");
    }

}
