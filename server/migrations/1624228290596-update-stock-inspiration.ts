import {MigrationInterface, QueryRunner} from "typeorm";

export class updateStockInspiration1624228290596 implements MigrationInterface {
    name = 'updateStockInspiration1624228290596'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `section_value` CHANGE `inspiredFromStock` `inspiredFromStock` enum ('1', '100', '101', '102', '103', '104', '200', '201', '202') NULL");
        await queryRunner.query("ALTER TABLE `section` CHANGE `stockInspiration` `stockInspiration` set ('1', '100', '101', '102', '103', '104', '200', '201', '202') NOT NULL DEFAULT ''");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `section` CHANGE `stockInspiration` `stockInspiration` set ('1', '2', '3', '4') NOT NULL DEFAULT ''");
        await queryRunner.query("ALTER TABLE `section_value` CHANGE `inspiredFromStock` `inspiredFromStock` enum ('1', '2', '3', '4') NULL");
    }

}
