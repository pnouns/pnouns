import {MigrationInterface, QueryRunner} from "typeorm";

export class publishBadges1628389339671 implements MigrationInterface {
    name = 'publishBadges1628389339671'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` ADD `showBadges` tinyint NULL");
        await queryRunner.query("ALTER TABLE `nickname` ADD `showBadgesByDefault` tinyint NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `nickname` DROP COLUMN `showBadgesByDefault`");
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `showBadges`");
    }

}
