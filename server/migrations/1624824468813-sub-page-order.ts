import {MigrationInterface, QueryRunner} from "typeorm";

export class subPageOrder1624824468813 implements MigrationInterface {
    name = 'subPageOrder1624824468813'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` ADD `listedSubPageOrder` text NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `listedSubPageOrder`");
    }

}
