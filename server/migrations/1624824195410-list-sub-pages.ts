import {MigrationInterface, QueryRunner} from "typeorm";

export class listSubPages1624824195410 implements MigrationInterface {
    name = 'listSubPages1624824195410'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `listed_sub_page` (`id` varchar(36) NOT NULL, `customName` varchar(255) NULL, `showSubtitle` tinyint NOT NULL, `customSubtitle` varchar(255) NULL, `caption` varchar(2048) NOT NULL, `includePassword` tinyint NOT NULL, `parentId` varchar(36) NULL, `linkedId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `listed_sub_page` ADD CONSTRAINT `FK_022c5f8901818d31b3b0da9309b` FOREIGN KEY (`parentId`) REFERENCES `page`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `listed_sub_page` ADD CONSTRAINT `FK_80928de6cadea466b6aad23d311` FOREIGN KEY (`linkedId`) REFERENCES `page`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `listed_sub_page` DROP FOREIGN KEY `FK_80928de6cadea466b6aad23d311`");
        await queryRunner.query("ALTER TABLE `listed_sub_page` DROP FOREIGN KEY `FK_022c5f8901818d31b3b0da9309b`");
        await queryRunner.query("DROP TABLE `listed_sub_page`");
    }

}
