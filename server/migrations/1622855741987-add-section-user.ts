import {MigrationInterface, QueryRunner} from "typeorm";

export class addSectionUser1622855741987 implements MigrationInterface {
    name = 'addSectionUser1622855741987'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `section` ADD `userId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `section` ADD CONSTRAINT `FK_10cf9f2210a8531b3bfded465e6` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `section` DROP FOREIGN KEY `FK_10cf9f2210a8531b3bfded465e6`");
        await queryRunner.query("ALTER TABLE `section` DROP COLUMN `userId`");
    }

}
