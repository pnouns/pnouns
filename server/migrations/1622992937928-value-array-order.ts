import {MigrationInterface, QueryRunner} from "typeorm";

export class valueArrayOrder1622992937928 implements MigrationInterface {
    name = 'valueArrayOrder1622992937928'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `section` ADD `valueOrder` text NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `section` DROP COLUMN `valueOrder`");
    }

}
