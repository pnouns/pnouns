import {MigrationInterface, QueryRunner} from "typeorm";

export class addPasswordFormat1624722342435 implements MigrationInterface {
    name = 'addPasswordFormat1624722342435'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` ADD `defaultPasswordFormat` enum ('1', '2', '3') NULL");
        await queryRunner.query("ALTER TABLE `page` CHANGE `defaultJoiningWord` `defaultJoiningWord` enum ('1', '2', '3', '4', '5', '6', '7', '8') NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` CHANGE `defaultJoiningWord` `defaultJoiningWord` enum ('1', '2', '3', '4', '5', '6', '7') NULL");
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `defaultPasswordFormat`");
    }

}
