import {MigrationInterface, QueryRunner} from "typeorm";

export class addSectionOrder1623021023094 implements MigrationInterface {
    name = 'addSectionOrder1623021023094'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` ADD `sectionOrder` text NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `sectionOrder`");
    }

}
