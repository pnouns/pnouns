import {MigrationInterface, QueryRunner} from "typeorm";

export class addStaffSupporter1628384125923 implements MigrationInterface {
    name = 'addStaffSupporter1628384125923'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `user` ADD `staff` set ('FOUNDER', 'LEAD_DEV') NULL");
        await queryRunner.query("ALTER TABLE `user` ADD `supporter` enum ('GOLD', 'PLATINUM', 'DIAMOND') NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `supporter`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `staff`");
    }

}
