import {MigrationInterface, QueryRunner} from "typeorm";

export class pageNamePronouns1622829616336 implements MigrationInterface {
    name = 'pageNamePronouns1622829616336'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` ADD `displayName` varchar(31) NULL");
        await queryRunner.query("ALTER TABLE `page` ADD `nameSectionId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `page` ADD UNIQUE INDEX `IDX_dd45a2dd37ff2620227f1b723a` (`nameSectionId`)");
        await queryRunner.query("ALTER TABLE `page` ADD `pronounSectionId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `page` ADD UNIQUE INDEX `IDX_43aa1f139e2fb422aee5a20096` (`pronounSectionId`)");
        await queryRunner.query("CREATE UNIQUE INDEX `REL_dd45a2dd37ff2620227f1b723a` ON `page` (`nameSectionId`)");
        await queryRunner.query("CREATE UNIQUE INDEX `REL_43aa1f139e2fb422aee5a20096` ON `page` (`pronounSectionId`)");
        await queryRunner.query("ALTER TABLE `page` ADD CONSTRAINT `FK_dd45a2dd37ff2620227f1b723ad` FOREIGN KEY (`nameSectionId`) REFERENCES `section`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `page` ADD CONSTRAINT `FK_43aa1f139e2fb422aee5a200962` FOREIGN KEY (`pronounSectionId`) REFERENCES `section`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `page` DROP FOREIGN KEY `FK_43aa1f139e2fb422aee5a200962`");
        await queryRunner.query("ALTER TABLE `page` DROP FOREIGN KEY `FK_dd45a2dd37ff2620227f1b723ad`");
        await queryRunner.query("DROP INDEX `REL_43aa1f139e2fb422aee5a20096` ON `page`");
        await queryRunner.query("DROP INDEX `REL_dd45a2dd37ff2620227f1b723a` ON `page`");
        await queryRunner.query("ALTER TABLE `page` DROP INDEX `IDX_43aa1f139e2fb422aee5a20096`");
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `pronounSectionId`");
        await queryRunner.query("ALTER TABLE `page` DROP INDEX `IDX_dd45a2dd37ff2620227f1b723a`");
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `nameSectionId`");
        await queryRunner.query("ALTER TABLE `page` DROP COLUMN `displayName`");
    }

}
